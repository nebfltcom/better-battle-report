import { Color3, LinesMesh, Mesh, MeshBuilder, Quaternion, Scene, StandardMaterial, Vector3 } from "babylonjs";
import { AllData, KinematicData, MissileInfo } from "./json";
import { RenderSettings } from './settings';
import { getId, getK } from "./utils";

//const MAINCOLOR3 = new Color3(1,0,0);
const MAINCOLOR3 = { //TODO: color accessibility settings
  "Chaff" : new Color3(1,1,1),
  "Flare" : new Color3(0.6,0.4,0.2)
}

//NOTE: this is a bit temporary to allow for quick implementation of chaff and flares, but eventually will be most/all signatures

export default class SignatureRenderer {
  id: string;
  mesh: Mesh;
  material: StandardMaterial;
  time: number;
  duration: number;
  displayed: boolean = false;
  scene: Scene;
  constructor(scene: Scene, allData: AllData, missileInfo: MissileInfo) {
    this.id = "signature" + missileInfo.Id;
    this.scene = scene;
    //Searching for missile kinematics data in allData
    for(let missileKinematics of allData.MissileKinematics) {
      if(getId(missileKinematics) === getId(missileInfo)) {
        let isChaff = missileInfo.Name.includes("EA12 Chaff"); //temp hard code

        //Set time and duration
        this.time = Math.max(0,...missileKinematics.Log.map(k => k !== null ? k.Time : 0));
        this.duration = isChaff ? 45 : 40; //more temp hard code and magic numbers woo

        //Create signature mesh
        this.mesh = MeshBuilder.CreateSphere(`${this.id}Mesh`, {
          diameter: 10,
          segments: 8
        });
        let lastK = missileKinematics.Log.find(k => k.Time == this.time);
        this.mesh.position = new Vector3(lastK?.Transform.Position.X, lastK?.Transform.Position.Y, lastK?.Transform.Position.Z);

        //Create and apply material to signature mesh
        this.material = new StandardMaterial(`${this.id}Material`);
        this.material.ambientColor = isChaff ? MAINCOLOR3["Chaff"] : MAINCOLOR3["Flare"];
        this.material.backFaceCulling = false;
        this.material.alpha = 0.1;

        this.mesh.material = this.material;

        this.mesh.enableEdgesRendering(0.9999);
        this.mesh.edgesWidth = 3;
        this.mesh.edgesColor = this.material.ambientColor.toColor4(0.8);

        this.scene.removeMesh(this.mesh);
        break;
      }
    }
  }
  update(time: number) {
    if(time >= this.time && time < this.time + this.duration) {
      if(!this.displayed) {
        this.scene.addMesh(this.mesh);
        this.displayed = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.mesh);
        this.displayed = false;
      }
    }
  }
  clear() {
    this.mesh.dispose(false, true);
  }
}