const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
  onLoadReport: (callback) => ipcRenderer.on('loadReport', callback)
});