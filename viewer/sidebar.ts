import { AdvancedDynamicTexture, Button, ScrollViewer, StackPanel } from 'babylonjs-gui';
import { Emitter } from 'nanoevents';
import { SettingsManager } from './settings';
import SettingsPanel from './sidebar/settings';
import LayersPanel from './sidebar/layers';
import ShipsPanel from './sidebar/ships';
import { AllData } from './json';
const tabs = [ 'Ships', 'Settings' , 'Layers'];

export default class SideBar {
  fullscreenGuiTexture: AdvancedDynamicTexture;
  emitter: Emitter;
  settingsManager: SettingsManager;
  shipsPanel: ShipsPanel;
  constructor(fullscreenGuiTexture: AdvancedDynamicTexture, emitter: Emitter, settingsManager: SettingsManager) {
    this.fullscreenGuiTexture = fullscreenGuiTexture;
    this.emitter = emitter;
    this.settingsManager = settingsManager;

    //Register tab buttons
    for(let tab of tabs) {
      let button = this.fullscreenGuiTexture.getControlByName(`Side Bar - Tab - ${tab}`) as Button;
      button.onPointerClickObservable.add(() => { this.loadTab(tab); });
    }
    this.loadTab(tabs[0]);

    //Register ships panel
    this.shipsPanel = new ShipsPanel(this.fullscreenGuiTexture, this.emitter, this.settingsManager.settings);

    //Register settings panel
    new SettingsPanel(this.fullscreenGuiTexture.getControlByName('Side Bar - Settings - Panel') as StackPanel, this.emitter, this.settingsManager);

    //Register layers panel
    new LayersPanel(this.fullscreenGuiTexture, this.fullscreenGuiTexture.getControlByName('Side Bar - Layers - Panel') as StackPanel, this.emitter, this.settingsManager);
  }
  loadTab(input: string) {
    for(let tab of tabs) {
      let button = this.fullscreenGuiTexture.getControlByName(`Side Bar - Tab - ${tab}`) as Button;
      let viewer = this.fullscreenGuiTexture.getControlByName(`Side Bar - ${tab}`) as ScrollViewer;
      if(tab === input) {
        button.color = '#FF9A00FF';
        viewer.isVisible = true;
      }
      else {
        button.color = '#0080FFFF';
        viewer.isVisible = false;
      }
    }
  }
  onLoad(allData: AllData) {
    this.shipsPanel.generateShipsList(allData);
  }
  clear() {
    this.shipsPanel.clear();
  }
}