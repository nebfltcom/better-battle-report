import { ArcRotateCamera, Color3, Color4, DeviceSourceManager, DeviceType, Mesh, MeshBuilder, Quaternion, Scene, StandardMaterial, Vector3 } from 'babylonjs';
import { RenderSettings } from './settings';

const NUM_FOCUS_STEPS: number = 50; //how many update calls it takes the camera focus coroutine to execute

export default class Camera {
  //Start Settings Section
  originColor: Color4;
  originSize: number;
  panMaxSpeed: number;
  panAcceleration: number;
  panDeceleration: number;
  drawDistance: number;
  skyboxColor: Color3;
  //End Settings Section
  scene: Scene;
  camera: ArcRotateCamera;
  cameraOrigin: Mesh;
  cameraSkybox: Mesh;
  cameraSkyboxMaterial: StandardMaterial;
  deviceSourceManager: DeviceSourceManager;
  cameraTarget: Vector3 = Vector3.Zero();
  cameraKeyboardPanVelocity: Vector3 = Vector3.Zero();
  //focusing
  isFocusing: boolean = false;
  focusStep: number = 0;
  focusStepLength: number = 0;
  focusDirection: Vector3 = Vector3.Zero();
  focusTarget: Vector3 = Vector3.Zero();
  isFollowing: boolean = false;
  constructor(scene: Scene, renderSettings: RenderSettings) {
    //Initialize settings
    this.originColor = new Color4(
      renderSettings.camera.origin.color.r,
      renderSettings.camera.origin.color.g,
      renderSettings.camera.origin.color.b,
      renderSettings.camera.origin.color.a
    );
    this.originSize = renderSettings.camera.origin.size;
    this.panMaxSpeed = renderSettings.camera.pan.maxSpeed;
    this.panAcceleration = renderSettings.camera.pan.acceleration;
    this.panDeceleration = renderSettings.camera.pan.deceleration;
    this.drawDistance = renderSettings.skybox.size;
    this.skyboxColor = new Color3(
      renderSettings.skybox.color.r,
      renderSettings.skybox.color.g,
      renderSettings.skybox.color.b
    );

    //Initialize input device manager
    this.deviceSourceManager = new DeviceSourceManager(scene.getEngine());
    this.scene = scene;

    //Create camera
    this.camera = new ArcRotateCamera('camera', -Math.PI/4, Math.PI/4, 1000, new Vector3(0, 0, 0), scene);
    this.camera.minZ = 0;
    this.camera.wheelDeltaPercentage = 0.05;
    this.camera.panningSensibility = 10;
    this.camera.setTarget(this.cameraTarget);
    this.camera.attachControl(null, true, false);
    this.camera.inertia = 0;
    this.camera.fov = renderSettings.camera.fov * (Math.PI / 180);

    //Add camera origin indicator
    this.cameraOrigin = MeshBuilder.CreateLineSystem('lineSystem', {
      lines: [
        [this.cameraTarget, new Vector3(this.originSize,0,0)],
        [this.cameraTarget, new Vector3(-this.originSize,0,0)],
        [this.cameraTarget, new Vector3(0,this.originSize,0)],
        [this.cameraTarget, new Vector3(0,-this.originSize,0)],
        [this.cameraTarget, new Vector3(0,0,this.originSize)],
        [this.cameraTarget, new Vector3(0,0,-this.originSize)]
      ],
      colors: [
        [this.originColor, this.originColor],
        [this.originColor, this.originColor],
        [this.originColor, this.originColor],
        [this.originColor, this.originColor],
        [this.originColor, this.originColor],
        [this.originColor, this.originColor]
      ]
    }, scene);

    //Add sphere to enforce camera draw distance
    this.camera.maxZ = this.drawDistance;
    this.cameraSkybox = MeshBuilder.CreateSphere('skyboxMesh', { diameter: this.drawDistance}, scene);
    this.cameraSkyboxMaterial = new StandardMaterial(`skyboxMaterial`);
    this.cameraSkyboxMaterial.ambientColor = this.skyboxColor;
    this.cameraSkyboxMaterial.backFaceCulling = false;
    this.cameraSkybox.material = this.cameraSkyboxMaterial;
  }
  update(deltaTime: number) {
    let direction = Vector3.Zero();
    let speedScalar = 1;

    if (window.selector?.active == false) {
      this.isFollowing = false;
    }

    if(this.isFocusing) {
      speedScalar = this.focusStepLength / this.panMaxSpeed, 1;

      //if we would travel slower than the max pan speed anyway, just snap to the desired point
      if (speedScalar < 1) {
        this.focusStep = NUM_FOCUS_STEPS;
      }

      if (this.focusStep == NUM_FOCUS_STEPS) {
        this.cameraTarget = this.focusTarget;
        this.camera.setTarget(this.cameraTarget);
        this.cameraOrigin.position = this.camera.getTarget();
        this.cameraSkybox.position = this.camera.position;
        this.cameraKeyboardPanVelocity = Vector3.Zero();

        this.isFocusing = false;
        this.focusStep = 0;
        this.focusStepLength = 0;
        this.focusDirection = Vector3.Zero();
        this.focusTarget = Vector3.Zero();
        this.isFollowing = window.selector.active ? true : false; //if we focused on something selected, follow it until we move
        return;
      }

      direction.addInPlace(this.focusDirection.normalize());
      this.focusStep++;
    }
    else {
      let keyboardDevice = this.deviceSourceManager.getDeviceSource(DeviceType.Keyboard);
      //Shift
      if(keyboardDevice?.getInput(16) === 1) {
        speedScalar = 4; //4 is arbitrary TODO: make this a settings with a clamp
      }
      //W
      if(keyboardDevice?.getInput(87) === 1) {
        direction.addInPlace(Vector3.Forward());
      }
      //A
      if(keyboardDevice?.getInput(65) === 1) {
        direction.addInPlace(Vector3.Left());
      }
      //S
      if(keyboardDevice?.getInput(83) === 1) {
        direction.addInPlace(Vector3.Backward());
      }
      //D
      if(keyboardDevice?.getInput(68) === 1) {
        direction.addInPlace(Vector3.Right());
      }
      //E
      if(keyboardDevice?.getInput(69) === 1) {
        direction.addInPlace(Vector3.Up());
      }
      //C
      if(keyboardDevice?.getInput(67) === 1) {
        direction.addInPlace(Vector3.Down());
      }
      //F
      if(keyboardDevice?.getInput(70) === 1) {
        //focus on selected object or, if nothing is selected, the origin
        let target = !window.selector.active ? Vector3.Zero() : new Vector3(
          window.selector.selectedObjectPickable?.absolutePosition._x,
          window.selector.selectedObjectPickable?.absolutePosition._y,
          window.selector.selectedObjectPickable?.absolutePosition._z
        );
        this.startFocusCoroutine(target);
      }
    }
    if(direction.length() > 0.1 ) {
      this.isFollowing = false; //stop following if we are moving somewhere
      direction.normalize();
      if (!this.isFocusing) //focus code relies on world coordinates, not relative to the view
        direction.applyRotationQuaternionInPlace(Quaternion.FromEulerAngles(0,((3 * Math.PI)/2) - this.camera.alpha,0));
      this.cameraKeyboardPanVelocity.addInPlace(direction.scaleInPlace(this.panAcceleration * deltaTime));
    }
    else {
      let newSpeed = Math.max(this.cameraKeyboardPanVelocity.length() - (this.panDeceleration * deltaTime), 0);
      this.cameraKeyboardPanVelocity.normalize();
      this.cameraKeyboardPanVelocity.scaleInPlace(newSpeed);
    }
    if(this.cameraKeyboardPanVelocity.length() > this.panMaxSpeed) {
      this.cameraKeyboardPanVelocity.normalize();
      this.cameraKeyboardPanVelocity.scaleInPlace(this.panMaxSpeed);
    }
    
    if (this.isFollowing) { //TODO: this needs to be made better. if it can work with sprites as well as meshes that'd be even better. Currently it only works in this VERY SPECIFIC sequence
      let position = new Vector3().copyFrom(window.selector.selectedObjectPickable == null ? this.cameraTarget : window.selector.selectedObjectPickable.absolutePosition); //... is there a nicer way to do this?
      this.cameraTarget = position;
      this.camera.setTarget(window.selector.selectedObjectPickable == null ? this.cameraTarget : window.selector.selectedObjectPickable.absolutePosition); //getting the value again instead of using position makes the camera rotation follow the movement as well?
      //NOTE: I've no idea why it works that way, and if position is used instead of absolutePosition it breaks
    } else {
      this.cameraTarget.addInPlace(this.cameraKeyboardPanVelocity.scaleInPlace(speedScalar)); //multiply final displacement by speed scalar when shift held
      this.camera.setTarget(this.cameraTarget);
    }

    this.cameraOrigin.position = this.camera.getTarget();
    this.cameraSkybox.position = this.camera.position;
  }

  //TODO: allow hotload settings changes?
  //Dynamically increase the draw distance based on map size if current setting is too small to see the entire map from the edge. Does not permanently change the setting
  adjustSkybox(scene: Scene, mapRadius: number = 0) {
    this.drawDistance = Camera.CalculateDrawDistance(this.drawDistance, mapRadius);
    this.cameraSkybox.dispose();
    
    this.camera.maxZ = this.drawDistance;
    this.cameraSkybox = MeshBuilder.CreateSphere('skyboxMesh', { diameter: this.drawDistance}, scene);
    this.cameraSkybox.material = this.cameraSkyboxMaterial;
  }

  static CalculateDrawDistance(distanceSetting: number, mapRadius: number) {
    return Math.max(distanceSetting, mapRadius * 7); //To be able to see the whole map from the edge of the map we need the radius of the sphere to equal the diameter of the map, plus a little extra to allow for fully zoomed out viewing.
  }

  startFocusCoroutine(target: Vector3 = Vector3.Zero()) {
    this.isFocusing = true;
    this.focusTarget = target;
    this.focusDirection = this.focusTarget.subtract(this.cameraOrigin.position);
    this.focusStepLength = this.focusDirection.length() / NUM_FOCUS_STEPS;
    this.focusDirection.normalize();
  }
}