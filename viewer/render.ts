import { Color3, Scene, ScenePerformancePriority, SpriteManager, Vector3 } from 'babylonjs';
import deepmerge from 'deepmerge';
import localforage from 'localforage';
import { gzip, ungzip } from 'pako';
import BeamRenderer from './beam';
import Camera from './camera';
import GridRenderer from './grid';
import Gui from './gui';
import { AllData } from './json';
import MissileRenderer from './missile';
import ObstacleRenderer from './obstacle';
import { RenderSettings, SettingsManager } from './settings';
import ShellRenderer from './shell';
import ShipRenderer from './ship';
import { getId } from './utils';
import EwarRenderer from './ewar';
import RayFieldRenderer from './rayfield';
import PoiRenderer from './poi';
import ExplosionRenderer from './explosion';
import SignatureRenderer from './signature';
const semver = require('semver');
const config = require('./config.json');
const packageJson = require('../package.json');

import SelectionManager from './selector';
declare global {
  interface Window {
    selector: SelectionManager;
  }
}

export default class Renderer {
  settingsManager: SettingsManager = new SettingsManager();
  gui: Gui;
  scene: Scene;
  camera: Camera;
  report: AllData;
  selectionManager: SelectionManager;
  layerStatus = { //See layers.ts
    'Grid': this.settingsManager.settings.grid.show,
    'EWAR': true,
    'Missiles': true,
    'Missile Sprites': false,
    'Rayfields': true,
    'Obstacles': true,
    'Obstacle Opacity': true,
    'POIs': true,
    'Shells': true,
    'Shell Sprites': false,
    'Damage Numbers Internal': false,
    'Damage Numbers Armor': false
  };
  //renderer objects
  grid: GridRenderer;
  ships: ShipRenderer[] = [];
  missiles: MissileRenderer[] = [];
  shells: ShellRenderer[] = [];
  beams: BeamRenderer[] = [];
  obstacles: ObstacleRenderer[] = [];
  ewarList: EwarRenderer[] = [];
  rayfields: RayFieldRenderer[] = [];
  pois: PoiRenderer[] = [];
  explosions: ExplosionRenderer[] = [];
  signatures: SignatureRenderer[] = [];
  constructor(scene: Scene) {
    //Scene setup
    this.scene = scene;
    this.scene.performancePriority = ScenePerformancePriority.Intermediate;
    this.scene.ambientColor = new Color3(1,1,1);

    //Initializing camera
    this.camera = new Camera(this.scene, this.settingsManager.settings);

    //GUI setup
    this.gui = new Gui(this.scene, this.settingsManager, this.camera);
    this.gui.emitter.on('openReport', this.openReport.bind(this));
    this.gui.emitter.on('reloadReport', this.reload.bind(this));
    this.gui.emitter.on('toggleLayer', (layer: string) => {
      this.layerStatus[layer] = ! this.layerStatus[layer];
    });

    //Initializing grid
    this.grid = new GridRenderer(this.scene, this.settingsManager.settings, this.gui.emitter);
    
    //Check if report in cache exists
    let isReportCached = sessionStorage.getItem('report');
    if(isReportCached !== null) {
      //Show loading indicator in GUI
      this.gui.setLoadingIndicator(true);

      //Loading gzipped cached report
      try {
        localforage.getItem('report', (err, cachedReport) => {
          let report: AllData = JSON.parse(ungzip(cachedReport as Uint8Array, { to: 'string' }));
          this.loadReport(report);
        });
      }
      catch(err) {
        //Hide loading indicator in GUI
        this.gui.setLoadingIndicator(false); 
      }
    }
  }
  openReport() {
    if(!this.gui.loadingIndicator.isVisible) {
      let input = document.createElement('input');
      input.type = 'file';
      input.accept = '.json,.bbr';
      input.onchange = (e: any) => {
        let file: File = e.target.files[0];
        let reader = new FileReader();
        reader.readAsText(file,'UTF-8');
        reader.onload = async (readerEvent: any) => {
          let content = readerEvent.target.result;
          input.style.display = 'none';
          let report: AllData | null = null;
          //Show loading indicator in GUI
          this.gui.setLoadingIndicator(true);
          try {
            report = JSON.parse(content);
          }
          catch(err) {
            let unzippedContent = ungzip(await file.arrayBuffer(), { to: 'string' });
            report = JSON.parse(unzippedContent);
          }
          if(report !== null) {
            this.loadReport(report);
          }
          //Hide loading indicator in GUI
          this.gui.setLoadingIndicator(false);
        }
      }
      input.click();
    }
  }
  async loadReport(allData: AllData) {
    this.report = allData;

    console.log(this.report); //DEBUG

    //Show loading indicator in GUI
    this.gui.setLoadingIndicator(true);

    //Clear any previous rendered elements
    this.clear();

    //Check if the report is too old
    if(typeof this.report.ModVersion !== 'string') {
      this.report.ModVersion = '0.0.0';
    }
    this.report.ModVersion = semver.coerce(this.report.ModVersion).raw;
    if(!semver.satisfies(this.report.ModVersion, config.modVersion)) {
      //Hide loading indicator in GUI
      this.gui.setLoadingIndicator(false);
      window.alert(`Report version (${this.report.ModVersion}) is too old for the viewer (requires report version ${config.modVersion})`);
      return null;
    }
    //Check if viewer is too old
    if(typeof this.report.ViewerVersion !== 'string') {
      this.report.ViewerVersion = '>=0.0.0';
    }
    if(!semver.satisfies(packageJson.version, this.report.ViewerVersion)) {
      //Hide loading indicator in GUI
      this.gui.setLoadingIndicator(false);
      window.alert(`Viewer version (${packageJson.version}) is too old for the report (requires viewer version ${this.report.ViewerVersion})`);
      return null;
    }

    //Update camera skybox and renderDistance based on map radius
    //TODO: do something with the map name?
    this.camera.adjustSkybox(this.scene, allData.MapInfo?.Radius);

    //Update grid size based on map radius
    if (this.settingsManager.settings.grid.sphericalCoordinates === false && this.settingsManager.settings.grid.resize) { //TODO: currently only applies to cartesian grid
      let newGridSettings = (new SettingsManager()).settings;
      let gridInterval = this.settingsManager.settings.grid.interval;
      let gridSize = (allData.MapInfo?.Radius * 2) - (gridInterval * 2) + ((gridInterval * 2) - ((allData.MapInfo?.Radius * 2) % (gridInterval * 2))); //NOTE: must be a multiple of settings.grid.interval * 2
      newGridSettings.grid.size = Math.max(this.settingsManager.settings.grid.size, gridSize);
      this.grid.clear(); //removes meshes of previous grids
      this.grid = new GridRenderer(this.scene, newGridSettings, this.gui.emitter);
    }

    //Initialize any empty arrays
    await (new Promise<void>((r) => {
      this.report = deepmerge(new AllData(), this.report);
      window.setTimeout(r,0);
    }));

    //Update GUI
    await (new Promise<void>((r) => {
      this.gui.loadReport(this.report);
      window.setTimeout(r,0);
    }));

    //Store all promises
    let allPromises: Promise<void>[] = [];

    //Initializing selection system manager
    this.selectionManager = new SelectionManager(this.scene, this.gui.emitter, this.camera);

    //Initializing sprite managers
    let numDecoys = allData.MissileInfo.filter(m => m.IsDecoy === true).length;
    let missileSpikerSpriteManager = new SpriteManager("missileSpikerSpriteManager", new URL('assets/spiker.png', import.meta.url).pathname, allData.MissileInfo.length - numDecoys, 64, this.scene);
    missileSpikerSpriteManager.isPickable = true;
    let missileDecoySpriteManager = new SpriteManager("missileDecoySpriteManager", new URL('assets/spiker-decoy.png', import.meta.url).pathname, numDecoys, 64, this.scene);
    missileDecoySpriteManager.isPickable = true;
    let shellSpriteManager = new SpriteManager("shellSpriteManager", new URL('assets/shell.png', import.meta.url).pathname, allData.ShellInfo.length, 64, this.scene);
    shellSpriteManager.isPickable = true;

    //Initializing ships
    for(let shipInfo of this.report.ShipInfo) { //Don't use ShipMeshes, it's instanced data
      allPromises.push(new Promise<void>((r) => {
        this.ships.push(new ShipRenderer(this.scene, this.gui.emitter, this.settingsManager.settings, this.report, shipInfo));
        window.setTimeout(r,0);
      }));
    }

    //Initializing missiles
    for(let missileInfo of this.report.MissileInfo) {
      allPromises.push(new Promise<void>((r) => {
        //different sprite manager based on missile info
        let spriteManager = missileInfo.IsDecoy ? missileDecoySpriteManager : missileSpikerSpriteManager;
        this.missiles.push(new MissileRenderer(this.scene, this.settingsManager.settings, spriteManager, this.report, missileInfo));
        window.setTimeout(r,0);
      }));
    }

    //Initializing shells
    for(let shellInfo of this.report.ShellInfo) {
      allPromises.push(new Promise<void>((r) => {
        this.shells.push(new ShellRenderer(this.scene, this.settingsManager.settings, shellSpriteManager, this.report, shellInfo));
        window.setTimeout(r,0);
      }));
    }

    //Initializing beams
    for(let beamInfo of this.report.BeamInfo) {
      allPromises.push(new Promise<void>((r) => {
        this.beams.push(new BeamRenderer(this.scene, this.report, getId(beamInfo)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing obstacles
    for(let mapMeshesData of this.report.MapMeshes) {
      allPromises.push(new Promise<void>((r) => {
        this.obstacles.push(new ObstacleRenderer(this.scene, this.settingsManager.settings, this.report, getId(mapMeshesData)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing ewar
    for(let ewarInfo of this.report.EWarInfo) { //Don't use EWarMeshes, it's instanced data
      allPromises.push(new Promise<void>((r) => {
        this.ewarList.push(new EwarRenderer(this.scene, this.settingsManager.settings, this.report, getId(ewarInfo)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing rayfields
    for(let rayfieldInfo of this.report.RayFieldData) {
      allPromises.push(new Promise<void>((r) => {
        this.rayfields.push(new RayFieldRenderer(this.scene, this.report, getId(rayfieldInfo)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing POIs
    for(let poiInfo of this.report.MapPOIs) {
      allPromises.push(new Promise<void>((r) => {
        this.pois.push(new PoiRenderer(this.scene, this.settingsManager.settings, this.report, getId(poiInfo)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing explosions
    for(let explosion of this.report.ExplosionData) {
      allPromises.push(new Promise<void>((r) => {
        this.explosions.push(new ExplosionRenderer(this.scene, this.report, getId(explosion)));
        window.setTimeout(r,0);
      }));
    }

    //Initializing decoy sigs
    //TODO: expand entire signature system
    for(let missileInfo of this.report.MissileInfo) {
      if (missileInfo.Name.includes("EA12 Chaff") || missileInfo.Name.includes("EA20 Flare"))
        allPromises.push(new Promise<void>((r) => {
          this.signatures.push(new SignatureRenderer(this.scene, this.report, missileInfo));
          window.setTimeout(r,0);
        }));
    }

    //Wait for all loading to be finished
    await Promise.all<void>(allPromises);

    //TODO: build a pollable sprite manager that assigns sprites from here based on querying each sprited renderer in the update call
    //NOTE: was trying to do a recyclable sprite thing. doesn't work
    /*let timeTable:number[][] = [];
    let maxTime = 0;
    for(let missile of this.missiles) {
      let realMaxTime = missile.maxTime + missile.trailTime;
      timeTable.push([missile.minTime, realMaxTime]);
      if(maxTime < realMaxTime) {
        maxTime = realMaxTime;
      }
    }
    for(let shell of this.shells) {
      let realMaxTime = shell.maxTime + 0.25; //ISSUE: pull LINETRAIL from shell.ts
      timeTable.push([shell.minTime, realMaxTime]);
      if(maxTime < realMaxTime) {
        maxTime = realMaxTime;
      }
    }
    
    let maxOccurances = 0;
    if (timeTable.length > 0) {
      for (let time = timeTable[0][0]; time <= maxTime; time+=0.01) {
        let occurances = 0;
        for(let index = 0; index < timeTable.length; index++) {
          let times = timeTable[index];
          if (times[0] <= time && times[1] >= time) {
            occurances++;
          }
          if(maxOccurances < occurances) {
            maxOccurances = occurances;
          }
        }
      }
    }
    console.log(maxOccurances);*/

    //Hoist report into the global scope for developer console access
    (window as any).report = this.report;

    //Global scope for selection system
    window.selector = this.selectionManager;

    //Store report into cache
    try {
      let content = gzip(JSON.stringify(this.report));
      localforage.setItem('report', content);
      sessionStorage.setItem('report', 'cached');
    }
    catch(err) {}

    //Hide loading indicator in GUI
    this.gui.setLoadingIndicator(false);
  }
  update(deltaTime: number) {
    //Update GUI and calculate current time to use
    this.gui.update(deltaTime);
    let time = this.gui.currTime;

    //Tooltip
    let hoveredId = "";
    let tooltipMatched = this.gui.tooltip.currentId == hoveredId;
    if (tooltipMatched) {
      this.gui.tooltip.hide();
    }
    else {
      hoveredId = this.gui.tooltip.currentId;
    }

    if (this.gui.tooltip.isSet) {
      tooltipMatched = true;
    }

    //Grid visibility toggle
    this.grid.update(this.layerStatus['Grid']);

    //Update ships with current time
    for(let ship of this.ships) {
      ship.update(time, this.layerStatus['Grid'], this.layerStatus['Damage Numbers Internal'], this.layerStatus['Damage Numbers Armor']);
      if (!tooltipMatched && hoveredId == ship.id) {
        tooltipMatched = true;
        this.gui.tooltip.currentObject = ship.shipMesh;
        this.gui.tooltip.show(ship.tooltipLines[0], ship.tooltipLines[1], ship.tooltipLines[2], ship.tooltipLines[3]);
      }
    }

    //Update missiles with current time
    for(let missile of this.missiles) {
      missile.update(time, this.layerStatus['Missiles'], this.layerStatus['Missile Sprites']);
      if (!tooltipMatched && hoveredId == missile.id) {
        tooltipMatched = true;
        this.gui.tooltip.currentObject = missile.missileMesh;
        this.gui.tooltip.show(missile.tooltipLines[0], missile.tooltipLines[1], missile.tooltipLines[2], missile.tooltipLines[3]);
      }
    }

    //Update shells with current time
    for(let shell of this.shells) {
      shell.update(time, this.layerStatus['Shells'], this.layerStatus['Shell Sprites']);
      if (!tooltipMatched && hoveredId == shell.id) {
        tooltipMatched = true;
        this.gui.tooltip.currentObject = shell.shellMesh;
        this.gui.tooltip.show(shell.tooltipLines[0], shell.tooltipLines[1], shell.tooltipLines[2], shell.tooltipLines[3]);
      }
    }

    //Update beams with current time
    for(let beam of this.beams) {
      beam.update(time);
    }

    //Update obstacles with current time
    for(let obstacle of this.obstacles) {
      obstacle.update(time, this.layerStatus['Obstacles'], this.layerStatus['Obstacle Opacity']);
    }

    //Update ewar with current time
    for(let ewar of this.ewarList) {
      ewar.update(time, this.layerStatus['EWAR']);
    }

    //let startTime = performance.now();
    //Update rayfields with current time
    for(let rayfield of this.rayfields) {
      rayfield.update(time, this.layerStatus['Rayfields']);
    }
    //let endTime = performance.now();
    //console.log(`rayfield update took ${endTime - startTime} ms`)

    //update POIs with current time
    for(let poi of this.pois) {
      poi.update(time, this.layerStatus['POIs']);
    }

    //update explosions with current time
    for(let explosion of this.explosions) {
      explosion.update(time);
    }

    //update explosions with current time
    for(let signature of this.signatures) {
      signature.update(time);
    }

    //update selection match guard
    if (!window.selector?.matchGuard && window.selector?.matched) {
      window.selector.matchGuard = true;
    }

    //Update camera
    this.camera.update(deltaTime);
  }
  clear() {
    //Clear GUI elements
    this.gui.clear();

    //Clear previous ship renderers (and associated resources)
    while(this.ships.length > 0) {
      this.ships[0].clear();
      this.ships.splice(0,1);
    }

    //Clear previous missile renderers (and associated resources)
    while(this.missiles.length > 0) {
      this.missiles[0].clear();
      this.missiles.splice(0,1);
    }

    //Clear previous shell renderers (and associated resources)
    while(this.shells.length > 0) {
      this.shells[0].clear();
      this.shells.splice(0,1);
    }

    //Clear previous beam renderers (and associated resources)
    while(this.beams.length > 0) {
      this.beams[0].clear();
      this.beams.splice(0,1);
    }

    //Clear previous obstacle renderers (and associated resources)
    while(this.obstacles.length > 0) {
      this.obstacles[0].clear();
      this.obstacles.splice(0,1);
    }

    //Clear previous ewar renderers (and associated resources)
    while(this.ewarList.length > 0) {
      this.ewarList[0].clear();
      this.ewarList.splice(0,1);
    }

    //Clear previous rayfield renderers (and associated resources)
    while(this.rayfields.length > 0) {
      this.rayfields[0].clear();
      this.rayfields.splice(0,1);
    }

    //Clear previous poi renderers (and associated resources)
    while(this.pois.length > 0) {
      this.pois[0].clear();
      this.pois.splice(0,1);
    }

    //Clear previous explosion renderers (and associated resources)
    while(this.explosions.length > 0) {
      this.explosions[0].clear();
      this.explosions.splice(0,1);
    }

    //Clear previous explosion renderers (and associated resources)
    while(this.signatures.length > 0) {
      this.signatures[0].clear();
      this.signatures.splice(0,1);
    }
  }
  async reload() {
    this.clear();
    if(typeof this.report !== 'undefined') {
      await this.loadReport(this.report);
    }
  }
}