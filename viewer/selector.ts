import { AbstractMesh, PickingInfo, Scene, Sprite, Vector3 } from "babylonjs";
import Camera from "./camera";
import { Emitter } from "nanoevents";

//TODO: make this based on distance of selection from camera
const IGNORE_SELECT_IF_MOVED_MORE_THAN = 1; //an arbitrary low number that's not zero

const SELECTABLE_TYPES = [
  "ship",
  "missile",
  "shell"
]

//TODO: port the selector to use the emitter instead of the current system
export default class SelectionManager {
  scene: Scene;
  cameraController: Camera;
  emitter: Emitter;
  //data
  selectedObjectId: string; 
  objectType: string;
  selectedObjectPickable: AbstractMesh | null;
  //ops
  active: boolean;
  matched: boolean; //set once a matching ID is found by the corresponding Renderer
  matchGuard: boolean; //set after the first render.update() call to block unneeded calls
  private cameraPos: Vector3;

  constructor(scene: Scene, emitter: Emitter, cameraController: Camera) {
    this.scene = scene;
    this.emitter = emitter;
    this.cameraController = cameraController;
    this.selectedObjectId = '';
    this.objectType = '';
    this.active = false;
    this.matched = false;
    this.matchGuard = false;
    this.cameraPos = Vector3.Zero();

    scene.onPointerDown = () => {
      this.cameraPos = new Vector3().copyFrom(this.scene.cameras[0].position);
    }

    scene.onPointerUp = (evt, pickInfo) => {
      //ignore selection logic if the camera is moved more than a set amount
      if (this.cameraPos.subtract(this.scene.cameras[0].position).length() > IGNORE_SELECT_IF_MOVED_MORE_THAN) { return; }

      //pickInfo doesn't include sprites I guess? so we do it ourselves
      var isSprite = scene.pickSprite(evt.x, evt.y);

      this.select(pickInfo, isSprite);
    } //onPointerUp

    this.emitter.on('selectObject', (id: string) => {
      this.select(null, null, id);
    });
  } //constructor
  select(pickInfo: PickingInfo | null, isSprite: PickingInfo | null, selectedId: string | null = null) {
    //prevents jumping to newly selected object if camera was following an object previously
    this.cameraController.isFollowing = false;

    //deselect if nothing pickable is clicked on
    if (pickInfo?.hit == false && isSprite?.hit == false && selectedId == null) {
      this.selectedObjectId = '';
      this.objectType = '';
      this.selectedObjectPickable = null;
      this.active = false;
      this.matched = false;
      this.matchGuard = false;
      this.emitter.emit('selectionCleared'); //catcher at sidebar/ships.ts:42
      return;
    }

    //populate data
    if(pickInfo?.pickedMesh) {
      this.selectedObjectPickable = pickInfo.pickedMesh;
      this.active = true;
      this.matched = false;
      this.matchGuard = false;

      for (let index = 0; index < SELECTABLE_TYPES.length; index++) {
        const element = SELECTABLE_TYPES[index];
        if(pickInfo.pickedMesh.name.includes(element)) {
          this.objectType = element;
          this.selectedObjectId = pickInfo.pickedMesh.name.substring(0, pickInfo.pickedMesh.name.indexOf("Mesh"));
          break;
        }
      }
    } else if(isSprite?.pickedSprite) {
      this.selectedObjectPickable = null; //NOTE: unable to set a sprite as an option. Sprite has no absolutePosition, and using position breaks the following code... somehow
      //instead of setting the pickable mesh here, it will be set in the relevant update call by whatever Renderer the ID matches
      this.active = true;
      this.matched = false;
      this.matchGuard = false;
        
      for (let index = 0; index < SELECTABLE_TYPES.length; index++) {
        const element = SELECTABLE_TYPES[index];
        if(isSprite.pickedSprite.name.includes(element)) {
          this.objectType = element;
          this.selectedObjectId = isSprite.pickedSprite.name.substring(0, isSprite.pickedSprite.name.indexOf("Sprite"));
          break;
        }
      }
    } else if(selectedId) {
      this.selectedObjectPickable = null; //instead of setting the pickable mesh here, it will be set in the relevant update call by whatever Renderer the ID matches
      this.active = true;
      this.matched = false;
      this.matchGuard = false;
        
      for (let index = 0; index < SELECTABLE_TYPES.length; index++) {
        const element = SELECTABLE_TYPES[index];
        if(selectedId.includes(element)) {
          this.objectType = element;
          this.selectedObjectId = selectedId;
          break;
        }
      }
    }

    if(this.objectType == "ship") {
      this.emitter.emit('shipSelected', this.selectedObjectId); //catcher at sidebar/ships.ts:48
    }
    else { //TEMP: right now we don't need anything beyond 'not a ship'
      this.emitter.emit('selectionCleared'); //TEMP //catcher at sidebar/ships.ts:42
    }
  }
}