import { Color3, Vector3 } from 'babylonjs';
import { IdData, KinematicData, AllData } from './json';

export function getId(id: IdData): string {
  return id.Type + id.Id;
}

//Linear interpolation function
export function getK(time: number, kHistory: KinematicData[], snap: boolean = false): KinematicData | null {
  if (kHistory === null || kHistory.length === null || kHistory.length <= 0) { return null; }
  let kLast = kHistory[0];
  if(kLast === null || typeof kLast === 'undefined') { return null; }
  let kNext: KinematicData | null = null;
  for(let k of kHistory) {
    if(k.Time <= time) {
      kLast = k;
    }
    if(k.Time > time && kNext === null) {
      kNext = k;
    }
  }
  if(kNext !== null && !snap) {
    //Nullchecks. Some data types will have null values that they don't need to reduce filesize or carry additional information. In these cases the returned Vector3 will be Vector3.Zero()
    let newPos = Vector3.Zero();
    let newRot = Vector3.Zero();

    if (kLast.Transform.Position != null && kNext.Transform.Position != null) {
      let kNextPos = new Vector3(kNext.Transform.Position.X, kNext.Transform.Position.Y, kNext.Transform.Position.Z);
      let kLastPos = new Vector3(kLast.Transform.Position.X, kLast.Transform.Position.Y, kLast.Transform.Position.Z);
      let posDiff = (kNextPos.subtract(kLastPos));
      newPos = kLastPos.add(posDiff.scale((time - kLast.Time) / (kNext.Time - kLast.Time)));
    }

    if(kLast.Transform.Rotation != null && kNext.Transform.Rotation != null) {
      let kNextRot = new Vector3(kNext.Transform.Rotation.X, kNext.Transform.Rotation.Y, kNext.Transform.Rotation.Z);
      let kLastRot = new Vector3(kLast.Transform.Rotation.X, kLast.Transform.Rotation.Y, kLast.Transform.Rotation.Z);
      let rotDiff = (kNextRot.subtract(kLastRot));
      //Correct for any rotation boundaries (going from 0 to 360)
      rotDiff = new Vector3(
        rotDiff.x > 180 ? rotDiff.x - 360 : rotDiff.x,
        rotDiff.y > 180 ? rotDiff.y - 360 : rotDiff.y,
        rotDiff.z > 180 ? rotDiff.z - 360 : rotDiff.z
      );
      rotDiff = new Vector3(
        rotDiff.x < -180 ? rotDiff.x + 360 : rotDiff.x,
        rotDiff.y < -180 ? rotDiff.y + 360 : rotDiff.y,
        rotDiff.z < -180 ? rotDiff.z + 360 : rotDiff.z
      );
      newRot = kLastRot.add(rotDiff.scale((time - kLast.Time) / (kNext.Time - kLast.Time)));
      newRot = new Vector3(newRot.x % 360, newRot.y % 360, newRot.z % 360);
    }

    return {
      Time: time,
      Transform: {
        Position: { X: newPos.x, Y: newPos.y, Z: newPos.z },
        Rotation: { X: newRot.x, Y: newRot.y, Z: newRot.z }
      }
    };
  }
  return kLast;
}

//TODO: complete and port this to rest of app
export function getOwnerK(allData: AllData, id: IdData): KinematicData[] | null {
  let ret: KinematicData[] | null = null;

  switch (id.Type) {
    case "ship":
      for(let shipKinematics of allData.ShipKinematics) {
        if(getId(shipKinematics) === getId(id)) {
          ret = shipKinematics.Log;
          break;
        }
      }
      break;
    default:
      console.log("unexpected id: " + getId(id));
      break;
  }

  if (ret === null) {
    console.log("unable to find owner kData for id: " + getId(id));
  }

  return ret;
}

export function randomBetween(min: number, max: number): number {
  return (Math.random() * (max - min) + min);
}

export class TeamColor {
  A: Color3;
  B: Color3;
  Neutral: Color3;
  constructor(a: Color3, b: Color3, neutral: Color3 = new Color3(1,1,1)) {
    this.A = a;
    this.B = b;
    this.Neutral = neutral;
  }
}