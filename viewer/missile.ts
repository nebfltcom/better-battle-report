import { Color3, Color4, LinesMesh, Mesh, MeshBuilder, MultiMaterial, Quaternion, Scene, Sprite, SpriteManager, StandardMaterial, Vector3 } from "babylonjs";
import { AllData, KinematicData, MissileInfo } from "./json";
import { RenderSettings } from './settings';
import { getId, getK } from "./utils";

const LINECOLOR3 = new Color3(1,0.6,0);

export default class MissileRenderer {
  //Start Settings Section
  trailTime: number;
  trailDensity: number;
  mainColor3 = {
    "Offensive" : new Color3(0.7,0,0),
    "Defensive" : new Color3(0,0.7,0),
    "Decoy" : new Color3(1,0.8,0),
    "Utility" : new Color3(0,0.4,1),
    "Selected" : new Color3(1,1,1)
  };
  mainAlpha = {
    "Offensive" : 1,
    "Defensive" : 1,
    "Decoy" : 1,
    "Utility" : 1,
    "Selected" : 1
  }
  //End Settings Section
  id: string;
  tooltipLines: string[] = ["Missile", `Name: `, `Size: `, `Loadout: `];
  scene: Scene;
  missileMesh: Mesh;
  missileMultiMaterial: MultiMaterial;
  kHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  linePath: Vector3[];
  lineCount: number = 0;
  lineMesh: LinesMesh;
  displayed: boolean = false;
  spriteManager: SpriteManager;
  missileSprite: Sprite;
  missileSpriteColors: Color4[] = [];
  isDecoy: boolean;
  constructor(scene: Scene, renderSettings: RenderSettings, spriteManager: SpriteManager, allData: AllData, info: MissileInfo) {
    //Initialize settings
    this.trailTime = renderSettings.missile.trailTime;
    this.trailDensity = renderSettings.missile.trailDensity;
    let offensiveColor = renderSettings.missile.colorOffensive;
    let defensiveColor = renderSettings.missile.colorDefensive;
    let decoyColor = renderSettings.missile.colorDecoy;
    let selectedColor = renderSettings.missile.colorSelected;
    let utilityColor = renderSettings.missile.colorUtility;
    this.mainColor3["Offensive"] = new Color3(offensiveColor.r, offensiveColor.g, offensiveColor.b);
    this.mainColor3["Defensive"] = new Color3(defensiveColor.r, defensiveColor.g, defensiveColor.b);
    this.mainColor3["Decoy"] = new Color3(decoyColor.r, decoyColor.g, decoyColor.b);
    this.mainColor3["Selected"] = new Color3(selectedColor.r, selectedColor.g, selectedColor.b);
    this.mainColor3["Utility"] = new Color3(utilityColor.r, utilityColor.g, utilityColor.b);
    this.mainAlpha["Offensive"] = offensiveColor.a;
    this.mainAlpha["Defensive"] = defensiveColor.a;
    this.mainAlpha["Decoy"] = decoyColor.a;
    this.mainAlpha["Selected"] = selectedColor.a;
    this.mainAlpha["Utility"] = utilityColor.a;

    //Just in case (see Utility.WeaponRole.cs)
    this.mainColor3["None"] = new Color3(decoyColor.r, decoyColor.g, decoyColor.b);
    this.mainAlpha["None"] = decoyColor.a;

    this.id = getId(info);
    let loadout = info.Loadout == undefined || info.Loadout == null ? '<DATA INCOMPLETE>' : info.Loadout; //Compat
    this.tooltipLines[1] = `Name: ${info.Name}`;
    this.tooltipLines[2] = `Size: ${info.Size}`;
    this.tooltipLines[3] = `Loadout: ${loadout}`; //TEMP
    this.scene = scene;
    this.spriteManager = spriteManager;
    this.isDecoy = info.IsDecoy;
    //Searching for missile kinematics data in allData
    for(let missileKinematics of allData.MissileKinematics) {
      if(getId(missileKinematics) === this.id) {
        //Create missile mesh
        this.missileMesh = MeshBuilder.CreateCapsule(`${this.id}Mesh`, {
          orientation: Vector3.Forward(),
          height: 3,
          radius: 0.25,
          capSubdivisions: 1,
          subdivisions: 1,
          tessellation: 3
        });
        //Create and apply material to ship mesh
        this.missileMultiMaterial = new MultiMaterial(`${this.id}matMulti`);
        let matMain = new StandardMaterial(`${this.id}matMain`);
        matMain.ambientColor = this.isDecoy ? this.mainColor3["Decoy"] : this.mainColor3[info.Role];
        matMain.backFaceCulling = false;
        matMain.alpha = this.isDecoy ? this.mainAlpha["Decoy"] : this.mainAlpha[info.Role];
        this.missileMultiMaterial.subMaterials.push(matMain);
        let matSelected = new StandardMaterial(`${this.id}matSelected`);
        matSelected.ambientColor = this.mainColor3["Selected"];
        matSelected.backFaceCulling = false;
        matSelected.alpha = this.mainAlpha["Selected"];
        this.missileMultiMaterial.subMaterials.push(matSelected);
        this.missileMesh.material = this.missileMultiMaterial;
        //Create sprite
        this.missileSpriteColors.push(matMain.ambientColor.toColor4(1));
        this.missileSpriteColors.push(matSelected.ambientColor.toColor4(1));
        this.missileSprite = new Sprite(`${this.id}Sprite`, this.spriteManager);
        this.missileSprite.color = this.missileSpriteColors[0];
        //Set kHistory
        this.kHistory = missileKinematics.Log;
        this.maxTime = Math.max(0,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.maxTime,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.lineCount = this.getLineCountFromTime(this.trailTime);
        //Generate line path data
        this.linePath = [];
        for(let i = this.getLineCountFromTime(this.minTime);i < this.getLineCountFromTime(this.maxTime);i++) {
          let k = getK(this.getTimeFromLineCount(i), this.kHistory);
          if(k !== null) {
            this.linePath.push(this.getLinePoint(k));
          }
        }
        //Create line mesh
        let firstLinePath = this.getLinePath(0);
        this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
          points: firstLinePath,
          updatable: true
        });

        this.missileMesh.isPickable = true;
        this.missileSprite.isPickable = false;

        this.scene.removeMesh(this.missileMesh);
        this.scene.removeMesh(this.lineMesh);
        this.missileSprite.isVisible = false;
      }
    }
  }
  update(time: number, layerOn: boolean, spriteOn: boolean) {
    if (this.displayed) {
      if(!layerOn) {
        this.scene.removeMesh(this.missileMesh);

        if(!spriteOn) {
          this.scene.removeMesh(this.lineMesh);
          this.displayed = false;
          return; //otherwise everything will flicker
        }
      }
    }

    let currK = getK(time, this.kHistory);
    if(currK !== null && time >= this.minTime && time <= this.maxTime + this.trailTime) {
      this.missileMesh.position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, currK.Transform.Position.Z);
      this.missileMesh.rotation = new Vector3(currK.Transform.Rotation.X * Math.PI / 180, currK.Transform.Rotation.Y * Math.PI / 180, currK.Transform.Rotation.Z * Math.PI / 180);
      this.missileSprite.position = this.missileMesh.position;
      this.missileSprite.size = Math.max(0.015 * this.scene.cameras[0].position.subtract(this.missileSprite.position).length(), 3);
      let currLinePath = this.getLinePath(time)
      this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
        points: currLinePath,
        updatable: true,
        instance: this.lineMesh
      });
      this.lineMesh.color = LINECOLOR3;
      if(this.isDecoy) this.lineMesh.alpha = 0.2;
      if(!this.displayed && layerOn) {
        this.scene.addMesh(this.missileMesh);
        this.scene.addMesh(this.lineMesh);
        this.displayed = true;
        this.missileSprite.isVisible = spriteOn;
        this.missileSprite.isPickable = true;
      }
      if(this.missileSprite.isVisible != spriteOn) {
        this.missileSprite.isVisible = spriteOn;
        this.missileSprite.isPickable = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.missileMesh);
        this.scene.removeMesh(this.lineMesh);
        this.displayed = false;
        this.missileSprite.isVisible = false;
        this.missileSprite.isPickable = false;
      }
    }

    //eugh
    if (!this.displayed) { return; }
    if (window.selector == null || window.selector == undefined) { return; }
    if (window.selector.active) {
      if (window.selector.matchGuard) { return; }
      if (window.selector.selectedObjectId == this.id) { //selected
        if (window.selector.selectedObjectPickable == null) { //if picked by the sprite the pickable will be null
          window.selector.selectedObjectPickable = this.missileMesh;
        }
        if (this.missileMesh.subMeshes[0].materialIndex != 1) {
          this.missileMesh.subMeshes[0].materialIndex = 1;
          this.missileSprite.color = this.missileSpriteColors[1];
          window.selector.matched = true;
        }
      } else if (this.missileMesh.subMeshes[0].materialIndex != 0) { //not selected; normal coloring
        this.missileMesh.subMeshes[0].materialIndex = 0;
        this.missileSprite.color = this.missileSpriteColors[0];
      }
    } else if (this.missileMesh.subMeshes[0].materialIndex != 0) { //not selected; normal coloring
      this.missileMesh.subMeshes[0].materialIndex = 0;
      this.missileSprite.color = this.missileSpriteColors[0];
    }
  }
  clear() {
    this.missileMesh.dispose(false, true);
    this.lineMesh.dispose(false, true);
    this.missileSprite.dispose();
  }
  getLinePoint(k: KinematicData): Vector3 {
    let positionVector = new Vector3(k.Transform.Position.X, k.Transform.Position.Y, k.Transform.Position.Z);
    let rotationQuaterion = Quaternion.FromEulerAngles(k.Transform.Rotation.X * Math.PI / 180, k.Transform.Rotation.Y * Math.PI / 180, k.Transform.Rotation.Z * Math.PI / 180);
    let rotationDirectionVector = (new Vector3(0,0,-1)).applyRotationQuaternion(rotationQuaterion);
    return positionVector.add(rotationDirectionVector.clone().scale(1.5));
  }
  getLinePath(time: number): Vector3[] {
    let minTime = Math.min(Math.max(time - this.trailTime, this.minTime), this.maxTime);
    let minK = getK(minTime, this.kHistory);
    if(minK === null) { console.log('mink error'); return []; }
    let realTime = Math.min(time, this.maxTime);
    let realK = getK(realTime, this.kHistory);
    if(realK === null) { console.log('realk error'); return []; }
    let newLinePath: Vector3[] = [];
    for(let i = 0;i < this.lineCount;i++) {
      let currTime = this.getTimeFromLineCount(i) + (time - this.trailTime);
      let currRC = this.getLineCountFromTime(currTime) - this.getLineCountFromTime(this.minTime);
      if(currTime <= minTime || typeof this.linePath[currRC] === 'undefined') {
        newLinePath.push(this.getLinePoint(minK));
      }
      else if(currTime < realTime && i < this.lineCount - 1) {
        newLinePath.push(this.linePath[currRC]);
      }
      else {
        newLinePath.push(this.getLinePoint(realK));
      }
    }
    return newLinePath;
  }
  getLineCountFromTime(time: number): number {
    return Math.max(Math.floor(time * this.trailDensity),1);
  }
  getTimeFromLineCount(rc: number): number {
    return rc / this.trailDensity;
  }
}