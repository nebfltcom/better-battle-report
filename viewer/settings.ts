import deepmerge from 'deepmerge';
import { LocalKey, LocalStorage } from 'ts-localstorage';
import { PartialDeep } from 'type-fest';

export class ColorData {
  r: number;
  g: number;
  b: number;
  a: number;
}

export class RenderSettings {
  skybox: {
    size: number;
    color: ColorData;
  } = {
    size: 5000,
    color: { r: 0.1, g: 0.1, b: 0.1, a: 1 }
  };
  camera: {
    origin: {
      size: number;
      color: ColorData;
    };
    pan: {
      maxSpeed: number;
      acceleration: number;
      deceleration: number;
    };
    fov: number;
  } = {
    origin: {
      size: 5,
      color: { r: 1, g: 0, b: 0, a: 0.25 }
    },
    pan: {
      maxSpeed: 3,
      acceleration: 10,
      deceleration: 15
    },
    fov: 45
  };
  grid: {
    show: boolean;
    sphericalCoordinates: boolean;
    resize: boolean;
    size: number;
    interval: number;
    dashSize: number;
    gapSize: number;
    dashAmount: number;
    dashWidth: number;
    color: ColorData;
  } = {
    show: true,
    sphericalCoordinates: true,
    resize: true,
    size: 2000,
    interval: 100,
    dashSize: 1,
    gapSize: 9,
    dashAmount: 1000,
    dashWidth: 1,
    color: { r: 1, g: 0.6, b: 0, a: 0.8 }
  };
  ship: {
    colorTeamA: ColorData;
    colorTeamB: ColorData;
    colorEliminated: ColorData;
    colorSelected: ColorData;
    colorDamaged: ColorData;
    trailTime: number;
    trailDensity: number;
    trailGradientQuality: number;
    trailAlpha: {
      start: number;
      end: number;
    };
  } = {
    colorTeamA: { r: 0.09, g: 0.39, b: 0.52, a: 1 },
    colorTeamB: { r: 0.8, g: 0.4, b: 0, a: 1 },
    colorEliminated: { r: 0.5, g: 0.5, b: 0.5, a: 1 },
    colorSelected: { r: 0, g: 0.8, b: 0.2, a: 1 },
    colorDamaged: { r: 0.4, g: 0, b: 0, a: 1 },
    trailTime: 300,
    trailDensity: 0.33,
    trailGradientQuality: 10,
    trailAlpha: {
      start: 0.5,
      end: 0
    }
  };
  missile: {
    colorOffensive: ColorData;
    colorDefensive: ColorData;
    colorDecoy: ColorData;
    colorSelected: ColorData;
    colorUtility: ColorData;
    trailTime: number;
    trailDensity: number;
  } = {
    colorOffensive: { r: 0.7, g: 0, b: 0, a: 1 },
    colorDefensive: { r: 0, g: 0.7, b: 0, a: 1 },
    colorDecoy: { r: 1, g: 0.8, b: 0, a: 1 },
    colorSelected: { r: 1, g: 1, b: 1, a: 1 },
    colorUtility: { r: 0, g: 0.4, b: 1, a: 1 },
    trailTime: 2.5,
    trailDensity: 10
  };
  obstacle: {
    color: ColorData;
    edge: {
      color: ColorData;
      width: number;
    }
  } = {
    color: { r: 0.5, g: 0.5, b: 0.5, a: 0.25 },
    edge: {
      color: { r: 1, g: 1, b: 1, a: 1 },
      width: 50
    }
  };
  useSigDiff: boolean = true;
  sigDiffColors: { //These base colors are taken straight from the game
    eo: ColorData; //NOTE: light blue coloration to tie in with missile seeker color language
    //radar:Colordata; //radar sig is split up into illuminator, jammer, and sensor typing
    radJam: ColorData;
    radSensor: ColorData; //NOTE: detection purple coloration to tie in with other detection color language. See this post: https://discord.com/channels/409638848302153728/905325713861771265/1084274351790096454
    radIllum: ColorData;
    commJam: ColorData;
    wake: ColorData;
    deception: ColorData;
  } = {
    eo: { r: 0.325, g: 1, b: 1, a: 0.05 },            //rgb(83, 255, 255)
    radJam: { r: 1, g: 0.145, b: 0.145, a: 0.05 },    //rgb(255, 37, 7)
    radSensor: { r: 0.666, g: 0.325, b: 1, a: 0.05 }, //rgb(170, 83, 255)
    radIllum: { r: 0.298, g: 1, b: 0.447, a: 0.05 },  //rgb(76, 255, 114)
    commJam: { r: 0.843, g: 1, b: 0.298, a: 0.05 },   //rgb(215, 255, 76)
    wake: { r: 1, g: 1, b: 1, a: 0.05 },              //TODO: decide on a color. orange?
    deception: { r: 1, g: 1, b: 1, a: 0.05 }          //deception has to do with em sig, which probably won't ever be touched
  };
  shell: {
    colorSelected: ColorData;
  } = {
    colorSelected:  { r: 0, g: 0.8, b: 0.2, a: 1 }
  };
  poi: {
    alpha: number;
  } = {
    alpha: 0.05
  }
}

export class SettingsManager {
  key: LocalKey<RenderSettings> = new LocalKey<RenderSettings>('renderSettings', null);
  settings: RenderSettings = new RenderSettings();
  constructor() {
    let storedSettings = LocalStorage.getItem(this.key);
    if(storedSettings !== null) {
      this.settings = deepmerge(this.settings, storedSettings);
    }
  }
  set(newSettings: PartialDeep<RenderSettings>) {
    this.settings = deepmerge(this.settings, newSettings) as RenderSettings;
    LocalStorage.setItem(this.key, this.settings);
  }
}