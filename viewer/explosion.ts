import { Color3, Color4, Mesh, MeshBuilder, Scene, StandardMaterial, Vector3 } from "babylonjs";
import { AllData } from "./json";
import { RenderSettings } from './settings';
import { getId } from "./utils";

export default class ExplosionRenderer {
  //color: Color3; //TODO: settings
  duration: number;
  id: string;
  mesh: Mesh;
  material: StandardMaterial;
  time: number;
  displayed: boolean;
  scene: Scene;
  constructor(scene: Scene, allData: AllData, id: string) {

    this.id = id;
    //Searching for explosion data in allData
    for(let explosion of allData.ExplosionData) {
      if(getId(explosion) === this.id) {
        this.time = explosion.Time;
        this.duration = 5;
        this.displayed = false;
        this.scene = scene;

        //Generating mesh
        this.mesh = MeshBuilder.CreateSphere(`${this.id}Mesh`, {
          diameter: explosion.Radius * 2,
          segments: 8
        });
        this.mesh.position = new Vector3(explosion.Position.X, explosion.Position.Y, explosion.Position.Z);

        //Create and apply material
        this.material = new StandardMaterial(`${this.id}Material`, scene);
        this.material.ambientColor = new Color3(0,0.9,0.3);
        this.material.backFaceCulling = false;
        this.material.alpha = 0.1;

        this.mesh.material = this.material;
        this.mesh.enableEdgesRendering(0.9999);
        this.mesh.edgesWidth = 15;
        this.mesh.edgesColor = this.material.ambientColor.toColor4(0.8);

        this.scene.removeMesh(this.mesh);
        break;
      }
    }
  }
  update(time: number) {
    if(time >= this.time && time < this.time + this.duration) {
      if(!this.displayed) {
        this.scene.addMesh(this.mesh);
        this.displayed = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.mesh);
        this.displayed = false;
      }
    }
  }
  clear() {
    this.mesh.dispose(false, true);
  }
}