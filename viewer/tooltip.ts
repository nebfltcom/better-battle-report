import { AbstractMesh, PickingInfo, Scene, Vector3 } from "babylonjs";
import { AdvancedDynamicTexture, Rectangle, StackPanel, TextBlock } from "babylonjs-gui";
import Camera from "./camera";

const UPDATE_FREQUENCY_LIMITER = 30; //NOTE: may require tuning when the system is more fleshed out

export default class TooltipRenderer {
  private scene: Scene;
  private fullscreenGuiTexture: AdvancedDynamicTexture;
  private camera: Camera;
  //gui data
  private container: Rectangle;
  private stackPanel: StackPanel;
  private titleText: TextBlock;
  private line1: TextBlock;
  private line2: TextBlock;
  private line3: TextBlock;
  private line4: TextBlock;
  //ops
  private frequencyLimitGuard: number = 0;
  currentId: string = "";
  isSet: boolean = false;
  currentObject: AbstractMesh | null = null;

  constructor(scene: Scene, fullscreenGuiTexture: AdvancedDynamicTexture, camera: Camera) {
    this.scene = scene;
    this.fullscreenGuiTexture = fullscreenGuiTexture;
    this.camera = camera;

    //Init elements
    this.container = this.fullscreenGuiTexture.getControlByName('Tooltip Box') as Rectangle;
    this.stackPanel = this.fullscreenGuiTexture.getControlByName('Tooltip Panel') as StackPanel;
    this.titleText = this.fullscreenGuiTexture.getControlByName('Tooltip Title') as TextBlock;
    this.line1 = this.fullscreenGuiTexture.getControlByName('Tooltip 1') as TextBlock;
    this.line2 = this.fullscreenGuiTexture.getControlByName('Tooltip 2') as TextBlock;
    this.line3 = this.fullscreenGuiTexture.getControlByName('Tooltip 3') as TextBlock;
    this.line4 = this.fullscreenGuiTexture.getControlByName('Tooltip 4') as TextBlock;

    this.container.isEnabled = false; //prevents it from occluding the mouse during fast movements
    this.container.isVisible = false;
  }
  update() {
    //Move tooltip to the mouse position if it is active
    if (this.container.isVisible) {
      this.container.left = this.scene.pointerX + 25;
      this.container.top = this.scene.pointerY + 25;

      //distance indicator
      let distance = 0;
      let reference = '';
      if (window.selector.selectedObjectPickable?.position != undefined && this.currentObject?.position != undefined) {
        distance = Math.abs(Vector3.Distance(this.currentObject?.position, window.selector.selectedObjectPickable.position));
        reference = 'from selected object';
      }
      else if (this.currentObject?.position != undefined) {
        distance = Math.abs(Vector3.Distance(this.currentObject?.position, this.camera.cameraTarget));
        reference = 'from camera origin';
      }
      this.line4.text = `Distance: ${(distance * 10).toFixed(2)}m ${reference}`;
    }

    if(this.frequencyLimitGuard++ != UPDATE_FREQUENCY_LIMITER) { return; }
    this.frequencyLimitGuard = 0;

    let pickInfo:PickingInfo | null = this.scene.pick(this.scene.pointerX, this.scene.pointerY, undefined);
    if (pickInfo.hit) {
      let name = pickInfo.pickedMesh?.name;
      if(name == undefined) {
        this.changeTarget("");
        return;
      }
      this.changeTarget(name.substring(0, name.indexOf("Mesh")));
    }
    else {
      pickInfo = this.scene.pickSprite(this.scene.pointerX, this.scene.pointerY, undefined); //why is this one nullable and regular pick isn't? w/e
      if(pickInfo == null || !pickInfo.hit || pickInfo.pickedSprite?.name == undefined) {
        this.changeTarget("");
        return;
      }
      this.changeTarget(pickInfo.pickedSprite.name.substring(0, pickInfo.pickedSprite.name.indexOf("Sprite")));
    }
  }

  show(title: string, line1: string, line2: string, line3: string) {
    this.titleText.text = title;
    this.line1.text = line1;
    this.line2.text = line2;
    this.line3.text = line3;
    this.line4.text = "";
    this.container.isVisible = true;
    this.update(); //force update to stop single frame weirdness
    this.isSet = true;
  }
  hide() {
    this.container.isVisible = false;
  }
  private changeTarget(id: string) {
    if (this.currentId == id) { return; }
    this.currentId = id;
    this.isSet = false;
  }
}