import { Color3, Color4, LinesMesh, MeshBuilder, Scene, Vector3 } from "babylonjs";
import { AllData, KinematicData } from "./json";
import { getId, getK } from "./utils";

const LINECOLOR3 = new Color3(0.3,1,0);

export default class BeamRenderer {
  id: string;
  scene: Scene;
  kStartHistory: KinematicData[];
  kEndHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  pulsed: boolean;
  pulsePeriod: number;
  lineMesh: LinesMesh;
  lineColor3: Color3;
  lineColor4: Color4;
  lineWidth: number;
  displayed: boolean = false;
  constructor(scene: Scene, allData: AllData, id: string) {
    this.id = id;
    this.scene = scene;
    //Searching for beam info data in allData
    for(let beamInfo of allData.BeamInfo) {
      if(getId(beamInfo) === this.id) {
        this.pulsed = beamInfo.Pulsed;
        this.pulsePeriod = beamInfo.DamagePeriod;
        this.lineColor3 = new Color3(
          beamInfo.Color.r,
          beamInfo.Color.g,
          beamInfo.Color.b
        );
        this.lineColor4 = new Color4(
          beamInfo.Color.r,
          beamInfo.Color.g,
          beamInfo.Color.b,
          beamInfo.Color.a
        );
        this.lineWidth = beamInfo.Width;
      }
    }
    //Searching for beam start kinematics data in allData
    for(let beamStartKinematics of allData.BeamStartKinematics) {
      if(getId(beamStartKinematics) === this.id) {
        //Set kHistory
        this.kStartHistory = beamStartKinematics.Log;
        this.maxTime = Math.max(0,...this.kStartHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.maxTime,...this.kStartHistory.map(k => k !== null ? k.Time : 0));
      }
    }
    //Searching for beam start kinematics data in allData
    for(let beamEndKinematics of allData.BeamEndKinematics) {
      if(getId(beamEndKinematics) === this.id) {
        //Set kHistory
        this.kEndHistory = beamEndKinematics.Log;
        this.maxTime = Math.max(this.maxTime,...this.kEndHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.minTime,...this.kEndHistory.map(k => k !== null ? k.Time : 0));
        //Create line mesh
        this.lineMesh = MeshBuilder.CreateLines(`${this.id}LineMesh`, {
          points: this.getLinePath(0),
          updatable: true
        });
        this.scene.removeMesh(this.lineMesh);
      }
    }
  }
  update(time: number) {
    //Checking if current time aligns with a pulse
    let allowDisplay = !this.pulsed;
    for(let k of this.kStartHistory) {
      if(time >= k.Time && time <= k.Time + this.pulsePeriod) {
        allowDisplay = true;
      }
    }
    if(allowDisplay && time >= this.minTime && time <= this.maxTime) {
      this.lineMesh = MeshBuilder.CreateLines(`${this.id}LineMesh`, {
        points: this.getLinePath(time),
        updatable: true,
        instance: this.lineMesh
      });
      this.lineMesh.color = this.lineColor3;
      if(this.lineWidth > 0.1) {
        this.lineMesh.enableEdgesRendering();
        this.lineMesh.edgesWidth = this.lineWidth * 500;
        this.lineMesh.edgesColor = this.lineColor4;
      }
      if(!this.displayed) {
        this.scene.addMesh(this.lineMesh);
        this.displayed = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.lineMesh);
        this.displayed = false;
      }
    }
  }
  clear() {
    this.lineMesh.dispose(false, true);
  }
  getLinePath(time: number): Vector3[] {
    let realTime = Math.min(time, this.maxTime);
    let startK = getK(realTime, this.kStartHistory, this.pulsed);
    if(startK === null) { return []; }
    let endK = getK(realTime, this.kEndHistory, this.pulsed);
    if(endK === null) { return []; }
    let newLinePath: Vector3[] = [];
    newLinePath.push(new Vector3(startK.Transform.Position.X, startK.Transform.Position.Y, startK.Transform.Position.Z));
    newLinePath.push(new Vector3(endK.Transform.Position.X, endK.Transform.Position.Y, endK.Transform.Position.Z));
    return newLinePath;
  }
}