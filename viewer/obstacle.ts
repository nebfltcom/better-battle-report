import { Color3, Color4, Mesh, MeshBuilder, MultiMaterial, Scene, StandardMaterial, Vector3, VertexData } from "babylonjs";
import { AllData, KinematicData } from "./json";
import { RenderSettings } from './settings';
import { getId, getK } from "./utils";

export default class ObstacleRenderer {
  //Start Settings Section
  color: Color4;
  edgeColor: Color4;
  edgeSize: number;
  //End Settings Section
  id: string;
  obstacleMesh: Mesh;
  //obstacleMaterial: StandardMaterial;
  obstacleMultiMat: MultiMaterial;
  kHistory: KinematicData[];
  scene: Scene;
  displayed: boolean = false;
  emptyMesh: boolean = false;
  constructor(scene: Scene, renderSettings: RenderSettings, allData: AllData, id: string) {
    //Initialize settings
    this.color = new Color4(
      renderSettings.obstacle.color.r,
      renderSettings.obstacle.color.g,
      renderSettings.obstacle.color.b,
      renderSettings.obstacle.color.a
    );
    this.edgeColor = new Color4(
      renderSettings.obstacle.edge.color.r,
      renderSettings.obstacle.edge.color.g,
      renderSettings.obstacle.edge.color.b,
      renderSettings.obstacle.edge.color.a
    );
    this.edgeSize = renderSettings.obstacle.edge.width;

    this.id = id;
    this.scene = scene;
    //Searching for obstacle mesh data in allData
    for(let obstacleMeshesData of allData.MapMeshes) {
      if(getId(obstacleMeshesData) === this.id) {
        //Generating single obstacle mesh from mesh collection data
        if(obstacleMeshesData.Meshes[0].PrimitiveCollider == null) { //ISSUE: a little hard coded
          let bmeshCollection: Mesh[] = [];
          for(let obstacleMeshData of obstacleMeshesData.Meshes) {
            let newBMesh = new Mesh(`${this.id}ObstacleMesh`);
            let vertexData = new VertexData();

            if(obstacleMeshData.MeshVerticesData.length == 0 || obstacleMeshData.MeshTriangleData.length == 0) {
              continue;
            }

            vertexData.positions = obstacleMeshData.MeshVerticesData.map(e => [e.X, e.Y, e.Z]).flat(); //ISSUE: problem if there's no meshData. put a null check and just delete the ObstacleRenderer if no meshdata exists? Recorder.cs
            vertexData.indices = obstacleMeshData.MeshTriangleData;
            vertexData.applyToMesh(newBMesh);
            bmeshCollection.push(newBMesh);
          }
          if(bmeshCollection.length == 0) {
            this.emptyMesh = true;
            break;
          }
          
          //Apply single obstacle mesh
          this.obstacleMesh = Mesh.MergeMeshes(bmeshCollection, true, true) as Mesh; //parameter 3 is allow32BitIndices. For detailed maps with large objects like Perdito
        }
        else {
          let primitive = obstacleMeshesData.Meshes[0].PrimitiveCollider;
          if(primitive.Type == "Box") {
            this.obstacleMesh = MeshBuilder.CreateBox(`${this.id}ObstacleMesh`, {
              width: primitive.Parameters.X,
              height: primitive.Parameters.Y,
              depth: primitive.Parameters.Z
            });
          }
          else if(primitive.Type == "Sphere") {
            this.obstacleMesh = MeshBuilder.CreateSphere(`${this.id}ObstacleMesh`, {
              diameter: primitive.Parameters.X * 2
            });
          }
          else if(primitive.Type == "Capsule") {
            this.obstacleMesh = MeshBuilder.CreateCapsule(`${this.id}ObstacleMesh`, {
              orientation: primitive.Parameters.X == 0 ? new Vector3(1,0,0) : primitive.Parameters.X == 1 ? new Vector3(0,1,0) : new Vector3(0,0,1),
              height: primitive.Parameters.Y,
              radius: primitive.Parameters.Z
            });
          }
          this.obstacleMesh.scaling = new Vector3(primitive.Scale.X, primitive.Scale.Y, primitive.Scale.Z);
        }
        this.obstacleMesh.enableEdgesRendering();
        this.obstacleMesh.edgesWidth = this.edgeSize;
        this.obstacleMesh.edgesColor = this.edgeColor;
        //Create and apply material to obstacle mesh
        let zOffset = 5; //basing this off the line width like ships results in very odd behaviour due to high line width. 5 is arbitrary, but based on limited testing to avoid edge flickering
        this.obstacleMultiMat = new MultiMaterial(`${this.id}matMulti`);
        let obstacleMaterial = new StandardMaterial(`${this.id}matMain`);
        obstacleMaterial.ambientColor = new Color3(this.color.r, this.color.g, this.color.b); //why is there Color3.ToColor4 but no Color4.ToColor3???
        obstacleMaterial.alpha = this.color.a;//0.25;
        obstacleMaterial.backFaceCulling = false;
        obstacleMaterial.zOffset = zOffset;
        let opaqueMaterial = new StandardMaterial(`${this.id}matOpaque`);
        opaqueMaterial.ambientColor = new Color3(this.color.r, this.color.g, this.color.b);
        opaqueMaterial.alpha = 1;
        opaqueMaterial.backFaceCulling = false;
        opaqueMaterial.zOffset = zOffset;
        this.obstacleMultiMat.subMaterials.push(obstacleMaterial);
        this.obstacleMultiMat.subMaterials.push(opaqueMaterial);
        this.obstacleMesh.material = this.obstacleMultiMat;
        this.scene.removeMesh(this.obstacleMesh);
        break;
      }
    }
    //Searching for obstacle kinematics data in allData
    for(let obstacleKinematics of allData.MapKinematics) {
      if(getId(obstacleKinematics) === this.id) {
        //Set kHistory
        this.kHistory = obstacleKinematics.Log;
        break;
      }
    }
  }
  update(time: number, layerOn: boolean, opacityOn: boolean) {
    if (this.emptyMesh) { return; } //TEMP: come up with a better solution

    if (!layerOn && this.displayed) {
      this.scene.removeMesh(this.obstacleMesh);
      this.displayed = false;
    }
    if (!layerOn) return; //otherwise everything will flicker

    if (!this.displayed) {
      this.scene.addMesh(this.obstacleMesh);
      this.displayed = true;
    }

    if (opacityOn && this.obstacleMesh.subMeshes[0].materialIndex != 0) {
      this.obstacleMesh.subMeshes[0].materialIndex = 0;
    } else if (!opacityOn && this.obstacleMesh.subMeshes[0].materialIndex == 0) {
      this.obstacleMesh.subMeshes[0].materialIndex = 1;
    }

    let currK = getK(time, this.kHistory);
    if(currK !== null) {
      this.obstacleMesh.position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, currK.Transform.Position.Z);
      this.obstacleMesh.rotation = new Vector3(currK.Transform.Rotation.X * Math.PI / 180, currK.Transform.Rotation.Y * Math.PI / 180, currK.Transform.Rotation.Z * Math.PI / 180);
    }
  }
  clear() {
    this.obstacleMesh?.dispose(false, true);
  }
}