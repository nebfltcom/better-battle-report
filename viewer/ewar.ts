import { Color3, Mesh, MeshBuilder, Scene, StandardMaterial, Vector3, VertexData } from "babylonjs";
import { AllData, ColorData, IdData, KinematicData } from "./json";
import { RenderSettings } from './settings';
import { getId, getK } from "./utils";


const BASECOLOR3 = new Color3(1.0,1.0,1.0);

//These are the base-game colors for the ewar effects of the given signature type
//TODO: probably just put these colors in settings actually, that way they can be used for other things
const EWARCOLOR3MAP = { //These base colors are taken straight from the game
  "Electro-Optical" : new Color3(1.0, 0.145, 0.145), //rgb(255, 37, 7) //original red
  //"Radar" //radar sig is split up into illuminator, jammer, and sensor
  "Jammer" : new Color3(1.0, 0.145, 0.145), //rgb(255, 37, 7)
  "Wake" : BASECOLOR3, //technically possible? //TODO: use to mark the reach of flare signatures?
  "Communications" : new Color3(0.843, 1.0, 0.298), //rgb(215, 255, 76)
  "Illuminator" : new Color3(0.298, 1.0, 0.447), //rgb(76, 255, 114)
  "Deception" : BASECOLOR3, //afaik only used for the Masquerade which has no visual component, but the game isn't done and modders are modders
  "Sensor" : new Color3(0.13, 0.35, 0.82) //rgb(33, 89, 209) //original blue
}

//List of signature types we want to override for color language or differentiation purposes.
const EWAR_COLOR_OVERRIDE: string[] = [
  "Sensor", //Detection purple coloration to tie in with other detection color language. See this post: https://discord.com/channels/409638848302153728/905325713861771265/1084274351790096454
  "Electro-Optical", //EO cones in-game are red, which is inconsistent with the color language associated with the signature type
]

interface ColorMap {
  [ewarColorMap: string]: Color3;
}

export default class EwarRenderer {
  //Start Settings Section
  sigDiffColorMap: ColorMap;
  useSigDiff: boolean;
  //End Settings Section

  id: string;
  scene: Scene;
  ewarMesh: Mesh;
  ewarMaterial: StandardMaterial;
  kHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  displayed: boolean = false;

  omni: boolean;
  sigType: string;
  ewarType: string;
  meshIndex: number;
  meshColor: ColorData;
  forceColorOverride: boolean; //dedicated servers cannot capture color information, so we override it. SigDiff will supercede this if true
  meshColorOverride: boolean = false;
  //TODO: when ships are selectable, have a list of all jamming sources currently affecting the ship with what power at time t
  //  -maybe have a way of testing whether a track would be seen were it not for jamming? As a performance metric?

  constructor(scene: Scene, renderSettings: RenderSettings, allData: AllData, id: string) {
    //Settings init
    this.useSigDiff = renderSettings.useSigDiff;
    this.sigDiffColorMap = {
      "Electro-Optical" : new Color3(
        renderSettings.sigDiffColors.eo.r,
        renderSettings.sigDiffColors.eo.g,
        renderSettings.sigDiffColors.eo.b
      ),
      "Jammer" : new Color3(
        renderSettings.sigDiffColors.radJam.r,
        renderSettings.sigDiffColors.radJam.g,
        renderSettings.sigDiffColors.radJam.b
      ),
      "Wake" : new Color3(
        renderSettings.sigDiffColors.wake.r,
        renderSettings.sigDiffColors.wake.g,
        renderSettings.sigDiffColors.wake.b
      ),
      "Communications" : new Color3(
        renderSettings.sigDiffColors.commJam.r,
        renderSettings.sigDiffColors.commJam.g,
        renderSettings.sigDiffColors.commJam.b
      ),
      "Illuminator" : new Color3(
        renderSettings.sigDiffColors.radIllum.r,
        renderSettings.sigDiffColors.radIllum.g,
        renderSettings.sigDiffColors.radIllum.b
      ),
      "Sensor" : new Color3(
        renderSettings.sigDiffColors.radSensor.r,
        renderSettings.sigDiffColors.radSensor.g,
        renderSettings.sigDiffColors.radSensor.b
      ),
    }
    //End Settings

    this.id = id;
    this.scene = scene;
    this.forceColorOverride = allData.CapturedByDedicatedServer;

    //Searching for ewar info data in allData
    for(let ewarInfo of allData.EWarInfo) {
      if(getId(ewarInfo) === this.id) {
        this.omni = ewarInfo.Omni;
        this.sigType = ewarInfo.SigType;
        this.ewarType = ewarInfo.EWarType;
        this.meshIndex = ewarInfo.MeshIndex;
        this.meshColor = ewarInfo.Color;

        if (this.forceColorOverride || this.meshColor === null || this.meshColor === undefined) {
          let overrideColor = BASECOLOR3.multiply(EWARCOLOR3MAP[this.getRelevantTypeOrSignature()]);
          this.meshColor = new ColorData;
          this.meshColor.r = overrideColor.r;
          this.meshColor.g = overrideColor.g;
          this.meshColor.b = overrideColor.b;
        }

        //If there is no entry in the override array, override will not trigger
        if (this.useSigDiff && EWAR_COLOR_OVERRIDE.indexOf(this.getRelevantTypeOrSignature()) > -1)
          this.meshColorOverride = true;
      }
    }

    //Searching for ewar mesh data in allData
    let ewarMeshesData = allData.EWarMeshes[this.meshIndex];

    if(ewarMeshesData.Meshes[0].PrimitiveCollider == null) { //ISSUE: a little hard coded
      //Generating single ewar mesh from mesh collection data. There should really only be one but 
      let bmeshCollection: Mesh[] = [];
      for(let ewarMeshData of ewarMeshesData.Meshes) {
        let newBMesh = new Mesh(`${this.id}EwarMesh`);
        let vertexData = new VertexData();
        vertexData.positions = ewarMeshData.MeshVerticesData.map(e => [e.X, e.Y, e.Z]).flat();
        vertexData.indices = ewarMeshData.MeshTriangleData;
        vertexData.applyToMesh(newBMesh);
        bmeshCollection.push(newBMesh);
      }
      //Apply single ewar mesh
      this.ewarMesh = Mesh.MergeMeshes(bmeshCollection) as Mesh;
    }
    else {
      let primitive = ewarMeshesData.Meshes[0].PrimitiveCollider;
      if(primitive.Type == "Box") {
        this.ewarMesh = MeshBuilder.CreateBox(`${this.id}ObstacleMesh`, {
          width: primitive.Parameters.X,
          height: primitive.Parameters.Y,
          depth: primitive.Parameters.Z
        });
      }
      else if(primitive.Type == "Sphere") {
        this.ewarMesh = MeshBuilder.CreateSphere(`${this.id}ObstacleMesh`, {
          diameter: primitive.Parameters.X * 2,
          segments: 16
        });
      }
      else if(primitive.Type == "Capsule") {
        this.ewarMesh = MeshBuilder.CreateCapsule(`${this.id}ObstacleMesh`, {
          orientation: primitive.Parameters.X == 0 ? new Vector3(1,0,0) : primitive.Parameters.X == 1 ? new Vector3(0,1,0) : new Vector3(0,0,1),
          height: primitive.Parameters.Y,
          radius: primitive.Parameters.Z
        });
      }
      this.ewarMesh.scaling = new Vector3(primitive.Scale.X, primitive.Scale.Y, primitive.Scale.Z);
    }

    //Create and apply material to ewar mesh
    this.ewarMaterial = new StandardMaterial(`${this.id}EwarMaterial`);
    if (this.meshColorOverride)
      this.ewarMaterial.ambientColor = BASECOLOR3.multiply(this.sigDiffColorMap[this.getRelevantTypeOrSignature()]); //Illuminators have radar signature, but illuminator typing
    else
      this.ewarMaterial.ambientColor = new Color3(this.meshColor.r, this.meshColor.g, this.meshColor.b); //The alpha pulled from the shader is 1, which is not useful here.
    this.ewarMaterial.backFaceCulling = false;
    this.ewarMaterial.alpha = 0.05; //pretty arbitrary number TODO: probably make it a setting //ISSUE: does alpha even work?

    this.ewarMesh.material = this.ewarMaterial;
    this.ewarMesh.enableEdgesRendering(0.9999);
    this.ewarMesh.edgesWidth = 20;
    this.ewarMesh.edgesColor = this.ewarMaterial.ambientColor.toColor4(0.8);

    scene.removeMesh(this.ewarMesh);

    //Searching for ewar kinematics data in allData
    for(let ewarKinematics of allData.EWarKinematics) {
      if(getId(ewarKinematics) === this.id) {
        //Set kHistory
        this.kHistory = ewarKinematics.Log;
        this.maxTime = Math.max(0,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.maxTime,...this.kHistory.map(k => k !== null ? k.Time : 0));
      }
    }
  }

  update(time: number, layerOn: boolean) {
    if (!layerOn && this.displayed) {
      this.scene.removeMesh(this.ewarMesh);
      this.displayed = false;
    }
    if (!layerOn) return; //otherwise everything will flicker

    let currK = getK(time, this.kHistory);
    if(currK !== null && time >= this.minTime && time <= this.maxTime) {
      this.ewarMesh.position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, currK.Transform.Position.Z);
      this.ewarMesh.rotation = this.omni ? new Vector3(0.0,0.0,0.0) : new Vector3(currK.Transform.Rotation.X * Math.PI / 180, currK.Transform.Rotation.Y * Math.PI / 180, currK.Transform.Rotation.Z * Math.PI / 180);

      if(!this.displayed) {
        this.scene.addMesh(this.ewarMesh);
        this.displayed = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.ewarMesh);
        this.displayed = false;
      }
    }
  }

  clear() {
    this.ewarMesh.dispose(false, true);
  }

  //components with radar signature are differentiated by type (illuminator, jammer, sensor)
  getRelevantTypeOrSignature() {
    return this.sigType == "Radar" ? this.ewarType : this.sigType;
  }
}