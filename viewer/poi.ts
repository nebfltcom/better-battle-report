import { Color3, Color4, Mesh, MeshBuilder, MultiMaterial, Scene, StandardMaterial, Vector3, VertexData } from "babylonjs";
import { AllData } from "./json";
import { RenderSettings } from './settings';
import { getId, TeamColor } from "./utils";

export default class PoiRenderer {
  id: string;
  scene: Scene;
  poiMesh: Mesh;
  multiMat: MultiMaterial;
  colors: TeamColor;
  alpha: number;
  times: number[];
  ownership: boolean[];
  displayed: boolean = false;
  constructor(scene: Scene, renderSettings: RenderSettings, allData: AllData, id: string) {

    this.id = id;
    this.colors = new TeamColor(
      new Color3(renderSettings.ship.colorTeamA.r, renderSettings.ship.colorTeamA.g, renderSettings.ship.colorTeamA.b),
      new Color3(renderSettings.ship.colorTeamB.r, renderSettings.ship.colorTeamB.g, renderSettings.ship.colorTeamB.b),
      new Color3(renderSettings.ship.colorEliminated.r, renderSettings.ship.colorEliminated.g, renderSettings.ship.colorEliminated.b)
    );
    this.alpha = renderSettings.poi.alpha;
    //Searching for poi data in allData
    for(let poiData of allData.MapPOIs) {
      if(getId(poiData) === this.id) {
        this.times = poiData.Times;
        this.ownership = poiData.Ownership;
        this.scene = scene

        //Generating mesh
        this.poiMesh = MeshBuilder.CreateSphere(`${this.id}ObstacleMesh`, { diameter: poiData.Radius * 2 });
        this.poiMesh.position = new Vector3(poiData.Position.X, poiData.Position.Y, poiData.Position.Z);

        //Create and apply material
        this.multiMat = new MultiMaterial( `${this.id}POI`, this.scene);
        let poiAlpha = this.alpha;//0.05

        let matNeutral = new StandardMaterial( `${this.id}matNeutral`, this.scene);
        matNeutral.ambientColor = this.colors.Neutral;
        matNeutral.backFaceCulling = false;
        matNeutral.alpha = poiAlpha;
        let matA = new StandardMaterial( `${this.id}matA`, this.scene);
        matA.ambientColor = this.colors.A;
        matA.backFaceCulling = false;
        matA.alpha = poiAlpha;
        let matB = new StandardMaterial( `${this.id}matB`, this.scene);
        matB.ambientColor = this.colors.B;
        matB.backFaceCulling = false;
        matB.alpha = poiAlpha;

        this.multiMat.subMaterials.push(matNeutral);
        this.multiMat.subMaterials.push(matA);
        this.multiMat.subMaterials.push(matB);
        this.poiMesh.material = this.multiMat;

        this.scene.removeMesh(this.poiMesh);
        break;
      }
    }
  }
  update(time: number, layerOn: boolean) {
    if (!layerOn && this.displayed) {
      this.scene.removeMesh(this.poiMesh);
      this.displayed = false;
    }
    if (!layerOn) return; //otherwise everything will flicker

    if (!this.displayed) {
      this.scene.addMesh(this.poiMesh);
      this.displayed = true;
    }

    let ownership: boolean | null = null;
    for (let index = 0; index < this.times.length; index++) {
      let nextIsNull = index + 1 == this.times.length;
      if (time > this.times[index] && nextIsNull ? true : time < this.times[index + 1]) {
        ownership = this.ownership[index];
        break;
      }
      else if (nextIsNull) {
        ownership = this.ownership[index];
      }
    }
    if (ownership === null && this.poiMesh.subMeshes[0].materialIndex != 0) { //Neutral
      this.poiMesh.subMeshes[0].materialIndex = 0;
    }
    else if (ownership === true && this.poiMesh.subMeshes[0].materialIndex != 1) { //TeamA
      this.poiMesh.subMeshes[0].materialIndex = 1;
    }
    else if (ownership === false && this.poiMesh.subMeshes[0].materialIndex != 2) { //TeamB
      this.poiMesh.subMeshes[0].materialIndex = 2;
    }
  }
  clear() {
    this.poiMesh.dispose(false, true);
  }
}