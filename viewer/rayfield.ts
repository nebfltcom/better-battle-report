import { Color3, InstancedLinesMesh, LinesMesh, MeshBuilder, Scene, Vector3, Matrix, Angle, Quaternion, TransformNode } from "babylonjs";
import { AllData, IdData, KinematicData, OwnerData, ShipInfo, Vector3Data } from "./json";
import { getId, getK, getOwnerK, randomBetween } from "./utils";

const MAINCOLOR3 = new Color3(1,1,1);

export default class RayFieldRenderer {
  id: string;
  scene: Scene;
  kHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  rayMesh: LinesMesh; //root mesh for instancing
  rayMeshes: InstancedLinesMesh[];

  rayMagnitude: number;
  munitionSpeed: number;
  munitionLifetime: number;

  owner: IdData;
  ownerKHistory: KinematicData[];
  ownerMaxTime: number;
  ownerMinTime: number;

  displayed: boolean = false;
  constructor(scene: Scene, allData: AllData, id: string) {
    this.id = id;
    this.scene = scene;
    //Searching for shell kinematics data in allData
    for(let RayField of allData.RayFieldData) {
      if(getId(RayField) === this.id) {
        //Set kHistory
        this.kHistory = RayField.Rays;
        this.maxTime = Math.max(0,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.maxTime,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.maxTime = this.maxTime + RayField.MunitionLifetime; //time adjustment for the travel time of the last bullet

        //Set munition data
        this.rayMagnitude = RayField.MunitionSpeed * allData.FixedDeltaTime;
        this.munitionSpeed = RayField.MunitionSpeed;
        this.munitionLifetime = RayField.MunitionLifetime;
        this.owner = RayField.Owner;

        //Set ownerKHistory
        let ownerKHistory = getOwnerK(allData, this.owner);
        if (ownerKHistory === null) {
          console.log("owner kData not found for id: " + this.id + " and owner id: " + getId(this.owner));
        }
        else {
          this.ownerKHistory = ownerKHistory;
          this.ownerMaxTime = Math.max(0,...this.ownerKHistory.map(k => k !== null ? k.Time : 0));
          this.ownerMinTime = Math.min(this.ownerMaxTime,...this.ownerKHistory.map(k => k !== null ? k.Time : 0));

          //Fill in missing position data
          for (let index = 0; index < this.kHistory.length; index++) {
            let ownerK = getK(this.kHistory[index].Time, this.ownerKHistory);
            if (ownerK != null) {
              let ownerPos = new Vector3(ownerK.Transform.Position.X, ownerK.Transform.Position.Y, ownerK.Transform.Position.Z);
              let offset = new Vector3(RayField.Offset.X, RayField.Offset.Y, RayField.Offset.Z);
              let quat = Quaternion.FromEulerAngles(ownerK.Transform.Rotation.X * Math.PI / 180, ownerK.Transform.Rotation.Y * Math.PI / 180, ownerK.Transform.Rotation.Z * Math.PI / 180);
              let newPos = Vector3.Zero();
              offset.rotateByQuaternionToRef(quat, newPos);
              newPos.addInPlace(ownerPos);

              this.kHistory[index].Transform.Position = new Vector3Data();
              this.kHistory[index].Transform.Position.X = newPos.x;
              this.kHistory[index].Transform.Position.Y = newPos.y;
              this.kHistory[index].Transform.Position.Z = newPos.z;
            }
            if (RayField.CylinderCast) { //ISSUE: accuracy value will not reflect changes from accuracy modifiers if the muzzle is not set to ignore them. not a problem for vanilla as of 3/29/2024 but you know what modders are like
              let jitter = this.randomRayInConeToCylinder(this.kHistory[index].Transform.Rotation, RayField.Accuracy / 2, this.munitionLifetime * this.munitionSpeed);
              if (this.kHistory[index].Transform.Position === null) { //Just in case so we don't crash out
                this.kHistory[index].Transform.Position = new Vector3Data();
              }
              this.kHistory[index].Transform.Position.X += jitter.X;
              this.kHistory[index].Transform.Position.Y += jitter.Y;
              this.kHistory[index].Transform.Position.Z += jitter.Z;
            }
          }
        }

        //generating the root mesh to be instanced
        let rootLinePath: Vector3[] = [Vector3.Zero(), new Vector3(0, 0, this.rayMagnitude)];
        this.rayMesh  = MeshBuilder.CreateLines(`${this.id}LineMesh_root`, {
          points: rootLinePath
        })
        //ISSUE: something here is weird. While any instanced line meshes are being drawn there is (at least) one line being drawn at the origin. I'm not sure if it's this one or an unplaced instance?
        //  -low priority unless the problem shows up with other instanced things that are far more noticeable than a single white line
        this.rayMesh.isVisible = false;
        this.rayMesh.setEnabled(false);

        this.rayMeshes = [];
        for (let index = 0; index < RayField.Rays.length; index++) {
          let lineMesh = this.rayMesh.createInstance(`${this.id}LineMesh${index}`)
          this.rayMeshes.push(lineMesh);
        }

        //ISSUE: is there a way to do this only setting one value like a parent?
        for (let index = 0; index < this.rayMeshes.length; index++) {
          this.scene.removeMesh(this.rayMeshes[index]);
        }
      }
    }
  }
  update(time: number, layerOn: boolean) {
    if (!layerOn && this.displayed) {
      for (let index = 0; index < this.rayMeshes.length; index++) {
        this.scene.removeMesh(this.rayMeshes[index]);
      }
      this.displayed = false;
    }
    if (!layerOn) return; //otherwise everything will flicker

    let currK = getK(time, this.kHistory);
    if(currK !== null && time >= this.minTime && time <= this.maxTime) {
      for (let index = 0; index < this.rayMeshes.length; index++) {
        let linePath = this.getLinePath(time, index);
        this.rayMeshes[index].position = linePath[0];
        this.rayMeshes[index].rotation = linePath[1];
      }

      //TODO: look at only adding meshes within the time window?
      if(!this.displayed) {
        for (let index = 0; index < this.rayMeshes.length; index++) {
          this.scene.addMesh(this.rayMeshes[index]);
        }
        this.displayed = true;
      }
    }
    else {
      if(this.displayed) {
        for (let index = 0; index < this.rayMeshes.length; index++) {
          this.scene.removeMesh(this.rayMeshes[index]);
        }
        this.displayed = false;
      }
    }
  }
  clear() {
    for (let index = 0; index < this.rayMeshes.length; index++) {
      this.rayMeshes[index].dispose(false, true);
    }
  }
  getLinePath(time: number, index: number): Vector3[] {
    //NOTE: the kinematic data for RayField is a little different than other kinematic logs.
    //  Instead of each entry being of one tracked object, each entry is the start of a path the object will follow from the logged time until that time + RayFieldData.MunitionLifetime
    //  NOTE: the use of the Rotation field has changed. Instead of a direction vector it is the Euler angles required to rotate Vector3.Forward() to face the desired direction
    let newLinePath: Vector3[] = [Vector3.Zero(), Vector3.Zero()];
    let startTime = this.kHistory[index].Time;

    if(time < startTime) { return newLinePath; }
    if(time > (startTime + this.munitionLifetime)) { return newLinePath; }

    newLinePath = [];
    //let startPoint = new Vector3(this.kHistory[index].Transform.Position.X, this.kHistory[index].Transform.Position.Y, this.kHistory[index].Transform.Position.Z);
    let startPoint = Vector3.Zero();

    if (this.kHistory[index].Transform.Position != null) { //data is constructed not stored
      startPoint = new Vector3(this.kHistory[index].Transform.Position.X, this.kHistory[index].Transform.Position.Y, this.kHistory[index].Transform.Position.Z);
    }

    /*if (this.ownerKHistory != null) {
      let ownerK = getK(time, this.ownerKHistory);
      if (ownerK != null) {
        startPoint = new Vector3(ownerK.Transform.Position.X, ownerK.Transform.Position.Y, ownerK.Transform.Position.Z).add(this.offset).add(startPoint);
      }
    }*/
    //let direction = new Vector3(this.kHistory[index].Transform.Rotation.X, this.kHistory[index].Transform.Rotation.Y, this.kHistory[index].Transform.Rotation.Z);
    let rotation = new Vector3(this.kHistory[index].Transform.Rotation.X, this.kHistory[index].Transform.Rotation.Y, this.kHistory[index].Transform.Rotation.Z);

    //Because we no longer get the direction vector we need to derive it
    //...or i suppose i could have just rotated the end point of the root mesh. Whatever. We're here now. oops
    let rotMat = Matrix.RotationYawPitchRoll(rotation.y, rotation.x, rotation.z);
    let forward = Vector3.Forward();
    let direction = Vector3.TransformCoordinates(forward, rotMat)
    
    //origin + length * direction
    let timedOrigin = startPoint.add(direction.scale(this.munitionSpeed * (time - startTime) ) );
    //let timedEndPoint = timedOrigin.add(direction.scale(this.rayMagnitude));
    
    newLinePath.push(timedOrigin);
    newLinePath.push(rotation);
    return newLinePath;
  }
  randomRayInConeToCylinder(direction: Vector3Data, halfAngle: number, range: number): Vector3Data { //From Nebulous.dll MathHelpers.cs
    let ret = new Vector3Data();
    let num = range * Math.tan(Math.PI / 180 * halfAngle);
    ret.X = num * randomBetween(-1, 1);
    ret.Y = num * randomBetween(-1, 1);
    ret.Z = 0;
    return /*Quaternion.LookRotation(direction) * */ret; //i uh, guess we don't need that bit
  }
}