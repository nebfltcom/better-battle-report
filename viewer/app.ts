import { Engine, Scene } from 'babylonjs';
import { AllData } from './json';
import Renderer from './render';;

export default class App {
  canvas: HTMLCanvasElement;
  engine: Engine;
  scene: Scene;
  renderer: Renderer;
  lastUpdateTime: number;
  constructor(canvas: HTMLCanvasElement) {
    this.canvas = canvas;
    this.engine = new Engine(this.canvas, true, { stencil: true }, true);
    this.scene = new Scene(this.engine);
    this.renderer = new Renderer(this.scene);

    //Initialize engine render loop
    this.engine.runRenderLoop(this.render.bind(this));

    //Resize engine on window resize
    window.addEventListener('resize', () => {
      this.engine.resize();
    });

    //Add global function for report loading
    (window as any).loadReport = (allData: AllData) => {
      this.renderer.loadReport(allData);
    };

    //Enable debugging if needed
    (window as any).d = () => {
      this.scene.debugLayer.show();
    };
  }
  render() {
    let deltaTime = this.engine.getDeltaTime();
    this.renderer.update(deltaTime / 1000);
    this.scene.render();
  }
}