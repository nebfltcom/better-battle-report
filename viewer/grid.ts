import { AbstractMesh, Color3, Color4, Curve3, LinesMesh, Mesh, MeshBuilder, Scene, Vector3 } from 'babylonjs';
import { RenderSettings } from './settings';
import { AdvancedDynamicTexture, TextBlock } from 'babylonjs-gui';
import { Emitter } from 'nanoevents';
import { emit } from 'process';

//ISSUE: grid will fail if the size is not a clean multiple of the interval
export default class GridRenderer {
  //Start Settings Section
  gridSize: number;
  gridColor: Color3;
  gridEdgeColor: Color4;
  gridSpherical: boolean;

  gridInterval: number;
  gridDashSize: number;
  gridGapSize: number;
  gridDashNb: number;
  gridDashWidth: number;
  //End Settings Section
  scene: Scene;
  emitter: Emitter;
  displayed: boolean;
  gridLines: LinesMesh[] = [];
  gridDistanceIndicators: Mesh[] = [];
  gridDistanceIndicatorPositions: Vector3[] = [];
  center: Vector3;
  constructor(scene: Scene, renderSettings: RenderSettings, emitter: Emitter) {
    //Initialize settings
    this.gridSize = renderSettings.grid.size;
    this.gridColor = new Color3(
      renderSettings.grid.color.r,
      renderSettings.grid.color.g,
      renderSettings.grid.color.b
    );
    this.gridEdgeColor = new Color4(
      renderSettings.grid.color.r,
      renderSettings.grid.color.g,
      renderSettings.grid.color.b,
      renderSettings.grid.color.a
    );
    this.gridSpherical = renderSettings.grid.sphericalCoordinates;

    this.gridInterval = renderSettings.grid.interval;
    this.gridDashSize = renderSettings.grid.dashSize;
    this.gridGapSize = renderSettings.grid.gapSize;
    this.gridDashNb = renderSettings.grid.dashAmount;
    this.gridDashWidth = renderSettings.grid.dashWidth;

    this.scene = scene;
    this.emitter = emitter;
    this.displayed = true;
    this.center = Vector3.Zero();

    if(this.gridSpherical) {
      //register emitter events to move grid on ship selection
      this.emitter.on('selectedPosition', (pos: Vector3) => {
        this.center = pos;
      });

      this.emitter.on('selectionCleared', () => {
        this.center = Vector3.Zero();
      });

      this.emitter.on('updateGridPosition', (pos: Vector3) => {
        this.center = pos;
      });

      //build grid
      let graduations = ["0.5", "1.0", "2.0", "3.0", "4.0", "5.0", "7.5", "10.0", "15.0", "20.0", "25.0"];
      let compass = [[new Vector3(0, 0, 50), new Vector3(0, 0, 2500)], [new Vector3(50, 0, 0), new Vector3(2500, 0, 0)], [new Vector3(0, 0, -50), new Vector3(0, 0, -2500)], [new Vector3(-50, 0, 0), new Vector3(-2500, 0, 0)]]

      for (let index = 0; index < compass.length; index++) {
        const element = compass[index];
        
        let line = MeshBuilder.CreateLines(`gridCompass${index}`, { points: element})
        line.alpha = 0; //make the central line clear to not get weird with grid transparency
        line.edgesColor = this.gridEdgeColor;
        line.edgesWidth = 400;
        line.enableEdgesRendering();
        this.gridLines.push(line);
      }

      for (let i = 0; i < graduations.length; i++) {
        let r = +graduations[i] * 100;
        let arc = Curve3.ArcThru3Points(new Vector3(r, 0, 0), new Vector3(0, 0, r), new Vector3(-r, 0, 0), 64, false, true);
        let arclines = MeshBuilder.CreateLines(`gridArc_${r * 10}m`, { points: arc.getPoints() });
        arclines.color = this.gridColor;
        arclines.edgesColor = this.gridEdgeColor;
        arclines.edgesWidth = this.gridDashWidth;
        arclines.enableEdgesRendering();
        this.gridLines.push(arclines);

        for (let j = 0; j < compass.length; j++) {
          let plane = MeshBuilder.CreatePlane(`distanceIndicator_c${j}_g${i}`, {size: 100}, this.scene);
          plane.billboardMode = Mesh.BILLBOARDMODE_ALL;
          let advTex = AdvancedDynamicTexture.CreateForMesh(plane);
          var text = new TextBlock(`text_c${j}_g${i}`, graduations[i]);
          text.width = 1;
          text.height = 0.4;
          text.color = "white";
          text.fontSize = 200;
          advTex.addControl(text);
          plane.position = new Vector3(Math.sin(j * Math.PI / 2) * r, 0, Math.cos(j * Math.PI / 2) * r);
          plane.renderingGroupId = 1; //forces text to appear over grid (rendering group 0)

          //performance
          plane.doNotSyncBoundingInfo = true;
          plane.freezeNormals();
          plane.material?.freeze();
          plane.cullingStrategy = AbstractMesh.CULLINGSTRATEGY_OPTIMISTIC_INCLUSION_THEN_BSPHERE_ONLY;

          this.gridDistanceIndicators.push(plane);
          this.gridDistanceIndicatorPositions.push(plane.position);
        }
      }
    } else { //Rectangular
      //TODO: make instanced?
      //This might just need a redo of some sort
      let realDashSize = (this.gridSize / this.gridDashNb) * (this.gridDashSize / (this.gridDashSize + this.gridGapSize));
      //Draw X-axis grid lines on YZ plane
      for(let y = -this.gridSize/2;y <= this.gridSize/2;y += this.gridInterval) {
        for(let z = -this.gridSize/2;z <= this.gridSize/2;z += this.gridInterval) {
          if(y === 0 || z === 0) {
            let gridLine = MeshBuilder.CreateDashedLines(`gridX0Y${y}Z${z}`, {
              points: [new Vector3(-(this.gridSize/2) + (realDashSize/2),y,z), new Vector3((this.gridSize/2) + (realDashSize/2),y,z)],
              dashSize: this.gridDashSize,
              gapSize: this.gridGapSize,
              dashNb: this.gridDashNb,
              updatable: true
            }, scene);
            gridLine.color = this.gridColor;
            if(this.gridDashWidth > 1) {
              if (this.displayed) gridLine.enableEdgesRendering();
              else gridLine.isVisible = false;
              gridLine.edgesWidth = this.gridDashWidth;
              gridLine.edgesColor = this.gridEdgeColor;
            }
            this.gridLines.push(gridLine);
          }
        }
      }
      //Draw Y-axis grid lines on XZ plane
      for(let x = -this.gridSize/2;x <= this.gridSize/2;x += this.gridInterval) {
        for(let z = -this.gridSize/2;z <= this.gridSize/2;z += this.gridInterval) {
          if(x === 0 || z === 0) {
            let gridLine = MeshBuilder.CreateDashedLines(`gridX${x}Y0Z${z}`, {
              points: [new Vector3(x,-(this.gridSize/2) + (realDashSize/2),z), new Vector3(x,(this.gridSize/2) + (realDashSize/2),z)],
              dashSize: this.gridDashSize,
              gapSize: this.gridGapSize,
              dashNb: this.gridDashNb,
              updatable: true
            }, scene);
            gridLine.color = this.gridColor;
            if(this.gridDashWidth > 1) {
              if (this.displayed) gridLine.enableEdgesRendering();
              else gridLine.isVisible = false;
              gridLine.edgesWidth = this.gridDashWidth;
              gridLine.edgesColor = this.gridEdgeColor;
            }
            this.gridLines.push(gridLine);
          }
        }
      }
      //Draw Z-axis grid lines on XY plane
      for(let x = -this.gridSize/2;x <= this.gridSize/2;x += this.gridInterval) {
        for(let y = -this.gridSize/2;y <= this.gridSize/2;y += this.gridInterval) {
          if(x === 0 || y === 0) {
            let gridLine = MeshBuilder.CreateDashedLines(`gridX${x}Y${y}Z0`, {
              points: [new Vector3(x,y,-(this.gridSize/2) + (realDashSize/2)), new Vector3(x,y,(this.gridSize/2) + (realDashSize/2))],
              dashSize: this.gridDashSize,
              gapSize: this.gridGapSize,
              dashNb: this.gridDashNb,
              updatable: true
            }, scene);
            gridLine.color = this.gridColor;
            if(this.gridDashWidth > 1) {
              if (this.displayed) gridLine.enableEdgesRendering();
              else gridLine.isVisible = false;
              gridLine.edgesWidth = this.gridDashWidth;
              gridLine.edgesColor = this.gridEdgeColor;
            }
            this.gridLines.push(gridLine);
          }
        }
      }
    }

  }
  update(layerOn: boolean) {
    if (!layerOn && this.displayed) {
      for (let index = 0; index < this.gridLines.length; index++) {
        this.gridLines[index].isVisible = false;
      }
      for (let index = 0; index < this.gridDistanceIndicators.length; index++) {
        this.gridDistanceIndicators[index].isVisible = false;
      }
      this.displayed = false;
    }

    if (layerOn && !this.displayed) {
      for (let index = 0; index < this.gridLines.length; index++) {
        this.gridLines[index].isVisible = true;
      }
      for (let index = 0; index < this.gridDistanceIndicators.length; index++) {
        this.gridDistanceIndicators[index].isVisible = true;
      }
      this.displayed = true;
    }

    if (layerOn) {
      for (let index = 0; index < this.gridLines.length; index++) {
        this.gridLines[index].position = this.center;
      }
      for (let index = 0; index < this.gridDistanceIndicators.length; index++) {
        this.gridDistanceIndicators[index].position = this.center.add(this.gridDistanceIndicatorPositions[index]);

        //shrink distance indicators as they close in on the camera
        if(this.scene.activeCamera != null) { //-.-
          let distancefromCamera = Vector3.Distance(this.scene.activeCamera.position, this.gridDistanceIndicators[index].position);
          let scalar = Math.min(1, distancefromCamera / 1000);
          (this.gridDistanceIndicators[index]).scaling = new Vector3(scalar, scalar, scalar);
        }
      }
    }
  }
  clear() {
    for (let index = 0; index < this.gridLines.length; index++) {
      this.gridLines[index].dispose();
    }
    for (let index = 0; index < this.gridDistanceIndicators.length; index++) {
      this.gridDistanceIndicators[index].dispose();
    }
  }
}

//this.emitter.emit('shipSelected', this.SelectedObject); //catcher at sidebar/ships.ts:48
/*
    this.emitter.on('selectObject', (id: string) => {
      this.select(null, null, id);
    });
*/