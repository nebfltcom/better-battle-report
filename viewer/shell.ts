import { Color3, Color4, LinesMesh, Mesh, MeshBuilder, MultiMaterial, Scene, Sprite, SpriteManager, StandardMaterial, Vector3 } from "babylonjs";
import { AllData, KinematicData, ShellInfo } from "./json";
import { TeamColor, getId, getK } from "./utils";
import { RenderSettings } from "./settings";

const LINETRAIL = 0.25;
const MAINCOLOR3 = new Color3(0.8,0.8,0.8);
const LINECOLOR3 = new Color3(1,1,1);

export default class ShellRenderer {
  id: string;
  tooltipLines: string[] = ["Shell", "Name: ", "", ""];
  scene: Scene;
  shellMesh: Mesh;
  shellMultiMaterial: MultiMaterial;
  kHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  lineMesh: LinesMesh;
  displayed: boolean = false;
  spriteManager: SpriteManager;
  shellSprite: Sprite;
  shellSpriteColors: Color4[] = [];
  constructor(scene: Scene, renderSettings: RenderSettings, spriteManager:SpriteManager, allData: AllData, info: ShellInfo) {
    this.id = getId(info);
    this.tooltipLines[1] = `Name: ${info.Name}`;
    this.scene = scene;
    this.spriteManager = spriteManager;
    this.shellSpriteColors.push(MAINCOLOR3.toColor4(1));
    this.shellSpriteColors.push(new Color4(renderSettings.shell.colorSelected.r, renderSettings.shell.colorSelected.g, renderSettings.shell.colorSelected.b, 1));
    //Searching for shell kinematics data in allData
    for(let shellKinematics of allData.ShellKinematics) {
      if(getId(shellKinematics) === this.id) {
        //Create shell mesh
        this.shellMesh = MeshBuilder.CreateSphere(`${this.id}Mesh`, {
          diameter: 0.25,
          segments: 1
        });
        //Create and apply material to ship mesh
        this.shellMultiMaterial = new MultiMaterial(`${this.id}matMulti`);
        let matMain = new StandardMaterial(`${this.id}matMain`);
        matMain.ambientColor = MAINCOLOR3;
        matMain.backFaceCulling = false;
        matMain.alpha = 1;
        let matSelected = new StandardMaterial(`${this.id}matSelected`);
        matSelected.ambientColor = new Color3(renderSettings.shell.colorSelected.r, renderSettings.shell.colorSelected.g, renderSettings.shell.colorSelected.b);
        matSelected.backFaceCulling = false;
        matSelected.alpha = 1;
        this.shellMultiMaterial.subMaterials.push(matMain);
        this.shellMultiMaterial.subMaterials.push(matSelected);
        this.shellMesh.material = this.shellMultiMaterial;
        //Set kHistory
        this.kHistory = shellKinematics.Log;
        this.maxTime = Math.max(0,...this.kHistory.map(k => k !== null ? k.Time : 0));
        this.minTime = Math.min(this.maxTime,...this.kHistory.map(k => k !== null ? k.Time : 0));
        //Create line mesh
        this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
          points: this.getLinePath(0),
          updatable: true
        });
        //Create sprite
        this.shellSprite = new Sprite(`${this.id}Sprite`, this.spriteManager);
        this.shellSprite.color = this.shellSpriteColors[0];

        this.shellMesh.isPickable = true;
        this.shellSprite.isPickable = false;

        this.scene.removeMesh(this.shellMesh);
        this.scene.removeMesh(this.lineMesh);
        this.shellSprite.isVisible = false;
      }
    }
  }
  update(time: number, layerOn: boolean, spriteOn: boolean) {
    if (!layerOn && this.displayed) {
      this.scene.removeMesh(this.shellMesh);
      this.scene.removeMesh(this.lineMesh);
      this.displayed = false;
    }
    if (!layerOn) return; //otherwise everything will flicker
    
    let currK = getK(time, this.kHistory);
    if(currK !== null && time >= this.minTime && time <= this.maxTime + LINETRAIL) {
      this.shellMesh.position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, currK.Transform.Position.Z);
      this.shellSprite.position = this.shellMesh.position;
      this.shellSprite.size = Math.max(0.0075 * this.scene.cameras[0].position.subtract(this.shellSprite.position).length(), 1);
      this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
        points: this.getLinePath(time),
        updatable: true,
        instance: this.lineMesh
      });
      this.lineMesh.color = LINECOLOR3;
      if(!this.displayed) {
        this.scene.addMesh(this.shellMesh);
        this.scene.addMesh(this.lineMesh);
        this.displayed = true;
        this.shellSprite.isVisible = spriteOn;
        this.shellSprite.isPickable = true;
      }
      if(this.shellSprite.isVisible != spriteOn) {
        this.shellSprite.isVisible = spriteOn;
        this.shellSprite.isPickable = true;
      }
    }
    else {
      if(this.displayed) {
        this.scene.removeMesh(this.shellMesh);
        this.scene.removeMesh(this.lineMesh);
        this.displayed = false;
        this.shellSprite.isVisible = false;
        this.shellSprite.isPickable = false;
      }
    }

    if (!this.displayed) { return; }
    if (window.selector == null || window.selector == undefined) { return; }
    if (window.selector.active) {
      if (window.selector.matchGuard) { return; }
      if (window.selector.selectedObjectId == this.id) { //selected
        if (window.selector.selectedObjectPickable == null) { //if picked by the sprite the pickable will be null
          window.selector.selectedObjectPickable = this.shellMesh;
        }
        if (this.shellMesh.subMeshes[0].materialIndex != 1) {
          this.shellMesh.subMeshes[0].materialIndex = 1;
          this.shellSprite.color = this.shellSpriteColors[1];
          window.selector.matched = true;
        }
      } else if (this.shellMesh.subMeshes[0].materialIndex != 0) { //not selected; normal coloring
        this.shellMesh.subMeshes[0].materialIndex = 0;
        this.shellSprite.color = this.shellSpriteColors[0];
      }
    } else if (this.shellMesh.subMeshes[0].materialIndex != 0) { //not selected; normal coloring
      this.shellMesh.subMeshes[0].materialIndex = 0;
      this.shellSprite.color = this.shellSpriteColors[0];
    }
  }
  clear() {
    this.shellMesh.dispose(false, true);
    this.lineMesh.dispose(false, true);
    this.shellSprite.dispose();
  }
  getLinePath(time: number): Vector3[] {
    let minTime = Math.min(Math.max(time - LINETRAIL, this.minTime), this.maxTime);
    let minK = getK(minTime, this.kHistory);
    if(minK === null) { return []; }
    let realTime = Math.min(time, this.maxTime);
    let realK = getK(realTime, this.kHistory);
    if(realK === null) { return []; }
    let newLinePath: Vector3[] = [];
    newLinePath.push(new Vector3(minK.Transform.Position.X, minK.Transform.Position.Y, minK.Transform.Position.Z));
    newLinePath.push(new Vector3(realK.Transform.Position.X, realK.Transform.Position.Y, realK.Transform.Position.Z));
    return newLinePath;
  }
}