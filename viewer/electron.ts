const { app, BrowserWindow } = require('electron');
const fs = require('fs');
const path = require('path');
const zlib = require('zlib');

app.commandLine.appendArgument('force_high_performance_gpu');

const createWindow = async () => {
  const win = new BrowserWindow({
    webPreferences: {
      preload: path.join(__dirname, '../build/electronpreload.js')
    },
    width: 1920,
    height: 1080
  });
  //win.removeMenu();
  await win.loadFile('./build/electron.html');
  if(process.argv.length > 1) {
    let report = null;
    for(let arg of process.argv) {
      if(report === null) {
        try {
          let content = fs.readFileSync(arg);
          let contentPath = path.parse(arg);
          try {
            if(contentPath.ext === '.json') {
              report = content;
            }
            else if(contentPath.ext === '.bbr') {
              report = zlib.unzipSync(content).toString();
            }
          }
          catch(err) {
            report = null;
          }
        }
        catch(err) {}
      }
    }
    if(report !== null) {
      win.webContents.send('loadReport', report);
    }
  }
}

app.whenReady().then(() => {
  createWindow();
  app.on('activate', () => {
    if(BrowserWindow.getAllWindows().length === 0) {
      createWindow();
    }
  });
})

app.on('window-all-closed', () => {
  if(process.platform !== 'darwin') {
    app.quit();
  }
});
