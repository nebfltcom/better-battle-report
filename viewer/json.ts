//TODO: give default values where it makes sense (like colors)

export class AllData {
  GameVersion: String;
  ModVersion: String;
  ViewerVersion: String;
  FixedDeltaTime: number;
  CapturedByDedicatedServer: boolean;
  MapInfo: MapInfo;
  MapMeshes: MeshCollectionData[] = [];
  MapKinematics: KinematicLog[] = [];
  MapPOIs: PoiData[] = []
  FleetInfo: FleetInfo[] = [];
  ShipMeshes: MeshCollectionData[] = [];
  ShipKinematics: KinematicLog[] = [];
  ShipInfo: ShipInfo[] = [];
  MissileKinematics: KinematicLog[] = [];
  MissileInfo: MissileInfo[] = [];
  ShellKinematics: KinematicLog[] = [];
  ShellInfo: ShellInfo[] = [];
  BeamStartKinematics: KinematicLog[] = [];
  BeamEndKinematics: KinematicLog[] = [];
  BeamInfo: BeamInfo[] = [];
  EWarMeshes: MeshCollectionData[] = [];
  EWarKinematics: KinematicLog[] = [];
  EWarInfo: EWarInfo[] = [];
  RayFieldData: RayFieldData[] = [];
  ExplosionData: ExplosionData[] = [];
  DamageData: DamageLog[] = [];
}

export class IdData {
  Id: number;
  Type: string;
}

export class MeshCollectionData extends IdData {
  Meshes: MeshData[];
}

export class MeshData {
  PrimitiveCollider: PrimitiveCollider;
  MeshVerticesData: Vector3Data[];
  MeshTriangleData: number[];
}

export class PrimitiveCollider {
  Type: string;
  Parameters: Vector3Data;
  Scale: Vector3Data;
}

export class KinematicLog extends IdData {
  Log: KinematicData[];
}

export class KinematicData {
  Time: number;
  Transform: TransformData;
}

export class TransformData {
  Position: Vector3Data;
  Rotation: Vector3Data;
}

export class Vector3Data {
  X: number;
  Y: number;
  Z: number;
}

export class MapInfo {
  Name: string;
  Key: string;
  Radius: number;
  GameMode: string;
  VictoryPoints: number;
  VPOffset: number;
  Victor: string;
}

export class OwnerData extends IdData {
  Owner: IdData;
}

export class FleetInfo extends OwnerData {
  Name: string;
  IsTeamA: boolean;
  PlayerName: string;
}

export class ShipInfo extends OwnerData {
  Name: string;
  Class: string;
  HullClassification: string;
  Fleet: IdData;
  TimeEliminated: number;
  TimeDefanged: number;
  MeshIndex: number;
}

export class MissileInfo extends OwnerData {
  Size: number;
  Name: string;
  IsDecoy: boolean;
  FiredDefensively: boolean;
  Role: string;
  Loadout: string;
}

export class ShellInfo extends OwnerData {
  Name: string;
}

export class ColorData {
  r: number;
  g: number;
  b: number;
  a: number;
}

export class BeamInfo extends OwnerData {
  Name: string;
  Pulsed: boolean;
  DamagePeriod: number;
  Width: number;
  Color: ColorData;
}

export class EWarInfo extends OwnerData {
  Name: string;
  SigType: string;
  EWarType: string;
  Omni: boolean; //NOTE: if this is true, rotation values will NOT be recorded.
  Color: ColorData;
  MeshIndex: number;
}

export class RayFieldData extends OwnerData {
  MunitionName: string;
  MunitionSpeed: number;
  MunitionLifetime: number;
  Offset: Vector3Data;
  CylinderCast: boolean;
  Accuracy: number;
  Rays: KinematicData[];
}

export class PoiData extends IdData {
  Name: string;
  Position: Vector3Data;
  Radius: number;
  Times: number[];
  Ownership: boolean[];
}

export class ExplosionData extends IdData {
  Time: number;
  Position: Vector3Data;
  Radius: number;
}

export class DamageLog extends IdData {
  Log: DamageInstance[];
}

export class DamageInstance {
  Time: number;
  Internal: number;
  Armor: number;
}