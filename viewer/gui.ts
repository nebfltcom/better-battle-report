import { createNanoEvents, Emitter } from 'nanoevents';
import { Scene } from 'babylonjs';
import { AdvancedDynamicTexture, Button, Control, InputText, Rectangle, Slider, TextBlock, Container } from 'babylonjs-gui';
import { AllData } from './json';
import { SettingsManager } from './settings';
import SideBar from './sidebar';
import TooltipManager from './tooltip';
import Camera from './camera';
const fullscreenGuiConfig = require('./fullscreenGuiConfig.json');

export interface GuiEvents {
  openReport: () => void,
  reloadReport: () => void,
  toggleLayer: (layer: string) => void
}

const baseScenarios: string[] = [
  "Deathmatch",
  "Control",
  "Tug Of War",
  "Two Flags",
  "Station Capture",
  "Center Flag"
];

export default class Gui {
  scene: Scene;
  emitter: Emitter;
  fullscreenGuiTexture: AdvancedDynamicTexture;
  settingsManager: SettingsManager;
  sideBarManager: SideBar;
  camera: Camera;
  //Top App Bar
  openButton: Button;
  playButton: Button;
  playbackInput: InputText;
  sideBarButton: Button;
  //Score
  scoreStep: number;
  maxScore: number;
  scoring: Rectangle;
  scoreTextTeamA: TextBlock;
  scoreTextTeamB: TextBlock;
  scoreBarTeamA: Slider;
  scoreBarTeamB: Slider;
  scoreTeamA: number[] = [];
  scoreTeamB: number[] = [];
  //Bottom App Bar
  bottomAppBar: Control;
  timeIndicator: TextBlock;
  timelineSlider: Slider;
  //Side Bar
  sideBarOpen: boolean = false;
  sideBar: Control;
  //Tooltip
  tooltip: TooltipManager;
  //Misc
  loadingIndicator: Rectangle;
  //Time Control
  currTime: number = 0;
  maxTime: number = 0;
  paused: boolean = true;
  playbackSpeed: number = 1;
  constructor(scene: Scene, settingsManager: SettingsManager, camera: Camera) {
    //Create emitter used to communicate to outside objects
    this.emitter = createNanoEvents<GuiEvents>();

    //Create gui
    this.scene = scene;
    this.fullscreenGuiTexture = AdvancedDynamicTexture.CreateFullscreenUI('fullscreenGui');
    this.fullscreenGuiTexture.parseSerializedObject(fullscreenGuiConfig);
    this.openButton = this.fullscreenGuiTexture.getControlByName('Open File') as Button;
    this.playButton = this.fullscreenGuiTexture.getControlByName('Play') as Button;
    this.playbackInput = this.fullscreenGuiTexture.getControlByName('Playback Speed') as InputText;
    this.sideBarButton = this.fullscreenGuiTexture.getControlByName('Open Side Bar') as Button;
    this.bottomAppBar = this.fullscreenGuiTexture.getControlByName('Bottom App Bar') as Control;
    this.timeIndicator = this.fullscreenGuiTexture.getControlByName('Time Indicator') as TextBlock;
    this.timelineSlider = this.fullscreenGuiTexture.getControlByName('Timeline') as Slider;
    this.sideBar = this.fullscreenGuiTexture.getControlByName('Side Bar') as Control;
    this.loadingIndicator = this.fullscreenGuiTexture.getControlByName('Loading Indicator') as Rectangle;
    this.scoring = this.fullscreenGuiTexture.getControlByName('Scoring') as Rectangle;
    this.scoreTextTeamA = this.fullscreenGuiTexture.getControlByName('TeamA Score') as TextBlock;
    this.scoreTextTeamB = this.fullscreenGuiTexture.getControlByName('TeamB Score') as TextBlock;
    this.scoreBarTeamA = this.fullscreenGuiTexture.getControlByName('TeamA Slider') as Slider;
    this.scoreBarTeamB = this.fullscreenGuiTexture.getControlByName('TeamB Slider') as Slider;

    this.camera = camera;
    this.tooltip = new TooltipManager(this.scene, this.fullscreenGuiTexture, this.camera);

    //Register settings manager
    this.settingsManager = settingsManager;

    //Create sidebar manager
    this.sideBarManager = new SideBar(this.fullscreenGuiTexture, this.emitter, this.settingsManager);

    //Register gui events
    this.openButton.onPointerClickObservable.add(() => { this.emitter.emit('openReport'); });
    this.playButton.onPointerClickObservable.add(this.onPlayButton.bind(this));
    this.playbackInput.onTextChangedObservable.add(this.onPlaybackSpeedInput.bind(this));
    this.sideBarButton.onPointerClickObservable.add(this.onSideBarButton.bind(this));
    this.timelineSlider.onValueChangedObservable.add(this.onTimelineSlider.bind(this));

    //spacebar pause control
    this.scene.onKeyboardObservable.add((kbInfo) => {
      switch (kbInfo.type) {
        case BABYLON.KeyboardEventTypes.KEYDOWN:
          switch (kbInfo.event.key) {
            case " ":
              this.onPlayButton();
              break;
          }
          break;
      }
    });
  }
  loadReport(allData: AllData) {
    let maxTime = 0;
    for(let mapKinematic of allData.MapKinematics) {
      maxTime = Math.max(maxTime,...mapKinematic.Log.map(k => k !== null ? k.Time : 0));
    }
    for(let shipKinematic of allData.ShipKinematics) {
      maxTime = Math.max(maxTime,...shipKinematic.Log.map(k => k !== null ? k.Time : 0));
    }
    for(let missileKinematic of allData.MissileKinematics) {
      maxTime = Math.max(maxTime,...missileKinematic.Log.map(k => k !== null ? k.Time : 0));
    }
    this.maxTime = maxTime;
    this.bottomAppBar.isVisible = true;

    this.sideBarManager.onLoad(allData);

    //Alter scoring based on scenario
    //  -Builds an array of scores at each scoring step based on the scenario and the status of each POI at each scoring step
    let scenarioIndex = baseScenarios.indexOf(allData.MapInfo.GameMode);
    if(scenarioIndex == 1 || scenarioIndex == 2) {
      this.scoring.isVisible = true;
      
      //calculate score arrays
      this.scoreStep = 10; //10 seconds is the score step for both control and tug of war. TODO: for modded scenarios we'll need this to be grabbed, but for now it's fine
      this.maxScore = allData.MapInfo.VictoryPoints;
      let initialScore = Math.abs(this.maxScore / scenarioIndex - this.maxScore); //...lol
      let scoreOffset = allData.MapInfo.VPOffset ?? 0;
      this.scoreTeamA.push(initialScore + Math.max(scoreOffset, 0));
      this.scoreTeamB.push(initialScore + Math.abs(Math.min(scoreOffset, 0)));

      let steppedTime = this.scoreStep;
      while(maxTime - steppedTime > 0) {
        let additionTeamA = 0;
        let additionTeamB = 0;
        for (let poiIndex = 0; poiIndex < allData.MapPOIs.length; poiIndex++) {
          const poiTimes = allData.MapPOIs[poiIndex].Times;
          const poiOwnership = allData.MapPOIs[poiIndex].Ownership;

          //same code as poi.ts update()
          let ownership: boolean | null = null;
          for (let index = 0; index < poiTimes.length; index++) {
            let nextIsNull = index + 1 == poiTimes.length;
            if (steppedTime > poiTimes[index] && nextIsNull ? true : steppedTime < poiTimes[index + 1]) {
              ownership = poiOwnership[index];
              break;
            }
            else if (nextIsNull) {
              ownership = poiOwnership[index];
            }
          }

          if (ownership === true) { //TeamA
            additionTeamA += 2;
            if (scenarioIndex == 2) { //tug of war also removes from the enemy
              additionTeamB -= 2;
            }
          }
          else if (ownership === false) { //TeamB
            additionTeamB += 2;
            if (scenarioIndex == 2) { //tug of war also removes from the enemy
              additionTeamA -= 2;
            }
          }
        }
        this.scoreTeamA.push(additionTeamA + (this.scoreTeamA[this.scoreTeamA.length - 1] || 0)!);
        this.scoreTeamB.push(additionTeamB + (this.scoreTeamB[this.scoreTeamB.length - 1] || 0)!);
        steppedTime += this.scoreStep;
      }
    }
    else {
      this.scoring.isVisible = false;
    }

    //alters the text in the settings tab to reflect temporary changes for maps larger than current settings accomodate
    let oldSkyboxSize = this.settingsManager.settings.skybox.size; //this occurs after Camera.adjustSkybox, so we can just grab the new camera.maxZ
    ((this.fullscreenGuiTexture.getControlByName('Skybox-Size') as Container).getChildByType('', 'InputText') as InputText).text = this.scene.cameras[0].maxZ.toString();
    this.settingsManager.set({ skybox: { size: oldSkyboxSize } }); //the above line has an onChanged event, so we need to reset the settings to the old value
    
    if(!this.settingsManager.settings.grid.sphericalCoordinates) { //TODO: Temporary bandaid
      let oldGridSize = this.settingsManager.settings.grid.size;
      ((this.fullscreenGuiTexture.getControlByName('Grid-Size') as Container).getChildByType('', 'InputText') as InputText).text = Math.max(oldGridSize, allData.MapInfo?.Radius * 2).toString();
      this.settingsManager.set({ grid: { size: oldGridSize } });
    }
  }
  update(deltaTime: number) {
    //Pause logic
    if(this.paused) {
      if(this.playButton.textBlock !== null) {
        this.playButton.textBlock.text = 'Play';
      }
    }
    else {
      if(this.playButton.textBlock !== null) {
        this.playButton.textBlock.text = 'Pause';
      }
      this.currTime += deltaTime * this.playbackSpeed;
    }

    //Looping logic
    if(this.currTime > this.maxTime) {
      this.currTime = 0;
    }
    if(this.currTime < 0) {
      this.currTime = this.maxTime;
    }

    //Update time indicator
    let currTimeMin = Math.floor(this.currTime / 60);
    let currTimeSec = this.currTime - (currTimeMin * 60);
    let maxTimeMin = Math.floor(this.maxTime / 60);
    let maxTimeSec = this.maxTime - (maxTimeMin * 60);
    this.timeIndicator.text = `${currTimeMin}:${currTimeSec < 10 ? '0' : ''}${currTimeSec.toFixed(2)} / ${maxTimeMin}:${maxTimeSec < 10 ? '0' : ''}${maxTimeSec.toFixed(2)}`;

    //Update timeline slider
    this.timelineSlider.value = this.maxTime === 0 ? 0 : this.currTime / this.maxTime;

    //Update score
    if (this.scoring.isVisible) {
      let scoreIndex = Math.floor(this.currTime / this.scoreStep);
      let currScoreTeamA = this.scoreTeamA[scoreIndex];
      let currScoreTeamB = this.scoreTeamB[scoreIndex];

      this.scoreBarTeamA.value = currScoreTeamA / this.maxScore * 100;
      this.scoreBarTeamB.value = currScoreTeamB / this.maxScore * 100;
      this.scoreTextTeamA.text = `${currScoreTeamA}`;
      this.scoreTextTeamB.text = `${currScoreTeamB}`;
    }

    //Update tooltip
    this.tooltip.update();
  }
  clear() {
    this.paused = true;
    this.currTime = 0;
    this.maxTime = 0;
    this.bottomAppBar.isVisible = false;
    this.scoring.isVisible = false;
    this.scoreTeamA = [];
    this.scoreTeamB = [];
    this.sideBarManager.clear();
  }
  onPlayButton() {
    if(this.paused) {
      this.paused = false;
    }
    else {
      this.paused = true;
    }
  }
  onTimelineSlider() {
    if(this.paused) {
      this.currTime = this.timelineSlider.value * this.maxTime;
    }
  }
  onPlaybackSpeedInput() {
    this.playbackSpeed = Number(this.playbackInput.text);
    this.playbackInput.text = this.playbackSpeed.toFixed(3);
  }
  onSideBarButton() {
    this.sideBarOpen = !this.sideBarOpen;
    if(this.sideBarOpen) {
      this.sideBar.isVisible = true;
      if(this.sideBarButton.textBlock !== null) {
        this.sideBarButton.textBlock.text = '>>';
      }
    }
    else {
      this.sideBar.isVisible = false;
      if(this.sideBarButton.textBlock !== null) {
        this.sideBarButton.textBlock.text = '<<';
      }
    }
  }
  setLoadingIndicator(isLoading: boolean) {
    this.loadingIndicator.isVisible = isLoading;
  } 
}