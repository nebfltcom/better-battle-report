import App from "./app";

const canvas = document.getElementById('renderCanvas');
const app = new App(canvas as HTMLCanvasElement);
(window as any).app = app;