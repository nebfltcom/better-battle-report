import { AbstractMesh, Color3, Color4, Curve3, LinesMesh, Mesh, MeshBuilder, MultiMaterial, Polar, Quaternion, Scene, StandardMaterial, Texture, Vector2, Vector3, VertexData } from "babylonjs";
import { AllData, DamageInstance, FleetInfo, KinematicData, ShipInfo } from "./json";
import { RenderSettings } from "./settings";
import { getId, getK, TeamColor } from "./utils";
import { Emitter } from "nanoevents";
import { AdvancedDynamicTexture, TextBlock } from "babylonjs-gui";

//const MAINCOLOR3_A = new Color3(0.09,0.39,0.52);
//const MAINCOLOR3_B = new Color3(0.88,0.29,0.00);
//const RIBBONCOLOR3 = new Color3(0.06,0.29,0.39);
//const RIBBONCOLOR4 = new Color4(0.06,0.29,0.39,1);
//const EDGECOLOR4 = new Color4(1,1,1,1);

//TODO: settings
/*const MAINCOLOR3 = new TeamColor(
  new Color3(0.09,0.39,0.52), //TeamA
  new Color3(0.80,0.40,0.00), //TeamB
  new Color3(0.50,0.50,0.50)  //Neutral
);

const RIBBONCOLOR3 = new TeamColor(
  new Color3(0.06,0.29,0.39), //TeamA
  new Color3(0.70,0.30,0.00)  //TeamB
  //Neutral not used
);

const EDGECOLOR3 = new TeamColor(
  new Color3(0.09,0.39,0.52), //TeamA
  new Color3(0.80,0.40,0.00), //TeamB
  new Color3(1.00,1.00,1.00)  //Neutral
);*/

const MAIN_MAT_INDEX = {
  "Main" : 0,
  "Eliminated" : 1,
  "Selected" : 2,
  "Damaged" : 3
};

export default class ShipRenderer {
  //Start Settings Section
  mainColor3: TeamColor;
  ribbonColor3: TeamColor;
  edgeColor3: TeamColor;
  selectedColor: Color3;
  trailTime: number;
  trailDensity: number;
  trailGradientQuality: number;
  trailStartAlpha: number;
  trailEndAlpha: number;
  sphericalCoordinates: boolean;
  //End Settings Section
  scene: Scene;
  emitter: Emitter; //emitter to allow ship to send instructions to the Ships SidePanel
  id: string;
  fleet: FleetInfo;
  tooltipLines: string[] = ["Ship", `Name: `, `-Class `, `Player: `]
  emptyMesh: boolean = false;
  shipMesh: Mesh;
  edgeWidth: number;
  shipMultiMat: MultiMaterial;
  //shipMatList: StandardMaterial[] = [];
  shipLength: number = 5;
  kHistory: KinematicData[];
  maxTime: number;
  minTime: number;
  eliminatedTime: number;
  eliminated: boolean = false;
  defangedTime: number;
  fangless: boolean = false;
  ribbonPath: Vector3[][];
  ribbonCount: number = 0;
  ribbonMesh: Mesh;
  ribbonMaterial: StandardMaterial;
  lineMesh: LinesMesh;
  pinPlane: Mesh[] = [];
  pinLineMesh: LinesMesh[] = [];
  gridPosition: Vector3 = Vector3.Zero();
  damageLog: DamageInstance[] = [];
  damageColor: Color3;
  damageTextMeshes: Mesh[] = [];
  damageTextTextures: AdvancedDynamicTexture[] = []; //have to keep track because I guess you can't get controls from it if you don't?
  //damageTextPool: Pool = new Pool();
  //damageTextCounter = 0; //This is just for naming atm
  constructor(scene: Scene, emitter:Emitter, renderSettings: RenderSettings, allData: AllData, shipInfo: ShipInfo) {
    //Initialize settings
    this.mainColor3 = new TeamColor(
      new Color3(renderSettings.ship.colorTeamA.r, renderSettings.ship.colorTeamA.g, renderSettings.ship.colorTeamA.b),
      new Color3(renderSettings.ship.colorTeamB.r, renderSettings.ship.colorTeamB.g, renderSettings.ship.colorTeamB.b),
      new Color3(renderSettings.ship.colorEliminated.r, renderSettings.ship.colorEliminated.g, renderSettings.ship.colorEliminated.b)
    );
    this.ribbonColor3 = new TeamColor(
      new Color3(renderSettings.ship.colorTeamA.r * 0.75, renderSettings.ship.colorTeamA.g * 0.75, renderSettings.ship.colorTeamA.b * 0.75),
      new Color3(renderSettings.ship.colorTeamB.r * 0.75, renderSettings.ship.colorTeamB.g * 0.75, renderSettings.ship.colorTeamB.b * 0.75)
      //Neutral unused
    );
    this.edgeColor3 = new TeamColor(
      new Color3(renderSettings.ship.colorTeamA.r, renderSettings.ship.colorTeamA.g, renderSettings.ship.colorTeamA.b),
      new Color3(renderSettings.ship.colorTeamB.r, renderSettings.ship.colorTeamB.g, renderSettings.ship.colorTeamB.b),
      new Color3(1,1,1)
    );
    this.selectedColor = new Color3(renderSettings.ship.colorSelected.r, renderSettings.ship.colorSelected.g, renderSettings.ship.colorSelected.b);
    this.damageColor = new Color3(renderSettings.ship.colorDamaged.r, renderSettings.ship.colorDamaged.g, renderSettings.ship.colorDamaged.b);
    this.trailTime = renderSettings.ship.trailTime;
    this.trailDensity = renderSettings.ship.trailDensity;
    this.trailGradientQuality = renderSettings.ship.trailGradientQuality;
    this.trailStartAlpha = renderSettings.ship.trailAlpha.start;
    this.trailEndAlpha = renderSettings.ship.trailAlpha.end;
    this.sphericalCoordinates = renderSettings.grid.sphericalCoordinates;

    this.scene = scene;
    this.id = getId(shipInfo);
    this.eliminatedTime = shipInfo.TimeEliminated;
    this.defangedTime = shipInfo.TimeDefanged;
    this.tooltipLines[1] += shipInfo.Name;
    let classification = shipInfo.HullClassification ? shipInfo.HullClassification : ""; //NOTE: compat
    this.tooltipLines[2] = `${shipInfo.Class}-Class ${classification}`;

    for (let fleetData of allData.FleetInfo) {
      if (getId(fleetData) === getId(shipInfo.Fleet)) {
        this.fleet = fleetData; //ISSUE: will not account for ship transfers to other players. but they can't transfer to a different team so whatever
        this.tooltipLines[3] += this.fleet.PlayerName ? this.fleet.PlayerName : "<DATA INCOMPLETE>"; //NOTE: compat
      }
    }

    //build damage text objects
    const damageFontSize = 75;
    //ISSUE: breaks, I THINK because of too many entries, but it seemed to break even with very few. TODO
    for (let damageLog of allData.DamageData) {
      if (getId(damageLog) === getId(shipInfo)) {
        this.damageLog = damageLog.Log;
        /*let index = 0;
        this.damageLog.forEach((e) => {
          let plane = MeshBuilder.CreatePlane(`${this.id}DamageTextMesh${index}`, {size: 200}, this.scene);
          plane.billboardMode = Mesh.BILLBOARDMODE_ALL;
          let advTex = AdvancedDynamicTexture.CreateForMesh(plane);
          var internalText = new TextBlock(`${this.id}DamageInternalText${index}`, e.Internal.toString());
          internalText.width = 0.5;
          internalText.height = 0.2;
          internalText.color = this.damageColor.toHexString();
          internalText.fontSize = damageFontSize;
          internalText.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
          advTex.addControl(internalText);
          var armorText = new TextBlock(`${this.id}DamageArmorText${index}`, e.Armor.toString());
          armorText.width = 0.5;
          armorText.height = 0.2;
          armorText.color = "white";
          armorText.fontSize = damageFontSize;
          armorText.textHorizontalAlignment = BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
          advTex.addControl(armorText);
          //plane.position = new Vector3(Math.sin(j * Math.PI / 2) * r, 0, Math.cos(j * Math.PI / 2) * r);
          plane.renderingGroupId = 1; //forces text to appear over grid (rendering group 0)
          plane.isVisible = false;

          //performance
          plane.doNotSyncBoundingInfo = true;
          plane.freezeNormals();
          plane.material?.freeze();
          plane.cullingStrategy = AbstractMesh.CULLINGSTRATEGY_OPTIMISTIC_INCLUSION_THEN_BSPHERE_ONLY;

          this.damageTextMeshes.push(plane);
          this.damageTextTextures.push(advTex);
          index++;
        });*/
        break;
      }
    }

    //Register emitter
    this.emitter = emitter;

    //Searching for ship mesh data in allData
    let shipMeshesData = allData.ShipMeshes[shipInfo.MeshIndex]; //instanced meshes
    //Generating single ship mesh from mesh collection data
    let bmeshCollection: Mesh[] = [];
    let vertexDistanceMax = Vector3.Zero();
    let useTailoredWidth = false;

    //TODO: better solution
    //for truly massive collidermeshes we'll just use a default value, otherwise calculate a line width that scales with mesh size
    let numVerts = 0;
    for (let shipMeshData of shipMeshesData.Meshes) {
      numVerts += shipMeshData.MeshVerticesData.length;
    }
    if (numVerts > 200000) {
      //200,000 is only going to catch a few hyperdetailed colliders, but is somewhat arbitrary
      //ISSUE: arbitrary, and this will need adjustment if/when the collider 
      vertexDistanceMax = new Vector3(10, 10, 10);
    } else {
      useTailoredWidth = true;
    }

    for (let shipMeshData of shipMeshesData.Meshes) {
      let newBMesh = new Mesh(`${this.id}MeshShip`);
      let vertexData = new VertexData();

      if(shipMeshData.MeshVerticesData.length == 0 || shipMeshData.MeshTriangleData.length == 0) {
        continue;
      }

      vertexData.positions = shipMeshData.MeshVerticesData.map((e) => [e.X,e.Y,e.Z,]).flat();
      vertexData.indices = shipMeshData.MeshTriangleData;

      //2.5r 90000 verts mutan et
      //3.7r 2300 verts shuttle
      //ISSUE: TailoredWidth does not operate well on meshes with many hundreds of thousands of verts
      if (useTailoredWidth) {
        //find AA max distances to make our own bounds (mesh bounds are not entirely reliable; see Halo mod Halcyon)
        for (let i = 0; i < vertexData.positions.length; i += 3) {
          const a = new Vector3(
            vertexData.positions[i],
            vertexData.positions[i + 1],
            vertexData.positions[i + 2]
          );
          for (let j = i + 3; j < vertexData.positions.length; j += 3) {
            const b = new Vector3(
              vertexData.positions[j],
              vertexData.positions[j + 1],
              vertexData.positions[j + 2]
            );
            let distance = b.subtract(a);
            let abs = new Vector3(
              Math.abs(distance.x),
              Math.abs(distance.y),
              Math.abs(distance.z)
            );
            if (abs.x > vertexDistanceMax.x) vertexDistanceMax.x = abs.x;
            if (abs.y > vertexDistanceMax.y) vertexDistanceMax.y = abs.y;
            if (abs.z > vertexDistanceMax.z) vertexDistanceMax.z = abs.z;
          }
        }
      }

      vertexData.applyToMesh(newBMesh);
      bmeshCollection.push(newBMesh);
    }
    let averageBoundLength = Math.max( (vertexDistanceMax.x + vertexDistanceMax.y + vertexDistanceMax.z) / 3, 1 );

    //Apply single ship mesh

    if(bmeshCollection.length == 0) {
      this.emptyMesh = true;
    } else {
      this.shipMesh = Mesh.MergeMeshes(bmeshCollection, true, true) as Mesh;
    }
    
    //let averageBoundLength = Math.max(this.shipMesh.getBoundingInfo().boundingSphere.radius / 5, 2); //5 is arbitrary but balanced

    if (this.shipMesh === null) {
      this.emptyMesh = true;
      //break; //?
    } //if there is no mesh, such as with Nebulous: Carrier Command's fighters (for now)
    else {
      this.shipMesh.enableEdgesRendering();
      this.shipMesh.edgesWidth = averageBoundLength;
      this.edgeWidth = averageBoundLength;
      this.shipMesh.edgesColor = this.edgeColor3.Neutral.toColor4(1); //neutral white when not eliminated
      //Get ship length
      let boundingBoxVectorArray: number[] = [];
      this.shipMesh
        .getBoundingInfo()
        .boundingBox.maximum.toArray(boundingBoxVectorArray);
      this.shipLength = Math.max(...boundingBoxVectorArray);
      //Create and apply material to ship mesh

      //MultiMaterial allows for color change when a ship is eliminated/selected/damaged
      this.shipMultiMat = new MultiMaterial(`${this.id}matMulti`, scene);
      let matMain = new StandardMaterial(`${this.id}matMain`, scene);
      matMain.ambientColor = this.fleet?.IsTeamA ? this.mainColor3.A : this.mainColor3.B;
      matMain.backFaceCulling = false;
      matMain.zOffset = this.shipMesh.edgesWidth + 0.5;
      let matElim = new StandardMaterial(`${this.id}matElim`, scene);
      matElim.ambientColor = this.mainColor3.Neutral;
      matElim.backFaceCulling = false;
      matElim.zOffset = this.shipMesh.edgesWidth + 0.5;
      let matSelect = new StandardMaterial(`${this.id}matSelect`, scene);
      matSelect.ambientColor = this.selectedColor;
      matSelect.backFaceCulling = false;
      matSelect.zOffset = this.shipMesh.edgesWidth + 0.5;
      let matDamage = new StandardMaterial(`${this.id}matDamage`, scene);
      matDamage.ambientColor = this.damageColor;
      matDamage.backFaceCulling = false;
      matDamage.zOffset = this.shipMesh.edgesWidth + 0.5;
      this.shipMultiMat.subMaterials.push(matMain);
      this.shipMultiMat.subMaterials.push(matElim);
      this.shipMultiMat.subMaterials.push(matSelect);
      this.shipMultiMat.subMaterials.push(matDamage);
      this.shipMesh.material = this.shipMultiMat;

      this.shipMesh.isPickable = true;

      //Pins
      if(this.sphericalCoordinates) {
        //register same emitter events as grid to move pin position
        this.emitter.on('selectedPosition', (pos: Vector3) => {
          this.gridPosition = pos;
        });

        this.emitter.on('selectionCleared', () => {
          this.gridPosition = Vector3.Zero();
        });

        this.emitter.on('updateGridPosition', (pos: Vector3) => {
          this.gridPosition = pos;
        });

        let pinPlaneMat = new StandardMaterial(`${this.id}PinMat`);
        pinPlaneMat.diffuseTexture = new Texture(new URL('assets/pin.png', import.meta.url).pathname);
        pinPlaneMat.opacityTexture = pinPlaneMat.diffuseTexture;
        pinPlaneMat.ambientColor = Color3.White();
        let mesh = MeshBuilder.CreatePlane(`${this.id}PinPlane`, {size: 10, sideOrientation: Mesh.DOUBLESIDE})
        mesh.material = pinPlaneMat;
        mesh.rotation = new Vector3(Math.PI / 2, 0, 0); //...?
        this.pinPlane['y'] = mesh;
        let arc = Curve3.ArcThru3Points(Vector3.Zero(), Vector3.Zero(), Vector3.Zero());
        let pinLine = MeshBuilder.CreateLines(`${this.id}PinLineMesh`, {
          points: [Vector3.Zero(), Vector3.Zero()]
        })
        pinLine.alpha = 0.5;
        this.pinLineMesh['y'] = pinLine;
      } else {
        let pinPlaneMat = new StandardMaterial(`${this.id}PinMat`);
        pinPlaneMat.diffuseTexture = new Texture(new URL('assets/pin.png', import.meta.url).pathname);
        pinPlaneMat.opacityTexture = pinPlaneMat.diffuseTexture;
        pinPlaneMat.ambientColor = Color3.White();
        let xMesh = MeshBuilder.CreatePlane(`${this.id}PinPlane_x`, {size: 10, sideOrientation: Mesh.DOUBLESIDE})
        xMesh.material = pinPlaneMat;
        xMesh.rotation = new Vector3(0, Math.PI / 2, 0); //...?
        this.pinPlane['x'] = xMesh;
        let pinLineX = MeshBuilder.CreateLines(`${this.id}PinLineMesh_x`, {
          points: [Vector3.Zero(), Vector3.Zero()],
          updatable: true
        })
        pinLineX.alpha = 0.5;
        this.pinLineMesh['x'] = pinLineX;
        let yMesh = MeshBuilder.CreatePlane(`${this.id}PinPlane_y`, {size: 10, sideOrientation: Mesh.DOUBLESIDE})
        yMesh.material = pinPlaneMat;
        yMesh.rotation = new Vector3(Math.PI / 2, 0, 0); //...?
        this.pinPlane['y'] = yMesh;
        let pinLineY = MeshBuilder.CreateLines(`${this.id}PinLineMesh_y`, {
          points: [Vector3.Zero(), Vector3.Zero()],
          updatable: true
        })
        pinLineY.alpha = 0.5;
        this.pinLineMesh['y'] = pinLineY;
        let zMesh = MeshBuilder.CreatePlane(`${this.id}PinPlane_z`, {size: 10, sideOrientation: Mesh.DOUBLESIDE})
        zMesh.material = pinPlaneMat;
        zMesh.rotation = new Vector3(0, 0, Math.PI / 2); //...?
        this.pinPlane['z'] = zMesh;
        let pinLineZ = MeshBuilder.CreateLines(`${this.id}PinLineMesh_z`, {
          points: [Vector3.Zero(), Vector3.Zero()],
          updatable: true
        })
        pinLineZ.alpha = 0.5;
        this.pinLineMesh['z'] = pinLineZ;
      }
    }
    //Searching for ship kinematics data in allData
    for (let shipKinematics of allData.ShipKinematics) {
      if (getId(shipKinematics) === this.id) {
        //Set kHistory
        this.kHistory = shipKinematics.Log;
        this.maxTime = Math.max( 0, ...this.kHistory.map((k) => (k !== null ? k.Time : 0)) );
        this.minTime = Math.min( this.maxTime, ...this.kHistory.map((k) => (k !== null ? k.Time : 0)) );
        this.ribbonCount = this.getRibbonCountFromTime(this.trailTime);
        //Generate ribbon path data
        this.ribbonPath = [];
        for ( let i = this.getRibbonCountFromTime(this.minTime); i < this.getRibbonCountFromTime(this.maxTime); i++ ) {
          let k = getK(this.getTimeFromRibbonCount(i), this.kHistory);
          if (k !== null) {
            this.ribbonPath.push(this.getRibbonPoint(k));
          }
        }
        //Create ribbon mesh
        let firstRibbonPath = this.getRibbonPath(0);
        this.ribbonMesh = MeshBuilder.CreateRibbon(`${this.id}MeshRibbon`, {
          pathArray: firstRibbonPath,
          updatable: true,
          sideOrientation: Mesh.DEFAULTSIDE,
          colors: this.getColors(firstRibbonPath.length),
        });
        this.ribbonMesh.hasVertexAlpha = true;
        //Create and apply material to ribbon mesh
        this.ribbonMaterial = new StandardMaterial(`${this.id}MaterialRibbon`);
        this.ribbonMaterial.ambientColor = Color3.White();
        this.ribbonMaterial.backFaceCulling = false;
        this.ribbonMesh.material = this.ribbonMaterial;
        //Create line mesh
        this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
          points: firstRibbonPath.map((r) => r[0]),
          updatable: true
        });
      }
    }
  }
  
  update(currTime: number, gridVisible: boolean, damageLayerInternal: boolean, damageLayerArmor: boolean) {
    //TODO: mod compatability layer needs to interface with this
    if (this.emptyMesh) { return; } //TEMP: come up with a better solution for N:CC by grabbing the actual fighter meshes and stapling them to the missile transforms. maybe grabbing missile meshes?

    let currK = getK(currTime, this.kHistory);
    if(currK === null) { return undefined; }
    
    //mesh transform
    this.shipMesh.position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, currK.Transform.Position.Z);
    this.shipMesh.rotation = new Vector3(currK.Transform.Rotation.X * Math.PI / 180, currK.Transform.Rotation.Y * Math.PI / 180, currK.Transform.Rotation.Z * Math.PI / 180);

    //ribbon mesh
    let newRibbonPath = this.getRibbonPath(currTime);
    this.ribbonMesh = MeshBuilder.CreateRibbon(`${this.id}MeshRibbon`, {
      pathArray: newRibbonPath,
      instance: this.ribbonMesh
    });
    this.lineMesh = MeshBuilder.CreateLines(`${this.id}MeshLine`, {
      points: newRibbonPath.map(r => r[0]),
      updatable: true,
      instance: this.lineMesh
    });
    //ribbon color
    this.lineMesh.color = this.fleet?.IsTeamA ? this.ribbonColor3.A : this.ribbonColor3.B;
    this.lineMesh.enableEdgesRendering();
    this.lineMesh.edgesWidth = 50;
    this.lineMesh.edgesColor = this.fleet?.IsTeamA ? this.ribbonColor3.A.toColor4(1) : this.ribbonColor3.B.toColor4(1);

    //pins
    this.pinPlane['y'].isVisible = gridVisible;
    this.pinLineMesh['y'].isVisible = gridVisible;
    if(!this.sphericalCoordinates){
      this.pinPlane['x'].isVisible = gridVisible;
      this.pinPlane['z'].isVisible = gridVisible;
      this.pinLineMesh['x'].isVisible = gridVisible;
      this.pinLineMesh['z'].isVisible = gridVisible;
    }

    if(gridVisible) {
      if(this.sphericalCoordinates) {
        if ((window.selector != null || window.selector != undefined) && window.selector.selectedObjectId == this.id) {
          this.emitter.emit('updateGridPosition', this.shipMesh.position); //catcher at grid.ts
        }

        //Need 3 points to generate an arc.
        //  -top of pin: ship position
        //  -bottom of pin: distance of ship from grid center along same angle as ship at grid height
        //  -midpoint of pin: vector line of magnitude distance through linear midpoint between top and bottom points

        //generate pins from the origin, then move them to the grid position
        let relativePosition = this.shipMesh.position.subtract(this.gridPosition)
        let shipPolar = Polar.FromVector2(new Vector2(relativePosition.x, relativePosition.z))

        //it is possible that theta is NaN. If so, just hide 
        if (!Number.isNaN(shipPolar.theta)) {
          if(shipPolar.theta == 0) { //sometimes the pin line will appear on the opposite side of the grid if theta is 0
            if(this.shipMesh.position.y > this.gridPosition.y && this.shipMesh.position.x < this.gridPosition.x) shipPolar.theta = Math.PI
            if(this.shipMesh.position.y < this.gridPosition.y && this.shipMesh.position.x < this.gridPosition.x) shipPolar.theta = -Math.PI
          }

          let distance = Vector3.Distance(Vector3.Zero(), relativePosition);
          let pinBasePolar = new Polar(distance, shipPolar.theta);

          if (!Number.isNaN(pinBasePolar.theta) || distance == 0) {
            let pinBaseVec = pinBasePolar.toVector2();

            let pinPlaneRelativePosition = new Vector3(pinBaseVec.x, 0, pinBaseVec.y);

            //https://stackoverflow.com/questions/50451331/find-midpoint-between-two-points-on-a-sphere
            let midpoint = new Vector3((relativePosition.x + pinPlaneRelativePosition.x) / 2, (relativePosition.y + pinPlaneRelativePosition.y) / 2, (relativePosition.z + pinPlaneRelativePosition.z) / 2);
            let L = Math.sqrt( Math.pow(midpoint.x, 2) + Math.pow(midpoint.y, 2) + Math.pow(midpoint.z, 2) );
            if (L != 0) { // :D
              let projectedMidpoint = new Vector3(midpoint.x / L * distance, midpoint.y / L * distance, midpoint.z / L * distance);

              let arc = Curve3.ArcThru3Points(pinPlaneRelativePosition, projectedMidpoint, relativePosition);

              if (arc.getPoints().length != 0) {
                this.pinLineMesh['y'].dispose(false, null, false); //can't update: number of points has to be able to change
                this.pinLineMesh['y'] = MeshBuilder.CreateLines(`${this.id}PinLineMesh`, {
                  points: arc.getPoints()
                });
                this.pinLineMesh['y'].alpha = 0.5;

                //move pins to final position
                this.pinLineMesh['y'].position = this.gridPosition;
                this.pinPlane['y'].position = pinPlaneRelativePosition.add(this.gridPosition);
              }
            }
          }
        }

        //override pin visibility
        if(Number.isNaN(shipPolar.theta) || Math.abs(this.gridPosition.y - this.shipMesh.position.y) < this.shipMesh.getBoundingInfo().boundingSphere.radius) {
          this.pinPlane['y'].isVisible = false;
          this.pinLineMesh['y'].isVisible = false;
        }
      } else { //cartesian grid
        this.pinPlane['x'].position = new Vector3(0, currK.Transform.Position.Y, currK.Transform.Position.Z);
        this.pinPlane['y'].position = new Vector3(currK.Transform.Position.X, 0, currK.Transform.Position.Z);
        this.pinPlane['z'].position = new Vector3(currK.Transform.Position.X, currK.Transform.Position.Y, 0);
        this.pinLineMesh['x'] = MeshBuilder.CreateLines(`${this.id}PinLineMesh_x`, {
          points: [this.pinPlane['x'].position, this.shipMesh.position.multiplyByFloats(Math.max((Math.abs(this.shipMesh.position.x) - this.shipLength) / Math.abs(this.shipMesh.position.x), 0), 1, 1)],
          updatable: true,
          instance: this.pinLineMesh['x']
        })
        this.pinLineMesh['y'] = MeshBuilder.CreateLines(`${this.id}PinLineMesh_y`, {
          points: [this.pinPlane['y'].position, this.shipMesh.position.multiplyByFloats(1, Math.max((Math.abs(this.shipMesh.position.y) - this.shipLength) / Math.abs(this.shipMesh.position.y), 0), 1)],
          updatable: true,
          instance: this.pinLineMesh['y']
        })
        this.pinLineMesh['z'] = MeshBuilder.CreateLines(`${this.id}PinLineMesh_z`, {
          points: [this.pinPlane['z'].position, this.shipMesh.position.multiplyByFloats(1, 1, Math.max((Math.abs(this.shipMesh.position.z) - this.shipLength) / Math.abs(this.shipMesh.position.z), 0))],
          updatable: true,
          instance: this.pinLineMesh['z']
        })
      }
    }

    //defanged
    if(this.defangedTime != undefined && this.defangedTime != -1) {
      if(currTime > this.defangedTime && this.fangless == false && !this.eliminated) {
        this.emitter.emit("changeShipStatus", `${this.id}`, `No Combat Ability at ${Math.floor(this.defangedTime / 60)}:${this.defangedTime % 60}`); //catcher at sidebar/ships.ts:39
        this.fangless = true;
      }
      if(currTime < this.defangedTime && this.fangless && !this.eliminated) {
        this.emitter.emit("changeShipStatus", `${this.id}`, "Combat Capable"); //catcher at sidebar/ships.ts:39
        this.fangless = false;
      }
    }

    //mesh color changing code (ribbon not supported)
    //TODO: clean this up to work better with the damage flash
    //elimination
    if(this.eliminatedTime != -1) {
      if(currTime > this.eliminatedTime && !this.eliminated) {
        this.eliminated = true;
        if (this.shipMesh.subMeshes[0].materialIndex != MAIN_MAT_INDEX["Selected"]) {
          this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Eliminated"];
        }
        this.shipMesh.edgesWidth = this.edgeWidth * 2; //Makes the color a little more noticable from far away
        this.shipMesh.edgesColor = this.fleet?.IsTeamA ? this.edgeColor3.A.toColor4(1) : this.edgeColor3.B.toColor4(1); //team color when eliminated
        //this.ribbonMesh.subMeshes[0].materialIndex = 1;
        this.emitter.emit("changeShipStatus", `${this.id}`, "Eliminated"); //catcher at sidebar/ships.ts:39
      }
      if(currTime < this.eliminatedTime && this.eliminated) {
        this.eliminated = false;
        if (this.shipMesh.subMeshes[0].materialIndex != MAIN_MAT_INDEX["Selected"]) {
          this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Main"];
        }
        this.shipMesh.edgesWidth = this.edgeWidth;
        this.shipMesh.edgesColor = this.edgeColor3.Neutral.toColor4(1); //neutral white when not eliminated
        //this.ribbonMesh.subMeshes[0].materialIndex = 0;
        let formattedNumber = (this.defangedTime % 60).toString(); //add leading zero to second counts less than 10
        if(this.defangedTime % 60 < 10) {
          formattedNumber = "0" + formattedNumber;
        }
        this.emitter.emit("changeShipStatus", `${this.id}`, this.fangless ? `No Combat Ability at ${Math.floor(this.defangedTime / 60)}:${formattedNumber}` : "Combat Capable"); //catcher at sidebar/ships.ts:39
      }
    }

    //TEMP until cleanup
    if (this.eliminated) this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Eliminated"];
    else this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Main"];

    //selection color supercedes elimination color
    if (window.selector != null && window.selector != undefined) { 
      if (window.selector.active) {
        //if (!window.selector.matchGuard) {
          if (window.selector.selectedObjectId == this.id) { //selected
            if (window.selector.selectedObjectPickable == null) { //if picked by the Ships SidePanel the pickable will be null
              window.selector.selectedObjectPickable = this.shipMesh;
              //window.selector.matched = true;
            }
            if (this.shipMesh.subMeshes[0].materialIndex != MAIN_MAT_INDEX["Selected"]) { //overwrite eliminated color
              this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Selected"];
              this.emitter.emit('selectedPosition', this.shipMesh.position); //catcher in grid.ts
            }
          } else if (this.shipMesh.subMeshes[0].materialIndex == MAIN_MAT_INDEX["Selected"]) { //not selected; normal coloring
            this.shipMesh.subMeshes[0].materialIndex = this.eliminated ? MAIN_MAT_INDEX["Eliminated"] : MAIN_MAT_INDEX["Main"];
          }
        //}
      } else if (this.shipMesh.subMeshes[0].materialIndex == MAIN_MAT_INDEX["Selected"]) { //not selected; normal coloring
        this.shipMesh.subMeshes[0].materialIndex = this.eliminated ? MAIN_MAT_INDEX["Eliminated"] : MAIN_MAT_INDEX["Main"];
      }
    }

    //Damage flash
    const damageFlashDuration = 0.3; //seconds
    const damageTextDuration = 1;
    const damageTextDistance = 10;
      //ISSUE: text system currently broken
    for (let index = 0; index < this.damageLog.length; index++) {
      const instance = this.damageLog[index];
      if (instance.Time > currTime) {
        //this.damageTextMeshes[index].isVisible = false;
        continue;
      }
      if (instance.Internal <= 0 /*&& instance.Armor <= 0*/) { //TODO: armor differentiation
        //this.damageTextMeshes[index].isVisible = false;
        continue;
      }

      //stop text
      /*if (currTime - instance.Time > damageTextDuration) {
        this.damageTextMeshes[index].isVisible = false;
        continue;
      }

      //damagetext
      if (damageLayerInternal || damageLayerArmor) {
        this.damageTextMeshes[index].isVisible = true;

        //individual text controls
        let armorText = this.damageTextTextures[index].getControlByName(`${this.id}DamageArmorText${index}`);
        if (armorText != null) armorText.isVisible = damageLayerArmor;
        let internalText = this.damageTextTextures[index].getControlByName(`${this.id}DamageInternalText${index}`);
        if (internalText != null) internalText.isVisible = damageLayerInternal;
        
        //text movement
        let startK = getK(instance.Time, this.kHistory);
        if(startK == null) {
          this.damageTextMeshes[index].isVisible = false;
        } else {
          if(this.scene.activeCamera != null) { //-.-
            //shrink numbers as they close in on the camera
            let distancefromCamera = Vector3.Distance(this.scene.activeCamera.position, this.damageTextMeshes[index].position);
            let scalar = Math.min(1, distancefromCamera / 1000);
            (this.damageTextMeshes[index]).scaling = new Vector3(scalar, scalar, scalar);
            
            //set position
            let lerp = /*damageTextDistance/ this.damageTextMeshes[index].getBoundingInfo().boundingBox.extendSize.y * (currTime - instance.Time) * scalar / damageTextDuration;
            this.damageTextMeshes[index].position = new Vector3(startK.Transform.Position.X, startK.Transform.Position.Y + lerp, startK.Transform.Position.Z);
          }
        }
        
        
        

      } else {
        this.damageTextMeshes[index].isVisible = false;
      }*/

      //stop flash
      if (currTime - instance.Time > damageFlashDuration) {
        continue;
      }

      if (currTime - instance.Time > (damageFlashDuration / 3.0) && currTime - instance.Time < ( (2.0 * damageFlashDuration) / 3.0)) { continue; }

      let damageMat = this.scene.getMaterialByName(`${this.id}matDamage`);
      if (damageMat instanceof StandardMaterial) {
        damageMat.ambientColor = this.damageColor;

        this.shipMesh.subMeshes[0].materialIndex = MAIN_MAT_INDEX["Damaged"];
      }
    }
  }
  clear() {
    this.shipMesh?.dispose(false, true); //just in case a shipmesh is null because of a broken collider or some other catastrophic failure
    this.ribbonMesh.dispose(false, true);
    this.lineMesh.dispose(false, true);
    this.damageTextMeshes.forEach((m) => m.dispose(false, true));

    this.pinLineMesh['x']?.dispose(false);
    this.pinLineMesh['y']?.dispose(false);
    this.pinLineMesh['z']?.dispose(false);
    
    this.pinPlane['x']?.dispose(false, true);
    this.pinPlane['y']?.dispose(false, true);
    this.pinPlane['z']?.dispose(false, true);
  }
  getRibbonPoint(k: KinematicData): Vector3[] {
    let positionVector = new Vector3(k.Transform.Position.X, k.Transform.Position.Y, k.Transform.Position.Z);
    let rotationQuaterion = Quaternion.FromEulerAngles(k.Transform.Rotation.X * Math.PI / 180, k.Transform.Rotation.Y * Math.PI / 180, k.Transform.Rotation.Z * Math.PI / 180);
    let rotationDirectionVector = (new Vector3(0,0,1)).applyRotationQuaternion(rotationQuaterion);
    let returnRibbonPoint: Vector3[] = [];
    for(let i = 0;i < this.trailGradientQuality;i++) {
      returnRibbonPoint.push(positionVector.add(rotationDirectionVector.clone().scale(i * this.shipLength / this.trailGradientQuality)));
    }
    return returnRibbonPoint;
  }
  getRibbonPath(time: number): Vector3[][] {
    let minTime = Math.min(Math.max(time - this.trailTime, this.minTime), this.maxTime);
    let minK = getK(minTime, this.kHistory);
    if(minK === null) { return []; }
    let realTime = Math.min(time, this.maxTime);
    let realK = getK(realTime, this.kHistory);
    if(realK === null) { return []; }
    let newRibbonPath: Vector3[][] = [];
    for(let i = 0;i < this.ribbonCount;i++) {
      let currTime = this.getTimeFromRibbonCount(i) + (time - this.trailTime);
      let currRC = this.getRibbonCountFromTime(currTime) - this.getRibbonCountFromTime(this.minTime);
      if(currTime <= minTime || typeof this.ribbonPath[currRC] === 'undefined') {
        newRibbonPath.push(this.getRibbonPoint(minK));
      }
      else if(currTime < realTime && i < this.ribbonCount - 1) {
        newRibbonPath.push(this.ribbonPath[currRC]);
      }
      else {
        newRibbonPath.push(this.getRibbonPoint(realK));
      }
    }
    return newRibbonPath;
  }
  getColors(pathLength: number): Color4[] {
    let returnColorArray: Color4[] = [];
    for(let i = 0;i < pathLength;i++) {
      for(let j = 0;j < this.trailGradientQuality;j++) {
        let color3 = this.fleet?.IsTeamA ? this.ribbonColor3.A : this.ribbonColor3.B;
        returnColorArray.push(color3.toColor4(((1 - ((j + 1) / this.trailGradientQuality)) * (this.trailStartAlpha - this.trailEndAlpha)) + this.trailEndAlpha))
      }
    }
    return returnColorArray;
  }
  getRibbonCountFromTime(time: number): number {
    return Math.max(Math.floor(time * this.trailDensity),1);
  }
  getTimeFromRibbonCount(rc: number): number {
    return rc / this.trailDensity;
  }
}