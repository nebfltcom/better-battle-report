import { AdvancedDynamicTexture, Button, StackPanel } from 'babylonjs-gui';
import { Emitter } from 'nanoevents';
import { SettingsManager } from '../settings';
const layers = [
'Grid',
'EWAR',
'Missiles',
'Missile Sprites',
'Rayfields',
'Obstacles',
'Obstacle Opacity',
'POIs',
'Shells',
'Shell Sprites', 
'Damage Numbers Internal',
'Damage Numbers Armor' ];
//TODO: add ship gradient trails?

//ISSUE: scroll bar does not appear when app is small like with settings panel?
export default class LayersPanel {
  fullscreenGuiTexture: AdvancedDynamicTexture;
  panel: StackPanel;
  emitter: Emitter;
  layerStatus: boolean[] = [true, true, true, false, true, true, true, true, true, false, false, false];
  constructor(guiTexture: AdvancedDynamicTexture, panel: StackPanel, emitter: Emitter, settingsManager: SettingsManager) {
    this.fullscreenGuiTexture = guiTexture;
    this.panel = panel;
    this.emitter = emitter;

    //initialize layers based on settings
    this.initLayer('Grid', settingsManager.settings.grid.show);

    for (let layer of layers) {
      let button = this.fullscreenGuiTexture.getControlByName(`Side Bar - Tab - Layers - ${layer}`) as Button;
      button.onPointerClickObservable.add(() => {
        this.emitter.emit('toggleLayer', layer); 
        this.toggleButton(layer);
      });
    }
  }
  toggleButton(input: string) {
    let button = this.fullscreenGuiTexture.getControlByName(`Side Bar - Tab - Layers - ${input}`) as Button;
    let index = layers.indexOf(input);
    if (this.layerStatus[index]) {
      button.color = '#FF9A00FF';
      this.layerStatus[index] = false;
    } else {
      button.color = '#0080FFFF';
      this.layerStatus[index] = true;
    }
  }
  initLayer(input: string, status: boolean) {
    if (this.layerStatus[layers.indexOf(input)] === status) { return; }
    
    this.toggleButton(input);
  }
}