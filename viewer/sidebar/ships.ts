import { AdvancedDynamicTexture, Container, Control, Rectangle, StackPanel, TextBlock } from "babylonjs-gui";
import { Emitter } from "nanoevents";
import { RenderSettings } from "../settings";
import { Color4 } from "babylonjs";
import { AllData, FleetInfo, ShipInfo } from "../json";
import { getId } from "../utils";

export default class ShipsPanel {
  //fullscreenGuiTexture: AdvancedDynamicTexture;
  emitter: Emitter;
  renderSettings: RenderSettings;
  panel: StackPanel;
  colorTeamA: Color4;
  colorTeamB: Color4;
  colorSelected: Color4;
  containerTeamA: Rectangle;
  containerTeamB: Rectangle;
  stackTeamA: StackPanel;
  stackTeamB: StackPanel;
  generatedContainers: Container[] = [];
  shipStatus: TextBlock[] = [];
  selectedShip: string = "";
  constructor(fullscreenGuiTexture: AdvancedDynamicTexture, emitter: Emitter, renderSettings: RenderSettings) {
    this.panel = fullscreenGuiTexture.getControlByName('Side Bar - Ships - Panel') as StackPanel;
    this.containerTeamA = fullscreenGuiTexture.getControlByName('Side Bar - Ships - Panel - TeamA') as Rectangle;
    this.containerTeamB = fullscreenGuiTexture.getControlByName('Side Bar - Ships - Panel - TeamB') as Rectangle;
    this.stackTeamA = fullscreenGuiTexture.getControlByName('Side Bar - Ships - Panel - TeamA - Stack') as StackPanel;
    this.stackTeamB = fullscreenGuiTexture.getControlByName('Side Bar - Ships - Panel - TeamB - Stack') as StackPanel;
    this.emitter = emitter;
    this.renderSettings = renderSettings;

    this.colorTeamA = new Color4(this.renderSettings.ship.colorTeamA.r, this.renderSettings.ship.colorTeamA.g, this.renderSettings.ship.colorTeamA.b, 1);
    this.colorTeamB = new Color4(this.renderSettings.ship.colorTeamB.r, this.renderSettings.ship.colorTeamB.g, this.renderSettings.ship.colorTeamB.b, 1);
    this.colorSelected = new Color4(this.renderSettings.ship.colorSelected.r, this.renderSettings.ship.colorSelected.g, this.renderSettings.ship.colorSelected.b, 1);

    this.containerTeamA.color = this.colorTeamA.toHexString();
    this.containerTeamB.color = this.colorTeamB.toHexString();

    this.emitter.on('changeShipStatus', (id: string, status: string) => {
      this.shipStatus[id].text = status;
    });
    this.emitter.on('selectionCleared', () => {
      if(this.selectedShip != "") {
        this.highlightContainer(fullscreenGuiTexture.getControlByName(`${this.selectedShip} Container`) as Container, false);
      }
      this.selectedShip = "";
    });
    this.emitter.on('shipSelected', (id: string) => {
      if(this.selectedShip != "") {
        this.highlightContainer(fullscreenGuiTexture.getControlByName(`${this.selectedShip} Container`) as Container, false);
      }
      this.selectedShip = id;
      this.highlightContainer(fullscreenGuiTexture.getControlByName(`${this.selectedShip} Container`) as Container, true);
    });
  }
  private createFleetHeader(info: FleetInfo) {
    let container = new Rectangle(`${getId(info.Owner)} Fleet Card`);
    container.adaptHeightToChildren = true;
    container.height = '23px';
    container.color = '#00000000';
    container.background = info.IsTeamA ? this.colorTeamA.toHexString() : this.colorTeamB.toHexString();

    let labelText = new TextBlock();
    labelText.text = `${info.PlayerName} - ${info.Name}`;
    labelText.color = 'white';
    labelText.fontSize = 20;
    labelText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    container.addControl(labelText);

    this.generatedContainers.push(container);
    info.IsTeamA ? this.stackTeamA.addControl(container) : this.stackTeamB.addControl(container);
  }
  private createShipContainer(info: ShipInfo, isTeamA: boolean) {
    let container = new Rectangle(`${getId(info)} Container`);
    container.adaptHeightToChildren = true;
    container.color = '#00000000';
    container.highlightLineWidth = 5;
    container.highlightColor = this.colorSelected.toHexString();
    container.height = '20px';

    let stack = new StackPanel(`${getId(info)} Panel`);
    stack.adaptHeightToChildren = true;
    //stack.paddingBottom = '10px';

    let textLine1 = new TextBlock();
    textLine1.text = info.Name;
    textLine1.color = 'white';
    textLine1.height = '23px';
    textLine1.fontSize = 18;
    stack.addControl(textLine1);

    let textLine2 = new TextBlock();
    textLine2.text = "Class: " + info.Class + " " + info.HullClassification;
    textLine2.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    textLine2.color = 'white';
    textLine2.height = '23px';
    textLine2.fontSize = 18;
    stack.addControl(textLine2);

    let textLine3 = new TextBlock();
    this.shipStatus[`${getId(info)}`] = textLine3;
    textLine3.text = "Combat Capable";
    textLine3.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    textLine3.color = 'white';
    textLine3.height = '23px';
    textLine3.fontSize = 18;
    stack.addControl(textLine3);

    container.addControl(stack);

    container.onPointerEnterObservable.add(() => {
      this.highlightContainer(container, true);
    });
    container.onPointerOutObservable.add(() => {
      if(this.selectedShip == getId(info)) { return; }
      this.highlightContainer(container, false);
    });
    container.onPointerClickObservable.add(() => {
      if(this.selectedShip == getId(info)) {
        this.emitter.emit('focusObject', `${getId(info)}`); //catcher at TODO
      }
      else {
        this.emitter.emit('selectObject', `${getId(info)}`); //catcher at selector.ts:57
      }
    });

    this.generatedContainers.push(container);
    isTeamA ? this.stackTeamA.addControl(container) : this.stackTeamB.addControl(container);
  }
  private highlightContainer(container: Container, value: boolean) {
    container.isHighlighted = value;
  }
  generateShipsList(allData: AllData) {
    for(let fleet of allData.FleetInfo) {
      this.createFleetHeader(fleet)
      for(let ship of allData.ShipInfo) {
        if(getId(ship.Fleet) != getId(fleet)) { continue; }

        this.createShipContainer(ship, fleet.IsTeamA);
      }
    }
  }
  clear() {
    this.selectedShip = '';
    for (let index = 0; index < this.generatedContainers.length; index++) {
      let control = this.generatedContainers[index];
      control.dispose();
    }
  }
}

