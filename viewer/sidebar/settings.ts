import { Button, Checkbox, ColorPicker, Container, Control, InputText, Rectangle, StackPanel, TextBlock } from 'babylonjs-gui';
import { Emitter } from 'nanoevents';
import { ColorData, RenderSettings, SettingsManager } from '../settings';

/* THINGS TO ADD
beam color: generic, in-game color TODO: capture beam color
ewar color: expose in-game/differentiated colors 
*/
export default class SettingsPanel {
  panel: StackPanel;
  emitter: Emitter;
  settingsManager: SettingsManager;
  constructor(panel: StackPanel, emitter: Emitter, settingsManager: SettingsManager) {
    this.panel = panel;
    this.panel.adaptHeightToChildren = true;
    this.emitter = emitter;
    this.settingsManager = settingsManager;

    //Skybox
    this.createGroupLabel('Skybox-Label', 'Skybox');
    this.createNumberInput('Skybox-Size', 'Size/Draw Distance:', () => this.settingsManager.settings.skybox.size, (v) => { this.settingsManager.set({ skybox: { size: v } }); });
    this.createColorInput('Skybox-Color', 'Color:', () => this.settingsManager.settings.skybox.color, (v) => { this.settingsManager.set({ skybox: { color: v } }); });

    //Camera
    this.createGroupLabel('Camera-Label', 'Camera');
    this.createNumberInput('Camera-OriginSize', 'Origin Size:', () => this.settingsManager.settings.camera.origin.size, (v) => { this.settingsManager.set({ camera: { origin: { size: v } } }); });
    this.createColorInput('Camera-OriginColor', 'Origin Color:', () => this.settingsManager.settings.camera.origin.color, (v) => { this.settingsManager.set({ camera: { origin: { color: v } } }); });
    this.createNumberInput('Camera-PanMaxSpeed', 'Max Pan Speed:', () => this.settingsManager.settings.camera.pan.maxSpeed, (v) => { this.settingsManager.set({ camera: { pan: { maxSpeed: v } } }); });
    this.createNumberInput('Camera-PanAcceleration', 'Pan Acceleration:', () => this.settingsManager.settings.camera.pan.acceleration, (v) => { this.settingsManager.set({ camera: { pan: { acceleration: v } } }); });
    this.createNumberInput('Camera-PanDrag', 'Pan Deceleration:', () => this.settingsManager.settings.camera.pan.deceleration, (v) => { this.settingsManager.set({ camera: { pan: { deceleration: v } } }); });
    this.createNumberInput('Camera-FoV', 'FoV', () => this.settingsManager.settings.camera.fov, (v) => { this.settingsManager.set({ camera: { fov: v } }); });

    //Grid
    this.createGroupLabel('Grid-Label', 'Grid');
    this.createBooleanInput('Grid-Visibility', "Show Grid at Start", () => this.settingsManager.settings.grid.show, (v) => { this.settingsManager.set({ grid: { show: v } }); });
    this.createBooleanInput('Spherical-Coordinates', 'Use Spherical Coordinates', () => this.settingsManager.settings.grid.sphericalCoordinates, (v) => { this.settingsManager.set({ grid: {sphericalCoordinates: v } })})
    this.createColorInput('Grid-Color', 'Color:', () => this.settingsManager.settings.grid.color, (v) => { this.settingsManager.set({ grid: { color: v } }); });
    if(!this.settingsManager.settings.grid.sphericalCoordinates) { //Rectangular Grid
      this.createBooleanInput('Grid-Resize', "Auto-Resize Grid to Map Size", () => this.settingsManager.settings.grid.resize, (v) => { this.settingsManager.set({ grid: { resize: v } }); });
      this.createNumberInput('Grid-Size', 'Size:', () => this.settingsManager.settings.grid.size, (v) => { this.settingsManager.set({ grid: { size: v } }); });
      this.createNumberInput('Grid-Interval', 'Interval:', () => this.settingsManager.settings.grid.interval, (v) => { this.settingsManager.set({ grid: { interval: v } }); });           //?
      this.createNumberInput('Grid-DashSize', 'Dash Size:', () => this.settingsManager.settings.grid.dashSize, (v) => { this.settingsManager.set({ grid: { dashSize: v } }); });          //?
      this.createNumberInput('Grid-DashGap', 'Gap Size:', () => this.settingsManager.settings.grid.gapSize, (v) => { this.settingsManager.set({ grid: { gapSize: v } }); });              //? these are confusingly named
      this.createNumberInput('Grid-DashAmount', 'Dash Amount:', () => this.settingsManager.settings.grid.dashAmount, (v) => { this.settingsManager.set({ grid: { dashAmount: v } }); });  //?
    }
    this.createNumberInput('Grid-DashWidth', 'Dash Width:', () => this.settingsManager.settings.grid.dashWidth, (v) => { this.settingsManager.set({ grid: { dashWidth: v } }); });      //?

    //Ships
    this.createGroupLabel('Ship-Label', 'Ship');
    this.createColorInput('Ship-TeamAColor', 'Team A Color:', () => this.settingsManager.settings.ship.colorTeamA, (v) => { this.settingsManager.set({ ship: { colorTeamA: v } }); }, false);
    this.createColorInput('Ship-TeamBColor', 'Team B Color:', () => this.settingsManager.settings.ship.colorTeamB, (v) => { this.settingsManager.set({ ship: { colorTeamB: v } }); }, false);
    this.createColorInput('Ship-EliminatedColor', 'Eliminated Color:', () => this.settingsManager.settings.ship.colorEliminated, (v) => { this.settingsManager.set({ ship: { colorEliminated: v } }); }, false);
    this.createColorInput('Ship-SelectedColor', 'Selected Color:', () => this.settingsManager.settings.ship.colorSelected, (v) => { this.settingsManager.set({ ship: { colorSelected: v } }); }, false);
    this.createColorInput('Ship-DamagedColor', 'Damaged Flash Color:', () => this.settingsManager.settings.ship.colorDamaged, (v) => { this.settingsManager.set({ ship: { colorDamaged: v } }); }, false);
    this.createNumberInput('Ship-TrailTime', 'Trail Time:', () => this.settingsManager.settings.ship.trailTime, (v) => { this.settingsManager.set({ ship: { trailTime: v } }); });
    this.createNumberInput('Ship-TrainDensity', 'Trail Density:', () => this.settingsManager.settings.ship.trailDensity, (v) => { this.settingsManager.set({ ship: { trailDensity: v } }); });
    this.createNumberInput('Ship-TrailGradientQuality', 'Trail Gradient Quality:', () => this.settingsManager.settings.ship.trailGradientQuality, (v) => { this.settingsManager.set({ ship: { trailGradientQuality: v } }); });
    this.createNumberInput('Ship-TrailGradientAlphaStart', 'Trail Gradient Start Alpha:', () => this.settingsManager.settings.ship.trailAlpha.start, (v) => { this.settingsManager.set({ ship: { trailAlpha: { start: v } } }); });
    this.createNumberInput('Ship-TrailGradientAlphaEnd', 'Trail Gradient End Alpha:', () => this.settingsManager.settings.ship.trailAlpha.end, (v) => { this.settingsManager.set({ ship: { trailAlpha: { end: v } } }); });

    //Missiles
    this.createGroupLabel('Missile-Label', 'Missile');
    this.createColorInput('Missile-OffensiveColor', 'Offensive Color:', () => this.settingsManager.settings.missile.colorOffensive, (v) => { this.settingsManager.set({ missile: { colorOffensive: v } }); });
    this.createColorInput('Missile-DefensiveColor', 'Defensive Color:', () => this.settingsManager.settings.missile.colorDefensive, (v) => { this.settingsManager.set({ missile: { colorDefensive: v } }); });
    this.createColorInput('Missile-DecoyColor', 'Decoy Color:', () => this.settingsManager.settings.missile.colorDecoy, (v) => { this.settingsManager.set({ missile: { colorDecoy: v } }); });
    this.createColorInput('Missile-UtilityColor', 'Utility Color:', () => this.settingsManager.settings.missile.colorUtility, (v) => { this.settingsManager.set({ missile: { colorUtility: v } }); });
    this.createColorInput('Missile-SelectedColor', 'Selected Color:', () => this.settingsManager.settings.missile.colorSelected, (v) => { this.settingsManager.set({ missile: { colorSelected: v } }); });
    this.createNumberInput('Missile-TrailTime', 'Trail Time:', () => this.settingsManager.settings.missile.trailTime, (v) => { this.settingsManager.set({ missile: { trailTime: v } }); });
    this.createNumberInput('Missile-TrainDensity', 'Trail Density:', () => this.settingsManager.settings.missile.trailDensity, (v) => { this.settingsManager.set({ missile: { trailDensity: v } }); });

    //Obstacles
    this.createGroupLabel('Obstacle-Label', 'Obstacle');
    this.createColorInput('Obstacle-Color', 'Color:', () => this.settingsManager.settings.obstacle.color, (v) => { this.settingsManager.set({ obstacle: { color: v } }); });
    this.createColorInput('Obstacle-EdgeColor', 'Edge Color:', () => this.settingsManager.settings.obstacle.edge.color, (v) => { this.settingsManager.set({ obstacle: { edge: { color: v } } }); });
    this.createNumberInput('Obstacle-EdgeWidth', 'Edge Width:', () => this.settingsManager.settings.obstacle.edge.width, (v) => { this.settingsManager.set({ obstacle: { edge: { width: v } } }); });

    //Misc
    this.createGroupLabel('EWar-Label', 'EWar');
    this.createBooleanInput('EWar-Color-Override', "Use Signature Differentiation Colors", () => this.settingsManager.settings.useSigDiff, (v) => { this.settingsManager.set({ useSigDiff: v }); });

    this.createGroupLabel('Shell-Label', 'Shell');
    this.createColorInput('Shell-SelectedColor', 'Selected Color:', () => this.settingsManager.settings.shell.colorSelected, (v) => { this.settingsManager.set({ shell: { colorSelected: v } }); }, false);

    this.createGroupLabel('POI-Label', 'POI');
    this.createNumberInput('POI-Alpha', 'Alpha:', () => this.settingsManager.settings.poi.alpha, (v) => { this.settingsManager.set({ poi: { alpha: v } }); }, 255, null, 255);

    let defaultsButton: Button = Button.CreateSimpleButton('Settings - Defaults', 'Reset to Defaults');
    defaultsButton.height = '60px';
    defaultsButton.paddingTop = '10px';
    defaultsButton.paddingLeft = '5px';
    defaultsButton.paddingRight = '5px';
    defaultsButton.color = '#FF9A00FF';
    defaultsButton.thickness = 3;
    defaultsButton.onPointerClickObservable.add(() => {
      this.settingsManager.set(new RenderSettings());
      window.location.reload();
    });
    this.panel.addControl(defaultsButton);
    let applyButton: Button = Button.CreateSimpleButton('Settings - Apply', 'Apply');
    applyButton.height = '60px';
    applyButton.paddingTop = '10px';
    applyButton.paddingLeft = '5px';
    applyButton.paddingRight = '5px';
    applyButton.color = '#0080FFFF';
    applyButton.thickness = 3;
    applyButton.onPointerClickObservable.add(() => {
      window.location.reload();
    });
    this.panel.addControl(applyButton);
  }
  createGroupLabel(controlName: string, label: string) {
    let text: TextBlock = new TextBlock();
    text.name = controlName;
    text.text = label;
    text.paddingLeft = '10px';
    text.paddingTop = '5px';
    text.paddingBottom = '5px';
    text.color = 'white';
    text.fontSize = 35;
    text.height = '45px';
    text.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    this.panel.addControl(text);
  }
  createNumberInput(controlName: string, label: string, getValue: () => number, onChange: (value: number) => void, scale = 1, lowerLimit: number | null = null, upperLimit: number | null = null) {
    let container: Container = new Container();
    container.name = controlName;
    container.height = '25px';
    container.paddingLeft = '10px';
    container.paddingRight = '10px';
    container.paddingTop = '5px';
    let labelText: TextBlock = new TextBlock();
    labelText.text = label;
    labelText.color = 'white';
    labelText.fontSize = 20;
    labelText.height = '100%';
    labelText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    container.addControl(labelText);
    let textBox: InputText = new InputText();
    textBox.text = textBox.text = Math.round(getValue() * scale).toString();
    textBox.color = '#FFFFFF';
    textBox.height = '100%';
    textBox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    textBox.onTextChangedObservable.add((e) => {
      let value: number | null = null;
      if (lowerLimit) {
        value = Math.max(Number(e.text)/scale, lowerLimit / scale)
      }
      if (upperLimit) {
        value = Math.min(value === null ? Number(e.text)/scale : value, upperLimit / scale);
      }
      if (value === null) {
        value = Number(e.text);
      }
      if(!Number.isNaN(value)) {
        onChange(value);
      }
      else {
        //textBox.text = getValue().toString();
        textBox.text = Math.round(getValue() * scale).toString();
      }
    });
    container.addControl(textBox);
    this.panel.addControl(container);
  }
  createColorInput(controlName: string, label: string, getValue: () => ColorData, onChange: (value: ColorData) => void, useAlpha = true) {
    let container: Container = new Container();
    container.name = controlName;
    container.height = '25px';
    container.paddingLeft = '10px';
    container.paddingRight = '10px';
    container.paddingTop = '5px';
    let labelText: TextBlock = new TextBlock();
    labelText.text = label;
    labelText.color = 'white';
    labelText.fontSize = 20;
    labelText.height = '100%';
    labelText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    container.addControl(labelText);
    let stack: StackPanel = new StackPanel();
    stack.ignoreLayoutWarnings = true;
    stack.isVertical = false;
    stack.height = '20px';
    stack.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    let colorRectangle: Rectangle = new Rectangle();
    colorRectangle.width = '30px';
    colorRectangle.paddingRight = '5px';
    colorRectangle.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    stack.addControl(colorRectangle);
    //TODO: replace with a color picker
    let rText: TextBlock = new TextBlock();
    rText.text = 'R';
    rText.color = 'white';
    rText.fontSize = 14;
    rText.height = '25px';
    rText.width = '15px';
    rText.paddingRight = '5px';
    rText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    rText.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    rText.isVisible = false;
    stack.addControl(rText);
    let rTextBox: InputText = new InputText();
    rTextBox.color = '#FFFFFF';
    rTextBox.height = '20px';
    rTextBox.paddingRight = '5px';
    rTextBox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    rTextBox.isVisible = false;
    stack.addControl(rTextBox);
    let gText: TextBlock = new TextBlock();
    gText.text = 'G';
    gText.color = 'white';
    gText.fontSize = 14;
    gText.height = '25px';
    gText.width = '15px';
    gText.paddingRight = '5px';
    gText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    gText.isVisible = false;
    stack.addControl(gText);
    let gTextBox: InputText = new InputText();
    gTextBox.color = '#FFFFFF';
    gTextBox.height = '20px';
    gTextBox.paddingRight = '5px';
    gTextBox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    gTextBox.isVisible = false;
    stack.addControl(gTextBox);
    let bText: TextBlock = new TextBlock();
    bText.text = 'B';
    bText.color = 'white';
    bText.fontSize = 14;
    bText.height = '25px';
    bText.width = '15px';
    bText.paddingRight = '5px';
    bText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    bText.isVisible = false;
    stack.addControl(bText);
    let bTextBox: InputText = new InputText();
    bTextBox.color = '#FFFFFF';
    bTextBox.height = '20px';
    bTextBox.paddingRight = '5px';
    bTextBox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    bTextBox.isVisible = false;
    stack.addControl(bTextBox);
    let aText: TextBlock = new TextBlock();
    aText.text = 'A';
    aText.color = 'white';
    aText.fontSize = 14;
    aText.height = '25px';
    aText.width = '15px';
    aText.paddingRight = '5px';
    aText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    aText.isVisible = false;
    stack.addControl(aText);
    let aTextBox: InputText = new InputText();
    aTextBox.color = '#FFFFFF';
    aTextBox.height = '20px';
    aTextBox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    aTextBox.isVisible = false;
    stack.addControl(aTextBox);
    //END TODO
    container.addControl(stack);
    let updateDisplay = () => {
      let colorData = getValue();
      rTextBox.text = Math.round(colorData.r * 255).toString();
      gTextBox.text = Math.round(colorData.g * 255).toString();
      bTextBox.text = Math.round(colorData.b * 255).toString();
      //aTextBox.text = colorData.a.toString();
      aTextBox.text = Math.round(colorData.a * 255).toString();
      colorRectangle.background = `#${('0' + Number(rTextBox.text).toString(16)).substr(-2)}${('0' + Number(gTextBox.text).toString(16)).substr(-2)}${('0' + Number(bTextBox.text).toString(16)).substr(-2)}`;
      colorRectangle.alpha = colorData.a;
    }
    let onInputChange = () => {
      let colorData = getValue();
      let r = Number(rTextBox.text);
      let g = Number(gTextBox.text);
      let b = Number(bTextBox.text);
      let a = Number(aTextBox.text);
      if(!Number.isNaN(r)) {
        colorData.r = Math.max(Math.min(r/255,255),0);
      }
      if(!Number.isNaN(g)) {
        colorData.g = Math.max(Math.min(g/255,255),0);
      }
      if(!Number.isNaN(b)) {
        colorData.b = Math.max(Math.min(b/255,255),0);
      }
      if(!Number.isNaN(a)) {
        colorData.a = Math.max(Math.min(a/255,255),0);
      }
      return colorData;
    }
    updateDisplay();
    rTextBox.onTextChangedObservable.add(() => { onChange(onInputChange()); updateDisplay(); });
    gTextBox.onTextChangedObservable.add(() => { onChange(onInputChange()); updateDisplay(); });
    bTextBox.onTextChangedObservable.add(() => { onChange(onInputChange()); updateDisplay(); });
    aTextBox.onTextChangedObservable.add(() => { onChange(onInputChange()); updateDisplay(); });
    //colorRectangle.onPointerClickObservable.add(() => {colorPicker.isVisible = !colorPicker.isVisible;});
    colorRectangle.onPointerClickObservable.add(() => { //Collapse inputs when not in use
      rTextBox.isVisible = !rTextBox.isVisible;
      rText.isVisible = !rText.isVisible;
      gTextBox.isVisible = !gTextBox.isVisible;
      gText.isVisible = !gText.isVisible;
      bTextBox.isVisible = !bTextBox.isVisible;
      bText.isVisible = !bText.isVisible;
      if (useAlpha) {
        aTextBox.isVisible = !aTextBox.isVisible;
        aText.isVisible = !aText.isVisible;
      }
    });
    this.panel.addControl(container);
  }
  createBooleanInput(controlName: string, label: string, getValue: () => boolean, onChange: (value: boolean) => void) {
    let container: Container = new Container();
    container.name = controlName;
    container.height = '25px';
    container.paddingLeft = '10px';
    container.paddingRight = '10px';
    container.paddingTop = '5px';
    let labelText: TextBlock = new TextBlock();
    labelText.text = label;
    labelText.color = 'white';
    labelText.fontSize = 20;
    labelText.height = '100%';
    labelText.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT;
    container.addControl(labelText);
    let checkbox: Checkbox = new Checkbox();
    checkbox.height = '100%';
    checkbox.width = '6%';
    checkbox.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_RIGHT;
    checkbox.color = "white";
    checkbox.isChecked = getValue();
    checkbox.onIsCheckedChangedObservable.add((e) => {
      let value = Boolean(e);
      onChange(value);
    });
    container.addControl(checkbox);
    
    this.panel.addControl(container);
  }
}