const fs = require('fs');
const path = require('path');

const config = require('../src/config.json');
const template = require('./template');

(async () => {
  //Create csproj file
  console.info(`[Mod Setup] Generating csproj file`);
  fs.writeFileSync(path.resolve('./src/mod.csproj'), template.generateCSProj());

  //Packaging mod
  console.info(`[Mod Setup] Packaging mod files`);
  try{ fs.mkdirSync(path.resolve(`./mod`)); } catch(err) {}
  try{ fs.mkdirSync(path.resolve(`./mod/${config.name}`)); } catch(err) {}
  try{ fs.copyFileSync(path.resolve(`./src/Preview.png`), path.resolve(`./mod/${config.name}/Preview.png`)); } catch(err) {}
  fs.writeFileSync(path.resolve(`./mod/${config.name}/ModInfo.xml`), template.generateModInfo());
})();
