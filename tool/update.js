const child_process = require('child_process');
const fs = require('fs-extra');
const path = require('path');

const config = require('../src/config.json');
const template = require('./template');

(async () => {
  //Copy game DLLs
  console.info(`[Mod Setup] Copying game DLLs`);
  try { fs.rmSync(path.resolve('./src/lib/'), { recursive: true, force: true }); } catch(err) {}
  try{ fs.mkdirSync(path.resolve(`./src/lib`)); } catch(err) {}
  try{ fs.cpSync(path.resolve('./tmp/gamedir/Nebulous_Data/Managed/Nebulous.dll'), path.resolve('./src/lib/Nebulous.dll')); } catch(err) {}
  try{ fs.cpSync(path.resolve('./tmp/gamedir/Nebulous_Data/Managed/Mirror.dll'), path.resolve('./src/lib/Mirror.dll')); } catch(err) {}
  try{ fs.cpSync(path.resolve('./tmp/gamedir/Nebulous_Data/Managed/UnityEngine.UI.dll'), path.resolve('./src/lib/UnityEngine.UI.dll')); } catch(err) {}
  try{ fs.cpSync(path.resolve('./tmp/gamedir/Nebulous_Data/Managed/Unity.TextMeshPro.dll'), path.resolve('./src/lib/Unity.TextMeshPro.dll')); } catch(err) {}

  //Create csproj file
  console.info(`[Mod Setup] Generating csproj file`);
  fs.writeFileSync(path.resolve('./src/mod.csproj'), template.generateCSProj());

  //Build mod
  try { fs.rmSync(path.resolve('./src/bin'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./src/bin')); } catch(err) {}
  console.info(`[Mod Setup] Building mod`);
  let buildProcess = child_process.spawnSync('dotnet.exe', ['build'], {
    cwd: path.resolve('./src')
  });
  let buildOutput = buildProcess.stdout.toString();
  console.info(buildOutput);
  if(!buildOutput.includes('Build succeeded')) {
    throw 'Mod build failed!';
  }

  //Copy mod files
  console.info(`[Mod Setup] Copying mod files`);
  try{ fs.mkdirSync(path.resolve(`./tmp/gamedir/Mods`)); } catch(err) {}
  try{ fs.mkdirSync(path.resolve(`./tmp/gamedir/Mods/${config.name}`)); } catch(err) {}
  try{ fs.copyFileSync(path.resolve(`./src/bin/Debug/net462/${config.name}.dll`), path.resolve(`./tmp/gamedir/Mods/${config.name}/${config.name}.dll`)); } catch(err) {}
  try{ fs.copyFileSync(path.resolve(`./src/bin/Debug/net462/${config.name}.pdb`), path.resolve(`./tmp/gamedir/Mods/${config.name}/${config.name}.pdb`)); } catch(err) {}
  dllList = fs.readdirSync(path.resolve('./src/bin/Debug/net462/'));
  for(let dll of dllList) {
    if(!dll.includes('Nebulous') && !dll.includes('Mirror') && !dll.includes(config.name)) {
      try{ fs.copyFileSync(path.resolve(`./src/bin/Debug/net462/${dll}`), path.resolve(`./tmp/gamedir/Mods/${config.name}/${dll}`)); } catch(err) {}
    }
  }
  try{ fs.copyFileSync(path.resolve(`./src/Preview.png`), path.resolve(`./tmp/gamedir/Mods/${config.name}/Preview.png`)); } catch(err) {}

  //Generate ModInfo file
  console.info(`[Mod Setup] Generating ModInfo file`);
  fs.writeFileSync(path.resolve(`./tmp/gamedir/Mods/${config.name}/ModInfo.xml`), template.generateModInfo());
})();
