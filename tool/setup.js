const fs = require('fs-extra');
const path = require('path');
const { getGamePath } = require('steam-game-path');

const GAMEID = 887570;

(async () => {
  //Clear gamedir directory
  console.info(`[Mod Setup] Clearing ./tmp/gamedir`);
  try { fs.rmSync(path.resolve('./tmp/gamedir'), { recursive: true, force: true }); } catch(err) {}
  try { fs.mkdirSync(path.resolve('./tmp/gamedir')); } catch(err) {}
  
  //Get steam game path
  console.info(`[Mod Setup] Searching for game in Steam`);
  let gamePathObject = getGamePath(GAMEID, true);
  let gamePathString = path.resolve(gamePathObject.game.path);

  //Copy steam game into gamedir
  console.info(`[Mod Setup] Copying Steam game files into ./tmp/gamedir`);
  try{ fs.copySync(gamePathString, path.resolve('./tmp/gamedir')); } catch(err) {}
  try { fs.rmSync(path.resolve('./tmp/gamedir/Mods'), { recursive: true, force: true }); } catch(err) {}
})();
