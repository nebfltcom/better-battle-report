using System;
using System.Collections.Generic;
using System.Reflection;
using Game.Sensors;
using UnityEngine;

namespace BBRMod {
  internal class MissileRecorder {
    private List<KinematicLogger> _kinematicLoggers = new List<KinematicLogger>();
    private List<MissileInfo> _infoData = new List<MissileInfo>();
    private List<Munitions.Missile> _missiles = new List<Munitions.Missile>();
    private List<bool> _stopTracking = new List<bool>();
    public void Start() {
    }
    public void Update() {
      float currTime = ModUtils.Instance.GetTime();
      for(int i = 0;i < this._missiles.Count;i++) {
        var currMissile = this._missiles[i];
        if(!currMissile.IsDead && !this._stopTracking[i]) {
          var body = (Rigidbody)typeof(Munitions.Missile).GetField("_body", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMissile);
          var newKinematicData = new KinematicData() {
            Time = currTime,
            Transform = new TransformData() {
              Position = new Vector3Data() {
                X = body.position.x,
                Y = body.position.y,
                Z = body.position.z
              },
              Rotation = new Vector3Data() {
                X = body.rotation.eulerAngles.x,
                Y = body.rotation.eulerAngles.y,
                Z = body.rotation.eulerAngles.z
              }
            }
          };
          this._kinematicLoggers[i].AddData(newKinematicData);
        }
        else if(currMissile.IsDead && !this._stopTracking[i]) {
          this._stopTracking[i] = true;
        }
      }
    }
    public void Clear() {
      this._kinematicLoggers.Clear();
      this._infoData.Clear();
      this._missiles.Clear();
      this._stopTracking.Clear();
    }
    public void AddMissile(Munitions.Missile missile) {
      try {
        var id = (uint)this._missiles.Count;
        var missileInfo = this.ExtractMissileInfo(missile, id);
        this._kinematicLoggers.Add(new KinematicLogger(id, "missile"));
        this._infoData.Add(missileInfo);
        this._missiles.Add(missile);
        this._stopTracking.Add(false);
      }
      catch(Exception err) {
        Debug.Log($"[BBRMod] Missile error occurred:");
        Debug.Log(err);
      }
    }
    private MissileInfo ExtractMissileInfo(Munitions.Missile missile, uint id) {
      var salvo = (Munitions.ActiveMissileSalvo)typeof(Munitions.Missile).GetProperty("_salvo", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(missile);
      uint shipId;
      if(salvo != null) {
        shipId = salvo.FromShip.NetID.netId;
      }
      else {
        var launchedShip = (Game.Units.ShipController)typeof(Munitions.LaunchedLookaheadMunition).GetProperty("_launchedFrom", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(missile);
        shipId = launchedShip.NetID.netId;
      }
      var sensorTrack = missile.gameObject.GetComponent<SensorTrackableObject>();
      bool isDecoy = missile.IsDecoy;
      if (sensorTrack != null) { //Decoy support module is not marked as a decoy on the missile component, but is in the SensorTrackableObject
        isDecoy = isDecoy || sensorTrack.IsDecoy;
      }
      string loadout = null;
      if (missile is Munitions.ModularMissiles.ModularMissile) { //TODO: find a way to do loadouts for non modular munitions as well? As what though?
        loadout = (string)ProtectedData.GetPrivateProperty(missile, "_contactIntel");
        var start = loadout.IndexOf("</color>");
        while (start > -1) {
          loadout = loadout.Remove(start, "</color>".Length);
          start = loadout.IndexOf("</color>");
        }
        start = loadout.IndexOf("<color=");
        while (start > -1) {
          loadout = loadout.Remove(start, "<color=#000000>".Length); //String length is known and that's all that matters
          start = loadout.IndexOf("<color=");
        }
      }
      return new MissileInfo() {
        Id = id,
        Type = "missile",
        Owner = new IdData() {
          Id = shipId,
          Type = "ship"
        },
        Size = ((Munitions.IMissile)missile).Size,
        Name = missile.name == "Submunition Decoy(Clone)" ? "Submunition Decoy" : missile.MunitionName, //Special case for decoy support module. TODO: make this less shit
        IsDecoy = isDecoy,
        FiredDefensively = ((Munitions.IMissile)missile).FiredDefensively,
        Role = missile.Role.ToString(),
        Loadout = loadout
      };
    }
    public List<KinematicLog> GetKinematicData() {
      var missileLog = new List<KinematicLog>();
      foreach(var logger in this._kinematicLoggers) {
        missileLog.Add(logger.GetData());
      }
      return missileLog;
    }
    public List<MissileInfo> GetInfoData() {
      return this._infoData;
    }
  }
}