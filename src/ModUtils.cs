using UnityEngine;
using Unity.Collections;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System;

namespace BBRMod {
  public class ModUtils {
    private static ModUtils _instance = (ModUtils)null;
    public static ModUtils Instance {
      get {
        if(ModUtils._instance == null) {
          ModUtils._instance = new ModUtils();
        }
        return ModUtils._instance;
      }
    }
    public float GetTime() {
      return Time.time - (float) Game.SkirmishGameManager.Instance.GameStartTime;
    }

    public static string GetId(IdData data) {
      return data.Type + data.Id.ToString();
    }

    public static string GenerateConfig() {
      return @"<?xml version=""1.0""?>
<BBRConfig xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">
  <DedicatedServers>
    <AutoSave>false</AutoSave> <!-- If true, BBR will automatically save a report as soon as it is ready. -->
    <Directory> <!-- The directory to save .bbr files to by default. If blank it will save to the default skirmish report directory. -->
      <!-- The default save directory for the game on a Windows machine as an example -->
      <!-- <string>C:/Program Files (x86)/Steam/steamapps/common/Nebulous/Saves/SkirmishReports</string> -->
      <string></string>
    </Directory>
  </DedicatedServers>
</BBRConfig>";
    }

    /*https://www.youtube.com/watch?v=ITogH7lJTyE
    async Task<KeyValuePair<int?, Exception>> TryCatchAsyncAwait() {
      try {
        int? data = await Task<T>;
        return new KeyValuePair<int?, Exception>(data, null);
      } catch (Exception err) {
        Debug.Log("[BBRMod] async error caught: " + err);
        return new KeyValuePair<int?, Exception>(null, err);
      }
    }*/
    
    //returns 2^power
    public static int TwoToPow(int power) {
      return 1 << power;
    }

    public static Vector3 DirectionFromEnum(Utility.Direction dir) {
      return dir switch { //...neat. that's kinda sexy
        Utility.Direction.Forward => Vector3.forward,
        Utility.Direction.Backward => Vector3.back,
        Utility.Direction.Leftward => Vector3.left,
        Utility.Direction.Rightward => Vector3.right,
        Utility.Direction.Upward => Vector3.up,
        Utility.Direction.Downward => Vector3.down,
        _ => Vector3.zero,
      };
    }

    //performs a linecast from the start to the end point in a stepped fashion that captures all colliders along the line
		//because unity just... doesn't have this?
		public static RaycastHit[] LinecastAll(Vector3 start, Vector3 end, int layerMask = Physics.DefaultRaycastLayers, float epsilon = 0.01f, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) {
			List<RaycastHit> hitList = new List<RaycastHit>();
			RaycastHit hit;

			bool flag = Physics.Linecast(start, end, out hit, layerMask, queryTriggerInteraction);

			if ( ! flag ) { return hitList.ToArray(); }

			hitList.Add(hit);

			epsilon = Mathf.Clamp(epsilon, 0.01f, Math.Abs(Vector3.Distance(start, end))); //TEST min values
			Vector3 directionVec = (end - start).normalized;

			while(flag) { //ISSUE: may result in infinite loop? I don't think it will but we gotta remember that i'm dumb
				flag = Physics.Linecast(hit.point + epsilon * directionVec, end, out hit, layerMask, queryTriggerInteraction);

				if (flag) {
					hitList.Add(hit);
				}

				if (Vector3.Distance(hit.point + epsilon * directionVec, end) < epsilon) { break; } //if remaining distance below step size we force a break
			}

			return hitList.ToArray();
		}

    //draws a line on the screen like Debug.DrawLine but not conditional to the editor
    private static GameObject _lineSource;
    private static GameObject _drawnLinesRoot;
    public static void DrawLine(Vector3 start, Vector3 end, Color startColor, float duration = 0.2f, float startWidth = 0.1f, float endWidth = 0.1f, Color endColor = default) {
      duration = Math.Max(duration, 0.2f);
      startWidth = Math.Max(startWidth, 0.01f);
      endWidth = Math.Max(endWidth, 0.01f);
      if (endColor == default) { endColor = startColor; }
      LineRenderer lr;

      if (_lineSource == null) {
        _drawnLinesRoot = new GameObject("Drawn Lines Root");
        _lineSource = new GameObject();
        _lineSource.transform.SetParent(_drawnLinesRoot.transform);
        _lineSource.transform.position = start;
        _lineSource.AddComponent(typeof(LineRenderer));
        lr = _lineSource.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = startColor;
        lr.endColor = endColor;
        lr.startWidth = startWidth;
        lr.endWidth = endWidth;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(_lineSource, duration);
        return;
      }

      GameObject myLine = GameObject.Instantiate(_lineSource, start, Quaternion.identity, _drawnLinesRoot.transform);
      lr = myLine.GetComponent<LineRenderer>();
      lr.startColor = startColor;
      lr.endColor = endColor;
      lr.startWidth = startWidth;
      lr.endWidth = endWidth;
      lr.SetPosition(0, start);
      lr.SetPosition(1, end);
      GameObject.Destroy(myLine, duration);
    }

    //draws the given bounds on the screen like DebugExtensions.DrawBounds but not conditional to the editor
    public static void DrawBounds(Bounds bounds, Color color, float duration = 0.0f, float startWidth = 0.1f, float endWidth = 0.1f, Color endColor = default) {
      Vector3 center = bounds.center;
      float x = bounds.extents.x;
      float y = bounds.extents.y;
      float z = bounds.extents.z;
      Vector3 start = center + new Vector3(x, y, z);
      Vector3 vector3_1 = center + new Vector3(x, y, -z);
      Vector3 vector3_2 = center + new Vector3(-x, y, z);
      Vector3 vector3_3 = center + new Vector3(-x, y, -z);
      Vector3 vector3_4 = center + new Vector3(x, -y, z);
      Vector3 end = center + new Vector3(x, -y, -z);
      Vector3 vector3_5 = center + new Vector3(-x, -y, z);
      Vector3 vector3_6 = center + new Vector3(-x, -y, -z);
      DrawLine(start, vector3_2, color, duration, startWidth, endWidth, endColor);
      DrawLine(start, vector3_1, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_2, vector3_3, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_1, vector3_3, color, duration, startWidth, endWidth, endColor);
      DrawLine(start, vector3_4, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_1, end, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_2, vector3_5, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_3, vector3_6, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_4, vector3_5, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_4, end, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_5, vector3_6, color, duration, startWidth, endWidth, endColor);
      DrawLine(vector3_6, end, color, duration, startWidth, endWidth, endColor);
    }

    private static GameObject _cubeSource;
    private static GameObject _drawnBoundsRoot;
    public static void DrawBoundsLite(Bounds bounds, Color color, float duration = 0.0f) {
      if (_cubeSource == null) {
        _drawnBoundsRoot = new GameObject("Drawn Bounds Root");
        _cubeSource = GameObject.CreatePrimitive(PrimitiveType.Cube);
        _cubeSource.transform.SetParent(_drawnBoundsRoot.transform);
        

        var collider = _cubeSource.GetComponent<Collider>();
        if (collider != null) {
          GameObject.Destroy(collider);
        }

        MeshRenderer mr = _cubeSource.GetComponent<MeshRenderer>();
        Material mat = new Material(Shader.Find("Sprites/Default")); //ideally i'd have some sort of wireframe shader but it's really not a priority to figure that out right now
        mat.color = color;
        mr.material = mat;

        _cubeSource.transform.position = bounds.center;
        _cubeSource.transform.localScale = bounds.size;
        GameObject.Destroy(_cubeSource, duration);
        return;
      }

      GameObject cube = GameObject.Instantiate(_cubeSource, bounds.center, Quaternion.identity, _drawnBoundsRoot.transform);
      cube.transform.localScale = bounds.size;
    }

    private static GameObject _markerRoot;
    private static RaycastHit[] _lastHits;
    public static void DebugManualLinecastAll(Vector3 start, Vector3 end, int layerMask = Physics.DefaultRaycastLayers, float epsilon = 0.01f, QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal) {
      if (_markerRoot == null) {
        _markerRoot = new GameObject("Manual Linecast Marker Root");
      }
      var duration = 100f;

      _lastHits = LinecastAll(start, end, layerMask, epsilon, queryTriggerInteraction);
      GameObject[] hitMarkers = new GameObject[_lastHits.Length];
      Material mat = new Material(Shader.Find("Sprites/Default"));
      mat.color = Color.green;
      int index = 0;
      foreach (var hit in _lastHits) {
        hitMarkers[index] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        hitMarkers[index].transform.SetParent(_markerRoot.transform);
        hitMarkers[index].transform.position = hit.point;
        hitMarkers[index].transform.localScale = Vector3.one * 5;
        MeshRenderer mr = hitMarkers[index].GetComponent<MeshRenderer>();
        mr.material = mat;
        GameObject.Destroy(hitMarkers[index], duration);
        index++;
      }
      DrawLine(start, end, Color.red, duration, 2, 1, Color.blue);
    }
  }

  public enum PrimitiveColliderType {
    None,
    Box,
    Sphere,
    Capsule
  }

  public static class ExtensionMethods {

    public static string RemoveAllOccurencesOf(this string s, string part) { //LeetCode problem 1910 solution by sidney31
      while(s.Contains(part))
        s = s.Remove(s.IndexOf(part), part.Length);
      return s;
    }

    //rotate a Vector around an axis. Lifted straight from https://stackoverflow.com/questions/69245724/rotate-a-vector-around-an-axis-in-3d-space
    //NOTE: the code using this was deprecated
    public static Vector3 RotateVectorAroundAxis(this Vector3 vec, Vector3 axis, float angle) {
        Vector3 vxp = Vector3.Cross(axis, vec);
        Vector3 vxvxp = Vector3.Cross(axis, vxp);
        return vec + Mathf.Sin(angle) * vxp + (1 - Mathf.Cos(angle)) * vxvxp;
    }

    //reciprocals each element of a vector
    public static Vector3 Reciprocal(this Vector3 vec) {
      return new Vector3(1 / vec.x, 1 / vec.y, 1 / vec.z);
    }

    //Binary search a sorted list of ints and finds either the index of first match, or the index before where the match would be
    public static int FindFirstOrHoleSorted<T>(this List<int> list, int num) {
      int left = -1;
      int right = list.Count;
      
      while (right > left + 1) {
        int middle = (left + right) / 2;
        if (list[middle] >= num)
          right = middle;
        else
          left = middle;
      }
      
      if (right < list.Count && list[right] == num) {
        return right;
      }
      
      return left;
    }

    //https://gist.github.com/marcosecchi/e8dc19f27c9ab48bc490cb444488d8e6
    /// <summary>
    /// Returns the full hierarchy name of the game object.
    /// </summary>
    /// <param name="go">The game object.</param>
    public static string GetFullName (this GameObject go) {
      string name = go.name;
      while (go.transform.parent != null) {

        go = go.transform.parent.gameObject;
        name = go.name + "/" + name;
      }
      return name;
    }

    /// <summary>
    /// Returns the hierarchy name of the game object up to (and possibly including) a given cutoff string.
    /// </summary>
    /// <param name="go">The game object.</param>
    /// <param name="cutoff">The name to stop ascending the hierarchy</param>
    /// <param name="inclusive">include the cutoff name or not</param>
    public static string GetFullName(this GameObject go, string cutoff = "", bool inclusive = true) { //TEST cutoff default parameter
      string name = go.name;
      while (go.transform.parent != null) {

        go = go.transform.parent.gameObject;
        if (go.name == cutoff && !inclusive) { break; }
        name = go.name + "/" + name;
        if (go.name == cutoff) { break; }
      }
      return name;
    }

    //copies a mesh quickly and with low overhead. Lifted from https://github.com/GeorgeAdamon/FastMeshCopy/blob/master/UnityProject/Assets/FastMeshCopy/Runtime/FastMeshCopy.cs
    //NOTE: used to circumvent a problem with recording EWAR where recording the mesh was changing the source mesh for some reason. Maybe I'm dumb, but this works so why reinvent the wheel
    private const int SHORT_SIZE  = 2;
    private const int INT_SIZE    = 4;
    private const int FLOAT_SIZE  = 4;
    //private const int FLOAT2_SIZE = 8;
    //private const int FLOAT3_SIZE = 12;
    //private const int FLOAT4_SIZE = 16;
    public static void MeshCopyTo(this Mesh inMesh, ref Mesh outMesh) {
      if (inMesh == null) return;

      if (outMesh == null) {
        outMesh = new Mesh();
      }
      else {
        outMesh.Clear();
      }

      outMesh.name   = inMesh.name;
      outMesh.bounds = inMesh.bounds;

      using (var readArray = Mesh.AcquireReadOnlyMeshData(inMesh)) {
        //-------------------------------------------------------------
        // INPUT INFO
        //-------------------------------------------------------------
        var readData = readArray[0];

        // Formats
        var vertexFormat = inMesh.GetVertexAttributes();
        var indexFormat  = inMesh.indexFormat;
        var isIndexShort = indexFormat == IndexFormat.UInt16;

        // Counts
        var vertexCount = readData.vertexCount;
        var indexCount =
          isIndexShort ? readData.GetIndexData<ushort>().Length : readData.GetIndexData<uint>().Length;

        // Element Size in bytes
        var indexSize  = isIndexShort ? SHORT_SIZE : INT_SIZE;
        var vertexSize = 0;

        for (var i = 0; i < vertexFormat.Length; i++) {
          // 4 bytes per component by default
          var size = FLOAT_SIZE;

          switch (vertexFormat[i].format) {
            case VertexAttributeFormat.Float16:
            case VertexAttributeFormat.UNorm16:
            case VertexAttributeFormat.SNorm16:
            case VertexAttributeFormat.UInt16:
            case VertexAttributeFormat.SInt16:
              size = 2;
              break;
            case VertexAttributeFormat.UNorm8:
            case VertexAttributeFormat.SNorm8:
            case VertexAttributeFormat.UInt8:
            case VertexAttributeFormat.SInt8:
              size = 1;
              break;
          }

          vertexSize += vertexFormat[i].dimension * size;
        }


        //-------------------------------------------------------------
        // OUTPUT SETUP
        //-------------------------------------------------------------
        var writeArray = Mesh.AllocateWritableMeshData(1);
        var writeData  = writeArray[0];
        writeData.SetVertexBufferParams(vertexCount, vertexFormat);
        writeData.SetIndexBufferParams(indexCount, indexFormat);

        //-------------------------------------------------------------
        // MEMORY COPYING
        //-------------------------------------------------------------
        NativeArray<byte> inData;
        NativeArray<byte> outData;

        // Vertices
        inData  = readData.GetVertexData<byte>();
        outData = writeData.GetVertexData<byte>();

      #if USE_UNSAFE
        unsafe {
          UnityUnsafeUtility.MemCpy(outData.GetUnsafePtr(), inData.GetUnsafeReadOnlyPtr(),
            vertexCount * vertexSize);
        }
      #else
        inData.CopyTo(outData);
      #endif


        // Indices
        inData  = readData.GetIndexData<byte>();
        outData = writeData.GetIndexData<byte>();

      #if USE_UNSAFE
        unsafe {
          UnityUnsafeUtility.MemCpy(outData.GetUnsafePtr(), inData.GetUnsafeReadOnlyPtr(),
            indexCount * indexSize);
        }
      #else
        inData.CopyTo(outData);
      #endif

        //-------------------------------------------------------------
        // FINALIZATION
        //-------------------------------------------------------------
        writeData.subMeshCount = inMesh.subMeshCount;

        // Set all sub-meshes
        for (var i = 0; i < inMesh.subMeshCount; i++) {
          writeData.SetSubMesh(i,
            new SubMeshDescriptor(
              (int) inMesh.GetIndexStart(i),
              (int) inMesh.GetIndexCount(i)
            )
          );
        }

        Mesh.ApplyAndDisposeWritableMeshData(writeArray, outMesh);
      }
    }
  }
}