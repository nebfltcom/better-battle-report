using UnityEngine;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.IO.Compression;
using System.Collections;
using System.Threading.Tasks;
using System.IO;
using System;

//TODO: add ui system to save reports after exiting the lobby
namespace BBRMod {
  internal class AsyncWriteManager : MonoBehaviour { //for the coroutine. maybe there's a better way of doing it that can survive scene changes with more reliability?
    private static GameObject _monoHolder = (GameObject)null; //monobehaviours can't exist on their own
    private static AsyncWriteManager _instance = (AsyncWriteManager)null;
    public static AsyncWriteManager Instance {
      get {
        if (AsyncWriteManager._monoHolder == null) {
          if (AsyncWriteManager._initialized) {
            Debug.LogError("<color=\"yellow\">[BBRMod]</color> AsyncWriteManager destroyed; this shouldn't happen but Unity is Unity sometimes.");
          }
          AsyncWriteManager._initialized = true;
          AsyncWriteManager.CompressedBytes = new byte[0];
          AsyncWriteManager.JsonBytes = new byte[0];

          if (AsyncWriteManager.reportRequested && ! AsyncWriteManager.reportDelivered) {
            AsyncWriteManager.reportInterrupted = true;
            Debug.Log("[BBRMod] AsyncWriteManager was interrupted while writing previous report. Attempting to write again.");
          }

          AsyncWriteManager._monoHolder = new GameObject("BBR AsyncWriteManager");
          //ISSUE: DontDestroyOnLoad doesn't seem to always work as required?
          DontDestroyOnLoad(AsyncWriteManager._monoHolder); //data persistence when switching scenes (like between menus)
        }

        if (AsyncWriteManager._instance == null) {
          AsyncWriteManager._instance = AsyncWriteManager._monoHolder.AddComponent<AsyncWriteManager>();
        }
        return AsyncWriteManager._instance;
      }
    }
    private static bool _initialized = false; //prevents error message on first constructor call
    private static AllData _lastData = null; //stores last data in the event that writing is disrupted and does not finish before a new recording is started
    public static bool reportDelivered = false;
    public static bool reportRequested = false;
    public static bool reportInterrupted = false;
    private Coroutine _coroutineOperation = null;
    public bool IsWorking => this._coroutineOperation != null;

    //Serialize and write
    //private Task _serializationTask = default;
    //private Task _serializationTask_DEBUG = default; //if in debug mode we'll be writing a .json file as well as a .bbr file; might as well do em both at the same time
    private Task _writeBytesTask = default; //specifically for writing the byte[] TODO: join the two systems cus that's a bit silly
    public static string reportName = "";
    public static string filePath = null; //null means write to default

    //Serialization only
    private Task<byte[]> _memoryTask = default;
    public static byte[] CompressedBytes { get; private set; }
    public static byte[] JsonBytes { get; private set; }

    //Write after serialized
    private bool _writeWhenReady = false;

    //

    private JsonSerializerOptions JsonOptions = new JsonSerializerOptions { 
        WriteIndented = false,
        Converters = {
          new CompactFloatConverter()
        },
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
      };

    public static void ResetFlags() {
      AsyncWriteManager.reportRequested = false;
      AsyncWriteManager.reportDelivered = false;
      AsyncWriteManager.reportInterrupted = false;
      AsyncWriteManager.reportName = "";
      AsyncWriteManager.filePath = null;
    }

    public static void ForceClearData() {
      AsyncWriteManager._lastData = null; 
      AsyncWriteManager.CompressedBytes = new byte[0];
      AsyncWriteManager.JsonBytes = new byte[0];
    }

    public void ForceHalt() {
      this.StopAllCoroutines();
      //this._serializationTask = default;
      //this._serializationTask_DEBUG = default;
      this._writeBytesTask = default;
      this._memoryTask = default;
      this._coroutineOperation = null;
    }

    /*public void StartSerializeWrite_DEPRECATED(string fileName, bool debug = false) {
      if (this._coroutineSerialize != null) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> A data serialization coroutine is already running!");
        return;
      }

      if (Recorder.Instance.IsRunning) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Data serialization requested while recorder is still running!");
        return;
      }

      AsyncWriteManager.reportDelivered = false;

      AsyncWriteManager.filePath ??= Game.Reports.FullAfterActionReport.SaveDir;

      this._coroutineSerialize = this.StartCoroutine(this.CoroutineSerializeAndWrite(fileName, filePath, debug));
    }

    private IEnumerator CoroutineSerializeAndWrite(string filename, string filePath, bool debug) {
      AsyncWriteManager._lastData ??= Recorder.Instance.GetData();

      using System.IO.FileStream compressedFileStream = System.IO.File.Create(System.IO.Path.Combine(filePath, filename));
      using var compressor = new GZipStream(compressedFileStream, System.IO.Compression.CompressionLevel.Optimal);
      this._serializationTask = JsonSerializer.SerializeAsync(compressor, AsyncWriteManager._lastData, JsonOptions);

      if (debug) {
          //CompactFloatConverter.boxedTicks = 0;
          //CompactFloatConverter.stringTicks = 0;
        string jsonFilename = filename.Replace("bbr", "json");
        using System.IO.FileStream jsonFileStream = System.IO.File.Create(System.IO.Path.Combine(filePath, jsonFilename));
        this._serializationTask_DEBUG = JsonSerializer.SerializeAsync(jsonFileStream, AsyncWriteManager._lastData, JsonOptions);

        while (!this._serializationTask_DEBUG.IsCompleted) { //keeps the using declarations in scope until they're done
          yield return new WaitForSeconds(2); //2 is arbitrary
        }
          //Debug.Log($"[BBRMod] boxedTicks: {CompactFloatConverter.boxedTicks}");
          //Debug.Log($"[BBRMod] stringTicks: {CompactFloatConverter.stringTicks}");
      }

      while (!this._serializationTask.IsCompleted) { //keeps the using declarations in scope until they're done
        yield return new WaitForSeconds(2);
      }

      if (this._serializationTask.Exception != null) {
        InformationBoxManager.Instance.DisplayInformation("BBR has encountered an error and was unable to save your report.");
        Debug.Log("[BBRMod] Exception thrown in serialization task.");
        Debug.Log(this._serializationTask.Exception);
      }
      else {
        InformationBoxManager.Instance.DisplayInformation("BBR has finished saving your report.");
        Debug.Log("[BBRMod] Finished saving report.");
      }


      AsyncWriteManager.reportDelivered = true;
      AsyncWriteManager._lastData = null;
      AsyncWriteManager.reportName = "";
      AsyncWriteManager.filePath = null;
      this._serializationTask = default;
      this._serializationTask_DEBUG = default;
      this._coroutineSerialize = null;
    }*/

    public void StartSerialization() {
      if (this._coroutineOperation != null) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> A data serialization coroutine is already running!");
        return;
      }

      if (Recorder.Instance.IsRunning) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Data serialization requested while recorder is still running!");
        return;
      }

      AsyncWriteManager.reportDelivered = false;

      this._coroutineOperation = this.StartCoroutine(this.CoroutineSerialize());
    }

    //ISSUE: serialization restart may not function correctly
    private IEnumerator CoroutineSerialize() {
      //System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
      //watch.Start();
      AsyncWriteManager._lastData ??= Recorder.Instance.GetData();

      this._memoryTask = CompressAsync();
      //int j = 0;
      while (!this._memoryTask.IsCompleted) {
        //if (j == 3) {throw new NotImplementedException("fuck");}
        yield return new WaitForSeconds(1);
        //j++;
        //Debug.Log(j);
      }

      if (this._memoryTask.Exception != null) {
        InformationBoxManager.Instance.DisplayInformation("BBR has encountered an error and was unable to serialize your recorded data.");
        Debug.Log("[BBRMod] Exception thrown in compressed serialization task.");
        Debug.Log(this._memoryTask.Exception);
        yield break;
      }

      //Debug.LogError(this._memoryTask.Result.Length);
      //System.IO.File.WriteAllBytes(System.IO.Path.Combine(filePath, "memory test.bbr"), this._memoryTask.Result);

      AsyncWriteManager.CompressedBytes = this._memoryTask.Result;
      AsyncWriteManager.reportDelivered = true;
      Debug.Log($"[BBRMod] Compression complete. Array is {AsyncWriteManager.CompressedBytes.Length} bytes long.");

      if (BBRMod.DEBUG) {
        this._memoryTask = JsonAsync();
        while (!this._memoryTask.IsCompleted) {
          yield return new WaitForSeconds(2);
        }

        if (this._memoryTask.Exception != null) {
          InformationBoxManager.Instance.DisplayInformation("BBR has encountered an error and was unable to serialize your recorded data.");
          Debug.Log("[BBRMod] Exception thrown in uncompressed serialization task.");
          Debug.Log(this._memoryTask.Exception);
        }
        else {
          AsyncWriteManager.JsonBytes = this._memoryTask.Result;
          Debug.Log($"[BBRMod] Uncompressed serialization complete. Array is {AsyncWriteManager.JsonBytes.Length} bytes long.");
        }
      }

      if (BBRMod.autoSave || this._writeWhenReady) {
        AsyncWriteManager.filePath ??= Game.Reports.FullAfterActionReport.SaveDir;

        string fullPath = Path.Combine(AsyncWriteManager.filePath, AsyncWriteManager.reportName);
        using FileStream compressedStream = new FileStream(fullPath, FileMode.Create);
        this._writeBytesTask = compressedStream.WriteAsync(AsyncWriteManager.CompressedBytes, 0, AsyncWriteManager.CompressedBytes.Length);

        while (!this._writeBytesTask.IsCompleted) {
          yield return new WaitForSeconds(2);
        }
        
        if (this._writeBytesTask.Exception != null) {
          InformationBoxManager.Instance.DisplayInformation("BBR has encountered an error and was unable to save your report.");
          Debug.Log("[BBRMod] Exception thrown in write bytes task.");
          Debug.Log(this._writeBytesTask.Exception);
        }
        else {
          InformationBoxManager.Instance.DisplayInformation("BBR has finished saving your report.");
          Debug.Log($"[BBRMod] Report autosaved to \"{fullPath}\"");
        }

        if (BBRMod.DEBUG) {
          string jsonFilename = AsyncWriteManager.reportName.Replace("bbr", "json");
          fullPath = Path.Combine(AsyncWriteManager.filePath, jsonFilename);
          using FileStream jsonStream = new FileStream(fullPath, FileMode.Create);
          jsonStream.Write(AsyncWriteManager.JsonBytes, 0, AsyncWriteManager.JsonBytes.Length);
        }
      }

      AsyncWriteManager._lastData = null;
      this._memoryTask = default;
      this._writeBytesTask = default;
      this._coroutineOperation = null;
      this._writeWhenReady = false;
      
      //watch.Stop();
      //Debug.LogError($"{watch.ElapsedMilliseconds} ms");
    }

    async Task<byte[]> JsonAsync() {
      using var jsonMemoryStream = new System.IO.MemoryStream();
      await JsonSerializer.SerializeAsync(jsonMemoryStream, AsyncWriteManager._lastData, JsonOptions);

      return jsonMemoryStream.ToArray();
    }

    async Task<byte[]> CompressAsync() {
      using var compressedMemoryStream = new System.IO.MemoryStream();
      using (var GZipStream = new GZipStream(compressedMemoryStream, System.IO.Compression.CompressionLevel.Optimal, false)) {
        await JsonSerializer.SerializeAsync(GZipStream, AsyncWriteManager._lastData, JsonOptions);
      }

      return compressedMemoryStream.ToArray();
    }

    public void WriteFromBytes(string fileName) { //TODO: support debug printing of .json when system is added
      if (this._coroutineOperation != null) {
        Debug.Log("[BBRMod] Serialization is not complete, BBR will save when ready.");
        InformationBoxManager.Instance.DisplayInformation("Serialization is not complete, BBR will save when ready.", 5);
        AsyncWriteManager.reportName = fileName;
        this._writeWhenReady = true;
        return;
      }

      if (!AsyncWriteManager.reportDelivered) {
        Debug.Log("[BBRMod] Serialization was not started. BBR will begin, and save when ready.");
        InformationBoxManager.Instance.DisplayInformation("Serialization was not started. BBR will begin, and save when ready.", 5);
        this.StartSerialization();
        AsyncWriteManager.reportName = fileName;
        this._writeWhenReady = true;
        return;
      }

      AsyncWriteManager.filePath ??= Game.Reports.FullAfterActionReport.SaveDir;

      string fullPath = Path.Combine(AsyncWriteManager.filePath, fileName);
      using FileStream compressedStream = new FileStream(fullPath, FileMode.Create);
      compressedStream.Write(AsyncWriteManager.CompressedBytes, 0, AsyncWriteManager.CompressedBytes.Length);

      Debug.Log($"[BBRMod] Report saved to \"{fullPath}\"");

      if (BBRMod.DEBUG) {
        string fullPathJson = fullPath.Replace("bbr", "json");
        using FileStream jsonStream = new FileStream(fullPathJson, FileMode.Create);
        jsonStream.Write(AsyncWriteManager.JsonBytes, 0, AsyncWriteManager.JsonBytes.Length);
      }
      
      InformationBoxManager.Instance.DisplayInformation("BBR has finished saving your report.");
      this._writeWhenReady = false;
    }
  }
}