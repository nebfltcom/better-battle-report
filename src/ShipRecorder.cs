using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Game.Units;
using TMPro;
using UnityEngine;

namespace BBRMod {
  internal class ShipRecorder {
    private List<KinematicLogger> _kinematicLoggers = new List<KinematicLogger>();
    private List<ShipInfo> _infoData = new List<ShipInfo>();
    private List<Game.Units.ShipController> _shipControllers = new List<Game.Units.ShipController>();
    private List<float> _totalDamageRcvd = new List<float>();
    private List<float> _totalDamageRepaired = new List<float>();
    private List<DamageLog> _damageInstances = new List<DamageLog>();
    public void Start() {
      this._shipControllers = (List<Game.Units.ShipController>)typeof(Game.SkirmishGameManager).GetField("_allShips", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(Game.SkirmishGameManager.Instance);
      foreach(var shipController in this._shipControllers) {
        this._kinematicLoggers.Add(new KinematicLogger(shipController.NetID.netId, "ship", true));
        this._infoData.Add(this.ExtractShipInfo(shipController));
        this._totalDamageRcvd.Add(shipController.Network_totalDamageReceived);
        this._totalDamageRepaired.Add(shipController.Network_totalDamageRepaired);
        this._damageInstances.Add(new DamageLog() {
          Log = new List<DamageInstance>(),
          Id = shipController.NetID.netId,
          Type = "ship"
        });
      }
    }
    public void Update() {
      float currTime = ModUtils.Instance.GetTime();
      for(int i = 0;i < this._shipControllers.Count;i++) {
        var log = this._kinematicLoggers[i].GetData().Log;
        if (log.Count > 0 && currTime - log.Last().Time < 0.1f) { //reduce update frequency to 0.1s instead of 0.02s
          return; //TODO: unify this system instead of having it be different in each file
        }

        var currShipController = this._shipControllers[i];
        var newKinematicData = new KinematicData() {
          Time = currTime,
          Transform = new TransformData() {
            Position = new Vector3Data() {
              X = currShipController.transform.position.x,
              Y = currShipController.transform.position.y,
              Z = currShipController.transform.position.z
            },
            Rotation = new Vector3Data() {
              X = currShipController.transform.eulerAngles.x,
              Y = currShipController.transform.eulerAngles.y,
              Z = currShipController.transform.eulerAngles.z
            }
          }
        };
        this._kinematicLoggers[i].AddData(newKinematicData);

        //Damage instances
        //ISSUE: this doesn't capture flanking damage
        /*if (currShipController.Network_totalDamageReceived - this._totalDamageRcvd[i] > 0.05) {
          this._damageInstances[i].Log.Add(new DamageInstance() {
            Time = currTime,
            Amount = currShipController.Network_totalDamageReceived - this._totalDamageRcvd[i]
          });
          Debug.LogError("Damage");
          Debug.LogError(currShipController.Network_totalDamageReceived - this._totalDamageRcvd[i]);
          this._totalDamageRcvd[i] += currShipController.Network_totalDamageReceived;
        }*/

        //Repair instances
        /*if (currShipController.Network_totalDamageRepaired - this._totalDamageRepaired[i] > 0.05) {
          this._damageInstances[i].Log.Add(new DamageInstance() {
            Time = currTime,
            Amount = -1 * (currShipController.Network_totalDamageRepaired - this._totalDamageRepaired[i])
          });
          Debug.LogError("Repair");
          Debug.LogError(-1 * (currShipController.Network_totalDamageRepaired - this._totalDamageRepaired[i]));
          this._totalDamageRepaired[i] += currShipController.Network_totalDamageRepaired;
        }*/

        if (currShipController.IsEliminated && this._infoData[i].TimeEliminated < 0) {
          this._infoData[i].TimeEliminated = currTime;
        }

        if (this._infoData[i].TimeDefanged > -1) { continue; }
        if (((Ships.ShipStatusSummary)ProtectedData.GetPrivateField(currShipController, "_shipStatus")).Fangless) {
          this._infoData[i].TimeDefanged = (int)ProtectedData.GetPrivateField(currShipController, "_defangedTime");
        }
      }
    }
    public void Clear() {
      this._kinematicLoggers.Clear();
      this._infoData.Clear();
      this._shipControllers.Clear();
      this._damageInstances.Clear();
      this._totalDamageRcvd.Clear();
      this._totalDamageRepaired.Clear();
    }
    private ShipInfo ExtractShipInfo(Game.Units.ShipController shipController) {
      return new ShipInfo() {
        Id = shipController.NetID.netId,
        Type = "ship",
        Owner = new IdData() {
          Id = shipController.OwnedBy.NetID.netId,
          Type = "player"
        },
        Name = shipController.Ship.ShipName,
        Class = shipController.Ship.Hull.ClassName,
        HullClassification = shipController.Ship.Hull.HullClassification,
        Fleet = new IdData() {
          Id = shipController.OwnedBy.NetID.netId,
          Type = "fleet"
        },
        TimeEliminated = -1,
        TimeDefanged = -1
      };
    }
    public List<KinematicLog> GetKinematicData() {
      var shipLog = new List<KinematicLog>();
      foreach(var logger in this._kinematicLoggers) {
        shipLog.Add(logger.GetData());
      }
      return shipLog;
    }
    public List<ShipInfo> GetInfoData() {
      return this._infoData;
    }
    public List<DamageLog> GetDamageData() {
      return this._damageInstances;
    }

    public void SetMeshInstance(int shipControllerIndex, int meshIndex) {
      this._infoData[shipControllerIndex].MeshIndex = meshIndex;
    }

    public void AddDamageInstance(ShipController controller, Ships.DamageFrame frame, float armorDamage) {
      for (int i = 0; i < this._damageInstances.Count; i++) {
        if (this._damageInstances[i].Id != controller.NetID.netId) { continue; }

        var internalDamage = 0f;
        for (int j = 0; j < frame?.DamagedParts.Count; j++) {
          internalDamage += frame.DamagedParts[j].DamageDone;
        }

        //Add all damage at a single time into one record
        var currTime = ModUtils.Instance.GetTime();
        if (this._damageInstances[i].Log.Count > 0 && this._damageInstances[i].Log.Last().Time == currTime) {
          this._damageInstances[i].Log[this._damageInstances[i].Log.Count - 1].Internal += internalDamage;
          this._damageInstances[i].Log[this._damageInstances[i].Log.Count - 1].Armor += armorDamage;
        } else {
          this._damageInstances[i].Log.Add(new DamageInstance() {
            Time = currTime,
            Internal = internalDamage,
            Armor = armorDamage
          });
        }

        break;
      }
    }
  }
}