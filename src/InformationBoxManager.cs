using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.UI;

//TODO: make it a window that's closeable and draggable
//  -this could really be a whole fleshed out system
//a simple text box to communicate information to the user without relying on Debug.LogError and cluttering up player.log
namespace BBRMod {
  internal class InformationBoxManager : MonoBehaviour { //for the coroutine. maybe there's a better way of doing it that can survive scene changes?
    private static GameObject _informationBox = (GameObject)null; //monobehaviours can't exist on their own
    private static InformationBoxManager _instance = (InformationBoxManager)null;
    public static InformationBoxManager Instance {
      get {
        if (InformationBoxManager._informationBox == null) {

          InformationBoxManager._informationBox = BuildInfoBox();
          //ISSUE: DontDestroyOnLoad can't be ised on a non-root object, and this can't be root because canvas
          //  -maybe unparent it while not displaying, so it can be not destroyed, then parent only when it needs to show something? bleh
          //DontDestroyOnLoad(InformationBoxManager._informationBox); //data persistence when switching scenes (like between menus)
        }

        if (InformationBoxManager._instance == null) {
          InformationBoxManager._instance = InformationBoxManager._informationBox.AddComponent<InformationBoxManager>();
        }
        return InformationBoxManager._instance;
      }
    }
    private Coroutine _coroutineDisplay;
    private Color _textColor = Color.white;
    private Color _backgroundColor = new Color(0.1f, 0.1f, 0.1f, 0.95f); //dark gray //ISSUE: I don't think alpha works properly? low prio
    public void SetTextColor(Color textColor) {
      this._textColor = textColor;
    }
    public void SetBackgroundColor(Color backgroundColor) {
      this._backgroundColor = backgroundColor;
    }

    //some information is lost on inactivity
    private void OnEnable() {
      TextMeshProUGUI textComponent = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
      textComponent.color = _textColor;

      CanvasRenderer canvasRenderer = this.gameObject.GetComponent<CanvasRenderer>();
      canvasRenderer.SetColor(_backgroundColor);

      /*CanvasRenderer[] closeBgRenderer = gameObject.transform.GetComponentsInChildren<CanvasRenderer>();
      for (int i = 0; i < closeBgRenderer.Length; i++) //ew
      {
        
        closeBgRenderer[i].SetColor(new Color(1,0,0,1));
      }*/
      CanvasRenderer closeBgRenderer = this.gameObject.transform.GetChild(1).GetChild(0).GetComponent<CanvasRenderer>();
      closeBgRenderer.SetColor(new Color(0,0,0,0));
      TextMeshProUGUI closeTextRenderer = this.gameObject.transform.GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>();
      closeTextRenderer.color = Color.red;
      
    }

    public void DisplayInformation(string message, int duration = -1) {
      if (Game.GameManager.Instance?.IsDedicatedServer ?? false) { return; } //no need to display stuff to a server
      if (this.gameObject.activeSelf == false) {
        this.gameObject.SetActive(true);
      }
      if (this._coroutineDisplay != null) {
        this.StopCoroutine(this._coroutineDisplay);
      }
      this._coroutineDisplay = this.StartCoroutine(this.CoroutineDisplay(message, duration));
    }

    public void HideInformation() {
      if (this.gameObject.activeSelf == false) { return; }
      if (this._coroutineDisplay != null) {
        this.StopCoroutine(this._coroutineDisplay); //stops previous box if there is one
        this._coroutineDisplay = null;
      }
      if (this.gameObject.activeSelf == true) {
        this.gameObject.SetActive(false);
      }
    }

    private IEnumerator CoroutineDisplay(string message, int duration) {
      var infoTextGo = this.gameObject.transform.GetChild(0);
      var informationText = infoTextGo.GetComponent<TextMeshProUGUI>();
      informationText.text = message;

      if (duration <= 0) { yield break; } //infinite duration permissible now that there's a close button

      yield return new WaitForSeconds(duration);
      this.gameObject.SetActive(false);
      this._coroutineDisplay = null;
    }

    private static GameObject BuildInfoBox() {
      //Debug.Log("[BBRMod] Building info box...");
      //Box background
      GameObject infoBox = new GameObject("BBR Information Box", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image) );
      Transform canvas = GameObject.Find("/Canvas").transform;
      infoBox.transform.SetParent(canvas);

      //Body text NOTE: place first so the close button is put on the top layer
      GameObject informationTextGo = new GameObject("Text (TMP)", typeof(RectTransform), typeof(CanvasRenderer), typeof(TextMeshProUGUI) );
      informationTextGo.transform.SetParent(infoBox.transform);

      //Close button
      GameObject closeButtonGo = new GameObject("Close Button", typeof(RectTransform), typeof(CanvasRenderer), typeof(Button) );
      closeButtonGo.transform.SetParent(infoBox.transform);
      Button buttonComponent = closeButtonGo.GetComponent<Button>();
      var closeEvent = new Button.ButtonClickedEvent();
      closeEvent.AddListener(() => {Instance.HideInformation();});
      buttonComponent.onClick = closeEvent;

      //Close button background
      GameObject closeBgGo = new GameObject("Image", typeof(RectTransform), typeof(CanvasRenderer), typeof(Image) );
      closeBgGo.transform.SetParent(closeButtonGo.transform);

      //Close button text
      GameObject closeTextGo = new GameObject("Text (TMP)", typeof(RectTransform), typeof(CanvasRenderer), typeof(TextMeshProUGUI) );
      closeTextGo.transform.SetParent(closeButtonGo.transform);
      var closeTextComponent = closeTextGo.GetComponent<TextMeshProUGUI>();
      closeTextComponent.text = "Close";
      closeTextComponent.alignment = TextAlignmentOptions.Center;

      //Box dimensions for placing children
      RectTransform infoBoxRT = infoBox.GetComponent<RectTransform>();
      var parentSize = infoBoxRT.GetParentSize();
      var boxWidth = parentSize.y / 3f;
      var boxHeight = boxWidth * 0.75f; //just used to shorten the box a little. system not complete and not a little hard coded
      infoBoxRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, Mathf.Max(parentSize.y / 10f - boxHeight / 4f, parentSize.y / 10f), boxHeight); //near bottom (ideally the least irritating place for it's most common use case: in debrief)
      infoBoxRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, parentSize.x / 2f - boxWidth / 2f, boxWidth); //center

      //Place close button
      RectTransform closebuttonRT = closeButtonGo.GetComponent<RectTransform>();
      closebuttonRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 40);
      closebuttonRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, boxWidth);
      RectTransform closeBgRT = closeBgGo.GetComponent<RectTransform>();
      closeBgRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 40);
      closeBgRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, boxWidth);

      //Place info text
      var textBounds = boxWidth * 0.9f;
      RectTransform infoTextRT = informationTextGo.GetComponent<RectTransform>();
      infoTextRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, Mathf.Max(boxWidth / 10f - textBounds / 4f, boxWidth / 10f), textBounds); //near top
      infoTextRT.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, Mathf.Max(boxWidth / 10f - textBounds / 4f, boxWidth / 10f), textBounds); //near left

      //Select text font
      TextMeshProUGUI infoTextComponent = infoBox.GetComponentInChildren<TextMeshProUGUI>();
      var loadedFonts = Resources.FindObjectsOfTypeAll(typeof(TMP_FontAsset));
      foreach (var font in loadedFonts) {
        if (font.name == "Freeroad Light SDF") {
          infoTextComponent.font = (TMP_FontAsset)font;
        }
        if (font.name == "BOMBARD_ SDF") {
          closeTextComponent.font = (TMP_FontAsset)font;
        }
      }

      infoBox.SetActive(false);
      return infoBox;
    }
  }
}