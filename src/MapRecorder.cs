using System.Collections.Generic;
using System.Linq;
using Game;
using Game.Map;
using UnityEngine;

//TODO: tweak logger implementation so force-recording is overridable for map obstacles. 
namespace BBRMod {
  internal class MapRecorder {
    private List<KinematicLogger> _kinematicLoggers = new List<KinematicLogger>();
    private List<Collider> _mapObstacles = new List<Collider>();
    private string _mapName = "";
    private string _mapKey = "";
    private float _mapRadius = 0f;
    private string _gameMode = "";
    private int _victoryPoints = 0;
    private int _vpOffset = 0;
    private string _victor = "";
    public void Victor(string victor) => this._victor = victor;
    private List<CapturePoint> _mapPOIs = new List<CapturePoint>();
    private List<PoiData> _poiData = new List<PoiData>();
    private readonly string [] _baseScenarios = new string[6] {
      "Deathmatch",
      "Control",
      "Tug Of War",
      "Two Flags",
      "Station Capture",
      "Center Flag"
    };
    public void Start() {
      var gameManager = Game.SkirmishGameManager.Instance;
      var map = gameManager.LoadedMap;
      this._mapName = ((ISkirmishBattlespaceInfo)ProtectedData.GetPrivateField(gameManager, "_loadedMapInfo")).MapName; //map.MapName;
      this._mapKey = map.MapKey;
      this._mapRadius = map.Radius;
      this._gameMode = ((Missions.ScenarioGraph)ProtectedData.GetPrivateField(gameManager, "_clientScenario")).ScenarioName;
      this._victoryPoints = gameManager.Score.VictoryPoints;
      this._vpOffset  = gameManager.Score.Network_teamAPoints - gameManager.Score.Network_teamBPoints;
      //Standard game modes can have their score calculated based on the rules, or are clear enough that they do not need dedicated score tracking
      if (!this._baseScenarios.Contains(this._gameMode)) {
        Debug.Log($"[BBRMod] The scenario '{this._gameMode}' is unknown to BBR. Scoring information for this scenario has not yet been implemented.");
        //TODO: handle modded scenario
      }

      var activePOIs = gameManager.GetActiveCapturePoints();

      for (int i = 0; i < activePOIs.Count; i++) {
        var point = activePOIs[i];
        this._mapPOIs.Add(point);
        this._poiData.Add(new PoiData() {
          Id = (uint)point.PointID,
          Type = "poi",
          Name = point.PointName,
          Position = new Vector3Data(point.Position),
          Radius = point.Radius,
          Times = new List<float>() {0f},
          Ownership = new List<bool?>() { point.OwnedBy == Utility.TeamIdentifier.None ? null : point.OwnedBy == Utility.TeamIdentifier.TeamA }
        });
      }

      if (Recorder.Instance.IsPrecached && ! Recorder.Instance.MapMeshGrabber.IsUsed) { return; } //If we're precaching and MapMeshGrabber has already determined that it's not being used, we don't need MapRecorder to determine the same thing.

      var obstacles = map.Obstacles;

      var validColliderIndices = Recorder.Instance.MapMeshGrabber.ValidColliderIndices;
      for (uint i = 0; i < validColliderIndices.Length; i++) {
        this._mapObstacles.Add(obstacles[validColliderIndices[i]]);
        this._kinematicLoggers.Add(new KinematicLogger(i, "obstacle", true));
      }
    }
    public void Update() {
      float currTime = ModUtils.Instance.GetTime();
      for(int i = 0;i < this._mapObstacles.Count;i++) {
        var currObstacle = this._mapObstacles[i];
        var newKinematicData = new KinematicData() {
          Time = currTime,
          Transform = new TransformData() {
            Position = new Vector3Data() {
              X = currObstacle.transform.position.x,
              Y = currObstacle.transform.position.y,
              Z = currObstacle.transform.position.z
            },
            Rotation = new Vector3Data() {
              X = currObstacle.transform.eulerAngles.x,
              Y = currObstacle.transform.eulerAngles.y,
              Z = currObstacle.transform.eulerAngles.z
            }
          }
        };
        this._kinematicLoggers[i].AddData(newKinematicData);
      }
      for (int i = 0; i < this._mapPOIs.Count; i++) {
        var currPOI = this._mapPOIs[i];
        var currData = this._poiData[i];
        bool? currOwnership = currPOI.OwnedBy == Utility.TeamIdentifier.None ? null : currPOI.OwnedBy == Utility.TeamIdentifier.TeamA;
        if (currData.Ownership.Last() != currOwnership) {
          currData.Ownership.Add(currPOI.OwnedBy == Utility.TeamIdentifier.None ? null : currPOI.OwnedBy == Utility.TeamIdentifier.TeamA);
          currData.Times.Add(currTime);
        }
      }
    }
    public void Clear() {
      this._kinematicLoggers.Clear();
      this._mapObstacles.Clear();
      this._mapName = "";
      this._mapKey = "";
      this._mapRadius = 0f;
      this._poiData.Clear();
      this._mapPOIs.Clear();
    }
    public List<KinematicLog> GetData() {
      var obstacleLog = new List<KinematicLog>();

      foreach(var logger in this._kinematicLoggers) {
        obstacleLog.Add(logger.GetData());
      }
      return obstacleLog;
    }

    public MapInfo GetMapInfo() {
      return new MapInfo() {
        Name = this._mapName,
        Key = this._mapKey,
        Radius = this._mapRadius,
        GameMode = this._gameMode,
        VictoryPoints = this._victoryPoints,
        VPOffset = this._vpOffset,
        Victor = this._victor
      };
    }
    public List<PoiData> GetPoiData() {
      return this._poiData;
    }
  }
}