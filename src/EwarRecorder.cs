using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Game.EWar;
using UnityEngine;

//TODO: refactor so an error anywhere in the process leads to the ewar instance not being logged, so the rest of the system doesn't break
//...does that just mean a try catch in main?

namespace BBRMod {
  internal class EWarRecorder {
    //export
    private List<KinematicLogger> _kinematicLoggers = new List<KinematicLogger>();
    private List<EWarInfo> _infoData = new List<EWarInfo>();
    private List<MeshCollectionData> _meshes = new List<MeshCollectionData>(); //NOTE: instanced; _meshes.Count cannot be used to set/get id's

    //internal data sources
    private List<Game.EWar.ActiveEWarEffect> _ewarInstances = new List<Game.EWar.ActiveEWarEffect>(); //init data in Update()
    private List<MeshFilter> _areaDisplays = new List<MeshFilter>(); //differentiates sensors
    private Dictionary<Game.EWar.ActiveEWarEffect, Mesh> _constructedMeshes = new Dictionary<Game.EWar.ActiveEWarEffect, Mesh>(); //contents flushed upon completion of ewar registration
    private Dictionary<Type, Game.EWar.EWarWeaponType> _ewarWeaponTypeLookupDict = new Dictionary<Type, Game.EWar.EWarWeaponType> {
      {typeof(Game.EWar.ActiveJammingEffect), Game.EWar.EWarWeaponType.Jammer},
      {typeof(Game.EWar.ActiveCommsJammer), Game.EWar.EWarWeaponType.Jammer},
      {typeof(Game.EWar.ActiveSensorJammer), Game.EWar.EWarWeaponType.Jammer},
      {typeof(Game.EWar.SensorIlluminator), Game.EWar.EWarWeaponType.Illuminator}
    };

    //utility
    private List<bool> _active = new List<bool>();
    private List<EWarMeshParameters> _parameters = new List<EWarMeshParameters>(); //right now just used for mesh instancing, but can easily be tooled to provide other parameters for more in-depth EWAR analysis in the viewer

    public void Start() {
    }
    public void Update() {
      float currTime = ModUtils.Instance.GetTime();
      for(int i = 0;i < this._active.Count;i++) {
        var areaDisplay = this._areaDisplays[i];
        var ewar = this._ewarInstances[i];
        var ewarActiveSelf = (ewar == null) || ewar.gameObject.activeSelf; //this feels weird
        
        if(this._active[i] && ewarActiveSelf) {
          var log = this._kinematicLoggers[i].GetData().Log;
          if (log.Count > 0 && currTime - log.Last().Time < 0.1f) { //reduce update frequency to 0.1s instead of 0.02s
            return;
          }

          var currTransform = (ewar == null) ? areaDisplay.transform : ewar.transform;
          var rot = this._infoData[i].Omni ? null : new Vector3Data() { //omni have null rotation values; it's just a sphere 
            X = currTransform.rotation.eulerAngles.x,
            Y = currTransform.rotation.eulerAngles.y,
            Z = currTransform.rotation.eulerAngles.z
          };
          var newKinematicData = new KinematicData() {
            Time = currTime,
            Transform = new TransformData() {
              Position = new Vector3Data() {
                X = currTransform.position.x,
                Y = currTransform.position.y,
                Z = currTransform.position.z
              },
              Rotation = rot
            }
          };
          this._kinematicLoggers[i].AddData(newKinematicData);
        }
        else if(this._active[i] && !ewar.gameObject.activeSelf) {
          this._active[i] = false;
        }
      }
    }
    public void Clear() {
      this._kinematicLoggers.Clear();
      this._infoData.Clear();
      this._meshes.Clear();

      this._ewarInstances.Clear();
      this._areaDisplays.Clear();
      this._constructedMeshes.Clear();

      this._active.Clear();
      this._parameters.Clear();
    }

    //used to acquire enemy EWAR meshes
    public void ConstructEWar(Game.EWar.ActiveEWarEffect ewar, Mesh mesh, /*Vector3 scale,*/ float radius, Vector3 posOffset) {
      if(mesh == null && radius == 0) {
        Debug.Log("[BBRMod] ConstructEWar() failed: mesh is null");
        return;
      }

      if (this._constructedMeshes.ContainsKey(ewar)) {
        Debug.Log("[BBRMod] constructed mesh already present"); //TEMP
        return;
      }

      var result = (Mesh)null;
      if (mesh != null) { //not omni
        result = mesh;
        var verts = new List<Vector3>();
        for (int i = 0; i < mesh.vertices.Length; i++) {
          var vert = mesh.vertices[i];
          vert += posOffset;
          //var realVector = Vector3.Scale(vert, scale);
          //verts.Add(realVector);
          verts.Add(vert);
        }
        
        /*Debug.Log("[BBRMod] offset vertices:");
        foreach (var vert in verts) {
          Debug.Log($"[{vert.x}, {vert.y}, {vert.z}]");
        }*/
        result.SetVertices(verts);
      }

      this._constructedMeshes.Add(ewar, result);
    }

    public void RegisterEWar(Game.EWar.ActiveEWarEffect instance) {
      IEWarHost host = (IEWarHost)ProtectedData.GetPrivateProperty(instance, "_launchedFrom");
      if (host is null) { //When EWAR weapons are created their fire() methods are called. apparently this happens when transferring ownership as well. ISSUE: what happens when the ewar is already firing?
        //Debug.Log("[BBRMod] IEWarHost of Ewar instance was null. This is normal if a ship just transferred owners.");
        //Also fires on reactor blooms? TODO: integrate Blooms into this
        return;
      }

      Game.Units.ShipController controller = null;
      Munitions.Missile missile = null;
      List<Ships.Controls.IWeapon> allWeapons = new List<Ships.Controls.IWeapon>();
      Ships.WeaponComponent weapon = null;
      if (host is Game.Units.ShipController) {
        controller = (Game.Units.ShipController)host;
        allWeapons = (List<Ships.Controls.IWeapon>)ProtectedData.GetPrivateField(controller, "_allWeapons");
      } else if (host is Munitions.Missile) {
        missile = (Munitions.Missile)host;
      }

      //deciding between missile and ship based ewar
      bool matchFound = false;
      if(missile == null) { //ship case
        List<Ships.Controls.IWeapon> validTargets = new List<Ships.Controls.IWeapon>();
        for (int i = 0; i < allWeapons.Count; i++) {
          if(allWeapons[i].EWType != this._ewarWeaponTypeLookupDict[instance.GetType()]) {
            continue;
          }
          if(typeof(Ships.ContinuousWeaponComponent).IsAssignableFrom(allWeapons[i].GetType())) {
            validTargets.Add(allWeapons[i]);
            var muzzles = (Ships.Muzzle[])typeof(Ships.WeaponComponent).GetField("_muzzles", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(allWeapons[i]);
            for (int j = 0; j < muzzles.Length; j++) {
              if(muzzles[j] is Ships.RezFollowingMuzzle currMuzzle) {
                //If we are the host we can match the specific instance. If not, we have to guess based on the distance from valid weapons
                //TODO: this sucks
                var followingInstance = (Utility.NetworkPoolable)typeof(Ships.RezFollowingMuzzle).GetField("_followingInstance", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(currMuzzle);
                if(followingInstance is Game.EWar.ISettableEWarParameters) {
                  var ewar = (Game.EWar.ActiveEWarEffect)followingInstance;
                  if(object.ReferenceEquals(instance, ewar)) {
                    weapon = (Ships.WeaponComponent)allWeapons[i];
                    matchFound = true;
                    break;
                  }
                }
              }
            }
          }
          if(matchFound)
            break;
        }

        var distanceBand = 0.1;
        if(!matchFound) {
          if(validTargets.Count == 0) {
            Debug.Log($"[BBRMod] Error: no match found for ship based EWAR. Generated report may be incomplete.");
          }
          else if(validTargets.Count == 1) {
            weapon = (Ships.WeaponComponent)validTargets[0];
          }
          else { //validTargets.Count > 1

            for (int i = 0; i < validTargets.Count; i++) {
              var muzzles = (Ships.Muzzle[])typeof(Ships.WeaponComponent).GetField("_muzzles", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(validTargets[i]);
              for (int j = 0; j < muzzles.Length; j++) {
                var weaponPos = validTargets[i].AimFromPosition;
                var ewarPos = instance.transform.position;
                if(muzzles[j] is Ships.RezFollowingMuzzle currMuzzle) {
                  var muzzlePos = currMuzzle.transform.position;

                  //ISSUE: distance not accurate consistently
                  //this feels... urgh. NOTE: 0.05 is too tight a margin. may be affected by ping?
                  if(Vector3.Distance(muzzlePos, ewarPos) < distanceBand) { 
                    weapon = (Ships.WeaponComponent)validTargets[i];
                    matchFound = true;
                    break;
                  }
                  if(Vector3.Distance(weaponPos, ewarPos) < distanceBand) { 
                    weapon = (Ships.WeaponComponent)validTargets[i];
                    matchFound = true;
                    break;
                  }
                }
              }

              if(i + 1 == validTargets.Count) {
                if(!matchFound) {
                  i = -1;
                  distanceBand += 0.1;
                  if(distanceBand >= 3.0) { //ISSUE: 3 is an arbitrary number based on random testing. TODO: rework because this is fucking gross
                    Debug.Log($"[BBRMod] Error: distance band too large. no match found for ship based EWAR. Generated report may be incomplete.");
                    break; //just in case; nobody likes infinite loops, but this really should never happen. If we reach this I've severely fucked up somewhere
                  }
                }
              }
                

            }
          }
        }

        
      }
      /*else { //missile case
        Debug.Log($"[BBRMod] match found for missile based EWAR");
      }*/

      this.AddEWar(weapon, missile, instance);
    }

    public void AddEWar(Ships.WeaponComponent weapon, Munitions.Missile missile, Game.EWar.ActiveEWarEffect instance) {
      try {
        var id = (uint)this._active.Count;
        MeshFilter areaDisplay = (MeshFilter)typeof(Game.EWar.ActiveEWarEffect).GetField("_areaDisplay", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(instance);
        this._kinematicLoggers.Add(new KinematicLogger(id, "ewar"));
        this._infoData.Add(ExtractEWarInfo(weapon, missile, instance, id));
        this._active.Add(true);
        this._ewarInstances.Add(instance);
        this._areaDisplays.Add(areaDisplay);
        var parameters = new EWarMeshParameters(instance);

        /*Debug.Log("[BBRMod] new ewar params:");
        Debug.Log($"\t FoV {parameters.ConeFov}");
        Debug.Log($"\t range {parameters.MaxRange}");*/
        for (int i = 0; i < this._parameters.Count; i++) {
          /*Debug.Log($"[BBRMod] {i} testing against ewar params:");
          Debug.Log($"\t FoV {this._parameters[i].ConeFov}");
          Debug.Log($"\t range {this._parameters[i].MaxRange}");*/
          if(this._parameters[i] == parameters) {
            //Debug.Log("match found");
            parameters.MeshIndex = this._parameters[i].MeshIndex;
            break;
          }
        }
        if(parameters.MeshIndex == -1) {
          parameters.MeshIndex = this._meshes.Count;
          var meshData = ExtractMeshData(this._infoData.Last().Omni, areaDisplay, instance);

          this._meshes.Add(new MeshCollectionData() {
            Id = id,
            Type = "ewar",
            Meshes = new List<MeshData>{meshData}
          });
        }
        this._parameters.Add(parameters);
        this._infoData.Last().MeshIndex = parameters.MeshIndex;
      }
      catch(Exception err) {
        Debug.Log($"[BBRMod] EWar error occurred:");
        Debug.Log(err);
      }
    }

    private EWarInfo ExtractEWarInfo(Ships.WeaponComponent weapon, Munitions.Missile missile, Game.EWar.ActiveEWarEffect ewar, uint id) {
      
      IEWarHost host = (IEWarHost)ProtectedData.GetPrivateProperty(ewar, "_launchedFrom");

      Game.Units.ShipController ship = null;
      var weaponName = "Unknown";
      var ewarType = this._ewarWeaponTypeLookupDict[ewar.GetType()].ToString();
      uint ownerId;
      if(missile == null) {
        ship = host as Game.Units.ShipController;
        weaponName = weapon.WepName;
        ownerId = ship.NetID.netId;
      }
      else {
        ownerId = missile.LaunchedFrom.NetID.netId;
        weaponName = missile.MunitionName;
      }
      
      var sig = (Game.Sensors.SignatureType)ewar.GetType().GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
      var sigType = Game.Sensors.SignatureTypeExtensions.GetFullName(sig);
      var isOmni = (bool)ewar.GetType().GetField("_omnidirectional", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
      var areaRenderer = (MeshRenderer)typeof(Game.EWar.ActiveEWarEffect).GetField("_areaRenderer", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);

      return new EWarInfo() {
        Id = id,
        Type = "ewar",
        Owner = new IdData() {
          Id = ownerId,
          Type = "ship"
        },
        Name = weaponName,
        SigType = sigType,
        EWarType = ewarType,
        Omni = isOmni,
        Color = GetEwarColor(areaRenderer)
      };
    }

    //OLD
    private Munitions.Missile ExtractEWarMissile(Game.Units.ShipController controller, Game.EWar.ActiveEWarEffect instance) {
      var missile = (Munitions.Missile)null;
      try {
        var salvosInFlight = (List<Munitions.ActiveMissileSalvo>)typeof(Game.Units.ShipController).GetField("_salvosInFlight", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(controller);

        for (int i = 0; i < salvosInFlight.Count; i++) {
          //ISSUE: with mixed salvos will this have to loop through each missile instead of MissileType?
          missile = (Munitions.ModularMissiles.ModularMissile)salvosInFlight[i].MissileType;
          var component = (UnityEngine.Component)null;
          missile.gameObject.TryGetComponent(typeof(Munitions.ModularMissiles.Runtime.Support.RuntimeJammerSupport), out component);
          if(component != null) {
            var jammerModule = (Munitions.ModularMissiles.Runtime.Support.RuntimeJammerSupport)component;
            var ewar = (Game.EWar.ActiveEWarEffect)typeof(Munitions.ModularMissiles.Runtime.Support.RuntimeJammerSupport).GetField("_effect", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(jammerModule);

            if(object.ReferenceEquals(ewar, instance)) {
              break;
            }
          }
        }
      }
      catch(Exception err) {
        Debug.Log($"[BBRMod] EWar error occurred during missile extraction: ");
        Debug.Log(err);
      }
      return missile;
    }

    //TODO: integrate this instead of having this special case. With the addition of IEwarHost this whole thing needs a bit of a revamp anyway. EWAR as terrain should probably be handled by this as well.
    public void AddReactorBloomEWar(Effects.Modules.SpawnEWarEffectModule spawner, Vector3 position) { //Time is set if we're resuming this from a delayed coroutine
      if (spawner == null) {
        Debug.Log("[BBRMod] Unable to capture Reactor Bloom EWar effect; Effects.Modules.SpawnEWarEffectModule was null.");
        return;
      }

      var source = ((GameObject)ProtectedData.GetPrivateField(spawner, "_ewarEffect")).GetComponent<Game.EWar.ActiveSensorJammer>();
      if (source == null) {
        Debug.Log("[BBRMod] Unable to capture Reactor Bloom EWar effect; _ewarEffect Game.EWar.ActiveSensorJammer was null.");
        return;
      }

      //for reactor blooms we can futz kinematic data since we don't need to actually track it
      float duration = (float)ProtectedData.GetPrivateField(spawner, "_duration");
      var time = ModUtils.Instance.GetTime();
      var kInit = new KinematicData() {
        Time = time,
        Transform = new TransformData() {
          Position = new Vector3Data(position)
        }
      };
      var kFinal = new KinematicData() {
        Time = time + duration,
        Transform = new TransformData() {
          Position = new Vector3Data(position)
        }
      };

      //EWarInfo
      var id = (uint)this._active.Count;
      var ewarType = this._ewarWeaponTypeLookupDict[source.GetType()].ToString();
      var sig = (Game.Sensors.SignatureType)ProtectedData.GetPrivateField(spawner, "_sigType");
      var sigType = Game.Sensors.SignatureTypeExtensions.GetFullName(sig);
      var areaRenderer = (MeshRenderer)ProtectedData.GetPrivateField(source, "_areaRenderer");
      var areaDisplay = (MeshFilter)ProtectedData.GetPrivateField(source, "_areaDisplay");
      var maxRange = (float)ProtectedData.GetPrivateField(spawner, "_maxRange");
      var info = new EWarInfo() {
        Id = id,
        Type = "ewar",
        Owner = null,
        Name = "Reactor Bloom",
        SigType = sigType,
        EWarType = ewarType,
        Omni = true,
        Color = GetEwarColor(areaRenderer)
      };

      //EWarMeshParameters
      var parameters = new EWarMeshParameters(spawner);
      for (int i = 0; i < this._parameters.Count; i++) {
        if(this._parameters[i] == parameters) {
          parameters.MeshIndex = this._parameters[i].MeshIndex;
          break;
        }
      }
      if(parameters.MeshIndex == -1) {
        parameters.MeshIndex = this._meshes.Count;
        var meshdata = new MeshData() {
          PrimitiveCollider = new PrimitiveCollider() {
            Type = PrimitiveColliderType.Sphere.ToString(),
            //Center = new Vector3Data(position),
            Parameters = new Vector3Data() {X = maxRange, Y = 0f, Z = 0f},
            Scale = new Vector3Data(Vector3.one)
          }
        };

        this._meshes.Add(new MeshCollectionData() {
          Id = id,
          Type = "ewar",
          Meshes = new List<MeshData>{meshdata}
        });
      }

      //Data
      this._kinematicLoggers.Add(new KinematicLogger(id, "ewar"));
      this._kinematicLoggers.Last().AddData(kInit);
      this._kinematicLoggers.Last().AddData(kFinal);
      this._infoData.Add(info);
      this._active.Add(false); //we don't need to actually track it; we know it's stationary
      this._ewarInstances.Add(source);
      this._areaDisplays.Add(areaDisplay);
      this._parameters.Add(parameters);
      this._infoData[this._infoData.Count - 1].MeshIndex = parameters.MeshIndex;
    }

    //OSP radars are really a whole other system
    private void AddOrderableSensor(Ships.OrderableActiveSensorComponent mount, bool isActive) {
      var id = (uint)this._active.Count;
      var ewarInfo = ExtractEWarSensorInfo(mount, id);
      this._kinematicLoggers.Add(new KinematicLogger(id, "ewar"));
      this._infoData.Add(ewarInfo);
      this._active.Add(isActive);
      var parameters = new EWarMeshParameters(mount);

      //just null them out so counts stay the same
      this._ewarInstances.Add(null);

      var areaDisplay = (MeshFilter)typeof(Ships.OrderableActiveSensorComponent).GetField("_areaDisplay", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount);
      this._areaDisplays.Add(areaDisplay);

      for (int i = 0; i < this._parameters.Count; i++) {
        if(this._parameters[i] == parameters) {
          parameters.MeshIndex = this._parameters[i].MeshIndex;
          break;
        }
      }
      if(parameters.MeshIndex == -1) {
        parameters.MeshIndex = this._meshes.Count;
        var meshData = ExtractMeshData(ewarInfo.Omni, areaDisplay, (Game.EWar.ActiveEWarEffect)null);

        this._meshes.Add(new MeshCollectionData() {
          Id = id,
          Type = "ewar",
          Meshes = new List<MeshData>{meshData}
        });
      }
      this._parameters.Add(parameters);
      this._infoData[this._infoData.Count - 1].MeshIndex = parameters.MeshIndex;
    }

    public void HandleOrderedSensor(Ships.OrderableActiveSensorComponent mount, bool isActive) {
      var index = -1;
      var areaDisplay = (MeshFilter)typeof(Ships.OrderableActiveSensorComponent).GetField("_areaDisplay", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount);
      for (int i = this._areaDisplays.Count - 1; i >= 0; i--) { //go in reverse to find most recent entry
        if(object.ReferenceEquals(areaDisplay, this._areaDisplays[i])) {
          index = i;
          break;
        }
      }

      if(index > -1) { //match found
        if(this._active[index] == isActive) {} //erroneous call. Ships.OrderableActiveSensorComponent.UpdateDisplayAreaVisibility() is called twice when turning the radar off by cancelling the order
        else if(this._active[index]) {//stop old
          this._active[index] = isActive;
        }
        else {//start new
          AddOrderableSensor(mount, isActive);
        }
      }
      else //match not found
        AddOrderableSensor(mount, isActive);
    }

    private EWarInfo ExtractEWarSensorInfo(Ships.OrderableActiveSensorComponent mount, uint id) {
      var shipNetId = ((Ships.Controls.IWeapon)mount).Group.OnShip.netId;
      var ewarType = mount.EWType.ToString();
      var sig = (Game.Sensors.SignatureType)typeof(Ships.BaseActiveSensorComponent).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount);
      var sigType = Game.Sensors.SignatureTypeExtensions.GetFullName(sig);
      var areaRenderer = (MeshRenderer)ProtectedData.GetPrivateField(mount, "_areaRenderer");

      return new EWarInfo() {
        Id = id,
        Type = "ewar",
        Owner = new IdData() {
          Id = shipNetId,
          Type = "ship"
        },
        Name = mount.ComponentName,
        SigType = sigType,
        EWarType = ewarType,
        Omni = false, //there's straight up no field to check
        Color = GetEwarColor(areaRenderer)
      };
    }

    private MeshData ExtractMeshData(bool isOmni, MeshFilter areaDisplay, Game.EWar.ActiveEWarEffect ewar) {
      if (isOmni) {
        float maxRange;

        if (ewar == null) {
          maxRange = areaDisplay.transform.lossyScale.x; //i mean... if there's something funky here it'll be from mods or new stuff so i'll deal with it then x.x
        }
        else {
          maxRange = (float)ProtectedData.GetPrivateField(ewar, "_maxRange");
        }

        var primitive = new PrimitiveCollider() {
          Type = PrimitiveColliderType.Sphere.ToString(),
          Parameters = new Vector3Data() {X = maxRange, Y = 0f, Z = 0f},
          Scale = new Vector3Data(Vector3.one)
        };

        return new MeshData() {
          PrimitiveCollider = primitive
        };
      }

      var mesh = (Mesh)null;
      var scale = new Vector3(1f, 1f, 1f);
      //TODO: refactor and make this a little prettier because right now you're just in 'bash it into line' mode and you know it
      if (Game.GameManager.Instance.IsDedicatedServer) { //dedicated servers just have to be special
        if (ewar != null && this._constructedMeshes.ContainsKey(ewar)) { //ewar
          mesh = this._constructedMeshes[ewar];
          
          /*Debug.Log("[BBRMod] Retrieved vertices:");
          foreach (var vert in mesh.vertices) {
            Debug.Log($"[{vert.x}, {vert.y}, {vert.z}]");
          }*/
        }
        else { //sensors
          mesh = areaDisplay.mesh;
          scale = areaDisplay.transform.lossyScale;
        }
      }
      else if(areaDisplay.gameObject.activeSelf || ewar == null) { //ally ewar and sensors
        mesh = areaDisplay.mesh;
        scale = areaDisplay.transform.lossyScale;
      }
      else if(ewar != null) { //enemy ewar
        mesh = this._constructedMeshes[ewar];
      }

      var data = new MeshData() {
        MeshVerticesData = new List<Vector3Data>(),
        MeshTriangleData = new List<int>(mesh.triangles)
      };
      for(int j = 0;j < mesh.vertices.Length;j++) {
        var meshVerticesData = new Vector3Data();
        var realVector = Vector3.Scale(mesh.vertices[j], scale);
        meshVerticesData.X = realVector.x;
        meshVerticesData.Y = realVector.y;
        meshVerticesData.Z = realVector.z;
        data.MeshVerticesData.Add(meshVerticesData);
      }
      return data;
    }

    static private ColorData GetEwarColor(MeshRenderer areaRenderer) {
      ColorData color = null;

      try {
        if (!Game.GameManager.Instance.IsDedicatedServer) { //Dedicated servers don't have the color information
          var material = areaRenderer.sharedMaterial;
          var shader = material.shader;
          for (int i = 0; i < shader.GetPropertyCount(); i++) {
            if (shader.GetPropertyType(i) != UnityEngine.Rendering.ShaderPropertyType.Color) { continue; }
            //ISSUE: assumes only one color field
            var values = material.GetColor(shader.GetPropertyName(i));
            color = new ColorData() { r = values.r, g = values.g, b = values.b, a = values.a };
            break;
          }
        }
      } catch (Exception e) {
        Debug.Log("[BBRMod] unable to retrive ewar color.");
        Debug.Log(e);
      }

      return color;
    }

    public List<KinematicLog> GetKinematicData() {
      var ewarLog = new List<KinematicLog>();
      foreach(var logger in this._kinematicLoggers) {
        ewarLog.Add(logger.GetData());
      }
      return ewarLog;
    }
    public List<EWarInfo> GetInfoData() {
      return this._infoData;
    }

    public List<MeshCollectionData> GetMeshData() {
      return this._meshes;
    }

    private class EWarMeshParameters {
      // public Game.Sensors.SignatureType signatureType { get; set; }
      public bool Omnidirectional { get; set; }
      public float ConeFov { get; set; }
      public float MaxRange { get; set; }
      // public float effectAreaRatio { get; set; }
      // public float radiatedPower { get; set; }
      // public float gain { get; set; }
      // public float edgeFalloff { get; set; }
      // public bool showLOB { get; set; }
      public int MeshIndex { get; set; }

      public EWarMeshParameters(Game.EWar.ActiveEWarEffect ewar) {
        // this.signatureType = (Game.Sensors.SignatureType)typeof(Game.EWar.ActiveEWarEffect).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        this.Omnidirectional = (bool)typeof(Game.EWar.ActiveEWarEffect).GetField("_omnidirectional", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        this.ConeFov = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_coneFov", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        this.MaxRange = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_maxRange", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        // this.effectAreaRatio = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_effectAreaRatio", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        // this.radiatedPower = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_radiatedPower", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        // this.gain = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        // this.edgeFalloff = (float)typeof(Game.EWar.ActiveEWarEffect).GetField("_edgeFalloff", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        // this.showLOB = (bool)typeof(Game.EWar.ActiveEWarEffect).GetField("_showLOB", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(ewar);
        this.MeshIndex = -1;
      }

      /*public EWarMeshParameters(float halfFOV, Ships.OrderableActiveSensorComponent mount) {

        this.omnidirectional = false;
        this.coneFov = halfFOV * 2;
        this.maxRange = mount.MaxRange;
        
      }*/

      public EWarMeshParameters(Ships.OrderableActiveSensorComponent mount) {
        // this.signatureType = (Game.Sensors.SignatureType)typeof(Ships.BaseActiveSensorComponent).GetField("_sigType", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount);
        this.Omnidirectional = false;
        this.ConeFov = (float)typeof(Ships.OrderableActiveSensorComponent).GetField("_halfFOV", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount) * 2;
        this.MaxRange = mount.MaxRange;
        // this.effectAreaRatio = 0.0f;
        // this.radiatedPower = ((Game.Sensors.ISignalEmitter)mount).RadiatedPower;
        // this.gain = (float)typeof(Ships.BaseActiveSensorComponent).GetField("_gain", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(mount);
        // this.edgeFalloff = 0.0f;
        // this.showLOB = mount.ShowJammingLOB;
        this.MeshIndex = -1;
      }

      public EWarMeshParameters(Effects.Modules.SpawnEWarEffectModule spawner) {
        // this.signatureType = ;
        this.Omnidirectional = (bool)ProtectedData.GetPrivateField(spawner, "_omnidirectional");
        this.ConeFov = (float)ProtectedData.GetPrivateField(spawner, "_fov");
        this.MaxRange = (float)ProtectedData.GetPrivateField(spawner, "_maxRange");;
        // this.effectAreaRatio = ;
        // this.radiatedPower = ;
        // this.gain = ;
        // this.edgeFalloff = ;
        // this.showLOB = ;
        this.MeshIndex = -1;
      }

      public static bool operator == (EWarMeshParameters lhs, EWarMeshParameters rhs) {
        // if(lhs.signatureType != rhs.signatureType)
        //   return false;
        if(lhs.Omnidirectional != rhs.Omnidirectional)
          return false;
        if(lhs.ConeFov != rhs.ConeFov)
          return false;
        if(lhs.MaxRange != rhs.MaxRange)
          return false;
        // if(lhs.effectAreaRatio != rhs.effectAreaRatio)
        //   return false;
        // if(lhs.radiatedPower != rhs.radiatedPower)
        //   return false;
        // if(lhs.gain != rhs.gain)
        //   return false;
        // if(lhs.edgeFalloff != rhs.edgeFalloff)
        //   return false;
        // if(lhs.showLOB != rhs.showLOB)
        //   return false;
       return true;
      }
      public static bool operator != (EWarMeshParameters lhs, EWarMeshParameters rhs) {
        // if(lhs.signatureType != rhs.signatureType)
        //   return true;
        if(lhs.Omnidirectional != rhs.Omnidirectional)
          return true;
        if(lhs.ConeFov != rhs.ConeFov)
          return true;
        if(lhs.MaxRange != rhs.MaxRange)
          return true;
        // if(lhs.effectAreaRatio != rhs.effectAreaRatio)
        //   return true;
        // if(lhs.radiatedPower != rhs.radiatedPower)
        //   return true;
        // if(lhs.gain != rhs.gain)
        //   return true;
        // if(lhs.edgeFalloff != rhs.edgeFalloff)
        //   return true;
        // if(lhs.showLOB != rhs.showLOB)
        //   return true;

        return false;
      }
      public override bool Equals(System.Object otherItem) {
        if (otherItem is not EWarMeshParameters)
          return false;
        else {
          EWarMeshParameters newItem = (EWarMeshParameters) otherItem;
          // if(this.signatureType != newItem.signatureType)
          //   return false;
          if(this.Omnidirectional != newItem.Omnidirectional)
            return false;
          if(this.ConeFov != newItem.ConeFov)
            return false;
          if(this.MaxRange != newItem.MaxRange)
            return false;
          // if(this.effectAreaRatio != newItem.effectAreaRatio)
          //   return false;
          // if(this.radiatedPower != newItem.radiatedPower)
          //   return false;
          // if(this.gain != newItem.gain)
          //   return false;
          // if(this.edgeFalloff != newItem.edgeFalloff)
          //   return false;
          // if(this.showLOB != newItem.showLOB)
          //   return false;

          return true;
        }
      }
      public override int GetHashCode() {
        float seed = /*(int)this.signatureType + */(this.Omnidirectional ? 1 : 0) + this.ConeFov + this.MaxRange; //+ this.effectAreaRatio + this.radiatedPower + this.gain + this.edgeFalloff + (this.showLOB ? 1 : 0);
        return seed.GetHashCode();
      }
    }
  }
}