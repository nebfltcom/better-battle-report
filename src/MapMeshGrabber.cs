using System.Collections.Generic;
using UnityEngine;
using System;
using Utility;
using UnityMeshSimplifier;
using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using Game;
using Modding;
using Bundles;
using Game.Map;

//ISSUE: mesh decimation does not account for border edges and will mutate them. This will require modifying MeshSimplifier.cs; test solution on Tumbleweed/Giant Coral
//TODO: revisit mesh decimation parameters
namespace BBRMod {
  internal class MapMeshGrabber {
    //Maps to be skipped completely
    private readonly string[] _invalidMapKeys = {};

    //Perhaps there's a better solution for oddly constructed maps, but this will do for now
    //Forces a specific, tailored decimation quality that balances accuracy and filesize
    //THOUGHT: miight be able to delete verts based on similar work to the octree shell for Blue City, but that would be a large undertaking
    private readonly Dictionary<string, float> _forcedDecimationQualityFloor = new Dictionary<string, float>(){
      {"JpMjbGe557EYlcp3ZykEt2", 0.5f}, //Blue City is JUST a bunch of cubes, many of which are enclosed and are never seen nor interacted with, but are a part of the mesh regardless
      {"mToaWiv05k2I4zOM3ZStHQ", 0.5f}, //Spinal Swarm (4% by default)
      {"Rjkeo8zywkaMyO-WDeo4Qw", 0.7f}  //Terminus System 742 (sensor array in the center is 31% by default. it doesn't really need decimation though)
    };

    private System.Diagnostics.Stopwatch _performanceStopwatch = new System.Diagnostics.Stopwatch();

    private List<MeshCollectionData> _meshes = new List<MeshCollectionData>();
    private bool _isUsed = true, _tasksComplete = false, _finalized = false;
    private Dictionary<ColliderType, List<int>> _validColliderIndices = new Dictionary<ColliderType, List<int>>(){
      {ColliderType.Readable, new List<int>()},
      {ColliderType.Unreadable, new List<int>()},
      {ColliderType.Primitive, new List<int>()}
    };

    private const float MAX_TRIS_FLAT = 1; //TEST
    private const float MAX_RESOLUTION = 30f; // numTris / averageSideLength

    //collects all data to be passed to native arrays and jobs before scheduling
    private List<NativeArray<Vector3>> _preJobVertices = new List<NativeArray<Vector3>>();
    private List<NativeArray<int>> _preJobTris = new List<NativeArray<int>>();
    private List<float> _preJobDecimationQualities = new List<float>();

    //Job data
    private NativeArray<NativeArray<Vector3>> _jobVertices;
    private NativeArray<NativeArray<int>> _jobTris;
    private NativeArray<float> _jobDecimationQualities;

    //Job
    private DecimateMeshJob _decimateMeshJob;
    private JobHandle _jobHandle = default;

    public bool IsUsed => _isUsed;
    public bool TasksComplete => _tasksComplete;
    public bool IsColliderUnreadable(int i) => this._validColliderIndices[ColliderType.Unreadable].Contains(i);
    //Returns an array of ints that are the indices of all valid colliders, readable and unreadable, sorted.
    public int[] ValidColliderIndices => this._validColliderIndices[ColliderType.Unreadable].Concat(this._validColliderIndices[ColliderType.Readable].Concat(this._validColliderIndices[ColliderType.Primitive])).OrderBy(n => n).ToArray();
    public int[] MeshColliderIndices => this._validColliderIndices[ColliderType.Unreadable].Concat(this._validColliderIndices[ColliderType.Readable]).OrderBy(n => n).ToArray();

    public void Start() {
      _performanceStopwatch.Reset();
      _performanceStopwatch.Start();
      var map = Game.SkirmishGameManager.Instance.LoadedMap;
      var obstacles = map.Obstacles;

      //HARD CODED skip
      //Any map key that is included in _invalidMapKeys will be skipped here, resulting in no terrain data.
      for (int i = 0; i < this._invalidMapKeys.Length; i++) {
        if (map.MapKey == this._invalidMapKeys[i]) {
          this._isUsed = false;
          string info = $"[BBRMod] {map.MapName} has been manually marked as currently incompatible with BBR. This battle report will have no terrain. This is a known issue that may be fixed at a later time. - ShadowLotus";
          Debug.Log(info);
          InformationBoxManager.Instance.DisplayInformation(info, 10);
          _performanceStopwatch.Stop();
          return;
        }
      }

      //Test whether terrain is even needed
      var numObstacles = map.Obstacles.Count;
      if(numObstacles == 0) {//Are there obstacles at all?
        this._isUsed = false;
        Debug.Log("[BBRMod] No obstacles. MapMeshGrabber unused.");
        _performanceStopwatch.Stop();
        return;
      }

      //collider validity filter
      bool anyTerrain = false;
      List<string> unreadableColliderMeshNames = ResolveColliderMeshNames(map, ref anyTerrain);

      if ( ! anyTerrain) {//Are any obstacles terrain?
        this._isUsed = false;
        Debug.Log("[BBRMod] No terrain. MapMeshGrabber unused.");
        _performanceStopwatch.Stop();
        return;
      }

      if (this._validColliderIndices[ColliderType.Readable].Count == 0 && this._validColliderIndices[ColliderType.Unreadable].Count == 0) { return; }

      //scheduling
      //primitives do not require scheduling
      Queue<AssetStudio.Mesh> unreadableMeshData = new Queue<AssetStudio.Mesh>();

      if (this._validColliderIndices[ColliderType.Unreadable].Count > 0) {
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        stopwatch.Start();
        // AssetStudio
        ulong? sourceModId = map.SourceModId;
        List <FilePath> bundlePaths = new List<FilePath>();
        if (sourceModId == null) {
          //TODO: load vanilla map bundle if needed, but they've all been readable so far
        }
        else {
          var modRecord = ModDatabase.Instance.GetModByID((ulong)sourceModId);

          try {
            List<AssetBundle> bundleList = ((IEnumerable<AssetBundle>) ProtectedData.GetPrivateField(modRecord, "_loadedBundles")).ToList();

            if (bundleList.Count == 0) {
              Debug.Log($"[BBRMod] No bundles found for {modRecord?.Info?.ModName}. BBR will attempt to search other loaded bundles.");
              foreach (var mapInfo in BundleManager.Instance.AllMaps) {
                if (mapInfo.SourceModId == null) { continue; }
                var currModRecord = ModDatabase.Instance.GetModByID((ulong)mapInfo.SourceModId);
                if (map.MapName.Contains(mapInfo.MapName)) { //ISSUE: predicated on the way current map modifier code works in appending something to the original map name. Works for now, may be an issue in the future
                  bundleList.AddRange(((IEnumerable<AssetBundle>) ProtectedData.GetPrivateField(currModRecord, "_loadedBundles")).ToList());
                  modRecord = currModRecord;
                  OverrideMapName(unreadableColliderMeshNames, mapInfo.MapName); //ISSUE: may not be the correct name of the root object?
                  break;
                }
              }
            }

            for (int i = 0; i < bundleList.Count; i++) {

              FilePath filePath = new FilePath(modRecord.Info.AssetBundles[i], modRecord.Info.FileLocation.Directory);
              
              if (filePath.Exists) {
                bundlePaths.Add(filePath);
              }
              else {
                string lower = modRecord.Info.AssetBundles[i].ToLower();
                FilePath lowerPath = new FilePath(lower, modRecord.Info.FileLocation.Directory);
                if (lowerPath.Exists) {
                  bundlePaths.Add(lowerPath);
                } else {
                  Debug.Log("[BBRMod] filePath does not exist.");
                }
              }
            }
                
            AssetStudio.AssetsManager manager = new AssetStudio.AssetsManager();
            manager.LoadFiles(bundlePaths.ConvertAll(p => p.FullPath).ToArray());
            //var meshes = manager.GetColliderMeshes(unreadableColliderMeshNames.ToArray()); //?
            unreadableMeshData = new Queue<AssetStudio.Mesh>(manager.GetColliderMeshes(unreadableColliderMeshNames.ToArray()));

          } catch (System.Exception e) {
            Debug.LogError($"<color=\"yellow\">[BBRMod]</color> Exception caught while trying to access map information for {modRecord?.Info?.ModName}. Please save this log file and send it to ShadowLotus in the Discord.");
            Debug.Log(e);
          }
        }
        // END AssetStudio
        stopwatch.Stop();
        Debug.Log($"[BBRMod] AssetStudio tasks completed in {stopwatch.ElapsedMilliseconds}ms ({stopwatch.ElapsedTicks} ticks)");
      }

      foreach (var index in this.MeshColliderIndices) {
        try {
          AssetStudio.Mesh meshData = this.IsColliderUnreadable(index) ? unreadableMeshData.Dequeue() : null;
          this.ScheduleCollider(obstacles[index], index, meshData);
        } catch (InvalidOperationException) {
          Debug.LogError("<color=\"yellow\">[BBRMod]</color> attempted to dequeue an empty queue. An unreadable mesh was not retrieved correctly. Please save this log file and send it to ShadowLotus in the Discord.");
          this.ScheduleCollider(obstacles[index], index);
        }
      }

      this.ScheduleJobs();
    }
    public void Update() {
      if (!this._isUsed || this._tasksComplete || !this._jobHandle.IsCompleted) { return; }

      this._tasksComplete = true;
      this.FinalizeMeshData(); 

      _performanceStopwatch.Stop();
      Debug.Log($"[BBRMod] Map tasks complete in {_performanceStopwatch.ElapsedMilliseconds}ms.");
      InformationBoxManager.Instance.DisplayInformation($"[BBRMod] Map tasks complete in {_performanceStopwatch.ElapsedMilliseconds}ms.", 5);
      
    }
    public void Clear() {
      this._meshes.Clear();
      this._isUsed = true;
      this._tasksComplete = false;
      this._finalized = false;

      this._validColliderIndices[ColliderType.Readable].Clear();
      this._validColliderIndices[ColliderType.Unreadable].Clear();
      this._validColliderIndices[ColliderType.Primitive].Clear();

      this._preJobVertices.Clear();
      this._preJobTris.Clear();
      this._preJobDecimationQualities.Clear();

      this._decimateMeshJob = default;
      this._jobHandle = default;
    }
    public List<MeshCollectionData> GetData() {
      return this._meshes;
    }

    public static bool IsValidCollider(List<Collider> obstacles, int i) {
      Collider obstacle = obstacles[i];
      Game.Map.Battlespace map = Game.SkirmishGameManager.Instance.LoadedMap;
      Bounds mapBounds = new Bounds(map.transform.position, new Vector3(map.Radius, map.Radius, map.Radius) * 2f);

      var stackFrame = new System.Diagnostics.StackTrace(true).GetFrame(1);
      var callingMethodName = stackFrame.GetMethod().Name + " in " + new FilePath(stackFrame.GetFileName()).Name; 
      /*if(obstacle is not MeshCollider) { //some modded maps have colliders and rendered meshes separated, or purely cosmetic meshes. Theoretically this should never trigger as we're pulling from Obstacles
        Debug.Log($"[BBRMod] Non-mesh collider flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
        return false;
      }*/
      if ( ! obstacle.enabled ) { //some modded maps have colliders that are not enabled and thus not in-game, but are still in the map file. Theoretically this should never trigger as we're pulling from Obstacles
        Debug.Log($"[BBRMod] Non-enabled collider flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
        return false;
      }
      if ( ! mapBounds.Intersects(obstacle.bounds)) { //some modded maps have collider meshes that are outside of the battlespace radius that we want to ignore, but some have large objects that cut through the map, which we want to keep
        Debug.Log($"[BBRMod] Out of bounds flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
        return false;
      }
      for (int j = i - 1; j >= 0; j--) { //some modded maps have dupelicate colliders for the exact same object
        if (obstacle.bounds.center == obstacles[j].bounds.center && obstacle.bounds.extents == obstacles[j].bounds.extents) {
          Debug.Log($"[BBRMod] Duplicate bounds flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
          return false;
        }
      }
      if (obstacle is BoxCollider || obstacle is SphereCollider || obstacle is CapsuleCollider) {
        Debug.Log($"[BBRMod] Obstacle #{i} '{obstacle.name}' is a primitive {obstacle.GetType()}");
        return true;
      }
      if ( (obstacle as MeshCollider).sharedMesh == null) { //some modded maps have colliders that do not have a mesh (usually to enable the tac shader)
        Debug.Log($"[BBRMod] Missing mesh flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
        return false;
      }
      if((obstacle as MeshCollider).sharedMesh.vertexCount == 0) { //some modded maps have MeshColliders with zero verts and tris... for some reason
        Debug.Log($"[BBRMod] Zero vertices flagged obstacle #{i} '{obstacle.name}' to be ignored from {callingMethodName}");
        return false;
      }
      
      return true;
    }

    private List<string> ResolveColliderMeshNames(Battlespace map, ref bool anyTerrain) {
      List<string> unreadableColliderMeshNames = new List<string>();
      var obstacles = map.Obstacles;
      for (int i = 0; i < obstacles.Count; i++) {
        //modded maps are often sub-par in their construction; this filters out unwanted colliders
        if ( ! IsValidCollider(obstacles, i)) { continue; }
        
        //TODO: refactor, it's just kinda gross. low prio
        //Primitive Collider handler
        if (obstacles[i] is SphereCollider || obstacles[i] is BoxCollider || obstacles[i] is CapsuleCollider) {
          anyTerrain = true;
          
          this._validColliderIndices[ColliderType.Primitive].Add(i);
        }
        else if (obstacles[i] is MeshCollider meshCollider) {
          anyTerrain = true;
          var currMesh = meshCollider.sharedMesh;
          if (currMesh.isReadable) {
            this._validColliderIndices[ColliderType.Readable].Add(i);
          }
          else {
            this._validColliderIndices[ColliderType.Unreadable].Add(i);
            string mapRootName = map.name.Substring(0, map.name.LastIndexOf("(Clone)"));
            unreadableColliderMeshNames.Add(mapRootName + "/" + obstacles[i].gameObject.GetFullName(map.name, false));
          }
        }
      }
      return unreadableColliderMeshNames;
    }

    private void OverrideMapName(List<string> resolvedNames, string nameOverride) {
      for (int i = 0; i < resolvedNames.Count; i++) {
        string s =  resolvedNames[i];
        resolvedNames[i] = nameOverride + s.Substring(s.IndexOf("/"));
      }
    }

    //This will only be called after all jobs have completed their tasks
    private void FinalizeMeshData() {
      if (this._finalized) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Attempted to finalize map meshes twice. This message is preventing a crash. Please save the log and send it to ShadowLotus in the Discord.");
        return;
      }
      this._finalized = true;
      var obstacles = Game.SkirmishGameManager.Instance.LoadedMap.Obstacles;
      Queue<List<Vector3>> allVertices = new Queue<List<Vector3>>(this._jobVertices.ConvertAll(e => e.ToList()));
      Queue<List<int>> allTris = new Queue<List<int>>(this._jobTris.ConvertAll(e => e.ToList()));

      foreach (var i in this.ValidColliderIndices) {
        if (this._validColliderIndices[ColliderType.Primitive].Contains(i)) {
          this._meshes.Add(this.HandlePrimitiveCollider(obstacles[i]));
          continue;
        }

        if (allVertices.Count == 0 || allTris.Count == 0) {
          Debug.LogError("<color=\"yellow\">[BBRMod]</color> unexpectedly reached end of mesh queue. This report's terrain may be incorrect. Please save this log file and send it to ShadowLotus in the Discord.");
          break;
        }
        List<Vector3> vertices = allVertices.Dequeue();
        List<int> triangles = allTris.Dequeue();

        Vector3 scale = obstacles[i].transform.lossyScale; //doing it this way with the lambda expression below made capturing undecimated Avora >100x faster (270 seconds to <2)

        this._meshes.Add(new MeshCollectionData() {
          Id = (uint)this._meshes.Count,
          Type = "obstacle",
          Meshes = new List<MeshData>() { new MeshData() {
              MeshVerticesData = vertices.ConvertAll(v => new Vector3Data() {X = v.x * scale.x, Y = v.y * scale.y, Z = v.z * scale.z}), //instead of Vector3.Scale
              MeshTriangleData =  new List<int>(triangles)
            } //new MeshData
          } //Meshes[0]
        }); //new MeshCollectionData

      } //foreach _validColliderIndices

      //Garbage collection
      Debug.Log("[BBRMod] Disposing of Native Arrays"); //including just so if the disposals cause a hard crash the log shows we were here
      for (int i = 0; i < this._jobDecimationQualities.Length; i++) {
        //if (this._jobVertices.IsCreated == true) { //not needed?
        if (this._preJobVertices[i] != this._jobVertices[i]) { //If the array was not modified by a job disposing of both will cause a hard crash
          this._preJobVertices[i].Dispose();
        }
        this._jobVertices[i].Dispose();
        //}
        if (this._preJobTris[i] != this._jobTris[i]) {
          this._preJobTris[i].Dispose();
        }
        this._jobTris[i].Dispose();
      }
      this._jobVertices.Dispose();
      this._jobTris.Dispose();
      this._jobDecimationQualities.Dispose();
      Debug.Log("[BBRMod] Disposal complete");
    }

    //ISSUE: when determining resolution this does not consider how many objects there are, only how dense the given object is. OBL maps in particular have a fuckton of objects; with the exeption of Sector 821 many of them can be safely decimated
    private void PopulateJobData(Vector3[] vertices, int[] triangles, Collider collider, float maxResolution, float maxTrisFlat) {
      var avgSideLength = (collider.bounds.size.x + collider.bounds.size.y + collider.bounds.size.z ) / 3f;
      float targetTrisByRes = avgSideLength * maxResolution;
      float targetQuality = 1.0f;


      if ((float)triangles.Length / avgSideLength > maxResolution && triangles.Length > maxTrisFlat) {
        targetQuality = Mathf.Clamp(targetTrisByRes / (float)triangles.Length, 0.01f, 0.95f);
      }

      //HARD CODED
      if (this._forcedDecimationQualityFloor.ContainsKey(SkirmishGameManager.Instance.LoadedMap.MapKey)) {
        targetQuality = Mathf.Max(this._forcedDecimationQualityFloor[SkirmishGameManager.Instance.LoadedMap.MapKey], targetQuality);
      }

      this._preJobVertices.Add(new NativeArray<Vector3>(vertices, Allocator.Persistent));
      this._preJobTris.Add(new NativeArray<int>(triangles, Allocator.Persistent));
      this._preJobDecimationQualities.Add(targetQuality);
    }

    private void ScheduleJobs() {
      if (this._preJobVertices.Count != this._preJobTris.Count || this._preJobVertices.Count != this._preJobDecimationQualities.Count) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Encountered an error while trying to grab terrain information. Array counts are not equal. This battle report may have no terrain.");
        return;
      }
      //if (this._preJobDecimationQualities.Count == 0) { return; } //do we need this?
      
      this._jobVertices = new NativeArray<NativeArray<Vector3>>(this._preJobVertices.ToArray(), Allocator.Persistent);
      this._jobTris = new NativeArray<NativeArray<int>>(this._preJobTris.ToArray(), Allocator.Persistent);
      this._jobDecimationQualities = new NativeArray<float>(this._preJobDecimationQualities.ToArray(), Allocator.Persistent);

      this._decimateMeshJob = new DecimateMeshJob() {
        vertices = this._jobVertices,
        tris = this._jobTris,
        quality = this._jobDecimationQualities
      };

      this._jobHandle = this._decimateMeshJob.Schedule(this._jobVertices.Length, 1);
    }

    struct DecimateMeshJob : IJobParallelFor {
      public NativeArray<NativeArray<Vector3>> vertices;
      public NativeArray<NativeArray<int>> tris;
      public NativeArray<float> quality;

      public void Execute(int index) {
        //ISSUE: duplicate vertices that are part of the same mesh (at least i think that's the case?) will not be treated as border vertices and may create holes.
        //SOLUTION: merge all verts before running the decimator. This is on a different thread anyway, so who gives a shit about the cpu time.

        if (quality[index] > 0.95f) { //any mesh marked for decimation will have a quality < 0.95f, those not marked will be at 1.0f
          return;
        }

        MeshSimplifier meshSimplifier = new MeshSimplifier {
          Vertices = vertices[index].ToArray()
        };
        meshSimplifier.AddSubMeshTriangles(tris[index].ToArray());

        meshSimplifier.SimplifyMesh(quality[index]);

        List<int> newTris = new List<int>();
        for (int i = 0; i < meshSimplifier.SubMeshCount; i++) {
          newTris.AddRange(meshSimplifier.GetSubMeshTriangles(i));
        }

        vertices[index] = new NativeArray<Vector3>(meshSimplifier.Vertices, Allocator.Persistent);
        tris[index] = new NativeArray<int>(newTris.ToArray(), Allocator.Persistent);
      }
    }

    private void ScheduleCollider(Collider collider, int i, AssetStudio.Mesh unreadableMeshData = null) {
      if (unreadableMeshData == null) {
        this.ScheduleReadableCollider(collider, i);
      } else {
        this.ScheduleUnreadableCollider(collider, unreadableMeshData, i);
      }
    }

    private void ScheduleUnreadableCollider(Collider collider, AssetStudio.Mesh meshData, int i) {
      if (meshData == null) {
        Debug.Log($"[BBRMod] collider mesh not found for obstacle {i}, {collider.name}.");
        return;
      }
      int[] triangles = meshData.m_Indices.ConvertAll(i => (int)i).ToArray();
      Vector3[] vertices = new Vector3[meshData.m_VertexCount];
      for (int j = 0; j < meshData.m_VertexCount; j++) {
        vertices[j].x = meshData.m_Vertices[j * 3];
        vertices[j].y = meshData.m_Vertices[j * 3 + 1];
        vertices[j].z = meshData.m_Vertices[j * 3 + 2];
      }

      this.PopulateJobData(vertices, triangles, collider, MAX_RESOLUTION, MAX_TRIS_FLAT);
    }

    private void ScheduleReadableCollider(Collider collider, int i) {
      Mesh currMesh;
      //LOD system
      //Checks for the existence of LOD meshes, then grabs the next one up
      //Few maps have them, but one that does (King's Escapade) has immensely detailed meshes, and using the next LOD up makes the .bbr file 4MB lighter (and that's AFTER compression)
      //NOTE: we could remove this and just rely on mesh decimation
      var lodGroup = collider.gameObject.GetComponent<LODGroup>();
      if (lodGroup == null) {
        currMesh = (collider as MeshCollider).sharedMesh;
      }
      else {
        try {
          //we can't just assume the gameobject's children are the LODs. This gross thing finds the first LOD up
          var lods = lodGroup.GetLODs();
          var lod = lods[Mathf.Min(1, lods.Length - 1)]; //just in case some clown has an LOD with only one option
          currMesh = lod.renderers[0].gameObject.GetComponent<MeshFilter>().sharedMesh; //ISSUE: hard coded renderers index?
        } catch (Exception err) {
          currMesh = (collider as MeshCollider).sharedMesh; //If something goes wrong just use the high LOD
          Debug.Log($"[BBRMod] error encountered while trying to grab LOD meshes for obstacle {i}. Base collider mesh will be used.");
          Debug.Log(err);
        }
      }
      this.PopulateJobData(currMesh.vertices, currMesh.triangles, collider, MAX_RESOLUTION, MAX_TRIS_FLAT);
    }

    private MeshCollectionData HandlePrimitiveCollider(Collider collider) {
      return new MeshCollectionData() {
        Id = (uint)this._meshes.Count,
        Type = "obstacle",
        Meshes = new List<MeshData>() { new MeshData() {
            PrimitiveCollider = this.GetPrimitiveColliderData(collider),
            MeshVerticesData = new List<Vector3Data>(),
            MeshTriangleData = new List<int>()
          }
        }
      };
    }

    private PrimitiveCollider GetPrimitiveColliderData(Collider collider) {
      PrimitiveColliderType type = PrimitiveColliderType.None;
      Vector3 parameters = Vector3.zero;
      switch (collider) {
        case BoxCollider:
          type = PrimitiveColliderType.Box;
          var box = collider as BoxCollider;
          parameters = box.size;
          break;
        case SphereCollider:
          type = PrimitiveColliderType.Sphere;
          var sphere = collider as SphereCollider;
          parameters.x = sphere.radius;
          break;
        case CapsuleCollider:
          type = PrimitiveColliderType.Capsule;
          var capsule = collider as CapsuleCollider;
          parameters = new Vector3(capsule.direction, capsule.height, capsule.radius);
          break;
        default:
          Debug.LogError($"<color=\"yellow\">[BBRMod]</color> Unexpected primitive collider type for collider '{collider.name}'");
          break;
      }

      return new PrimitiveCollider() {
        Type = type.ToString(),
        Parameters = new Vector3Data() {X = parameters.x, Y = parameters.y, Z = parameters.z},
        Scale = new Vector3Data() {X = collider.transform.lossyScale.x, Y = collider.transform.lossyScale.y, Z = collider.transform.lossyScale.z}
      };
    }

    public enum ColliderType {
      Readable,
      Unreadable,
      Primitive
    }
  }
}