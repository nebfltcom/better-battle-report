using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ships;
using UnityEngine;

namespace BBRMod {
  internal class ShipMeshGrabber {
    private List<MeshCollectionData> _meshes = new List<MeshCollectionData>();
    private List<Game.Units.ShipController> _shipControllers = new List<Game.Units.ShipController>();
    private Dictionary<string, int> _shipSaveKeys = new Dictionary<string, int>();
    public void Start(ShipRecorder shipRecorder) {
      this._shipControllers = (List<Game.Units.ShipController>)typeof(Game.SkirmishGameManager).GetField("_allShips", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(Game.SkirmishGameManager.Instance);
      int controllerIndex = 0;
      foreach(var shipController in this._shipControllers) {
        var currHull = shipController.Ship.Hull;

        currHull.SpawnColliderSampler();
        var colliderSampler = currHull.MeshColliderSampler;
        string saveKey = shipController.Ship.Hull.SaveKey;
        if (colliderSampler is ModularColliderSampler) { //Modular hulls all have the same savekey, so we have to append distinguishing features. Keys are gross and long, but unique
          var primaryStructure = ((RandomModularHull.RandomHullConfiguration)ProtectedData.GetPrivateField((RandomModularHull)shipController.Ship.Hull, "_config")).PrimaryStructure;
          foreach (var section in primaryStructure) {
            saveKey += " - " + section.Key;
          }
        }

        //Mesh instancing
        if (this._shipSaveKeys.ContainsKey(saveKey)) {
          shipRecorder.SetMeshInstance(controllerIndex, this._shipSaveKeys[saveKey]);
          controllerIndex++;
          continue;
        }
        this._shipSaveKeys.Add(saveKey, this._meshes.Count);
        shipRecorder.SetMeshInstance(controllerIndex, this._meshes.Count);
        controllerIndex++;

        if (colliderSampler == null) {
          Debug.LogError($"<color=\"yellow\">[BBRMod]</color> Hull with name {shipController.Ship.Hull.ClassName} and saveKey {saveKey} has no mesh collider sampler. It will be absent from this report. Please inform the creator of the ship.");
          this._meshes.Add(new MeshCollectionData() {
            Id = shipController.NetID.netId,
            Type = "ship",
            Meshes = new List<MeshData>() {}
          });
          continue;
        }
        var colliders = (MeshCollider[])typeof(Ships.BaseColliderSampler).GetProperty("_allColliders", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(colliderSampler);
        var meshes = new List<MeshData>();
        for(int i = 0;i < colliders.Length;i++) {
          var currCollider = colliders[i];
          var currMesh = currCollider.sharedMesh;
          if(currMesh != null) {
            Matrix4x4 rotMat4 = Matrix4x4.Rotate(currCollider.transform.rotation);
            var currMeshData = new MeshData() {
              MeshVerticesData = new List<Vector3Data>(),
              MeshTriangleData = new List<int>(currMesh.triangles)
            };

            var scale = currCollider.transform.lossyScale;
            var scaledVerts = currMesh.vertices.ToList().ConvertAll(v => new Vector3() {x = v.x * scale.x, y = v.y * scale.y, z = v.z * scale.z}); //instead of Vector3.Scale

            for(int j = 0; j < scaledVerts.Count; j++) {
              var realVector = scaledVerts[j];
              realVector = rotMat4.MultiplyVector(realVector); //Fix for strange collider rotations (caused by Unity/Blender import mismatch?) (Rylek's)
              realVector += currCollider.transform.position; //some collider submeshes have their own transform offset as well (Aegir Tanager)

              currMeshData.MeshVerticesData.Add(new Vector3Data(realVector));
            }
            meshes.Add(currMeshData);
          }
        }
        this._meshes.Add(new MeshCollectionData() {
          Id = shipController.NetID.netId,
          Type = "ship",
          Meshes = meshes
        });
      }
    }
    public void Update() {
    }
    public void Clear() {
      this._meshes.Clear();
      this._shipControllers.Clear();
      this._shipSaveKeys.Clear();
    }
    public List<MeshCollectionData> GetData() {
      return this._meshes;
    }
  }
}