using System;
using System.Collections.Generic;

namespace BBRMod {
  public class AllData {
    public string GameVersion { get; set; }
    public string ModVersion { get; set; }
    public string ViewerVersion { get; set; }
    public float FixedDeltaTime { get; set; }
    public bool CapturedByDedicatedServer { get; set; }
    public MapInfo MapInfo { get; set; }
    public List<MeshCollectionData> MapMeshes { get; set; }
    public List<KinematicLog> MapKinematics { get; set; }
    public List<PoiData> MapPOIs { get; set; }
    public List<FleetInfo> FleetInfo { get; set; }
    public List<MeshCollectionData> ShipMeshes { get; set; }
    public List<KinematicLog> ShipKinematics { get; set; }
    public List<ShipInfo> ShipInfo { get; set; }
    public List<KinematicLog> MissileKinematics { get; set; }
    public List<MissileInfo> MissileInfo { get; set; }
    public List<KinematicLog> ShellKinematics { get; set; }
    public List<ShellInfo> ShellInfo { get; set; }
    public List<KinematicLog> BeamStartKinematics { get; set; }
    public List<KinematicLog> BeamEndKinematics { get; set; }
    public List<BeamInfo> BeamInfo { get; set; }
    public List<MeshCollectionData> EWarMeshes { get; set; }
    public List<KinematicLog> EWarKinematics { get; set; }
    public List<EWarInfo> EWarInfo { get; set; }
    public List<RayFieldData> RayFieldData { get; set; }
    public List<ExplosionData> ExplosionData { get; set; }
    public List<DamageLog> DamageData { get; set; }
  }

  public class IdData {
    public uint Id { get; set; }
    public string Type { get; set; }
  }

  public class MeshCollectionData : IdData {
    public List<MeshData> Meshes { get; set; }
  }

  public class MeshData {
    public PrimitiveCollider PrimitiveCollider { get; set; }
    public List<Vector3Data> MeshVerticesData { get; set; }
    public List<int> MeshTriangleData { get; set; }
  }

    public class PrimitiveCollider {
    public string Type { get; set; }
//    public Vector3Data Center { get; set; } //is this needed?
    //PrimitiveColliderType.Box, Parameters = Size
    //PrimitiveColliderType.Sphere, Parameters.X = radius, Parameters.Y NOT USED, Parameters.Z NOT USED
    //PrimitiveColliderType.Capsule, Parameters.X = direction, Parameters.Y = height, Parameters.Z = radius 
    public Vector3Data Parameters { get; set; }
    public Vector3Data Scale { get; set; }
  }

  public class KinematicLog : IdData {
    public List<KinematicData> Log { get; set; }
  }

  public class KinematicData {
    public float Time { get; set; }
    public TransformData Transform { get; set; }
  }

  public class TransformData {
    public Vector3Data Position { get; set; }
    public Vector3Data Rotation { get; set; }
  }

  public class Vector3Data : IEquatable<Vector3Data> {
    public float X { get; set; }
    public float Y { get; set; }
    public float Z { get; set; }

    public Vector3Data() {}
    public Vector3Data(float x, float y, float z) {
      X = x;
      Y = y;
      Z = z;
    }
    public Vector3Data(UnityEngine.Vector3 vec) {
      X = vec.x;
      Y = vec.y;
      Z = vec.z;
    }

    public bool Equals(Vector3Data other) {
      if (other == null) return false;
      if (this.X != other.X) return false;
      if (this.Y != other.Y) return false;
      if (this.Z != other.Z) return false;
      return true;
    }
  }

  public class MapInfo {
    public string Name { get; set; }
    public string Key { get; set; }
    public float Radius { get; set; }
    public string GameMode { get; set; }
    public int VictoryPoints { get; set; }
    public int VPOffset { get; set; }
    public string Victor { get; set; }
  }

  public class OwnerData : IdData {
    public IdData Owner { get; set; }
  }

  public class FleetInfo: OwnerData {
    public string Name { get; set; }
    public bool IsTeamA { get; set; }
    public string PlayerName { get; set; }
  }

  public class ShipInfo : OwnerData {
    public string Name { get; set; }
    public string Class { get; set; }
    public string HullClassification { get; set; }
    public IdData Fleet { get; set; }
    public float TimeEliminated { get; set; }
    public float TimeDefanged { get; set; }
    public int MeshIndex { get; set; } //points to the AllData.ShipMeshes index that contains the MeshData for this ship
  }

  public class MissileInfo : OwnerData {
    public int Size { get; set; }
    public string Name { get; set; }
    public bool IsDecoy { get; set; }
    public bool FiredDefensively { get; set; }
    public string Role { get; set; }
    public string Loadout { get; set; }
  }

  public class ShellInfo : OwnerData {
    public string Name { get; set; }
  }

  public class ColorData {
    public float r { get; set; }
    public float g { get; set; }
    public float b { get; set; }
    public float a { get; set; }
  }

  public class BeamInfo : OwnerData {
    public string Name { get; set; }
    public bool Pulsed { get; set; }
    public float DamagePeriod { get; set; }
    public float Width { get; set; }
    public ColorData Color { get; set; }
  }

  public class EWarInfo : OwnerData {
    public string Name { get; set; }
    public string SigType { get; set; }
    public string EWarType { get; set; }
    public bool Omni { get; set; } //NOTE: if this is true, rotation values will NOT be recorded.
    public ColorData Color { get; set; }
    public int MeshIndex { get; set; } //points to the AllData.EWarMeshes index that contains the MeshData for this EWar instance instead of storing 200 copies of the same cone
  }

  public class RayFieldData : OwnerData { //One per ship per ammunition type
    public string MunitionName { get; set; }
    public float MunitionSpeed { get; set; }
    public float MunitionLifetime { get; set; }
    public Vector3Data Offset { get; set; }
    public bool CylinderCast { get; set; }
    public float Accuracy { get; set; }
    public List<KinematicData> Rays { get; set; } //NOTE: Position is null, Rotation is the Euler angles required to rotate a mesh (specifically a {0,0,1} BABYLON.Vector3 line) to the recorded direction
  }

  public class PoiData : IdData {
    public string Name { get; set; }
    public Vector3Data Position { get; set; }
    public float Radius { get; set; }
    public List<float> Times { get; set; }
    public List<bool?> Ownership { get; set; }
  }

  public class ExplosionData : IdData {
    public float Time { get; set; }
    public Vector3Data Position { get; set; }
    public float Radius { get; set; }
  }

  public class DamageLog : IdData {
    public List<DamageInstance> Log { get; set; }
  }

  public class DamageInstance {
    public float Time { get; set; }
    public float Internal { get; set; }
    public float Armor { get; set; }
    //TODO:
    //  -specify components hit?
    //  -detailed breakdown?
    //  -source?
  }
}