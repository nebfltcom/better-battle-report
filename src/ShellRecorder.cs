using System;
using System.Collections.Generic;
using System.Reflection;
using Munitions;
using UnityEngine;

//ISSUE: shells fired close to the end of the game that are never repooled do not have an ending kData, so appear to disappear on firing in the viewer
namespace BBRMod {
  internal class ShellRecorder {
    private List<KinematicLogger> _kinematicLoggers = new List<KinematicLogger>();
    private List<ShellInfo> _infoData = new List<ShellInfo>();
    private List<Munitions.LookaheadMunitionBase> _shells = new List<Munitions.LookaheadMunitionBase>();
    private List<bool> _active = new List<bool>();
    public void Start() {
    }
    public void Update() {
    }
    public void Clear() {
      this._kinematicLoggers.Clear();
      this._infoData.Clear();
      this._shells.Clear();
      this._active.Clear();
    }
    public void AddShell(Munitions.LookaheadMunitionBase shell) {
      try {
        var id = (uint)this._shells.Count;
        var newLogger = new KinematicLogger(id, "shell");
        var shellKinematicData = this.ExtractKinematicData(shell);
        var shellInfo = this.ExtractShellInfo(shell, id);
        newLogger.AddData(shellKinematicData);

        this._kinematicLoggers.Add(newLogger);
        this._infoData.Add(shellInfo);
        this._shells.Add(shell);
        this._active.Add(true);
      }
      catch(Exception err) {
        Debug.Log($"[BBRMod] Shell error occurred:");
        Debug.Log(err);
      }
    }
    private ShellInfo ExtractShellInfo(Munitions.LookaheadMunitionBase shell, uint id) {
      var launchedShip = (Game.Units.ShipController)typeof(Munitions.LookaheadMunitionBase).GetField("_localLaunchedFrom", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(shell);
      string shellName = ((Munitions.LightweightKineticShell)ProtectedData.GetPrivateField(shell, "_appliedTemplate")).MunitionName; //ISSUE: NRE on dedicated servers

      //NOTE: from the client's PoV there is no part of the shell that ties it back to the ship that fired it. All relavent fields are null.
      //TODO: look into determining the closest ship at fire time and assigning that as the owner. very hacky. eugh.
      var owner = (IdData) null;
      if (launchedShip != null)
        owner = new IdData() {
          Id = launchedShip.NetID.netId,
          Type = "ship"
        };

      return new ShellInfo() {
        Id = id,
        Type = "shell",
        Owner = owner,
        Name = shellName
      };
    }
    public void RemoveShell(Munitions.LookaheadMunitionBase shell) {
      for(int i = 0;i < this._shells.Count;i++) {
        if(this._active[i] && object.ReferenceEquals(shell, this._shells[i])) {
          var launchedShip = (Game.Units.ShipController)ProtectedData.GetPrivateField(shell, "_localLaunchedFrom");
          if (launchedShip != null && this._infoData[i].Owner == null) {
            this._infoData[i].Owner = new IdData() {
              Id = launchedShip.NetID.netId,
              Type = "ship"
            };
          }
          this._kinematicLoggers[i].AddData(this.ExtractKinematicData(shell));
          this._active[i] = false;
        }
      }
    }
    private KinematicData ExtractKinematicData(Munitions.LookaheadMunitionBase shell) {
      float currTime = ModUtils.Instance.GetTime();
      var body = (Rigidbody)typeof(Munitions.LookaheadMunitionBase).GetField("_body", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(shell);
      return new KinematicData() {
        Time = currTime,
        Transform = new TransformData() {
          Position = new Vector3Data() {
            X = body.position.x,
            Y = body.position.y,
            Z = body.position.z
          }
        }
      };
    }
    public List<KinematicLog> GetKinematicData() {
      var shellLog = new List<KinematicLog>();
      foreach(var logger in this._kinematicLoggers) {
        shellLog.Add(logger.GetData());
      }
      return shellLog;
    }
    public List<ShellInfo> GetInfoData() {
      return this._infoData;
    }
  }
}