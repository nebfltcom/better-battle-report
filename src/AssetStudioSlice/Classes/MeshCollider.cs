using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssetStudio
{
    public sealed class MeshCollider : Component
    {
        public PPtr<Mesh> m_Mesh;

        public MeshCollider(ObjectReader reader) : base(reader)
        {
          
          //UnityEngine.Debug.Log($"MeshCollider constructor");
          var m_PhysicMaterial = new PPtr<Object>(reader);
          var isTrigger = reader.ReadBoolean();
          //UnityEngine.Debug.Log($"isTrigger: {isTrigger}");
          var enabled = reader.ReadBoolean();
          //UnityEngine.Debug.Log($"enabled: {enabled}");
          var convex = reader.ReadBoolean();
          //UnityEngine.Debug.Log($"convex: {convex}");
          for (int i = 0; i < 5; i++) {
            var burn = reader.ReadByte();
            //UnityEngine.Debug.Log($"burn {i}: {reader.ReadByte()}");
          }
          var cookingOptions = reader.ReadInt16();
          //UnityEngine.Debug.Log($"cookingOptions: {cookingOptions}");
          for (int i = 0; i < 2; i++) {
            var burn = reader.ReadByte();
            //UnityEngine.Debug.Log($"burn {i}: {reader.ReadByte()}");
          }
          m_Mesh = new PPtr<Mesh>(reader);
          //UnityEngine.Debug.Log($"collider mesh pointer path: {m_Mesh.m_PathID}");
          

          
          //Mesh mesh;
          /*if (m_Mesh.TryGet(out Mesh mesh)) {
            UnityEngine.Debug.Log($"mesh {mesh.m_Name} at path {mesh.m_PathID} has vert count: {mesh.m_VertexCount}");
          }*/
        }
    }
}
