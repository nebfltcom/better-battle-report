namespace AssetStudio
{
    public enum FileType
    {
        Unknown,
        AssetsFile,
        BundleFile,
        //WebFile,
        ResourceFile,
        //GZipFile,
        //BrotliFile,
        //ZipFile
    }
}
