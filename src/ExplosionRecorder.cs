using System.Collections.Generic;
using UnityEngine;

namespace BBRMod {
  internal class ExplosionRecorder {
    private List<ExplosionData> _explosions = new List<ExplosionData>();
    public void Start() {
    }
    public void Update() {
    }
    public void Clear() {
      this._explosions.Clear();
    }

    public void AddExplosion(Vector3 position, float radius) {
      this._explosions.Add(new ExplosionData() {
        Id = (uint)this._explosions.Count,
        Type = "explosion",
        Time = ModUtils.Instance.GetTime(),
        Position = new Vector3Data(position),
        Radius = radius
      });
    }

    public List<ExplosionData> GetData() {
      return this._explosions;
    }
  }
}