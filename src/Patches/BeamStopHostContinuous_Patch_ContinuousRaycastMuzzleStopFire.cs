using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamStopHostContinuous_Patch_ContinuousRaycastMuzzleStopFire {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] ContinuousRaycastMuzzle.StopFire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(ContinuousRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(ContinuousRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("stopfire")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void BeamStopContinuous_Host(ContinuousRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
  }
}