using HarmonyLib;
using System.Reflection;
using Game.Units;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class DamageInstanceArmor_Patch_ShipControllerDoArmorDamage {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] PulsedRaycastMuzzle.StopFire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(PulsedRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(ShipController)).Where(method => method.Name.ToLower().Contains("__dorpcdoarmordamage")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void DamageInstanceArmor(ShipController __instance, float damage) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      Recorder.Instance.ShipRecorder.AddDamageInstance(__instance, null, damage);
    }
  }
}