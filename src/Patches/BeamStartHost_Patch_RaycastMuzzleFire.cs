using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamStartHost_Patch_RaycastMuzzleFire {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] RaycastMuzzle.Fire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle)).Where(method => method.Name.ToLower().Contains("fire")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void BeamStart_Host(RaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      Recorder.Instance.BeamRecorder.StartBeam(__instance);
    }
  }
}
