using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;

namespace BBRMod {
  [HarmonyPatch]
  class BeamStartClient_Patch_RaycastMuzzleFireEffect {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] RaycastMuzzle.FireEffect patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle)).Where(method => method.Name.ToLower().Contains("fireeffect")).Cast<MethodBase>().First();
    }

    //Host: working
    //Client: working
    [HarmonyPostfix]
    static void BeamStart_Client(RaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      Recorder.Instance.BeamRecorder.StartBeam(__instance);
    }
  }
}

/*
    //Host: working
    //Client: working
    [HarmonyPatch(typeof(Ships.RaycastMuzzle), "FireEffect")]
    [HarmonyPostfix]
    static void BeamStart(Ships.RaycastMuzzle __instance) {
      Recorder.Instance.BeamRecorder.StartBeam(__instance);
    }
*/