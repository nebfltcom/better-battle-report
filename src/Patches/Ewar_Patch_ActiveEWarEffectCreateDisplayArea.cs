using HarmonyLib;
using System.Reflection;
using Game;
using Game.EWar;
using System.Linq;
using UnityEngine;
using Utility;

namespace BBRMod {
  [HarmonyPatch]
  class Ewar_Patch_ActiveEWarEffectCreateDisplayArea {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] ActiveEWarEffect.CreateDisplayArea patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(ActiveEWarEffect))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(ActiveEWarEffect)).Where(method => method.Name.ToLower().Contains("createdisplayarea")).Cast<MethodBase>().First();
    }

    //EWar
    //Host: working
    //Client: working
    [HarmonyPostfix]
    static void CaptureEwar(ActiveEWarEffect __instance, bool ____propertiesSet) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      
      if(!____propertiesSet) {
        return;
      }
      try {
        Recorder.Instance.EWarRecorder.RegisterEWar(__instance);
      }
      catch (System.Exception err) {
        Debug.Log("[BBRMod] error in CaptureEwar() : " + err);
      }
    }

    //Host: working
    //Client: working
    //NOTE: The DisplayArea is never fully constructed if the local player is not on the same team as the ewar instance, so we have to do it ourselves
    [HarmonyPrefix]
    static void CaptureEnemyEwarMesh(ActiveEWarEffect __instance,
      bool ____areaDisplayCreated, bool ____propertiesSet,
      bool ____omnidirectional, float ____maxRange,
      float ____coneFov, Mesh ____omnidirectionalSphereMesh) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      try {
        if (____areaDisplayCreated || !____propertiesSet || __instance.OwnedBy == null) {
          return;
        }

        if (__instance.OwnedBy.IsOnLocalPlayerTeam || SkirmishGameManager.Instance.LocalPlayer.IsSpectator) { return; }

        var mesh = (Mesh)null;
        var radius = 0f;
        var posOffset = new Vector3(0f, 0f, 0f);

        if (____omnidirectional) { //return a null mesh and use that as omni-flag
          radius = ____maxRange;
        }
        else {
          mesh = Procedural.ProceduralMesh.CreateCone(new ConeDescriptor(____maxRange, ____coneFov, 10f), 30, endcaps: true, reverseWinding: true);
          posOffset = new Vector3(0f, 0f, 10f);
        }
        Recorder.Instance.EWarRecorder.ConstructEWar(__instance, mesh, radius, posOffset);
      }
      catch(System.Exception err) {
        Debug.Log("[BBRMod] error in CaptureEnemyEwarMesh() : " + err);
      }
    }
  }
}

/*
    //EWar
    //Host: working
    //Client: working
    [HarmonyPatch(typeof(Game.EWar.ActiveEWarEffect), "CreateDisplayArea")]
    [HarmonyPostfix]
    static void CaptureAllyEwar(Game.EWar.ActiveEWarEffect __instance, bool ____propertiesSet) {
      if(!____propertiesSet)
        return;
      try {
        Recorder.Instance.EWarRecorder.RegisterEWar(__instance);
      }
      catch (System.Exception err) {
        Debug.Log("[BBRMod] error in AllyEwar() : " + err);
      }
    }

    //Host: working
    //Client: working
    //NOTE: The DisplayArea is never fully constructed if the local player is not on the same team as the ewar instance, so we have to do it ourselves
    [HarmonyPatch(typeof(Game.EWar.ActiveEWarEffect), "CreateDisplayArea")]
    [HarmonyPrefix]
    static void CaptureEnemyEwar(Game.EWar.ActiveEWarEffect __instance,
      bool ____areaDisplayCreated, bool ____propertiesSet,
      bool ____omnidirectional, float ____maxRange,
      float ____coneFov, Mesh ____omnidirectionalSphereMesh) {

      try {
        if (____areaDisplayCreated || !____propertiesSet || __instance.OwnedBy == null) {
          return;
        }
        else if (!__instance.OwnedBy.IsOnLocalPlayerTeam && !Game.SkirmishGameManager.Instance.LocalPlayer.IsSpectator) {
          var mesh = (Mesh)null;
          var scale = new Vector3(1f, 1f, 1f);
          var posOffset = new Vector3(0f, 0f, 0f);

          if (____omnidirectional) {
            ExtensionMethods.MeshCopyTo(____omnidirectionalSphereMesh, ref mesh);
            scale = new Vector3(____maxRange, ____maxRange, ____maxRange) * 2f;
          }
          else {
            mesh = Procedural.ProceduralMesh.CreateCone(new ConeDescriptor(____maxRange, ____coneFov, 10f), 30, endcaps: true, reverseWinding: true);
            posOffset = new Vector3(0f, 0f, 10f);
          }
          Recorder.Instance.EWarRecorder.ConstructEWar(__instance, mesh, scale, posOffset);
        }
      }
      catch(Exception err) {
        Debug.Log("[BBRMod] error in EnemyEwar() : " + err);
      }
    }

    //OSP active radars
    //Host: working
    //Client: working
    //NOTE: The active radars used by the OSP are in the same space as EWar to the player, but under the hood they are effectively an entirely different system
    [HarmonyPatch(typeof(Ships.OrderableActiveSensorComponent), "UpdateDisplayAreaVisibility")]
    [HarmonyPostfix]
    static void ActiveSensorChanged(Ships.OrderableActiveSensorComponent __instance, MeshFilter ____areaDisplay) {
      if( ! Recorder.Instance.IsRunning ) { return; } //UpdateDisplayAreaVisibility is called during initial setup when the mounts are initially spawned in, which causes exceptions

      try {
        Recorder.Instance.EWarRecorder.HandleOrderedSensor(__instance, __instance.Enabled && __instance.IsDoingWork);
      }
      catch (Exception err) {
        Debug.Log("[BBRMod] Error in ActiveSensorChanged() : " + err);
      }
    }
*/