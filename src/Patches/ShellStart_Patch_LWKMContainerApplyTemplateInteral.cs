using HarmonyLib;
using System.Reflection;
using Munitions;
using System.Linq;
using UnityEngine;
using Utility;

namespace BBRMod {
  [HarmonyPatch]
  class ShellStart_Patch_LWKMContainerApplyTemplateInteral {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] LightweightKineticMunitionContainer.ApplyTemplateInternal patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(LightweightKineticMunitionContainer))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(LightweightKineticMunitionContainer)).Where(method => method.Name.ToLower().Contains("applytemplateinternal")).Cast<MethodBase>().First();
    }

    //Shells
    //Host: incorrect initial kData
    //Client: working
    //Server: working lmao
    [HarmonyPostfix]
    static void ShellLaunched_Client(LightweightKineticMunitionContainer __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (NetworkObjectPooler.Instance.IsHost && !Game.GameManager.Instance.IsDedicatedServer) { return; } //while this does work on host as well, the position data is incorrect for containers that have been pooled rather than instantiated
      try {
        Recorder.Instance.ShellRecorder.AddShell(__instance);
      } catch (System.Exception err) {
        Debug.Log("[BBRMod] error in ShellLaunched_Client() : " + err);
      }
    }
  }
}

/*
    //Shells
    //Host: incorrect initial kData
    //Client: working
    [HarmonyPatch(typeof(Munitions.LightweightKineticMunitionContainer), "ApplyTemplateInternal")]
    [HarmonyPostfix]
    static void ShellLaunched_Client(Munitions.LightweightKineticMunitionContainer __instance) {
      if (NetworkObjectPooler.Instance.IsHost) { return; } //while this does work on host as well, the position data is incorrect for containers that have been pooled rather than instantiated
      try {
        Recorder.Instance.ShellRecorder.AddShell(__instance);
      } catch (Exception err) {
        Debug.Log("[BBRMod] error in ShellLaunched_Client() : " + err);
      }
    }
*/