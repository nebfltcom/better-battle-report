using HarmonyLib;
using System.Reflection;
using UI;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class ExportInterruptCheck_Patch_MenuControllerUpdate {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] MenuController.Update patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(MenuController))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(MenuController)).Where(method => method.Name.ToLower().Contains("update")).Cast<MethodBase>().First();
    }

    //while in menus perform checks for interruptions to the write process and restart it if one is detected
    [HarmonyPostfix]
    static void MenuUpdate() {
      if (AsyncWriteManager.Instance.enabled) {} //rebuild if destroyed
      if (AsyncWriteManager.reportRequested && ! AsyncWriteManager.reportDelivered && AsyncWriteManager.reportInterrupted) {
        AsyncWriteManager.reportInterrupted = false;
        ExportStart_Patch_FullAfterActionReportExport.ExportMatchReport(AsyncWriteManager.reportName);
      }
    }
  }
}

/*
    //while in menus perform checks for interruptions to the write process and restart it if one is detected
    [HarmonyPatch(typeof(UI.MenuController), "Update")]
    [HarmonyPostfix]
    static void MenuUpdate() {
      if (AsyncWriteManager.Instance.enabled) {} //rebuild if destroyed
      if (AsyncWriteManager.reportRequested && ! AsyncWriteManager.reportDelivered && AsyncWriteManager.reportInterrupted) {
        AsyncWriteManager.reportInterrupted = false;
        Patch_FullAfterActionReportExport.ExportMatchReport(AsyncWriteManager.reportName);
      }
    }
*/