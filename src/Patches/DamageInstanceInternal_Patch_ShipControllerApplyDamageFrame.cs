using HarmonyLib;
using System.Reflection;
using Game.Units;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class DamageInstanceInternal_Patch_ShipControllerApplyDamageFrame {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] PulsedRaycastMuzzle.StopFire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(PulsedRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(ShipController)).Where(method => method.Name.ToLower().Contains("__dorpcapplydamageframe")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void DamageInstanceInternal(ShipController __instance, Ships.DamageFrame frame) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      Recorder.Instance.ShipRecorder.AddDamageInstance(__instance, frame, 0);
    }
  }
}