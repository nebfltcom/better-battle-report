using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamSinglePulseClient_Patch_SinglePulseRaycastMuzzleFireEffect {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] SinglePulseRaycastMuzzle.FireEffect patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(SinglePulseRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(SinglePulseRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("fireeffect")).Cast<MethodBase>().First();
    }

    //Beams
    //ISSUE: auroras sometimes call StartBeam but end up with no kinematic data.
    //  -seems to be a timing issue, is very inconsistent. can happen as client or host
    //  -may be caused by ____beamLength == 0.0f
    //  -may be caused by the effect being fired, but stopping before a ray is actually cast (may be able to solve by deep-dive into the muzzle code)
    //  -auroras tend to have a behaviour where they fire one more time even if their target has been destroyed? perhaps that's the effect firing but not a raycast
    //ISSUE: beams with only one kData point don't seem to be displayed in the viewer. maybe something wrong, or need to make a minimum display time

    //Host: working
    //Client: working
    [HarmonyPostfix]
    static void BeamHandleSinglePulse_Client(SinglePulseRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      //Debug.Log("[BBRMod] BeamHandleSinglePulse_Client");

      float raycastRange = (float)ProtectedData.GetPrivateField(__instance, "_raycastRange");

      Recorder.Instance.BeamRecorder.StartBeam(__instance);
      Recorder.Instance.BeamRecorder.UpdateBeam(__instance, __instance.transform.position, __instance.transform.forward, raycastRange, ((IDirectDamageMuzzle)__instance).DamagePeriod);
      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
  }
}

/*
    //Beams
    //ISSUE: auroras sometimes call StartBeam but end up with no kinematic data.
    //  -seems to be a timing issue, is very inconsistent. can happen as client or host
    //  -may be caused by ____beamLength == 0.0f
    //  -may be caused by the effect being fired, but stopping before a ray is actually cast (may be able to solve by deep-dive into the muzzle code)
    //  -auroras tend to have a behaviour where they fire one more time even if their target has been destroyed? perhaps that's the effect firing but not a raycast
    //ISSUE: beams with only one kData point don't seem to be displayed in the viewer. maybe something wrong, or need to make a minimum display time

    //SinglePulseRaycastMuzzle special case: SinglePulseRaycastMuzzle (used for new Grazer) works a little differently; they don't call base.FireEffect(), and they don't call DoRaycast() clientside
    //Host: working
    //Client: working
    //TODO: test this with PulsedRaycastMuzzle as well, see if it solves the aurora problem
    [HarmonyPatch(typeof(Ships.SinglePulseRaycastMuzzle), "FireEffect")]
    [HarmonyPostfix]
    static void BeamHandleSinglePulse(Ships.SinglePulseRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      float raycastRange = (float)ProtectedData.GetPrivateField(__instance, "_raycastRange");

      Recorder.Instance.BeamRecorder.StartBeam(__instance);
      Recorder.Instance.BeamRecorder.UpdateBeam(__instance, __instance.transform.position, __instance.transform.forward, raycastRange, ((Ships.IDirectDamageMuzzle)__instance).DamagePeriod);
      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
*/