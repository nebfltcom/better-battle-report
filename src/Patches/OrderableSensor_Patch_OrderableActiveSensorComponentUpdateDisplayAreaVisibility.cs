using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;
using Utility;

namespace BBRMod {
  [HarmonyPatch]
  class OrderableSensor_Patch_OrderableActiveSensorComponentUpdateDisplayAreaVisibility {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] OrderableActiveSensorComponent.UpdateDisplayAreaVisibility patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(OrderableActiveSensorComponent))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(OrderableActiveSensorComponent)).Where(method => method.Name.ToLower().Contains("updatedisplayareavisibility")).Cast<MethodBase>().First();
    }

    //OSP active radars
    //Host: working
    //Client: working
    //NOTE: The active radars used by the OSP are in the same space as EWar to the player, but under the hood they are effectively an entirely different system
    [HarmonyPostfix]
    static void ActiveSensorChanged(OrderableActiveSensorComponent __instance, MeshFilter ____areaDisplay) {
      if( ! Recorder.Instance.IsRunning ) { return; } //UpdateDisplayAreaVisibility is called during initial setup when the mounts are initially spawned in, which causes exceptions

      try {
        Recorder.Instance.EWarRecorder.HandleOrderedSensor(__instance, __instance.Enabled && __instance.IsDoingWork);
      }
      catch (System.Exception err) {
        Debug.Log("[BBRMod] Error in ActiveSensorChanged() : " + err);
      }
    }
  }
}

/*
    //OSP active radars
    //Host: working
    //Client: working
    //NOTE: The active radars used by the OSP are in the same space as EWar to the player, but under the hood they are effectively an entirely different system
    [HarmonyPatch(typeof(Ships.OrderableActiveSensorComponent), "UpdateDisplayAreaVisibility")]
    [HarmonyPostfix]
    static void ActiveSensorChanged(Ships.OrderableActiveSensorComponent __instance, MeshFilter ____areaDisplay) {
      if( ! Recorder.Instance.IsRunning ) { return; } //UpdateDisplayAreaVisibility is called during initial setup when the mounts are initially spawned in, which causes exceptions

      try {
        Recorder.Instance.EWarRecorder.HandleOrderedSensor(__instance, __instance.Enabled && __instance.IsDoingWork);
      }
      catch (Exception err) {
        Debug.Log("[BBRMod] Error in ActiveSensorChanged() : " + err);
      }
    }
*/