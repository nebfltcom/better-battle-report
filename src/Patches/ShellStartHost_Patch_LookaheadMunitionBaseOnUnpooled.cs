using HarmonyLib;
using System.Reflection;
using Munitions;
using System.Linq;
using UnityEngine;
using Utility;

namespace BBRMod {
  [HarmonyPatch]
  class ShellStart_Patch_LookaheadMunitionBaseOnUnpooled {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] LookaheadMunitionBase.OnUnpooled patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(LookaheadMunitionBase))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(LookaheadMunitionBase)).Where(method => method.Name.ToLower().Contains("onunpooled")).Cast<MethodBase>().First();
    }

    //Shells
    //Host: working
    //Client: NRE __instance
    //Server: _appliedTemplate is null
    [HarmonyPostfix]
    static void ShellLaunched_Host(LookaheadMunitionBase __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if ( ! NetworkObjectPooler.Instance.IsHost) { return; }
      if (Game.GameManager.Instance.IsDedicatedServer) { return; }
      if(__instance is LightweightKineticMunitionContainer) {
        Recorder.Instance.ShellRecorder.AddShell(__instance);
      }
    }
  }
}

/*
    //Shells
    //Host: working
    //Client: NRE __instance
    [HarmonyPatch(typeof(Munitions.LookaheadMunitionBase), "OnUnpooled")]
    [HarmonyPostfix]
    static void ShellLaunched(Munitions.LookaheadMunitionBase __instance) {
      if ( ! NetworkObjectPooler.Instance.IsHost) { return; }
      if(__instance is Munitions.LightweightKineticMunitionContainer) {
        Recorder.Instance.ShellRecorder.AddShell(__instance);
      }
    }
*/