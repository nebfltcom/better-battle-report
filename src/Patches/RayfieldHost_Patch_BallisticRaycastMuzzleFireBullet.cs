using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class RayfieldHost_Patch_BallisticRaycastMuzzleFireBullet {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] BallisticRaycastMuzzle.FireBullet patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(BallisticRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(BallisticRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("firebullet")).Cast<MethodBase>().First();
    }

    [HarmonyPrefix]
    static void SteppedRaycastStart_Host(Ships.BallisticRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if ( ! Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      Recorder.Instance.RaycastRecorder.StartCapture(__instance);
    }

    [HarmonyPostfix]
    static void SteppedRaycastStop_Host(Ships.BallisticRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if ( ! Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      Recorder.Instance.RaycastRecorder.StopCapture(__instance);
    }
  }
}


/*
    [HarmonyPatch(typeof(Utility.MathHelpers), "RandomRayInCone")]
    [HarmonyPostfix]
    static void SteppedRaycastCapture(Vector3 __result) {
      if ( ! NetworkObjectPooler.Instance.IsHost) { return; } //probably won't be called, but no guarantee
      if (Recorder.Instance.RaycastRecorder.IsCapturing) {
        Recorder.Instance.RaycastRecorder.UpdateNonCylinderCast(__result);
      }
    }
*/