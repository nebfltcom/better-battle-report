using HarmonyLib;
using System.Reflection;
using Ships;
using Game.Units;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class ReactorBloom_Patch_InternalExplosionTriggerTriggerInternal {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] InternalExplosionTrigger.TriggerInternal patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(InternalExplosionTrigger))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(InternalExplosionTrigger)).Where(method => method.Name.ToLower().Contains("triggerinternal")).Cast<MethodBase>().First();
    }

    //ISSUE: sometimes results in an NRE on p2p client? only happened once
    [HarmonyPostfix]
    static void ReactorBloom(ShipController ship, float ____explosionRadius, GameObject ____spawnEffect) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      try {
        /*Debug.Log("[BBRMod] Reactor Bloom _spawnEffect component list:");
        foreach (var component in ____spawnEffect.GetComponents(typeof(Component))) {
          if (component == null) {
            Debug.Log($"\tcomponent is null");
            continue;
          }
          Debug.Log($"\ttype: {component.GetType()}");
        }*/

        var expComponent = ____spawnEffect.GetComponent<Munitions.AOEExplosionEffectModule>(); //ISSUE: this is null on dedicated servers
        float radius = ____explosionRadius;
        if (expComponent != null) { //I had a strange NRE where null was returned to expComponent. I was a spectating client, but i had been before and i don't recall changing this code between the two occurances
          radius = (float)ProtectedData.GetPrivateField(expComponent, "_blastRadius");
        }
        else {
          Debug.Log("[BBRMod] Unable to retrieve reactor bloom blast radius. Using the default 150.");
        }
        if (ship?.transform?.position != null) {
          Recorder.Instance.ExplosionRecorder.AddExplosion(ship.transform.position, radius);
          if (expComponent != null) {
            Recorder.Instance.EWarRecorder.AddReactorBloomEWar(expComponent.gameObject.GetComponent<Effects.Modules.SpawnEWarEffectModule>(), ship.transform.position); //ISSUE: NRE on dedicated servers
          }
        } else {
          Debug.Log("[BBRMod] Unable to find the location of the reactor bloom. This explosion will not appear in the battle report. Please send this log to ShadowLotus in the Discord.");
          Debug.Log($"\t ship is null? {ship == null}");
          Debug.Log($"\t ship.transform is null? {ship?.transform == null}");
          Debug.Log($"\t ship.transform.position is null? {ship?.transform?.position == null}");
        }
      } catch (System.Exception err) {
        Debug.Log("[BBRMod] Error in ReactorBloom() : " + err);
      }
    }
  }
}

/*
    //Reactor Blooms
    //ISSUE: sometimes results in an NRE? Inconsistent
    [HarmonyPatch(typeof(Ships.InternalExplosionTrigger), "TriggerInternal")]
    [HarmonyPostfix]
    static void ReactorBloom(ShipController ship, float ____explosionRadius, GameObject ____spawnEffect) {
      try {
        var expComponent = ____spawnEffect.GetComponent<Munitions.AOEExplosionEffectModule>();
        float radius = ____explosionRadius;
        if (expComponent != null) { //I had a strange NRE where null was returned to component. I was a spectating client, but i had been before and i don't recall changing this code between the two occurances
          radius = (float)ProtectedData.GetPrivateField(expComponent, "_blastRadius");
        }
        else {
          Debug.Log("[BBRMod] Unable to retrieve reactor bloom blast radius. Using the default 150.");
        }
        if (ship?.transform?.position != null) {
          Recorder.Instance.ExplosionRecorder.AddExplosion(ship.transform.position, radius);
          Recorder.Instance.EWarRecorder.AddReactorBloomEWar(expComponent.gameObject.GetComponent<Effects.Modules.SpawnEWarEffectModule>(), ship.transform.position);
        } else {
          Debug.Log("[BBRMod] Unable to find the location of the reactor bloom. This explosion will not appear in the battle report. Please send this log to ShadowLotus in the Discord.");
          Debug.Log($"\t ship is null? {ship == null}");
          Debug.Log($"\t ship.transform is null? {ship?.transform == null}");
          Debug.Log($"\t ship.transform.position is null? {ship?.transform?.position == null}");
        }
      } catch (Exception err) {
        Debug.Log("[BBRMod] Error in ReactorBloom() : " + err);
      }
    }
*/