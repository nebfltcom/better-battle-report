using HarmonyLib;
using System.Reflection;
using System.Linq;
using UnityEngine;
using Game.Reports;
using System.IO;

namespace BBRMod {
  [HarmonyPatch]
  class ExportStart_Patch_FullAfterActionReportExport {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] FullAfterActionReport.Export patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(FullAfterActionReport))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(FullAfterActionReport)).Where(method => method.Name.ToLower().Contains("export")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    internal static void ExportMatchReport(string filename) {
      AsyncWriteManager.reportRequested = true;
      AsyncWriteManager.reportName = filename;
      Debug.Log("[BBRMod] Battle report requested, writing data");

      string newCompressedFileName = filename.Replace("xml", "bbr");
      if ( ! newCompressedFileName.EndsWith(".bbr") ) {
        newCompressedFileName += ".bbr";
      }

      //AsyncWriteManager.Instance.StartSerializeWrite_DEPRECATED(newCompressedFileName, BBRMod.DEBUG);
      AsyncWriteManager.Instance.WriteFromBytes(newCompressedFileName);
      //AsyncWriteManager.Instance.StartSerialization();
    }
  }
}

/*
    [HarmonyPatch(typeof(Game.Reports.FullAfterActionReport), "Export")]
    [HarmonyPostfix]
    static void ExportMatchReport(string filename) {
      AsyncWriteManager.reportRequested = true;
      AsyncWriteManager.reportName = filename;
      Debug.Log("[BBRMod] Battle report requested, writing data");

      string newCompressedFileName = filename.Replace("xml", "bbr");
      if ( ! newCompressedFileName.EndsWith(".bbr") ) {
        newCompressedFileName += ".bbr";
      }

      AsyncWriteManager.Instance.StartSerializeWrite(newCompressedFileName, DEBUG);
    }
*/