using HarmonyLib;
using System.Reflection;
using Munitions;
using System.Linq;
using UnityEngine;
using Utility;

namespace BBRMod {
  [HarmonyPatch]
  class ShellStop_Patch_NetworkPoolableDoRpcRepoolAfterDelay {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] NetworkPoolable.__DoRpcRepoolAfterDelay patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(NetworkPoolable))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(NetworkPoolable)).Where(method => method.Name.ToLower().Contains("__dorpcrepoolafterdelay")).Cast<MethodBase>().First();
    }

    //Shells
    //Host: working
    //Client: working
    //NOTE: will fire upon many other events, including missile death
    [HarmonyPostfix]
    static void ShellFinished(NetworkPoolable __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (__instance is LightweightKineticMunitionContainer container) {
        try {
          Recorder.Instance.ShellRecorder.RemoveShell(container);
        } catch (System.Exception err) {
          Debug.Log("[BBRMod] error in ShellFinished : " + err);
        }
      } 
    }
  }
}

/*
    //Shells
    //Host: working
    //Client: working
    //NOTE: will fire upon many other events, including missile death
    [HarmonyPatch(typeof(Utility.NetworkPoolable), "__DoRpcRepoolAfterDelay")]
    [HarmonyPostfix]
    static void ShellFinished(Utility.NetworkPoolable __instance) {
      if (__instance is Munitions.LightweightKineticMunitionContainer container) {
        try {
          Recorder.Instance.ShellRecorder.RemoveShell(container);
        } catch (Exception err) {
          Debug.Log("[BBRMod] error in ShellFinished : " + err);
        }
      } 
    }
*/