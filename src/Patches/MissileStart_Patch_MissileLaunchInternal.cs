using HarmonyLib;
using System.Reflection;
using Munitions;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class MissileStart_Patch_MissileLaunchInternal {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] Missile.LaunchInternal patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(Missile))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(Missile)).Where(method => method.Name.ToLower().Contains("launchinternal")).Cast<MethodBase>().First();
    }

    //Missiles
    //Host: working
    //Client: working
    //NOTE: I've had odd instances of this failing. Unreproducable so far
    [HarmonyPostfix]
    static void MissileLaunched(Missile __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      Recorder.Instance.MissileRecorder.AddMissile(__instance);
    }
  }
}

/*
    //Missiles
    //Host: working
    //Client: working
    //NOTE: I've had odd instances of this failing. Unreproducable so far
    [HarmonyPatch(typeof(Munitions.Missile), "LaunchInternal")]
    [HarmonyPostfix]
    static void MissileLaunched(Munitions.Missile __instance) {
      Recorder.Instance.MissileRecorder.AddMissile(__instance);
    }
*/