using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamUpdate_Patch_RaycastMuzzleDoRaycast {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] RaycastMuzzle.DoRaycast patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle)).Where(method => method.Name.ToLower().Contains("doraycast")).Cast<MethodBase>().First();
    }

    //Beams
    //ISSUE: auroras sometimes call StartBeam but end up with no kinematic data.
    //  -seems to be a timing issue, is very inconsistent. can happen as client or host
    //  -may be caused by ____beamLength == 0.0f
    //  -may be caused by the effect being fired, but stopping before a ray is actually cast (may be able to solve by deep-dive into the muzzle code)
    //  -auroras tend to have a behaviour where they fire one more time even if their target has been destroyed? perhaps that's the effect firing but not a raycast
    //ISSUE: beams with only one kData point don't seem to be displayed in the viewer. maybe something wrong, or need to make a minimum display time
    //RESOLUTION: this is either a base game bug or intended behaviour. The first damaging raycast isn't fired until the next FixedUpdate after the Fire and FireEffect calls

    //Host: working
    //Client: working (but not for SinglePulseRaycastMuzzle)
    [HarmonyPostfix]
    static void BeamUpdate(RaycastMuzzle __instance, Vector3 position, Vector3 direction, float ____beamLength) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      
      try {
        if(____beamLength == 0.0f) { //ISSUE: does this destroy beams erroneously? also never seems to actually proc basically ever :/
          Recorder.Instance.BeamRecorder.StopBeam(__instance);
        }
        else {
          Recorder.Instance.BeamRecorder.UpdateBeam(__instance, position, direction, ____beamLength);
        }
      } catch (System.Exception err) {
        Debug.Log("[BBRMod] error in BeamUpdate() : " + err);
      }
    }
  }
}

/*
    //Host: working
    //Client: working (but not for SinglePulseRaycastMuzzle; see below)
    [HarmonyPatch(typeof(Ships.RaycastMuzzle), "DoRaycast")]
    [HarmonyPostfix]
    static void BeamUpdate(Ships.RaycastMuzzle __instance, Vector3 position, Vector3 direction, float ____beamLength) {
      
      try {
        if(____beamLength == 0.0f) //ISSUE: does this destroy beams on start sometimes?
          Recorder.Instance.BeamRecorder.StopBeam(__instance);
        else
          Recorder.Instance.BeamRecorder.UpdateBeam(__instance, position, direction, ____beamLength);
        
      } catch (Exception err) {
        Debug.Log("[BBRMod] error in BeamUpdate() : " + err);
      }
    }
*/