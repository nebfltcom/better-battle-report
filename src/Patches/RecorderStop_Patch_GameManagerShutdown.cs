using HarmonyLib;
using System.Reflection;
using Game;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class RecorderStop_Patch_GameManagerShutdown {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] GameManager.Shutdown patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(GameManager))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(GameManager)).Where(method => method.Name.ToLower().Contains("shutdown")).Cast<MethodBase>().First();
    }

    [HarmonyPrefix]
    static void Shutdown_Patch(GameManager __instance) {
      if(__instance is SkirmishGameManager) {
        Recorder.Instance.MatchStop();
      }
    }
  }
}

/*
    [HarmonyPatch(typeof(Game.GameManager), "Shutdown")]
    [HarmonyPrefix]
    static void GameShutdown(Game.GameManager __instance) {
      if(__instance is Game.SkirmishGameManager) {
        Recorder.Instance.MatchStop();
      }
    }
*/