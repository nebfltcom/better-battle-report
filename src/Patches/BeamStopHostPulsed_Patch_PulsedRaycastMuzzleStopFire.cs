using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamStopHostPulsed_Patch_PulsedRaycastMuzzleStopFire {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] PulsedRaycastMuzzle.StopFire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(PulsedRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(PulsedRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("stopfire")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void BeamStopPulsed_Host(PulsedRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
  }
}