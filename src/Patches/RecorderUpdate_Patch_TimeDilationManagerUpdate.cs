using HarmonyLib;
using System.Reflection;
using Game;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class RecorderUpdate_Patch_TimeDilationManagerUpdate {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] TimeDilationManager.FixedUpdate patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(TimeDilationManager))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(TimeDilationManager)).Where(method => method.Name.ToLower().Contains("fixedupdate")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void MatchUpdate() {
      if (Game.SkirmishGameManager.Instance != null) { //I think this is what caused FixedUpdate() NREs during deployment
        Recorder.Instance.Update();
      }
    }
  }
}

/*
    [HarmonyPatch(typeof(Game.TimeDilationManager), "FixedUpdate")]
    [HarmonyPostfix]
    static void MatchUpdate() {
      if (Game.SkirmishGameManager.Instance != null) { //I think this is what caused FixedUpdate() NREs during deployment
        Recorder.Instance.Update();
      }
    }
*/