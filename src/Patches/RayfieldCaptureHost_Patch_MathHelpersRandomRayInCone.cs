using HarmonyLib;
using System.Reflection;
using Utility;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class RayfieldCaptureHost_Patch_MathHelpersRandomRayInCone {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] MathHelpers.RandomRayInCone patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(MathHelpers))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(MathHelpers)).Where(method => method.Name.ToLower().Contains("randomrayincone")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void SteppedRaycastCapture_Host(Vector3 __result) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if ( ! NetworkObjectPooler.Instance.IsHost) { return; } //probably won't be called, but no guarantee
      if (Recorder.Instance.RaycastRecorder.IsCapturing) {
        Recorder.Instance.RaycastRecorder.UpdateNonCylinderCast(__result);
      }
    }
  }
}