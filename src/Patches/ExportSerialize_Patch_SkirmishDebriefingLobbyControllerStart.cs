using HarmonyLib;
using System.Reflection;
using Game;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class ExportSerialize_Patch_SkirmishDebriefingLobbyControllerStart {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] SkirmishDebriefingLobbyController.Start patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(SkirmishDebriefingLobbyController))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(SkirmishDebriefingLobbyController)).Where(method => method.Name.ToLower().Contains("start")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void DedicatedServerSerialize_Auto(SkirmishDebriefingLobbyController __instance) {
      //handle recording the victor here
      Game.Reports.FullAfterActionReport report = (Game.Reports.FullAfterActionReport)ProtectedData.GetPrivateField(__instance, "_report");
      Recorder.Instance.RecordVictor(report.WinningTeam.ToString());

      if (!Game.GameManager.Instance.IsDedicatedServer) { return; } //TODO: figure out why this breaks everything

      AsyncWriteManager.Instance.StartSerialization();

      if (BBRMod.autoSave) {
        string filename = "Skirmish Report - " + (report.Multiplayer ? "MP" : "SP") + " - " + report.Time.ToString("dd-MMM-yyyy HH-mm-ss") + ".bbr";
        AsyncWriteManager.reportRequested = true;
        AsyncWriteManager.reportName = filename;
        AsyncWriteManager.filePath = BBRMod.saveDir;
        Debug.Log("[BBRMod] Battle report requested");
      }
    }
  }
}
