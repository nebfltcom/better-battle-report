using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamSinglePulseHost_Patch_SinglePulseRaycastMuzzleFire {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] SinglePulseRaycastMuzzle.fire patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(SinglePulseRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(SinglePulseRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("fire")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void BeamHandleSinglePulse_Host(SinglePulseRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }

      float raycastRange = (float)ProtectedData.GetPrivateField(__instance, "_raycastRange");

      Recorder.Instance.BeamRecorder.StartBeam(__instance);
      Recorder.Instance.BeamRecorder.UpdateBeam(__instance, __instance.transform.position, __instance.transform.forward, raycastRange, ((IDirectDamageMuzzle)__instance).DamagePeriod);
      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
  }
}