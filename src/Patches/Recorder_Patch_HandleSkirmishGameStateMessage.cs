using HarmonyLib;
using System.Reflection;
using Game;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class Recorder_Patch_HandleSkirmishGameStateMessage {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] SkirmishGameManager.HandleGameStateMessage patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(SkirmishGameManager))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(SkirmishGameManager)).Where(method => method.Name.ToLower().Contains("handlegamestatemessage")).Cast<MethodBase>().First();
    }

    [HarmonyPostfix]
    static void State_Patch(Networking.SkirmishGameStateMessage message) {
      if(message.NewState.ToString() == "TransferringFleets") {
        //the map precache is something of a relic, but it does make the pause of loading busy maps (so far the highest is 4.7s) take place in loading rather than deployment
        Debug.Log("[BBRMod] Previous data cleared. Map loaded. Begin terrain precaching.");
        Recorder.Instance.Clear();
        Recorder.Instance.PreCacheMapMeshes();
      }
      if(message.NewState.ToString() == "Running") {
        Recorder.Instance.MatchStart();
      }
      else if(message.NewState.ToString() == "Finished") {
        Recorder.Instance.MatchStop();
      }
    }
  }
}

/*
    [HarmonyPatch(typeof(Game.SkirmishGameManager), "HandleGameStateMessage")]
    [HarmonyPrefix]
    static void MatchState(Networking.SkirmishGameStateMessage message) {
      if(message.NewState.ToString() == "TransferringFleets") {
        //the map precache is something of a relic, but it does make the pause of loading busy maps (so far the highest is 4.7s) take place in loading rather than deployment
        Debug.Log("[BBRMod] Previous data cleared. Map loaded. Begin terrain precaching.");
        Recorder.Instance.Clear();
        Recorder.Instance.PreCacheMapMeshes();
      }
      if(message.NewState.ToString() == "Running") {
        Recorder.Instance.MatchStart();
      }
      else if(message.NewState.ToString() == "Finished") {
        Recorder.Instance.MatchStop();
      }
    }
*/