using HarmonyLib;
using System.Reflection;
using Ships;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class BeamStopClient_Patch_RaycastMuzzleStopFireEffect {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] RaycastMuzzle.StopFireEffect patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(RaycastMuzzle)).Where(method => method.Name.ToLower().Contains("stopfireeffect")).Cast<MethodBase>().First();
    }

    //Beams
    //ISSUE: auroras sometimes call StartBeam but end up with no kinematic data.
    //  -seems to be a timing issue, is very inconsistent. can happen as client or host
    //  -may be caused by ____beamLength == 0.0f
    //  -may be caused by the effect being fired, but stopping before a ray is actually cast (may be able to solve by deep-dive into the muzzle code)
    //  -auroras tend to have a behaviour where they fire one more time even if their target has been destroyed? perhaps that's the effect firing but not a raycast
    //ISSUE: beams with only one kData point don't seem to be displayed in the viewer. maybe something wrong, or need to make a minimum display time

    //Host: working
    //Client: working
    [HarmonyPostfix]
    static void BeamStop_Client(RaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; } //StopFireEffect is called during initial setup when the mounts are initially spawned in, as well as in the fleet editor, which causes exceptions
      if (Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
  }
}

/*
    //Beams
    //ISSUE: auroras sometimes call StartBeam but end up with no kinematic data.
    //  -seems to be a timing issue, is very inconsistent. can happen as client or host
    //  -may be caused by ____beamLength == 0.0f
    //  -may be caused by the effect being fired, but stopping before a ray is actually cast (may be able to solve by deep-dive into the muzzle code)
    //  -auroras tend to have a behaviour where they fire one more time even if their target has been destroyed? perhaps that's the effect firing but not a raycast
    //ISSUE: beams with only one kData point don't seem to be displayed in the viewer. maybe something wrong, or need to make a minimum display time

    //Host: working
    //Client: working
    [HarmonyPatch(typeof(Ships.RaycastMuzzle), "StopFireEffect")]
    [HarmonyPostfix]
    static void BeamStop(Ships.RaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; } //StopFireEffect is called during initial setup when the mounts are initially spawned in, as well as in the fleet editor, which causes exceptions
      Recorder.Instance.BeamRecorder.StopBeam(__instance);
    }
*/