using HarmonyLib;
using System.Reflection;
using Ships;
using Game.Units;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  [HarmonyPatch]
  class RayfieldStartClient_Patch_RepeatingBallisticRaycastMuzzleFireEffect {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] RepeatingBallisticRaycastMuzzle.FireEffect patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(RepeatingBallisticRaycastMuzzle))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(RepeatingBallisticRaycastMuzzle)).Where(method => method.Name.ToLower().Contains("fireeffect")).Cast<MethodBase>().First();
    }

    //Non-physical stepped raycast weapons  (Defender/Pavise)
    //Host: working
    //Client: working, but latency skewed
    //TODO: look into adjusting for latency
    [HarmonyPostfix]
    static void SteppedRaycastStart_Client(RepeatingBallisticRaycastMuzzle __instance) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (Utility.NetworkObjectPooler.Instance.IsHost) { return; }

      try {
        Recorder.Instance.RaycastRecorder.VFXCaptureStart(__instance);
      } catch (System.Exception err) {
        Debug.Log("[BBRMod] Error in SteppedRaycastStart() : " + err);
      }
    }
  }
}

/*
    //Non-physical stepped raycast weapons  (Defender/Pavise)
    //We can't track individual bullets because, while they do exist, their class (BallisticRaycastMuzzle.RaycastBullet) is private
    //  Instead we have a single ray for every one fired. Still a lot of data - people regularly take over 10,000 of these on a single ship - but it's the smallest amount of data we can get away with and still track every shot.
    //  If absolute accuracy isn't desired we could cut the data into as little as 1/4 and still retain reasonable accuracy.
    //   more correctly it would be a shift from updating every 0.02s to updating every 0.1s. Defender/Pavise fire 1 bullet per 0.025s (per muzzle), which is effectively a 4 round burst over 0.1s
    //NOTE: Host we can track each individual ray as it actually is seen by the game. Client we cannot, but can approximate it by generating rays in the same way at the same timing as the VFX is displayed.
    //  -We can remove this implementation and only use the VFX trigger if seeing the accurate rays - even if only for the host's recording - is not important enough to warrant a seperate code path.

    //TODO: check if simType == Stepped Raycast? Not a problem for vanilla as of now but you know what modders are like
    //Ships.BallisticRaycastMuzzle.FireEffect() (method #2)
    //  -can't track individual bullets
    //Host: working
    //Client: working, but latency skewed
    //TODO: look into adjusting for latency
    [HarmonyPatch(typeof(Ships.RepeatingBallisticRaycastMuzzle), "FireEffect")]
    [HarmonyPostfix]
    static void SteppedRaycastStart__Client(Ships.RepeatingBallisticRaycastMuzzle __instance) {
      //if (NetworkObjectPooler.Instance.IsHost) { return; }
      try {
        Recorder.Instance.RaycastRecorder.VFXCaptureStart(__instance);
      } catch (Exception err) {
        Debug.Log("[BBRMod] Error in SteppedRaycastStart__Client() : " + err);
      }
    }
*/