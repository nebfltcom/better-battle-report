using HarmonyLib;
using System.Reflection;
using Game.EWar;
using System.Linq;
using UnityEngine;
using Utility;
using System;

namespace BBRMod {
  [HarmonyPatch]
  class EwarServer_Patch_ActiveEWarEffectSetParams {
    [HarmonyTargetMethod]
    static MethodBase TargetMethod()
    {
      //Debug.Log("[BBRMod] ActiveEWarEffect.SetParams patch");
      //foreach(var mb in AccessTools.GetDeclaredMethods(typeof(ActiveEWarEffect))) { Debug.Log(mb); } //DEBUG
      return AccessTools.GetDeclaredMethods(typeof(ActiveEWarEffect)).Where(method => method.Name.ToLower().Contains("setparams")).Cast<MethodBase>().First();
    }

//TODO: we could probably just use this method and always construct the cones ourselves. mid prio
//TODO: also a possibility is just not store the mesh and only store the parameters, then construct the cone mesh in the viewer. 
    [HarmonyPostfix]
    static void CaptureEwar_Server(
      ActiveEWarEffect __instance,
      bool omni,
      float fov,
      float range
    ) {
      if( ! Recorder.Instance.IsRunning ) { return; }
      if (!Game.GameManager.Instance.IsDedicatedServer) { return; }
      
      var mesh = (Mesh)null;
      var radius = 0f;
      var posOffset = new Vector3(0f, 0f, 0f);

      if (omni) { //return a null mesh and use that as omni-flag
        //ExtensionMethods.MeshCopyTo(____omnidirectionalSphereMesh, ref mesh);
        //scale = new Vector3(____maxRange, ____maxRange, ____maxRange) * 2f;
        radius = range;
      }
      else {
        //Debug.Log($"[BBRMod] range: {range}, fov {fov}");
        mesh = ProceduralCone.CreateCone(new ConeDescriptor(range, fov, 10f), 30, endcaps: true, reverseWinding: true);
        posOffset = new Vector3(0f, 0f, 10f);
      }

      try {
        Recorder.Instance.EWarRecorder.ConstructEWar(__instance, mesh, radius, posOffset);
      } catch (Exception e) {
        Debug.Log("[BBRMod] error in CaptureEwar_Server Construction.");
        Debug.Log(e);
      }
      try {
        Recorder.Instance.EWarRecorder.RegisterEWar(__instance);
      } catch (Exception e) {
        Debug.Log("[BBRMod] error in CaptureEwar_Server Registration.");
        Debug.Log(e);
      }
    }
  }
}




/*
    //EWar
    //Host: working
    //Client: working
    [HarmonyPostfix]
    static void CaptureEwar(ActiveEWarEffect __instance, bool ____propertiesSet) {
      Debug.Log("[BBRMod] CaptureEwar attempt");
      if(!____propertiesSet) {
        Debug.Log("\tfailed. Properties not set.");
        return;
      }
      try {
        Recorder.Instance.EWarRecorder.RegisterEWar(__instance);
      }
      catch (System.Exception err) {
        Debug.Log("[BBRMod] error in AllyEwar() : " + err);
      }
    }

    //Host: working
    //Client: working
    //NOTE: The DisplayArea is never fully constructed if the local player is not on the same team as the ewar instance, so we have to do it ourselves
    [HarmonyPrefix]
    static void CaptureEnemyEwarMesh(ActiveEWarEffect __instance,
      bool ____areaDisplayCreated, bool ____propertiesSet,
      bool ____omnidirectional, float ____maxRange,
      float ____coneFov, Mesh ____omnidirectionalSphereMesh) {
      Debug.Log("[BBRMod] CaptureEnemyEwarMesh attempt");

      try {
        if (____areaDisplayCreated || !____propertiesSet || __instance.OwnedBy == null) {
          Debug.Log($"\tfailed. Display created: {____areaDisplayCreated}; Properties not set: {!____propertiesSet}; Owner nulled: {__instance.OwnedBy == null}");
          return;
        }

        if (__instance.OwnedBy.IsOnLocalPlayerTeam || SkirmishGameManager.Instance.LocalPlayer.IsSpectator) { return; }

        var mesh = (Mesh)null;
        //var scale = new Vector3(1f, 1f, 1f);
        var radius = 0f;
        var posOffset = new Vector3(0f, 0f, 0f);

        if (____omnidirectional) { //return a null mesh and use that as omni-flag
          //ExtensionMethods.MeshCopyTo(____omnidirectionalSphereMesh, ref mesh);
          //scale = new Vector3(____maxRange, ____maxRange, ____maxRange) * 2f;
          radius = ____maxRange;
        }
        else {
          mesh = Procedural.ProceduralMesh.CreateCone(new ConeDescriptor(____maxRange, ____coneFov, 10f), 30, endcaps: true, reverseWinding: true);
          posOffset = new Vector3(0f, 0f, 10f);
        }
        Recorder.Instance.EWarRecorder.ConstructEWar(__instance, mesh, radius, posOffset);
      }
      catch(System.Exception err) {
        Debug.Log("[BBRMod] error in EnemyEwar() : " + err);
      }
    }
*/