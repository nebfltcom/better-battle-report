using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BBRMod {
  internal class KinematicLogger {
    private bool _zeroFirstKTime = false;
    private IdData _id;
    private List<KinematicData> _data = new List<KinematicData>();
    private KinematicData _lastData;
    public KinematicLogger(uint id, string type, bool zeroFirstKTime = false) {
      this._id = new IdData() {
        Id = id,
        Type = type
      };
      this._zeroFirstKTime = zeroFirstKTime;
    }
    public void AddData(KinematicData data, bool force = false) {
      //Force data collection if the last data point is 5 seconds old
      if(
        this._data.Count <= 1 ||
        force ||
        this._KDFilter(this._data[this._data.Count - 2], this._data[this._data.Count - 1], data) ||
        ( this._data.Last().Time + 5f < data.Time
          && !(this._data.Last().Transform.Position != null && this._data.Last().Transform.Position.Equals(data.Transform.Position))
          && !(this._data.Last().Transform.Rotation != null && this._data.Last().Transform.Rotation.Equals(data.Transform.Rotation)) )
        //if the kData is exactly the same, skip. This will save 95B(uncompressed) per object per 5 seconds of game time
        //for a 30 minute game that's 34.2K per object
        //...maybe this doesn't actually matter but i don't like them so unless it causes a problem imma leave it >:c
      ) {
        if(this._data.Count <= 0 && this._zeroFirstKTime) {
          data.Time = 0f;
        }
        this._data.Add(data);
      }
      this._lastData = data;
    }
    public KinematicLog GetData() {
      var newData = new List<KinematicData>(this._data);
      if(
        (this._data.Count() == 0 && this._lastData != null) ||
        (this._data.Count() > 0 && !object.ReferenceEquals(this._lastData, this._data.Last()))
      ) {
        newData.Add(this._lastData);
      }
      return new KinematicLog() {
        Id = this._id.Id,
        Type = this._id.Type,
        Log = newData
      };
    }
    private bool _KDFilter(KinematicData kd1, KinematicData kd2, KinematicData nkd) {
      //Create velocity vectors
      var kd1kd2Velocity = (new Vector3(kd2.Transform.Position.X, kd2.Transform.Position.Y, kd2.Transform.Position.Z) - new Vector3(kd1.Transform.Position.X, kd1.Transform.Position.Y, kd1.Transform.Position.Z)) / (kd2.Time - kd1.Time);
      var kd2nkdVelocity = (new Vector3(nkd.Transform.Position.X, nkd.Transform.Position.Y, nkd.Transform.Position.Z) - new Vector3(kd2.Transform.Position.X, kd2.Transform.Position.Y, kd2.Transform.Position.Z)) / (nkd.Time - kd2.Time);
      //Create angular velocity vectors (vectors created from euler angles)
      var kd1kd2Rotation = new Vector3(0,0,0);
      var kd2nkdRotation = new Vector3(0,0,0);
      if (kd2.Transform.Rotation != null && kd1.Transform.Rotation != null) {
        kd1kd2Rotation = (new Vector3(kd2.Transform.Rotation.X, kd2.Transform.Rotation.Y, kd2.Transform.Rotation.Z) - new Vector3(kd1.Transform.Rotation.X, kd1.Transform.Rotation.Y, kd1.Transform.Rotation.Z)) / (kd2.Time - kd1.Time);
        kd1kd2Rotation.x = kd1kd2Rotation.x > 180 ? kd1kd2Rotation.x - 360 : kd1kd2Rotation.x;
        kd1kd2Rotation.y = kd1kd2Rotation.y > 180 ? kd1kd2Rotation.y - 360 : kd1kd2Rotation.y;
        kd1kd2Rotation.z = kd1kd2Rotation.z > 180 ? kd1kd2Rotation.z - 360 : kd1kd2Rotation.z;
        kd2nkdRotation = (new Vector3(nkd.Transform.Rotation.X, nkd.Transform.Rotation.Y, nkd.Transform.Rotation.Z) - new Vector3(kd2.Transform.Rotation.X, kd2.Transform.Rotation.Y, kd2.Transform.Rotation.Z)) / (nkd.Time - kd2.Time);
        kd2nkdRotation.x = kd2nkdRotation.x > 180 ? kd2nkdRotation.x - 360 : kd2nkdRotation.x;
        kd2nkdRotation.y = kd2nkdRotation.y > 180 ? kd2nkdRotation.y - 360 : kd2nkdRotation.y;
        kd2nkdRotation.z = kd2nkdRotation.z > 180 ? kd2nkdRotation.z - 360 : kd2nkdRotation.z;
      }
      //Filter out if difference is too small
      float velocityThreshold = 0.125f;
      float rotationThreshold = 0.5f;
      return ((kd2nkdVelocity - kd1kd2Velocity).magnitude > velocityThreshold || (kd2nkdRotation - kd1kd2Rotation).magnitude > rotationThreshold);
    }
  }
}