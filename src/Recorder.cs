using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Game;

namespace BBRMod {
  internal class Recorder {
    private static Recorder _instance = (Recorder)null;
    public static Recorder Instance {
      get {
        Recorder._instance ??= new Recorder();
        return Recorder._instance;
      }
    }
    private const decimal _REDUCED_FIXED_UPDATE = 0.1m; //NOTE: current code assumes 0.1, and will need to be adjusted given a different value
    private bool _enabled = false;
    public bool IsRunning => this._enabled;

    private bool _dedicatedServer = false;

    //Recorders
    private FleetRecorder _fleetRecorder;
    public ShipRecorder ShipRecorder;
    private ShipMeshGrabber _shipMeshGrabber;
    private MapRecorder _mapRecorder;
    private MapMeshGrabber _mapMeshGrabber;
    private bool _preCacheEnabled = false;
    public bool IsPrecached => this._preCacheEnabled;
    public MapMeshGrabber MapMeshGrabber => this._mapMeshGrabber;
    public MissileRecorder MissileRecorder;
    public ShellRecorder ShellRecorder;
    public BeamRecorder BeamRecorder;
    public EWarRecorder EWarRecorder;
    public RaycastRecorder RaycastRecorder;
    public ExplosionRecorder ExplosionRecorder;

    public Recorder() {
      this._fleetRecorder = new FleetRecorder();
      this.ShipRecorder = new ShipRecorder();
      this._shipMeshGrabber = new ShipMeshGrabber();
      this._mapMeshGrabber = new MapMeshGrabber();
      this._mapRecorder = new MapRecorder();
      this.MissileRecorder = new MissileRecorder();
      this.ShellRecorder = new ShellRecorder();
      this.BeamRecorder = new BeamRecorder();
      this.EWarRecorder = new EWarRecorder();
      this.RaycastRecorder = new RaycastRecorder();
      this.ExplosionRecorder = new ExplosionRecorder();
    }
    public void PreCacheMapMeshes() {
      this._preCacheEnabled = true;
      this._mapMeshGrabber.Start();
    }
    public void MatchStart() {
      this._enabled = true;
      this._fleetRecorder.Start();
      this.ShipRecorder.Start();
      this._shipMeshGrabber.Start(this.ShipRecorder); //interop needed for mesh instancing
      this._mapRecorder.Start();
      this.MissileRecorder.Start();
      this.ShellRecorder.Start();
      this.BeamRecorder.Start();
      this.EWarRecorder.Start();
      this.RaycastRecorder.Start();
      this.ExplosionRecorder.Start();

      this._dedicatedServer = GameManager.Instance.IsDedicatedServer;
    }
    public void Update() {
      //TODO: move map capture either to the end, or build a temporary capture file system of... some sort
      if(this._preCacheEnabled) {
        this._mapMeshGrabber.Update();
        if (this._mapMeshGrabber.TasksComplete) {
          this._preCacheEnabled = false;
        }
      }

      if(this._enabled) {
        //Update these every FixedUpdate
        this.MissileRecorder.Update();
        this.ShellRecorder.Update();
        this.BeamRecorder.Update();
        this.RaycastRecorder.Update();
        if (!this._preCacheEnabled) {
          this._mapMeshGrabber.Update();
        }

        //Update these at a reduced rate, staggered to avoid demand spikes
        //float imprecision being weird
        switch (((decimal)Time.fixedTime % Recorder._REDUCED_FIXED_UPDATE) / 0.02m)
        {
          case 0:
            this.ShipRecorder.Update();
            break;
          case 1:
            this.EWarRecorder.Update();
            break;
          case 2:
            this._mapRecorder.Update();
            break;
          case 3:
            // empty
            break;
          case 4:
            // empty
            break;
          default:
            break;
        }

        //Empty
        //this.ExplosionRecorder.Update();
        //this._fleetRecorder.Update();
        //this._shipMeshGrabber.Update();
      }
    }
    public void MatchStop () {
      if(this._enabled) {
        Debug.Log("[BBRMod] Match not running, disabling recording");
        this._enabled = false;
      }
    }
    public void RecordVictor(string victor) {
      this._mapRecorder.Victor(victor);
    }
    public void Clear() {
      this._enabled = false;
      this._fleetRecorder.Clear();
      this.ShipRecorder.Clear();
      this._shipMeshGrabber.Clear();
      this._mapRecorder.Clear();
      this._mapMeshGrabber.Clear();
      this._preCacheEnabled = false;
      this.MissileRecorder.Clear();
      this.ShellRecorder.Clear();
      this.BeamRecorder.Clear();
      this.EWarRecorder.Clear();
      this.RaycastRecorder.Clear();
      this.ExplosionRecorder.Clear();

      this._dedicatedServer = false;

      AsyncWriteManager.ResetFlags();
      AsyncWriteManager.ForceClearData(); //ensure any previous data from the writer is cleared
    }
    public AllData GetData() {
      var allData = new AllData() {
        GameVersion = Application.version,
        ModVersion = BBRMod.version,
        ViewerVersion = Config.VIEWER_VERSION,
        CapturedByDedicatedServer = this._dedicatedServer,
        FixedDeltaTime = Time.fixedDeltaTime,
        MapInfo = this._mapRecorder.GetMapInfo(),
        MapMeshes = this._mapMeshGrabber.GetData(),
        MapKinematics = this._mapRecorder.GetData(),
        MapPOIs = this._mapRecorder.GetPoiData(),
        FleetInfo = this._fleetRecorder.GetInfoData(),
        ShipMeshes = this._shipMeshGrabber.GetData(),
        ShipKinematics = this.ShipRecorder.GetKinematicData(),
        ShipInfo = this.ShipRecorder.GetInfoData(),
        MissileKinematics = this.MissileRecorder.GetKinematicData(),
        MissileInfo = this.MissileRecorder.GetInfoData(),
        ShellKinematics = this.ShellRecorder.GetKinematicData(),
        ShellInfo = this.ShellRecorder.GetInfoData(),
        BeamStartKinematics = this.BeamRecorder.GetStartKinematicData(),
        BeamEndKinematics = this.BeamRecorder.GetEndKinematicData(),
        BeamInfo = this.BeamRecorder.GetInfoData(),
        EWarMeshes = this.EWarRecorder.GetMeshData(),
        EWarKinematics = this.EWarRecorder.GetKinematicData(),
        EWarInfo = this.EWarRecorder.GetInfoData(),
        RayFieldData = this.RaycastRecorder.GetData(),
        ExplosionData = this.ExplosionRecorder.GetData(),
        DamageData = this.ShipRecorder.GetDamageData()
      };

      //perform alignment check on aligned lists. NOTE: this should only change data that shouldn't have ever happened anyway, so make sure it's considered a bug with an appropriate message. Maybe force a copy of the player.log file?
      //TODO: hell, we may want to just make a bbr.log file at this point
      //TODO: better error handling
      //ISSUE: do fleetinfo and other semi-aligned lists need the same treatment?
      //NOTE: call KDataValidityCheck before AlignmentCheck. If there is KData missing AlignmentCheck will correct the aligned lists
      this.KDataValidityCheck(allData.MapKinematics);
      this.AlignmentCheck<MeshCollectionData, KinematicLog>(allData.MapMeshes, allData.MapKinematics);
      this.KDataValidityCheck(allData.ShipKinematics);
      this.AlignmentCheck<KinematicLog, ShipInfo>(allData.ShipKinematics, allData.ShipInfo);
      this.KDataValidityCheck(allData.MissileKinematics);
      this.AlignmentCheck<MissileInfo, KinematicLog>(allData.MissileInfo, allData.MissileKinematics);
      this.KDataValidityCheck(allData.ShellKinematics);
      this.AlignmentCheck<ShellInfo, KinematicLog>(allData.ShellInfo, allData.ShellKinematics);
      this.KDataValidityCheck(allData.BeamStartKinematics);
      this.KDataValidityCheck(allData.BeamEndKinematics);
      this.AlignmentCheck<KinematicLog, KinematicLog, BeamInfo>(allData.BeamStartKinematics, allData.BeamEndKinematics, allData.BeamInfo);
      this.AlignmentCheck<KinematicLog, KinematicLog>(allData.BeamStartKinematics, allData.BeamEndKinematics);
      this.AlignmentCheck<KinematicLog, BeamInfo>(allData.BeamStartKinematics, allData.BeamInfo);
      this.AlignmentCheck<KinematicLog, BeamInfo>(allData.BeamEndKinematics, allData.BeamInfo);
      this.KDataValidityCheck(allData.EWarKinematics);
      //NOTE: ewar meshes are instanced; data will not align
      this.AlignmentCheck<KinematicLog, EWarInfo>(allData.EWarKinematics, allData.EWarInfo);
      //ISSUE: if the owner of something is removed, that should be handled. Maybe just do a nullcheck in the viewer though

      //perform mesh data validation check on all meshes
      this.MeshDataValidityCheck(allData.MapMeshes, allData.MapKinematics);
      //NOTE: ship meshes are instanced
      //this.MeshDataValidityCheck(allData.ShipMeshes, allData.ShipKinematics);
      //NOTE: ewar meshes are instanced
      //this.MeshDataValidityCheck(allData.EWarMeshes, allData.EWarKinematics);

      return allData;
    }

    ///// DATA VALIDATORS /////

    //TODO: shuffle id's to remove holes in the data?
    //TODO: concat all errors and display them for the user
    private void KDataValidityCheck(List<KinematicLog> list) {
      for (int i = list.Count - 1; i >= 0; i--) {
        if (list[i].Log.Count == 0) {
          Debug.Log($"[BBRMod] {ModUtils.GetId(list[i])} contains no kinematic information.");
          list.RemoveAt(i);
        }
      }
    }

    private void AlignmentCheck<T, T2>(List<T> listA, List<T2> listB) where T : IdData where T2 : IdData {
      for (int i = listA.Count - 1; i >= 0; i--) {
        if ( ! listB.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listA[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listA contains {ModUtils.GetId(listA[i])} but listB does not.");
          listA.RemoveAt(i);
        }
      }
      for (int i = listB.Count - 1; i >= 0; i--) {
        if ( ! listA.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listB[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listB contains {ModUtils.GetId(listB[i])} but listA does not.");
          listB.RemoveAt(i);
        }
      }
    }

    private void AlignmentCheck<T, T2, T3>(List<T> listA, List<T2> listB, List<T3> listC) where T : IdData where T2 : IdData where T3 : IdData {
      for (int i = 0; i < listA.Count; i++) {
        if ( ! listB.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listA[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listA contains {ModUtils.GetId(listA[i])} but listB does not.");
          listA.RemoveAt(i);
        }
      }
      for (int i = 0; i < listA.Count; i++) {
        if ( ! listC.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listA[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listA contains {ModUtils.GetId(listA[i])} but listC does not.");
          listA.RemoveAt(i);
        }
      }

      for (int i = 0; i < listB.Count; i++) {
        if ( ! listA.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listB[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listB contains {ModUtils.GetId(listB[i])} but listA does not.");
          listB.RemoveAt(i);
        }
      }
      for (int i = 0; i < listB.Count; i++) {
        if ( ! listC.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listB[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listB contains {ModUtils.GetId(listB[i])} but listC does not.");
          listB.RemoveAt(i);
        }
      }
      
      for (int i = 0; i < listC.Count; i++) {
        if ( ! listA.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listC[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listC contains {ModUtils.GetId(listC[i])} but listA does not.");
          listC.RemoveAt(i);
        }
      }
      for (int i = 0; i < listC.Count; i++) {
        if ( ! listB.Any(d => ModUtils.GetId(d) == ModUtils.GetId(listC[i])) ) {
          Debug.Log($"[BBRMod] Data misalignment! listC contains {ModUtils.GetId(listC[i])} but listB does not.");
          listC.RemoveAt(i);
        }
      }
    }

    private void MeshDataValidityCheck(List<MeshCollectionData> list, List<KinematicLog> parallelList) {
      for (int i = 0; i < list.Count; i++) {
        for (int j = 0; j < list[i].Meshes.Count; j++) {
          var meshData = list[i].Meshes[j];
          if (meshData.PrimitiveCollider != null) { continue; }
          if (meshData.MeshTriangleData.Count != 0 && meshData.MeshVerticesData.Count != 0) { continue; }
          Debug.Log($"\t{ModUtils.GetId(list[i])} contains no mesh information.");
          //TODO: do something about it
        }
      }
    }

    //TODO: check instanced data
    private void InstancedDataValidityCheck() {

    }

  }
}