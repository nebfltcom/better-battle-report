using UnityEngine;
using Utility;

// Dedicated servers had a very weird issue where the dazzler cone would be incorrectly constructed if fired after a bellbird (this may not be the only repo steps)
// In trying to solve it I copied these methods from Procedural.ProceduralMesh to try and diagnose the problem, and that solved it
namespace BBRMod
{
  internal static class ProceduralCone
  {
    public static Mesh CreateCone(
      ConeDescriptor cone,
      int sides,
      bool endcaps,
      bool reverseWinding)
    {
      return ProceduralCone.CreateCone(cone.Height, cone.BaseRadius, cone.TopRadius, sides, endcaps, reverseWinding);
    }

    public static Mesh CreateCone(
      float height,
      float baseRadius,
      float topRadius,
      int sides,
      bool endcaps,
      bool reverseWinding)
    {
      Mesh cone = new Mesh();
      bool flag = true;
      int length = endcaps ? sides * 2 + 1 + (flag ? sides * 2 + 1 : 1) : sides + (flag ? sides : 1);
      int num1 = (endcaps ? sides : 0) + (flag & endcaps ? sides : 0) + (flag ? sides * 2 : sides);
      Vector3[] vertices = new Vector3[length];
      Vector3[] normals = new Vector3[length];
      int[] indices = new int[num1 * 3];
      int startIndex1 = 0;
      int startIndex2 = flag ? sides : 1;
      int centerVertex1 = startIndex2 + sides;
      int centerVertex2 = centerVertex1 + sides + 1;
      float radianStep = 6.28318548f / (float) sides;
      if (flag)
      {
        ProceduralCone.MakeVertexRing(ref vertices, ref normals, startIndex1, sides, topRadius, radianStep, 0.0f, reverseWinding);
        if (endcaps)
        {
          vertices[centerVertex2] = Vector3.zero;
          normals[centerVertex2] = reverseWinding ? new Vector3(0.0f, 0.0f, 1f) : new Vector3(0.0f, 0.0f, -1f);
          ProceduralCone.MakeVertexRing(ref vertices, ref normals, centerVertex2 + 1, sides, topRadius, radianStep, 0.0f, reverseWinding);
        }
      }
      else
      {
        vertices[startIndex1] = Vector3.zero;
        normals[startIndex1] = reverseWinding ? new Vector3(0.0f, 0.0f, 1f) : new Vector3(0.0f, 0.0f, -1f);
      }
      ProceduralCone.MakeVertexRing(ref vertices, ref normals, startIndex2, sides, baseRadius, radianStep, height, reverseWinding);
      if (endcaps)
      {
        vertices[centerVertex1] = new Vector3(0.0f, 0.0f, height);
        normals[centerVertex1] = reverseWinding ? new Vector3(0.0f, 0.0f, -1f) : new Vector3(0.0f, 0.0f, 1f);
        ProceduralCone.MakeVertexRing(ref vertices, ref normals, centerVertex1 + 1, sides, baseRadius, radianStep, height, reverseWinding);
      }
      int startI = 0;
      if (endcaps)
      {
        if (flag)
          startI += ProceduralCone.MakeCircleTrianglesWithCenter(ref indices, startI, centerVertex2, centerVertex2 + 1, sides, !reverseWinding);
        startI += ProceduralCone.MakeCircleTrianglesWithCenter(ref indices, startI, centerVertex1, centerVertex1 + 1, sides, reverseWinding);
      }
      if (flag)
      {
        for (int index1 = 0; index1 < sides; ++index1)
        {
          int num2 = startIndex1 + index1;
          int num3 = index1 == sides - 1 ? startIndex1 : startIndex1 + index1 + 1;
          int num4 = startIndex2 + index1;
          int num5 = index1 == sides - 1 ? startIndex2 : startIndex2 + index1 + 1;
          int[] numArray1 = indices;
          int index2 = startI;
          int num6 = index2 + 1;
          int num7 = num2;
          numArray1[index2] = num7;
          int[] numArray2 = indices;
          int index3 = num6;
          int num8 = index3 + 1;
          int num9 = reverseWinding ? num4 : num5;
          numArray2[index3] = num9;
          int[] numArray3 = indices;
          int index4 = num8;
          int num10 = index4 + 1;
          int num11 = reverseWinding ? num5 : num4;
          numArray3[index4] = num11;
          int[] numArray4 = indices;
          int index5 = num10;
          int num12 = index5 + 1;
          int num13 = num3;
          numArray4[index5] = num13;
          int[] numArray5 = indices;
          int index6 = num12;
          int num14 = index6 + 1;
          int num15 = reverseWinding ? num2 : num5;
          numArray5[index6] = num15;
          int[] numArray6 = indices;
          int index7 = num14;
          startI = index7 + 1;
          int num16 = reverseWinding ? num5 : num2;
          numArray6[index7] = num16;
        }
      }
      else
      {
        for (int index8 = 0; index8 < sides; ++index8)
        {
          int num17 = index8 == sides - 1 ? startIndex2 : startIndex2 + index8 + 1;
          int num18 = startIndex2 + index8;
          int[] numArray7 = indices;
          int index9 = startI;
          int num19 = index9 + 1;
          int num20 = startIndex1;
          numArray7[index9] = num20;
          int[] numArray8 = indices;
          int index10 = num19;
          int num21 = index10 + 1;
          int num22 = reverseWinding ? num18 : num17;
          numArray8[index10] = num22;
          int[] numArray9 = indices;
          int index11 = num21;
          startI = index11 + 1;
          int num23 = reverseWinding ? num17 : num18;
          numArray9[index11] = num23;
        }
      }
      cone.vertices = vertices;
      cone.normals = normals;
      cone.uv = new Vector2[vertices.Length];
      cone.triangles = indices;
      cone.RecalculateBounds();
      return cone;
    }

    private static void MakeVertexRing(
      ref Vector3[] vertices,
      ref Vector3[] normals,
      int startIndex,
      int count,
      float radius,
      float radianStep,
      float zValue,
      bool reverseWinding)
    {
      for (int index1 = 0; index1 < count; ++index1)
      {
        float f = radianStep * (float) index1;
        float x = Mathf.Cos(f) * radius;
        float y = Mathf.Sin(f) * radius;
        vertices[index1 + startIndex] = new Vector3(x, y, zValue);
        Vector3[] vector3Array = normals;
        int index2 = index1 + startIndex;
        Vector3 vector3_1;
        Vector3 vector3_2;
        if (!reverseWinding)
        {
          vector3_1 = new Vector3(x, y, 0.0f);
          vector3_2 = vector3_1.normalized;
        }
        else
        {
          vector3_1 = new Vector3(x, y, 0.0f);
          vector3_2 = -vector3_1.normalized;
        }
        vector3Array[index2] = vector3_2;
      }
    }

    private static int MakeCircleTriangles(
      ref int[] indices,
      int startI,
      int startVertex,
      int vertexCount,
      bool reverseWinding)
    {
      int num = vertexCount - 2;
      for (int index = 0; index < num; ++index)
      {
        indices[index * 3 + startI] = startVertex;
        if (reverseWinding)
        {
          indices[index * 3 + startI + 1] = startVertex + index + 2;
          indices[index * 3 + startI + 2] = startVertex + index + 1;
        }
        else
        {
          indices[index * 3 + startI + 1] = startVertex + index + 1;
          indices[index * 3 + startI + 2] = startVertex + index + 2;
        }
      }
      return num * 3;
    }

    private static int MakeCircleTrianglesWithCenter(
      ref int[] indices,
      int startI,
      int centerVertex,
      int startVertex,
      int triangleCount,
      bool reverseWinding)
    {
      for (int index = 0; index < triangleCount - 1; ++index)
      {
        indices[index * 3 + startI] = centerVertex;
        if (reverseWinding)
        {
          indices[index * 3 + startI + 1] = startVertex + index + 1;
          indices[index * 3 + startI + 2] = startVertex + index;
        }
        else
        {
          indices[index * 3 + startI + 1] = startVertex + index;
          indices[index * 3 + startI + 2] = startVertex + index + 1;
        }
      }
      int index1 = (triangleCount - 1) * 3 + startI;
      indices[index1] = centerVertex;
      if (reverseWinding)
      {
        indices[index1 + 1] = startVertex;
        indices[index1 + 2] = startVertex + (triangleCount - 1);
      }
      else
      {
        indices[index1 + 1] = startVertex + (triangleCount - 1);
        indices[index1 + 2] = startVertex;
      }
      return triangleCount * 3;
    }
  }
}