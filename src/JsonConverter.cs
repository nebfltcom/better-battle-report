using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace BBRMod {
  internal class CompactFloatConverter : JsonConverter<float> {
    public override float Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options) {
      return (float)reader.GetDouble();
    }
    public override void Write(Utf8JsonWriter writer, float value, JsonSerializerOptions options) {
      int roundedInt = UnityEngine.Mathf.RoundToInt(value * 100f);
      decimal dec = roundedInt / 100m;
      writer.WriteNumberValue(dec);
    }
  }
}