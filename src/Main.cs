﻿using UnityEngine;
using HarmonyLib;
using Modding;
using System.Xml;
using System;

namespace BBRMod {
  public class BBRMod : IModEntryPoint {
    internal static bool DEBUG = false;
    public static string version;
    public const string BUG_REPORT_URL = "https://github.com/ShadowLotus232/NEBFLTCOM-BBR-BugReporting/issues";
    public static string saveDir = null;
    public static bool autoSave = false;
    public void PreLoad() {
      BBRMod.version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
    }

    public void PostLoad() {
      Harmony harmony = new Harmony("nebulous.better-battle-report");
      harmony.PatchAll();
      //harmony.PatchAll(typeof(BBRMod));

      BBRMod.ReadConfig();

      Debug.Log($"[BBRMod] Better Battle Report v{BBRMod.version} loaded and ready to go!");
      Debug.Log($"\tIf you experience an issue with BBR please inform ShadowLotus in the Nebulous Discord.\tIf you experience a bug, please file a report at {BUG_REPORT_URL}");
    }

    /// <summary>
    /// Manually request to save BBR report. Will write asychronously, so before moving your server on to the next lobby you may want to check that the write has finished. This process can sometimes take a couple minutes.
    /// </summary>
    /// <param name="fileName">Name of the file to be written.</param>
    /// <param name="filePath">Full path of the directory to write the file to. If left null, BBR will write to the default Game.Reports.FullAfterActionReport.SaveDir</param>
    /// <param name="debug">If true, will also write an uncompressed json file (useful for debugging). False by default.</param>
    public static void DedicatedServerExport(string fileName, string filePath = null, bool debug = false) {
      if (Recorder.Instance.IsRunning) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Report requested while recorder is still running!");
        return;
      }

      BBRMod.DEBUG = debug;
      AsyncWriteManager.filePath = filePath;
      ExportStart_Patch_FullAfterActionReportExport.ExportMatchReport(fileName);
    }

    /// <summary>
    /// Force BBR to start serializing data to be requested later by byte[] GetReportBytes(). Just in case.
    /// </summary>
    public static void ForceStartSerialization() {
      AsyncWriteManager.Instance.StartSerialization();
    }

    //THOUGHT: do we need an interrupt checker for this one? It's a dedicated server only thing... 
    /// <summary>
    /// Request BBR report as a byte[]. Will serialize asychronously; use bool IsWritingReport() to check if it's done; use byte[] GetReportBytes() to recieve the information. This process can sometimes take a couple minutes.
    /// </summary>
    /// <returns>Array of bytes ready to be written as a .bbr file that the viewer can parse. Will return an empty array if the Recorder is still running, or if the report serialization has not been completed.</returns>
    public static byte[] GetReportBytes() {
      if (Recorder.Instance.IsRunning) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Report requested while recorder is still running!");
        return new byte[0];
      }

      if (IsWritingReport()) {
        Debug.LogError("<color=\"yellow\">[BBRMod]</color> Report requested while it's still being written!");
        return new byte[0];
      }

      Debug.Log($"[BBRMod] returned {AsyncWriteManager.CompressedBytes.Length} bytes.");

      return AsyncWriteManager.CompressedBytes;
    }

    /*static void DEBUG_Write() {
      var bytes = GetReportBytes();

      if (bytes.Length == 0) { return; }
      
      System.IO.File.WriteAllBytes(System.IO.Path.Combine(Game.Reports.FullAfterActionReport.SaveDir, "memory test.bbr"), bytes);
    }*/

    /// <summary>
    /// Checks if BBR if in the process of serializing a report.
    /// </summary>
    public static bool IsWritingReport() {
      return AsyncWriteManager.Instance.IsWorking;
    }

    /// <summary>
    /// Checks if BBR has been asked to, and finished preparing a report.
    /// </summary>
    public static bool IsReportReady() {
      return AsyncWriteManager.reportDelivered;
    }

    /// <summary>
    /// Halts any data serialization immediately and clears the entire write manager. All recorded data will be lost.
    /// </summary>
    public static void ForceHaltSerialization() {
      AsyncWriteManager.Instance.ForceHalt();
      Recorder.Instance.Clear();
    }

    private static void ReadConfig() { //TODO: maybe make this a TOML or something, but for now this will do
      Utility.FilePath configPath = new Utility.FilePath(System.Reflection.Assembly.GetExecutingAssembly().Location);
      configPath = new Utility.FilePath(configPath.FullPath.Substring(0, configPath.FullPath.Length - configPath.Name.Length) + "BBRConfig.xml");
      if (!configPath.Exists) {
        Debug.Log("BBRConfig.xml file not found! Generating default file.");
        System.IO.File.WriteAllText(configPath.FullPath, ModUtils.GenerateConfig());
        return;
      }
      //no null checks fuck it we ball
      XmlDocument configXML = new XmlDocument();
      configXML.Load(configPath.FullPath);
      XmlNodeList autosaveNode = configXML.GetElementsByTagName("AutoSave");
      BBRMod.autoSave = String.Equals(autosaveNode[0].InnerText, "true", StringComparison.OrdinalIgnoreCase); //only true will set as true, all other values false
      XmlNodeList directoryNode = configXML.GetElementsByTagName("Directory");
      BBRMod.saveDir = directoryNode[0].InnerText == "" ? null : directoryNode[0].InnerText; //force empty to null; path checks use null to set to default
    }
  }
}