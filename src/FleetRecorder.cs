using System;
using System.Collections.Generic;
using UnityEngine;

namespace BBRMod {
  internal class FleetRecorder {
    private List<FleetInfo> _infoData = new List<FleetInfo>();
    private List<Ships.Fleet> _fleets = new List<Ships.Fleet>();
    public void Start() {
      var players = Game.SkirmishGameManager.Instance.Players;
      foreach(var player in players) {
        if(!player.IsSpectator && player is Game.SkirmishPlayer) {
          try {
            var fleet = ((Game.SkirmishPlayer)player).PlayerFleet;
            var fleetInfo = this.ExtractFleetInfo(fleet, (Game.SkirmishPlayer)player);
            this._fleets.Add(fleet);
            this._infoData.Add(fleetInfo);
          }
          catch(Exception err) {
            Debug.Log($"[BBRMod] Fleet error occurred:");
            Debug.Log(err);
          }
        }
      }
    }
    public void Update() {
    }
    public void Clear() {
      this._infoData.Clear();
      this._fleets.Clear();
    }
    private FleetInfo ExtractFleetInfo(Ships.Fleet fleet, Game.SkirmishPlayer player) {
      return new FleetInfo() {
        Id = player.netId, //TODO: change to operate on number of fleets and match by ownerIdData
        Type = "fleet",
        Owner = new IdData() {
          Id = player.netId,
          Type = "player"
        },
        Name = fleet.FleetName,
        IsTeamA = player.TeamId == Utility.TeamIdentifier.TeamA,
        PlayerName = player.Network_playerName
      };
    }
    public List<FleetInfo> GetInfoData() {
      return this._infoData;
    }
  }
}