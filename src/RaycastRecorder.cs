using System;
using System.Collections.Generic;
using Game.Units;
using Munitions;
using Ships;
using UnityEngine;
using Utility;

//TODO: reduce data required more?
//  -capture every other or every fourth shot 50%/25%

namespace BBRMod {
  internal class RaycastRecorder {
    //export
    private List<RayFieldData> _infoData = new List<RayFieldData>();

    //internal data sources
    private KinematicData _recordedRay = (KinematicData)null; //Capture method #1. Also used as a null flag.
    private Dictionary<Ships.RepeatingBallisticRaycastMuzzle, List<KinematicData>> _raysByMuzzleDict = new Dictionary<Ships.RepeatingBallisticRaycastMuzzle, List<KinematicData>>(); //Capture method #2: spans on 
    private Dictionary<Ships.RepeatingBallisticRaycastMuzzle, float> _muzzleFireTimeDict = new Dictionary<Ships.RepeatingBallisticRaycastMuzzle, float>(); //Capture method #2
    private Dictionary<Ships.BallisticRaycastMuzzle, Vector3> _muzzleOffsetDict = new Dictionary<Ships.BallisticRaycastMuzzle, Vector3>();

    //utility
    private List<RayFieldParameters> _parameters = new List<RayFieldParameters>();
    private bool _isCapturing = false;
    public bool IsCapturing => this._isCapturing;

    public void Start() {
    }
    public void Update() {
    }
    public void Clear() {
      this._isCapturing = false;
      this._recordedRay = (KinematicData)null;
      this._infoData.Clear();
      this._parameters.Clear();
      this._raysByMuzzleDict.Clear();
      this._muzzleFireTimeDict.Clear();
      this._muzzleOffsetDict.Clear();
    }

    //Capture method #1
    //Host only, accurate to actual raycasts
    //Individual rays are captured as they are cast and require one alteration based on the cast method before being saved to the RayField
    public void StartCapture(Ships.BallisticRaycastMuzzle muzzle) {
      this._recordedRay = this.getLOB(muzzle); //NOTE: either position OR direction will be modified when BallisticRaycastMuzzle.FireBullet() is called
      this._isCapturing = true;

      if (this._muzzleOffsetDict.ContainsKey(muzzle)) { return; }
      //store the offset of this muzzle local to the position of the ship
      Vector3 offset = new Vector3(0,0,0);
      var ownerTransform = muzzle.gameObject.GetComponentInParent<ShipController>().transform;
      if (ownerTransform == null) {
        Debug.Log("[BBRMod] Unable to set offset for rayfield. Generated report may have defender/pavise-style weapons with inaccurate kinematic data. Please give this report to ShadowLotus in the Discord.");
        Debug.Log($"\tHierarchy: {muzzle.gameObject.GetFullName()}");
      } else {
        Matrix4x4 rotMat4 = Matrix4x4.Rotate(Quaternion.Inverse(ownerTransform.rotation));

        var basePos = muzzle.transform.parent.transform.position; //from center of mount rather than end of barrel for more readability and reliability
        offset = basePos - ownerTransform.position;
        offset = rotMat4.MultiplyPoint3x4(offset); //position of mount gameobject changes with parent rotation
      }
      
      this._muzzleOffsetDict.Add(muzzle, offset);
    }

    public void StopCapture(Ships.BallisticRaycastMuzzle muzzle) {
      this._isCapturing = false;
      this.ExtractRayFieldInfo(muzzle);
    }

    /*public void UpdateCylinderCast(Vector3 positionOffset) { //If the muzzle uses cylinderCast the origin of the ray will be offset by a random amount
      this._recordedRay.Transform.Position = this.ModifyRayOrigin(this._recordedRay.Transform.Position, positionOffset);
    }*/

    public void UpdateNonCylinderCast(Vector3 newDirection) { //If the muzzle does not use cylinderCast the direction of the ray will be randomized by the accuracy of the weapon
      this._recordedRay.Transform.Rotation = this.ModifyRayDirection(newDirection);
    }

    //Capture method #2: generate approximate data based on parameters in a time window given by VFX
    public void VFXCaptureStart(Ships.RepeatingBallisticRaycastMuzzle muzzle) {
      this._isCapturing = true;

      KinematicData kInit = this.getLOB(muzzle); //with this method the rays are approximated anyway, so the first one we just won't bother

      this._raysByMuzzleDict.Add(muzzle, new List<KinematicData>(){kInit});
      this._muzzleFireTimeDict.Add(muzzle, 0f);


      if (this._muzzleOffsetDict.ContainsKey(muzzle)) { return; }
      //store the offset of this muzzle local to the position of the ship
      Vector3 offset = new Vector3(0,0,0);
      var ownerTransform = muzzle.gameObject.GetComponentInParent<ShipController>().transform;
      if (ownerTransform == null) {
        Debug.Log("[BBRMod] Unable to set offset for rayfield. Generated report may have defender/pavise-style weapons with inaccurate kinematic data. Please give this report to ShadowLotus in the Discord.");
        Debug.Log($"\tHierarchy: {muzzle.gameObject.GetFullName()}");
      } else {
        Matrix4x4 rotMat4 = Matrix4x4.Rotate(Quaternion.Inverse(ownerTransform.rotation));

        var basePos = muzzle.transform.parent.transform.position; //from center of mount rather than end of barrel for more readability and reliability
        offset = basePos - ownerTransform.position;
        offset = rotMat4.MultiplyPoint3x4(offset); //position of mount gameobject changes with parent rotation
      }
      
      this._muzzleOffsetDict.Add(muzzle, offset);
    }

    public void VFXCaptureUpdate(Ships.RepeatingBallisticRaycastMuzzle muzzle) { //could be refactored to live in Update instead of being a harmony patch call
      float timePerRound = (float)ProtectedData.GetPrivateField(muzzle, "_timePerRound");
      this._muzzleFireTimeDict[muzzle] += Time.fixedDeltaTime;

      if ((double) this._muzzleFireTimeDict[muzzle] >= (double) timePerRound) {
        bool cylinderCast = (bool)ProtectedData.GetPrivateField(muzzle, "_cylinderCast");
        float accuracy = (float)ProtectedData.GetPrivateProperty(muzzle, "_accuracy");

        this._muzzleFireTimeDict[muzzle] -= timePerRound;
        KinematicData currK = this.getLOB(muzzle);
        Vector3 forwardVec = new Vector3(currK.Transform.Rotation.X, currK.Transform.Rotation.Y, currK.Transform.Rotation.Z);
        /*if (cylinderCast) {
          //currK.Transform.Position = this.ModifyRayOrigin(currK.Transform.Position, MathHelpers.RandomRayInConeToCylinder(forwardVec, accuracy / 2f, maxRange));
        }*/
        if (!cylinderCast) {
          currK.Transform.Rotation = this.ModifyRayDirection(MathHelpers.RandomRayInCone(forwardVec, accuracy));
        }
        this._raysByMuzzleDict[muzzle].Add(currK);
      }
    }

    public void VFXCaptureStop(Ships.RepeatingBallisticRaycastMuzzle muzzle) {
      this._isCapturing = false;
      this.ExtractRayFieldInfo(muzzle);
    }

    private void ExtractRayFieldInfo(Ships.BallisticRaycastMuzzle muzzle) {
      ShipController shipController = muzzle.gameObject.GetComponentInParent<ShipController>();
      var ammoType = ((IMagazine)ProtectedData.GetPrivateProperty(muzzle, "_ammoSource")).AmmoType;
      string munitionName = ammoType.MunitionName;
      float munitionSpeed = ammoType.FlightSpeed;
      float munitionLifetime = ammoType.Lifetime;
      Vector3 offset = this._muzzleOffsetDict[(Ships.RepeatingBallisticRaycastMuzzle)muzzle];
      
      RayFieldParameters currentParams = new RayFieldParameters() {
        ShipController = shipController,
        MunitionName = munitionName,
        MunitionSpeed = munitionSpeed,
        MunitionLifetime = munitionLifetime,
        Offset = offset
      };

      int foundIndex = this._parameters.FindIndex(x => x == currentParams);
      if(foundIndex > -1) {
        if (this._recordedRay is not null) {
          this._infoData[foundIndex].Rays.Add(this._recordedRay);
        }
        else {
          this._infoData[foundIndex].Rays.AddRange(this._raysByMuzzleDict[(Ships.RepeatingBallisticRaycastMuzzle)muzzle]);
        }
      }
      else {
        List<KinematicData> rayList;
        if (this._recordedRay is not null) {
          rayList = new List<KinematicData>(){this._recordedRay};
        }
        else {
          rayList = this._raysByMuzzleDict[(Ships.RepeatingBallisticRaycastMuzzle)muzzle];
        }

        RayFieldData data = new RayFieldData() {
          Id = (uint)this._infoData.Count,
          Type = "rayfield",
          Owner = new IdData() {
            Id = shipController.NetID.netId,
            Type = "ship"
          },
          MunitionName = munitionName,
          MunitionSpeed = munitionSpeed,
          MunitionLifetime = munitionLifetime,
          Offset = new Vector3Data(offset),
          CylinderCast = (bool)ProtectedData.GetPrivateField(muzzle, "_cylinderCast"),
          Accuracy = (float)ProtectedData.GetPrivateProperty(muzzle, "_accuracy"), //ISSUE: this will not reflect accuracy modifiers if the muzzle is set not to ignore them
          Rays = rayList
        };
        this._infoData.Add(data);
        this._parameters.Add(currentParams);
      }
      
      if (this._recordedRay is null) {
        this._raysByMuzzleDict.Remove((Ships.RepeatingBallisticRaycastMuzzle)muzzle);
        this._muzzleFireTimeDict.Remove((Ships.RepeatingBallisticRaycastMuzzle)muzzle);
      }
    }

    private KinematicData getLOB(Ships.BallisticRaycastMuzzle muzzle) {
      Vector3 transformRot = Quaternion.LookRotation(muzzle.transform.forward, muzzle.transform.up).eulerAngles * Mathf.Deg2Rad;

      return new KinematicData(){
        Time = ModUtils.Instance.GetTime(),
        Transform = new TransformData { //Position = null saves between 18 and 36 uncompresed bytes per bullet. at 240rps for the defender that's 4.32K-8.64K per second of fire, per mount (double for the pavise)
          Rotation = new Vector3Data { X = transformRot.x, Y = transformRot.y, Z = transformRot.z }
        },
      };
    }

    public Vector3Data ModifyRayOrigin(Vector3Data vecData, Vector3 positionOffset) {
      return new Vector3Data { X = vecData.X + positionOffset.x, Y = vecData.Y + positionOffset.y, Z = vecData.Z + positionOffset.z };
    }
    public Vector3Data ModifyRayDirection(Vector3 newDirection) {
      newDirection.Normalize(); //I'm not actually sure if the ray we get from this is normalized. Just in case
      
      Vector3 transformRot = Quaternion.LookRotation(newDirection, Vector3.up).eulerAngles * Mathf.Deg2Rad;
      return new Vector3Data { X = transformRot.x, Y = transformRot.y, Z = transformRot.z };
    }

    public List<RayFieldData> GetData() {
      return this._infoData;
    }

    private class RayFieldParameters {
      public ShipController ShipController { get; set; }
      public string MunitionName { get; set; }
      public float MunitionSpeed { get; set; }
      public float MunitionLifetime { get; set; }
      public Vector3 Offset { get; set; }

      public static bool operator == (RayFieldParameters lhs, RayFieldParameters rhs) {
        if(lhs.ShipController != rhs.ShipController)
          return false;
        if(lhs.MunitionName != rhs.MunitionName)
          return false;
        if(lhs.MunitionSpeed != rhs.MunitionSpeed)
          return false;
        if(lhs.MunitionLifetime != rhs.MunitionLifetime)
          return false;
        if(lhs.Offset != rhs.Offset)
          return false;
       return true;
      }

      public static bool operator != (RayFieldParameters lhs, RayFieldParameters rhs) {
        if(lhs.ShipController != rhs.ShipController)
          return true;
        if(lhs.MunitionName != rhs.MunitionName)
          return true;
        if(lhs.MunitionSpeed != rhs.MunitionSpeed)
          return true;
        if(lhs.MunitionLifetime != rhs.MunitionLifetime)
          return true;
        if(lhs.Offset != rhs.Offset)
          return true;
       return false;
      }

      public override bool Equals(System.Object otherItem) {
        if (otherItem is not RayFieldParameters)
          return false;
        else {
          RayFieldParameters newItem = (RayFieldParameters) otherItem;
          if(this.ShipController != newItem.ShipController)
            return false;
          if(this.MunitionName != newItem.MunitionName)
            return false;
          if(this.MunitionSpeed != newItem.MunitionSpeed)
            return false;
          if(this.MunitionLifetime != newItem.MunitionLifetime)
            return false;
          if(this.Offset != newItem.Offset)
            return false;
          return true;
        }
      }

      public override int GetHashCode() {
        float seed = ShipController.NetID.netId + this.MunitionSpeed + this.MunitionLifetime + this.Offset.x + this.Offset.y + this.Offset.z;
        return seed.GetHashCode();
      }
    }
  }
}