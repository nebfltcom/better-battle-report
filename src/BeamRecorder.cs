using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ships;
using UnityEngine;

//TODO: beams need a refactor
namespace BBRMod {
  internal class BeamRecorder {
    private List<KinematicLogger> _startKinematicLoggers = new List<KinematicLogger>();
    private List<KinematicLogger> _endKinematicLoggers = new List<KinematicLogger>();
    private List<BeamInfo> _infoData = new List<BeamInfo>();
    private List<Ships.RaycastMuzzle> _muzzles = new List<Ships.RaycastMuzzle>();
    private List<bool> _active = new List<bool>();
    public void Start() {
    }
    public void Update() {
    }
    public void Clear() {
      this._startKinematicLoggers.Clear();
      this._endKinematicLoggers.Clear();
      this._infoData.Clear();
      this._muzzles.Clear();
      this._infoData.Clear();
      this._active.Clear();
    }
    public void StartBeam(Ships.RaycastMuzzle muzzle) {
      try {
        var id = (uint)this._muzzles.Count;
        var beamInfo = this.ExtractBeamInfo(muzzle, id);
        this._startKinematicLoggers.Add(new KinematicLogger(id, "beam"));
        this._endKinematicLoggers.Add(new KinematicLogger(id, "beam"));
        this._infoData.Add(beamInfo);
        this._muzzles.Add(muzzle);
        this._active.Add(true);

        //TESTING: force capture of kData now to avoid very short aurora bursts from having no data
        //ISSUE: only works sometimes. This entire problem is a timing issue and I don't have the key yet
        //RESOLUTION: this is either a base game bug or intended behaviour (leaning towards latter based on how lasers work). The first damaging raycast isn't fired until the next FixedUpdate after the Fire and FireEffect calls
      }
      catch(Exception err) {
        Debug.Log($"[BBRMod] Beam error occurred:");
        Debug.Log(err);
      }
    }
    private BeamInfo ExtractBeamInfo(Ships.RaycastMuzzle muzzle, uint id) {
      var ship = (Game.Units.ShipController)typeof(Ships.Muzzle).GetProperty("_ship", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
      var weapon = (Ships.IMuzzleWeapon)typeof(Ships.Muzzle).GetProperty("_weapon", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
      var effects = (Ships.BeamMuzzleEffects)typeof(Ships.RaycastMuzzle).GetField("_effects", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(muzzle);
      LineRenderer line;
      var width = 0.01f;
      var color = new ColorData() { r = 0.3f, g = 1f, b = 0f, a = 1f };
      if(effects is Ships.LineBeamMuzzleEffects) {
        line = (LineRenderer)typeof(Ships.LineBeamMuzzleEffects).GetField("_beam", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(effects);
        if(line != null) {
          width = line.startWidth;
          var lineColor = line.startColor;
          /*
          color.r = lineColor.r;
          color.g = lineColor.g;
          color.b = lineColor.b;
          color.a = lineColor.a;
          */
        }
      }
      return new BeamInfo() {
        Id = id,
        Type = "beam",
        Owner = new IdData() {
          Id = ship.NetID.netId,
          Type = "ship"
        },
        Name = weapon is Ships.WeaponComponent component ? component.WepName : "Unknown",
        Pulsed = muzzle is Ships.PulsedRaycastMuzzle,
        DamagePeriod = muzzle is Ships.PulsedRaycastMuzzle ? ((Ships.IDirectDamageMuzzle)muzzle).DamagePeriod : 1f,
        Width = width,
        Color = color
      };
    }
    //TODO: refactor the update logic. Maybe I'm reading it wrong but it seems inefficient
    public void UpdateBeam(Ships.RaycastMuzzle muzzle, Vector3 position, Vector3 direction, float length, float forcedDeltaTime = 0.0f) { //forcedDeltaTime will add a second set of kinematics that amount of time later. used for SinglePulseRaycastMuzzle (new grazer)
      for(int i = 0;i < this._muzzles.Count;i++) {
        if(this._active[i] && object.ReferenceEquals(muzzle, this._muzzles[i])) {
          var newKinematicData = this.ExtractKinematicData(position, direction, length);
          this._startKinematicLoggers[i].AddData(newKinematicData[0], muzzle is Ships.PulsedRaycastMuzzle);
          this._endKinematicLoggers[i].AddData(newKinematicData[1], muzzle is Ships.PulsedRaycastMuzzle);
          if (forcedDeltaTime > 0.0f) { //grazer
            var forcedKinematicData = this.ExtractKinematicData(position, direction, length);
            forcedKinematicData[0].Time += forcedDeltaTime;
            forcedKinematicData[1].Time += forcedDeltaTime;
            this._startKinematicLoggers[i].AddData(forcedKinematicData[0], muzzle is Ships.PulsedRaycastMuzzle);
            this._endKinematicLoggers[i].AddData(forcedKinematicData[1], muzzle is Ships.PulsedRaycastMuzzle);
          }
          break;
        }
      }
    }
    private KinematicData[] ExtractKinematicData(Vector3 position, Vector3 direction, float length) {
      float currTime = ModUtils.Instance.GetTime();
      var startK = new KinematicData() {
        Time = currTime,
        Transform = new TransformData() {
          Position = new Vector3Data() {
            X = position.x,
            Y = position.y,
            Z = position.z
          }
        }
      };
      var ray = new Ray(position, direction);
      var endPosition = ray.GetPoint(length);
      var endK = new KinematicData() {
        Time = currTime,
        Transform = new TransformData() {
          Position = new Vector3Data() {
            X = endPosition.x,
            Y = endPosition.y,
            Z = endPosition.z
          }
        }
      };
      return new KinematicData[] { startK, endK };
    }
    public void StopBeam(Ships.RaycastMuzzle muzzle) {
      for(int i = 0;i < this._muzzles.Count;i++) {
        if(this._active[i] && object.ReferenceEquals(muzzle, this._muzzles[i])) {
          this._active[i] = false;

          //add padding if only one data point exists. This happens if the beam is marked as firing for only one FixedUpdate call, so it only casts one ray.
          float padTime = this._infoData[i].Pulsed ? this._infoData[i].DamagePeriod : 0.5f;
          if (this._startKinematicLoggers[i].GetData().Log.Count == 1) {
            var kData = this._startKinematicLoggers[i].GetData().Log[0];
            var paddedK = new KinematicData() {
              Time = kData.Time + padTime,
              Transform = kData.Transform
            };
            this._startKinematicLoggers[i].AddData(paddedK, true);
          }
          if (this._endKinematicLoggers[i].GetData().Log.Count == 1) {
            var kData = this._endKinematicLoggers[i].GetData().Log[0];
            var paddedK = new KinematicData() {
              Time = kData.Time + padTime,
              Transform = kData.Transform
            };
            this._endKinematicLoggers[i].AddData(paddedK, true);
          }
        }
      }
    }
    public List<KinematicLog> GetStartKinematicData() {
      var beamLog = new List<KinematicLog>();
      foreach(var logger in this._startKinematicLoggers) {
        var data = logger.GetData();
        if (data.Log.Count != 0) {
          beamLog.Add(logger.GetData());
        }
      }
      return beamLog;
    }
    public List<KinematicLog> GetEndKinematicData() {
      var beamLog = new List<KinematicLog>();
      foreach(var logger in this._endKinematicLoggers) {
        beamLog.Add(logger.GetData());
      }
      return beamLog;
    }
    public List<BeamInfo> GetInfoData() {
      return this._infoData;
    }
  }
}